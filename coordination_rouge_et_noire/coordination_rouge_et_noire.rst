
.. index::
   ! Coordination Rouge et Noire
   pair: International; Coordination Rouge et Noire
   pair: Coordination ; Rouge et Noire


.. _coordination_rouge_et_noire:
.. _coord_red_black:

====================================
Coordination Rouge et Noire
====================================

.. toctree::
   :maxdepth: 3

   statuts/statuts
