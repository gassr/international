
.. index::
   triple: International; Japon; Freeter's Union
   
   
.. _freeters_union:

====================================
Freeter's Union
====================================

Notre camarade japonais SONO Ryota – membre du syndicat Freeter's Union – a été arrêté le 23
septembre dernier lors d'une manifestation antiraciste en soutien à des paysans maliens, dans un
quartier populaire de Tokyo. Lors de cette arrestation ciblée, plusieurs manifestants ont pris des
coups et SONO Ryota a été blessé, notamment au visage  ; il est encore emprisonné à l'heure
actuelle. Militant antinucléaire (cf. l'interview parue dans le Combat syndicaliste d'octobre 2011 et
à paraître dans Terre & Liberté n°2 en novembre prochain, réalisée en août) il est aussi partie
prenante du réseau NoVox Japon  : c'est dire si la police cherchait à frapper un grand coup en
mettant hors d'état de nuire une figure connue et emblématique du milieu contestataire, en
ébullition depuis la catastrophe de la centrale de Fukushima. Cacher la réalité des radiations,
mentir à la population ne suffit pas : il faut faire taire les voix dissidentes.
La CNT-f se solidarise totalement avec SONO Ryota et réclame sa remise en liberté immédiate
ainsi que l'arrêt de la répression contre les militants antinucléaire, syndicalistes et autres.
Arrêt immédiat du nucléaire !

Liberté pour SONO Ryota !

Une attaque contre un seul est une attaque contre tous !

(Envoyez vos messages de soutien à 923solidarity@gmail.com) 


