
.. index::
   pair: International; Asie


.. _asie:

===============================
Asie
===============================

.. toctree::
   :maxdepth: 4

   chine/index
   israel/index
   palestine/index
   japon/index
