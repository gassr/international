
.. index::
   pair: Tribunal; Russel
   ! Israël


.. _israel:

=========
Israël
=========


.. seealso::

   - http://fr.wikipedia.org/wiki/Tribunal_Russell_sur_la_Palestine


.. contents::
   :depth: 3

Position de Chomsky
===================

.. seealso:: 

   - http://fr.wikipedia.org/wiki/Opinions_politiques_de_Noam_Chomsky


Chomsky se considère sioniste bien qu'il admette que sa définition du 
sionisme est considéré de nos jours comme anti-sioniste_ , résultat de 
ce qu'il perçoit comme ayant été un bouleversement (depuis les années 1940) 
dans la signification du sionisme (Chomsky Reader).

Il est membre du comité de parrainage du Tribunal Russell sur la Palestine dont
les travaux ont commencé le 4 mars 2009.


.. _anti-sioniste: http://fr.wikipedia.org/wiki/Anti-sioniste


MilitantEs Israéliens
===========================

.. toctree::
   :maxdepth: 3
   
   militantEs/index

BDS
===========================

.. toctree::
   :maxdepth: 3
   
   bds/index   

Organisations Israéliennes
===========================

.. toctree::
   :maxdepth: 3
   
   organisations/index
   
   
