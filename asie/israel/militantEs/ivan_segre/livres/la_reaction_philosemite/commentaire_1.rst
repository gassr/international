

======================================
Commentaires sur Affaires stratégiques
======================================

.. seealso:: http://www.affaires-strategiques.info/spip.php?article1579



Entretien avec Ivan Segré, auteur de « La réaction philosémite » (éd. Lignes), 
par Lauriane Crochemore, assistante à la communication.

Vous mettez en cause des intellectuels français en parlant d’une nouvelle 
"trahison des clercs". Que voulez-vous dire par là ?

J’ai sous-titré ce livre "La trahison des clercs" en référence au fameux ouvrage 
de Julien Benda, mais j’annonce d’emblée que l’horizon d’attente du lecteur va 
être pris à revers. 

Je m’explique : l’apparition d’un courant intellectuel français qui, au nom 
de la "défense d’Israël" et de la "lutte contre l’antisémitisme", a développé 
un argumentaire extrêmement réactionnaire, contre les Maghrébins ou les Noirs 
d’identité musulmane, plus largement contre les jeunes des quartiers populaires, 
et contre les progressistes, est un phénomène notoire. 

L’originalité de mon analyse, c’est de montrer qu’à y bien regarder, ce courant 
intellectuel français n’est absolument pas le symptôme d’un repli 
communautaire juif, comme on a trop vite voulu le dire, mais l’avant-garde 
d’une réaction idéologique dont le véritable mot d’ordre est la défense de 
l’Occident, et non la défense des juifs ou d’Israël.

Du reste, ces intellectuels s’opposent explicitement, parfois même avec 
acharnement, au communautarisme, et se revendiquent bien au contraire d’un 
universalisme, ce en quoi ils ont d’ailleurs raison, puisque la défense de 
l’Occident n’est pas un mot d’ordre communautaire, c’est un mot d’ordre 
universaliste, à condition bien sûr d’entendre universaliste au sens 
impérialiste du terme, car l’impérialisme est aussi une forme d’universalisme. 

Je me suis donc intéressé principalement à des intellectuels juifs qui, d’une 
manière ou d’une autre, se sont affirmés comme tels, et je soutiens qu’ils 
sont des "clercs", voulant dire par là qu’ils trahissent le particularisme 
juif ou sioniste pour un universalisme impérialiste. 

Ainsi pour prendre quelques exemples : lorsqu’Alexandre Adler écrit dans son 
livre "L’Odyssée américaine" que la capitale du monde juif n’est ni Jérusalem, 
ni Tel Aviv mais New York, il apparaît clairement qu’à ses yeux Israël ne 
représente aucune sorte de centralité, pas même juive, mais tout au plus une 
province américaine. 
Et Adler nous explique en effet dans son ouvrage que l’alliance avec les 
néo-conservateurs américains impliquera une reformulation de l’indépendance 
géostratégique israélienne, car selon lui Israël n’aura pas d’autre choix dans 
l’avenir que de renoncer à son indépendance pour "une sorte de mandat américain 
bienveillant", si bien que les Israéliens en seront réduits, ce sont ses 
propres termes, à assurer "la police au sol sur la frontière américaine du Jourdain". 

Vous m’accorderez que c’est là une défense d’Israël pour le moins soumise, 
sinon servile. 

Lorsqu’Alain Finkielkraut écrit que l’image inversée d’Auschwitz, c’est l’Amérique, 
ou qu’il prend position contre l’entrée de la Turquie en Europe, ou encore qu’il 
s’inquiète de ce que l’enseignement des Croisades est contesté dans les collèges 
des quartiers dits "difficiles", ceci après avoir pris position contre le voile 
et la kippa à l’école, vous m’avouerez que l’orientation communautaire est 
difficilement perceptible. 

Lorsque Jean-Claude Milner affirme sur France-Culture que "Les héritiers" de 
Bourdieu et Passeron est un livre antisémite, on perçoit clairement la portée 
idéologique de son propos, mais quant à la défense des Juifs ou d’Israël, on 
ne voit pas, à moins de comprendre que la sécurité des Juifs suppose une 
épuration radicale des universités françaises en matière de sociologie.






   
   
