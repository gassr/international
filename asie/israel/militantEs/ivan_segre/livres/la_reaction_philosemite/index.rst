
.. index::
   pair: Livre; La réaction philosémite
   ! La réaction philosémite


.. _la_reaction_philosémite:

============================
La réaction philosémite
============================

.. seealso::
  
   - http://www.editions-lignes.com/LA-REACTION-PHILOSEMITE.html
   
Sans aucun doute, le premier livre à s’attaquer avec exactitude et justesse à 
la violente campagne pseudo-« philosémite », dans laquelle Yvan Segré lit les 
traits d’une trahison politique (qu’il qualifie ici de « réactionnaire ») 
et non ceux d’une fidélité à l’universalisme juif. 

La Réaction philosémite, ou La trahison des clercs est l’ouvrage d’un logicien 
hors pair, que double, de bout en bout, **un ironiste rare**. 

Ivan Segré vit en Israël.


Commentaires
============

.. toctree::
   :maxdepth: 3
   
   commentaire_1



   
   
