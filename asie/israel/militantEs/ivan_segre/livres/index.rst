
.. index::
   pair: Livres; Ivan Segré


.. _livres_ivan_segre:

============================
Livres de Ivan Segré
============================


.. toctree::
   :maxdepth: 3
   
   la_reaction_philosemite/index
   penser_auschwitz/index
   
   
