
.. index::
   pair: Livre; Qu’appelle-t-on penser Auschwitz ?
   ! Qu’appelle-t-on penser Auschwitz ?


.. _penser_auschwitz:

==========================================
Qu’appelle-t-on penser Auschwitz ? (2009)
==========================================

.. seealso::
  
   - http://www.editions-lignes.com/Qu-appelle-t-on-penser-Auschwitz.html
   
``Qu’appelle-t-on penser Auschwitz ?`` la première et la plus importante partie 
de ce volume – qui donne son titre à l’ensemble –, est une étude de la thèse de 
Lacoue-Labarthe sur ce sujet (dans Fiction du politique, 1987), laquelle consiste 
en un commentaire de l’unique phrase que Heidegger ait jamais prononcée au 
sujet des chambres à gaz. 

L’enjeu est donc d’exposer d’abord la pensée de Heidegger au sujet d’Auschwitz  ; 
ensuite d’exposer la critique qu’en propose Lacoue-Labarthe  ; 
enfin de comparer cette dernière à la pensée de Hannah Arendt, toujours sur ce 
même sujet, ne serait-ce que parce que celle-ci fut très largement influencée 
par la méditation heideggerienne sur l’essence de la technique – et c’est l’un 
des enjeux de cette étude que de le montrer.  
   
