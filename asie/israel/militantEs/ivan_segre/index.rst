
.. index::
   pair: Ivan ; Segré
   ! Ivan Segré


.. _ivan_segre:

=======================
Ivan Segré
=======================


.. seealso::

   - https://fr.wikipedia.org/wiki/Ivan_Segr%C3%A9


.. contents::
   :depth: 3

Biographie
==========

La thèse de doctorat qu'il soutint à Nanterre il y a quelques années,
avec Daniel Bensaïd comme directeur de son travail, a donné lieu depuis à
deux ouvrages magistraux et complémentaires, Qu'appelle-t-on penser Auschwitz ?
et La Réaction philosémite : la trahison des clercs, tous les deux publiés par
Michel Surya aux nouvelles éditions Lignes en 2009.

Si le premier livre (préfacé par Alain Badiou) s'inscrit dans une perspective
plus philosophique afin d'interroger de manière serrée les grandes lectures
consacrées au judéocide nazi (entre autres par Hannah Arendt et
Philippe Lacoue-Labarthe), le second livre prend pour objet d'analyse les
diverses affirmations rédigées par un certain nombre de personnes occupant des
positions importantes dans le champ intellectuel et médiatique,
d'Alexandre Adler  à Alain Finkielkraut en passant par Pierre-André Taguieff.

On verra que ces propos forment ensemble une constellation idéologique cohérente
dont le présupposé (la lutte contre l'antisémitisme) camoufle en réalité la
thèse classiquement réactionnaire de la défense de l'Occident qui revêt
aujourd'hui le double habit de l'arabophobie et de l'islamophobie.

La rigueur de l'analyse et l'exhaustion critique des textes cités frappent par
leur puissance de subversion tranquille.

C'est qu'Ivan Segré n'est pas un polémiste frivole, mais un intellectuel
conséquent qui sait ce que parler veut dire, et qui sait aussi faire rimer
logique et ironie.

Pour découvrir plus avant son travail, et particulièrement La Réaction philosémite
concentré sur l'actualité de prises de position des nouveaux idéologues de la
réaction, nous proposons à la lecture en premier lieu la présentation des deux
livres en question sur le site des éditions Lignes
(http://www.editions-lignes.com/LA-REACTION-PHILOSEMITE.html).

Puis, en second lieu, nous citons le long entretien donné au site
affaires-stratégiques.info dépendant de l'IRIS qui explicite les enjeux
analytiques et le positionnement stratégique d'un homme à contre-courant
de l'orthodoxie.

Ivan Segré, docteur en philosophie et talmudiste et vit en Israël.


Articles sur/de Ivan Segré
===================================

.. toctree::
   :maxdepth: 3

   articles/index

Livres de Ivan Segré
===================================

.. toctree::
   :maxdepth: 3

   livres/index
