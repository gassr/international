
.. index::
   pair: Michel ; Warschawski


.. _michel_warschawski:

=======================
Michel Warschawski
=======================


.. seealso::

   - http://fr.wikipedia.org/wiki/Michel_Warschawski


Biographie
==========

Michel Warschawski (Mikado) (né en 1949 à Strasbourg) est un journaliste 
et militant pacifiste de gauche israélien, co-fondateur et président du 
Centre d'information alternative de Jérusalem. 

Anti-sioniste, il souhaite le remplacement d'Israël comme État juif par 
un État binational.


Articles sur/de Michel Warschawski
===================================

.. toctree::
   :maxdepth: 3
   
   articles/index
      

Actions 
========

2013
-----

.. seealso:: :ref:`intervention_warschawski_2013`
