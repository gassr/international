
.. _articles_michel_warschawski:

===============================
Articles sur Michel Warschawski
===============================

.. contents::
   :depth: 3


Mai 2002
==========


.. seealso::

   - http://nopasaran.samizdat.net/article.php3?id_article=393


Michel Warschawski est un homme rare. Il fait partie de cette poignée 
d’Israéliens qui n’acceptent pas l’inacceptable. 

C’est un militant de la paix et ce depuis l’année 1968. Cela lui a valu 
notamment de goûter aux geôles israéliennes durant près de deux ans, 
puisqu’il fut condamné pour avoir soutenu des organisations palestiniennes 
illégales. 

Il est également depuis de nombreuses années le président du Centre 
d’information alternative de Jérusalem.


   
