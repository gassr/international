

.. _activites_aatw_fevrier_2013:

=========================================================
Campagne de solidarité avec les Anarchistes contre le mur
=========================================================

Les Anarchistes contre le mur (AATW - http://awalls.org/) est un groupe 
d’activistes israéliens impliqués dans la longue lutte en cours pour les 
droits des Palestiniens. 

Ils militent plus spécifiquement avec le but de rejoindre les communautés 
palestiniennes dans leur lutte pour démanteler le mur de séparation israélien. 

Leur travail, en collaboration avec des partenaires, a été reconnu en 2008 
lorsqu’ils ont remporté la médaille Carl von Ossietzky (attribuée aux 
citoyens ou aux initiatives faisant avancer les droits fondamentaux).

Ils maintiennent un niveau intense de protestation depuis maintenant 
10 ans et les communautés qu’ils soutiennent ont continué à résister 
malgré la souffrance engendrée par la perte de 20 camarades et 
d’innombrables blessés. 

Ce courage est une source d’inspiration et les Anarchistes contre le mur 
refusent de suspendre leur soutien. 

Ces deux dernières années, AATW a été soumis à une vague croissante 
d’agressions contre des militants anti-occupation en Israël et en 
Cisjordanie, combinée à une campagne de persécution et de délégitimation.

Ils ont la chance d’avoir une équipe dédiée d’avocats qui travaillent 
sans relâche pour représenter les militants arrêtés lors de manifestations 
et d’actions. 

Entre 2009 et 2012, cela représente plus de 150 Israéliens et plus de 
250 Palestiniens défendus, dont 50 mineurs. 

Il est crucial que l’équipe juridique continue à travailler pour le 
mouvement, mais le coût des procès exige une lutte de tous les instants 
pour financer leurs défenses.

Merci de les aider à assurer aux Palestiniens et aux Israéliens qui 
luttent pour la justice qu’ils seront défendus face au système judiciaire 
civil et militaire.

Ecrire à l’IFA (Internationale des Fédérations Anarchistes) en envoyant 
vos chèques (à l’ordre de SEL, « soutien AATW » au verso) à::

    Société d’Entraide libertaire (SEL) c / o CESL
    BP 121
    25014 Besançon cedex
    IBAN : FR7610278085900002057210175 (virement)
       
   
   
