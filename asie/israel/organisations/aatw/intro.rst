
.. index::
   pair: Gil Na’amati; AATW


.. _aatw_qui:

============================
Qui sommes nous ?
============================

Anarchistes contre le mur (AATW) est un collectif d’activistes luttant 
contre toutes formes de ségrégation, d’apartheid, d’incarcération sociale 
et politique dénaturant  spécialement les valeurs démocratiques 
respectant le droits des minorités et d’autodétermination des peuples.

Le collectif Anarchiste contre le mur est particulièrement engagé depuis 
quatre ans dans la lutte contre la construction du mur et de la barrière 
de séparation que le gouvernement israélien a débuté depuis 2003. 

C’est afin de dénoncer le mensonge du plan sécuritaire israélien que 
le collectif a rejoint sur le terrain la lutte menée par les divers 
comités locaux des villages affectés par l’existence du mur.  

La lutte contre la construction et l’existence du mur s’étend à un 
nombre de villages palestiniens directement et indirectement concernés 
de plus en plus important, malgré les représailles, la répression et la 
violence perpétuées par l’armée israélienne contre la population 
palestinienne.

La présence du collectif sur le terrain favorise la création de réelles 
relations entre les deux peuples divisés par des politiques d’états. 
Elle encourage chaque village à s’engager dans la lutte globale s’étendant 
sur toute la longueur du mur. Elle maintient un point critique et alternatif 
de l’opinion publique israélienne trop souvent alignée à la politique de 
son gouvernement.

Les risques encourus par le collectif sont élevés et ont un prix. 
Un prix que chaque membre est près et disposé à payer de sa propre 
personne : Blessures irréversibles, année d’incarcération ou expulsion 
du territoire. Ils ont également un prix économique.
       
Le collectif Anarchiste contre le mur (Anarchists Against The Wall : AATW) 
a été formé durant le mois d’avril 2003, suite à un campement de résistance 
qui se teint aux abords du village de Masha, situé à 6 Km de la ligne verte. 

Sous la pression du chantier du mur confiscant et défigurant ses terres, 
le comité populaire local du village convoqua israéliennes et israéliens, 
à venir se joindre à la lutte contre le plan de ségrégation mis en force 
par leur propre gouvernement.

En décembre 2003, à Deir Ballut, village voisin de Masha, un nouveau 
camp de résistance pris le jour. Une des actions que le collectif AATW 
coordonna fut de revenir à Masha pour intervenir directement sur 
l’élévation de la barrière de séparation, en y sectionnant le grillage 
de son portail principal. Durant cette action, l’armée israélienne utilisa 
pour la première fois des tirs de vraies balles à bout portant contre 
le groupe de manifestants. 

Gil Na’amati, partenaire du collectif AATW fut grièvement blessé.

Couverte par la majeure partie des médias israéliens, cette action 
consolida la cohésion du groupe et frappa la conscience populaire 
israélienne indifférente jusqu’alors à la construction du mur.

       
2004, surnommée Intifada du mur,  fut l’année de soulèvement général 
qui s’étendit sur toute la longueur de la Cisjordanie. 
La majeure partie des villages affectés par la trajectoire du mur se 
révolta et se structura en comités locaux de résistance : Budrus, 
Deir Qadis, Azawia, Kharbata, Bidu, Beit Surik, Beit Liqia etc. 

Chaque jour avaient lieu deux à trois manifestations. 
Le collectif AATW pris de court, ne pouvait ni suivre ni participé à 
chacune de ces actions. Les manifestations se déroulaient en semaine 
réduisant ainsi la présence du collectif AATW sur le terrain, alors que 
l’armée israélienne ne sachant comment réagir face à ce soulèvement 
généralisé, intervenait avec violence et brutalité. 

Tanks, hélicoptères, troupes d’élites pénétraient de minuscules réalités 
rurales comme celle de Budrus, village de 1200 habitants.

En conséquence de ces interventions militaires d’urgences, quinze 
palestiniens furent tués durant les manifestations contre l’élévation 
du mur et  chaque jour, cinquante à cent palestiniens furent blessés 
par balles en caoutchouc et hospitalisés. Cette même année, l’armée 
cessa de construire le mur sur toute sa longueur, afin de se concentrer 
particulièrement sur certains tronçons stratégiques et d’en assurer 
la finition. 
En contrepartie du résultat obtenu par le soulèvement populaire 
palestinien : ralentissement de la construction du mur, la répression 
violente de l’armée en affaiblit la motivation et la persévérance. 

C’est au creux de cette vague qu’un nouveau village prit la relève : Bil’in.

En février 2005 Bil’in réuni par son comité populaire local débuta sa 
lutte de résistance contre la construction du mur. Bil’in marqua un 
point de transformation dans la planification et l’organisation des 
manifestations contre le mur. 
Les manifestations organisées en fin de semaine, favorisant ainsi la 
recrudescence de la présence du collectif AATW ainsi que d’autres 
mouvements israéliens solidaires de la cause palestinienne, prirent un 
pli d’ordre symbolique plus que d’action de confrontation directe, 
jouant ainsi sur le pouvoir de communication des médias. 

Malgré la permanence de la violence militaire, la présence des médias, 
d’internationaux et d’israéliens en contenait la profusion telle qu’elle 
fut vécue l’année précédente. La résistance contre la construction du 
mur pouvait ainsi se déployer à long terme. 
Chaque vendredi, depuis deux ans et demi, Bil’in tient tête à l’armée 
israélienne. 
La relance de Bil’in engagea de nouveaux villages, situés principalement 
aux abords de Jérusalem et au sud de Bethlehem à prendre action à la 
lutte : Abud, Al-Ma’sara, Beit Sira, Umm Salamuna, El Wallaja, Nilin  etc.
          

À l’instar de Budrus, le cas Bil’in révéla le mensonge du plan de 
sécurité du gouvernement israélien. Le mur ne sert pas à protéger la population civile israélienne, mais sert de rempart à l’acquisition illégale de nouveaux terrains en vue d’élargir et d’agrandir les colonies aux seins des territoires occupés. Alors que le tribunal international de la Hague condamna l’entreprise du gouvernement israélien, ce dernier poursuit sans égard son plan d’apartheid contre la population palestinienne qui consiste à diviser la Cisjordanie en cinq enclaves principales. Morcelant ainsi la Cisjordanie et par l’agrandissement de ses colonies existantes dont le mur n’est qu’une façade, le gouvernement israélien tente d’effacer de son panorama la réalité palestinienne et d’englober la Cisjordanie à son patrimoine national.  

Conquête d’un territoire par disparition de sa population locale.

Le mur n’est que l’un des éléments d’une machine d’apartheid beaucoup 
plus complexe et subtile faisant intervenir côte à côte le système 
judiciaire et militaire.   
   
La construction du mur touche à son terme. 
À l’intérieur de la Cisjordanie, si l’on considère une même parcelle 
de terrain, l’entité palestinienne est totalement retranchée de son 
propre territoire par l’imperméabilité du réseau des colonies et de 
son réseau de routes agencé.

Joindre le mouvement de soulèvement populaire palestinien devient une 
entreprise de plus en plus difficile, alors que nous sommes à l’orée 
d’une période de calme, annoncée par une série de nouvelles négociations, 
cachant cependant une prochaine révolte.

Le défi des Anarchistes contre le mur est de maintenir et d’élargir à 
long terme son réseau de communication et de présence sur le terrain 
en collaboration avec les divers comités populaires locaux.  
 
Les Anarchistes contre le mur perpétuent la visite de nouveaux villages 
susceptibles de participer à la lutte, maintiennent leur  présence durant 
les diverses manifestations organisées par les comités populaires locaux, 
programment  de nouvelles actions directes visant à enrayer le mécanisme 
du mur. 
En effet, le mur est une machine qui nécessite une surveillance ainsi 
qu’un entretien permanent. 
À grande échelle et long terme, les coûts excessifs de réparation 
d’entretien et de surveillance pourraient rendre inefficace le projet du mur.
      
Afin de rendre efficace son action, le collectif Anarchistes contre le 
mur appelle à un soutient financier afin de pourvoir principalement :

- Aux coûts de représentation légale qui s’élèvent à 30'000 euros par an.
- Aux coûts de transport qui s’élèvent à 3'600 euros par an.
- Aux coûts de matériel qui s’élèvent à  3'000 euros par an.
