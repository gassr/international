
.. index::
   ! Secrétariat International CNT


.. _secretariat_international_CNT_public:
.. _secretariat_international:

===================================
Secrétariat International CNT
===================================

.. seealso::

  - http://www.cnt-f.org/international/
  - :term:`secrétariat international`
  -  international@cnt-f.org
  - :ref:`terre_et_liberte`


.. contents::
   :depth: 3

Coordonnées
===============

==================  ===================
Site Internet       http://www.cnt-f.org/international
Courriel            international@cnt-f.org
Adresse             33 rue des Vignoles, BP 30423 75020 Paris
==================  ===================


Description
=============


Son site internet (www.cnt-f.org/international) veut le reflet des activités des
groupes de travail, et tous **les adhérents peuvent y participer à sa construction
par l'envoi de leurs informations**.

Par ailleurs, toutes les remarques et propositions tant sur le fond que sur la
forme, sont les bienvenues pour faire de ce site le support d’un travail
collectif.

Le Secrétariat International est composé d'adhérents de toute la confédération
et est représenté par trois mandatés, désignés par le :term:`congrès confédéral`.

Le travail du Secrétariat International se justifie par le développement des
relations internationales, mené par les syndicats et les militants eux-mêmes:
il apporte les conditions nécessaires pour que les relations de solidarité
puissent se développer et se renforcer.

Ainsi donc, il se structure à partir de groupes de travail (GTs) qui consolident
leurs échanges selon différentes zones géographiques, suivant de près leurs
problématiques spécifiques.

Chaque groupe de travail se réunit régulièrement ; les membres communiquent
par le biais de listes de diffusion ouvertes à tous les camarades qui
souhaiteraient participer.

Par ailleurs, chaque GT a une adresse officielle pour assurer le lien avec nos
partenaires internationaux et que vous pouvez également utiliser pour adresser
vos messages.

Voici la liste des adresses :

- GT Palestine - palestine@cnt-f.org
- GT Afrique - africa@cnt-f.org
- GT Amériques - america@cnt-f.org
- GT Europa - europa@cnt-f.org
- GT Océanie - international@cnt-f.org
- GT Asie - international@cnt-f.org

Et enfin, l'adresse officielle du SI - CNT est international@cnt-f.org


Terre et Liberté
================

.. seealso:: :ref:`terre_et_liberte`
