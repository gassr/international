

.. index::
   pair: 2012; Tournée France ESE
   pair: Vidéos; ESE
   pair: Grèce; Yorgos, Anthi et Nikos

      
.. _tournee_ese_france_2012:
   
===============================
Tournée en France de l'ESE 2012
===============================


.. seealso::

   - :ref:`grece_ese`
   - http://www.cnt-f.org/video/videos/44-international/343-video-des-camarades-grecs-de-l-ese-sur-les-origines-de-la-crise-en-grece
   - http://www.cnt-f.org/spip.php?article1926
   - http://athens.ese-gr.org/
   - http://www.cnt-f.org/59-62/2012/02/resume-des-dernieres-mesures-adoptees-en-grece/
   - http://fr.wikipedia.org/wiki/Crise_de_la_dette_publique_grecque
   - http://www.cnt-f.org/video/videos/44-international/353-rapide-presentation-de-la-tournee-de-lese-en-france-mai-2012
   
.. contents::
   :depth: 4

Résistances sociales et luttes anticapitalistes : l'alternative du syndicalisme de base en Grèce
================================================================================================

.. figure::  CNTGRECE.jpg

   Tournée en France de l'ESE
   

Vidéo_ des camarades grecs de l’ESE (Ελευθεριακή Συνδικαλιστική Ένωση , Union 
Syndicaliste Libertaire) sur les origines de la crise en Grèce

Projection et débat avec 3 militants anarcho-syndicalistes grecs (Yorgos, Anthi 
et Nikos) de l'ESE 

Face à l'ubiquité d'un discours qui tend de plus en plus à culpabiliser la 
société grecque de son sort, face à l'absence d'une information contrastée dans 
les médias « officiels », nous espérons que cette rencontre pourra être une 
source d'information directe, un lieu d'échanges ainsi qu'un moyen d'envisager 
nos luttes plus largement qu'au niveau local.

.. _Vidéo: http://www.cnt-f.org/video/videos/44-international/343-video-des-camarades-grecs-de-l-ese-sur-les-origines-de-la-crise-en-grece


Dates et villes de la tournée de Yorgos, Anthi et Nikos
=======================================================

- Nancy et Metz le 2 mai, 
- Strasbourg le 3 mai, 
- Dijon le 4 mai, 
- Lyon le 5 mai, 
- Paris le 6 mai, 
- Lille le 7 mai, 
- Villefranches sur Saône le 8 mai, 
- Grenoble le 9 mai, 
- Nimes le 10 mai, 
- Marseille le 11 mai, 
- Toulouse le 12 mai, 
- Limoges le 13 mai, 
- Vendée le 14 mai, 
- Nantes le 15 mai 
- et Rennes le 16 mai.


Lyon samedi 5 mai 2012
----------------------

.. seealso:: http://rebellyon.info/Debat-avec-des-syndicalistes.html

La CNT 69 invite des syndicalistes de l’EΣE : l’union syndicale libertaire. 
Un débat suivra la présentation des syndicalistes grecs. Venez discuter et 
échanger autour d’un repas grec après le débat ! Rendez-vous le samedi 5 mai à 
19h au local de la CNT.

Dans le cadre d’une tour­née orga­ni­sée en France par le secré­ta­riat inter­na­tio­nal 
de la CNT, trois mili­tan­tEs grecs de l’EΣE, un syn­di­cat frère de la CNT, vont 
venir parler de la réa­lité que vit le peuple grec, atta­ques du patro­nat, du 
gou­ver­ne­ment, de la police et de l’extrême droite. 

Mais aussi et sur­tout des luttes, des nou­vel­les formes d’entrai­des qui se 
met­tent en place, et de l’espoir qui naît. Avec l’échange et la dis­cus­sion 
nous lie­rons nos réa­li­tés pas si éloignées.

Le capi­ta­lisme n’a pas de fron­tière, Nous non plus !

Un débat suivra la pré­sen­ta­tion des syn­di­ca­lis­tes grecs. Venez dis­cu­ter et 
échanger autour d’un repas grec après le débat !

Rendez-vous le samedi 5 mai à 19h au local de la CNT 69 44 rue Burdeau 69001 Lyon.

Lille, lundi 7 mai 2012
-----------------------

.. seealso:: 

   - http://www.cnt-f.org/59-62/2012/04/rencontre-debat-avec-trois-militants-grecs-de-l-ese-union-syndicaliste-libertaire-le-7-mai-a-lille/
   - http://www.cnt-f.org/video/videos/44-international/353-rapide-presentation-de-la-tournee-de-lese-en-france-mai-2012



Enregistrement quasi intégral de la rencontre-débat avec l'ESE (partie 1)
=========================================================================

.. seealso:: http://www.cnt-f.org/video/videos/44-international/371-grece-enregistrement-quasi-integral-de-la-rencontre-debat-avec-l-ese-partie-1


::

	Sujet: 	[Liste-syndicats] [vidéo] enregistrement quasi intégral de la rencontre-débat avec l'ESE (partie 1)
	Date : 	Wed, 06 Jun 2012 15:21:00 +0200
	De : 	Secteur Vidéo CNT <secteur-video@cnt-f.org>
	Pour : 	Liste interne confederale des syndicats CNT <liste-syndicats@bc.cnt-fr.org>

*Grèce : enregistrement quasi intégral de la rencontre-débat avec l'ESE (partie 1)*

Du 2 au 16 mai 2012, trois syndicalistes grecs de l'EΣE étaient en
France à l'invitation du secrétariat international de la CNT. 

Ce montage reflète quasiment dans son intégralité l'intervention des camarades
grecs tout au long de leur tournée (même si l’intervention faite dans
chaque ville est bien sûr unique). Dans un premier temps, les camarades
de l'EΣE nous présentent leur organisation. Puis ils font un état des
lieux et nous parlent du fonctionnement syndical & politique en Grèce.

Ensuite, ils abordent différents thèmes (crise financière, alternatives
existantes, mouvement d'occupations, critique de leur pratique) et ils
terminent par quelques mises en garde. Dans un second temps, les
camarades de l'EΣE invitent au débat. 

Celui-ci n'apparaît pas intégralement (faute de batterie suffisante sur le 
caméscope). Les questions posées portent sur l'éducation, les néo-nazis, 
la politique, les médias, l'immigration et la santé. 

Au final, le montage s'articulera en 2 parties. D'ores et déjà, voici la 1ère 
partie (durée = 37:10)

http://www.cnt-f.org/video/videos/44-international/371-grece-enregistrement-quasi-integral-de-la-rencontre-debat-avec-l-ese-partie-1

La longueur de la vidéo est due à la volonté du Secteur Vidéo de
permettre à nos camarades grecs de visionner en grande partie la vidéo
en version originale. Le montage est donc basé sur une succession de
coupes à l'image en grec et en français. 

Toute la partie «rencontre-débat » a été filmée par le biais d'un petit 
caméscope posé sur un trépied, ce qui représente un long plan-séquence fixe. 

Pour dynamiser le rythme du montage, un faux effet « champ / contre champ » a
été utilisé. Cette technique donne l'illusion de plans différents mais,
en réalité, c'est la même image inversée. Ce travail vidéo n'a pu voir
le jour que grâce à la collaboration avec le Secteur Vidéo de plusieurs
personnes (membres de la CNT ou extérieures) dont notamment nos
camarades traductrices/teurs.

*Ελλάδα : σχεδόν ολόκληρο βίντεο της συνάντησης-συζήτησης με την ΕΣΕ
(μέρος Α')*

Από τις 2 ως τις 16 Μάιου 2012, τρεις συνδικαλιστές από την ΕΣΕ
επισκεφτήκαν την Γαλλία με την πρόσκληση της Διεθνής Γραμματείας της
CNT. Το συγκεκριμένο βίντεο παρουσιάζει σχεδόν εξολοκλήρου τις
παρεμβάσεις των Ελλήνων συντρόφων κατά την διάρκεια της επίσκεψης τους
(ακόμα και εάν η παρέμβαση που έκαναν σε κάθε πόλη που πήγαν ήταν,
βέβαια, μοναδική). Αρχικά, οι σύντροφοι από την ΕΣΕ μας παρουσιάζουν την
οργάνωση τους. Έπειτα, περιγράφουν την κατάσταση στην Ελλάδα, και μας
μιλάνε για τις συνδικαλιστικές και πολιτικές δομές της χώρας. Στην
συνέχεια ανοίγουν διάφορα θέματα συζήτησης (οικονομική κρίση, υπάρχουσες
εναλλακτικές, κίνημα καταλήψεων, κριτική της πρακτικής τους) και
καταλήγουν σε μερικές επιφυλάξεις. Στο δεύτερο σκέλος, οι σύντροφοι από
την ΕΣΕ καλούν στην συζήτηση, η οποία δεν βιντεοσκοπήθηκε ολόκληρη (λόγο
έλλειψης μπαταρίας της κάμερας). Τεθήκαν ζητήματα παιδείας, νεοναζισμού,
πολιτικής, ΜΜΕ, μετανάστευσης και υγείας. Τέλος, το μοντάζ αποτελείται
από 2 μέρη. Το Α' μέρος είναι ήδη διαθέσιμο εδώ (Διάρκεια = 37:10) :
http://www.cnt-f.org/video/videos/44-international/371-grece-enregistrement-quasi-integral-de-la-rencontre-debat-avec-l-ese-partie-1

Η διάρκεια του βίντεο οφείλεται στην επιθυμία του Τμήματος Βίντεο να
δώσει στους Έλληνες συντρόφους μας την δυνατότητα να δουν το μεγαλύτερο
μέρος του βίντεο στην γλώσσα που γυρίστηκε. Το μοντάζ βασίζεται λοιπόν
στην αλληλουχία πλάνων στα Ελληνικά και στα Γαλλικά. Όλο το μέρος
«συνάντηση-συζήτηση» γυρίστηκε με μια μικρή κάμερα σε τρίποδο και
αποτελεί ένα μακρύ σταθερό πλάνο-σεκάνς. Για ένα πιο δυναμικό ρυθμό του
μοντάζ, χρησιμοποιήθηκε η τεχνική του ψευδό - champ / contre champ,
δίνοντας την αίσθηση διαφορετικών πλάνων εκεί που στην πραγματικότητα
υπάρχει η ίδια, αλλά ανεστραμμένη εικόνα. Το βίντεο αυτό δεν θα υπήρξε
χωρίς την συνεργασία του Τμήματος Βίντεο με διάφορα άτομα (μέλη της CNT
και εξωτερικοί συνεργάτες), όπως οι σύντροφοι και συντρόφισσες μας
μεταφραστές/τριες.


Textes 
======

.. toctree::
   :maxdepth: 3
   
   texte_ese
   
   







