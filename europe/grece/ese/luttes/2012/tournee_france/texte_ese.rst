
.. index::
   pair: Grèce; tournée 2012 en France

.. _nous_ne_sommes_pas_tous_grecs:

==============================
Nous ne sommes pas tous grecs!
==============================

.. seealso:: 

   - :ref:`tournee_ese_france_2012`
   - :ref:`grece_ese`   

::

	Sujet: [Liste-syndicats] à destination des syndicats accueillant les camarades grecs
	Date : Fri, 4 May 2012 23:30:03 +0200 (CEST)
	De : educ57@cnt-f.org
	Pour : liste-syndicats@bc.cnt-fr.org

bonjour,

les camarades grecs de l'ESE (Yorgos, Anthi et Nikos) actuellement en tournée 
ont commencé mercredi 2 à Metz. Ceux-ci sont venus avec un texte d'appui, en 
français, à leur prise de parole.

Mais dans la mesure où celui-ci était difficielement compréhensible pour
un public non averti (seuls les anarchosyndicalistes en comprennaient le
sens...), nous avons reformulé avec eux dans un français plus accessible.

on en avait imprimé d'avance pour qu'ils repartent de Metz avec, mais au
dernier moment ils les ont oubliés...

Nous vous transmettons donc le texte reformulé en PJ, ils en auront besoin
en arrivant chez vous !


Texte 
=====

Dernièrement, à l'intérieur et l'extérieur de la Grèce il a éclaté une propagande 
imposant au peuple grec la rigueur, cela afin de dissimuler la stratégie des 
gouvernements capitalistes européens. 

Si on y regarde de plus près, les mesures prises par l'État grec et les patrons 
ces dernières années (réduction par une loi du salaire principal, abrogation de 
la négociation collective, l'abolition de la capacité des syndicats à faire 
des recours d’une part à  l’organisme de médiation et d'arbitrage, augmentation 
des licenciements, l'élimination des permanences au domaine public etc.) on 
s'aperçoit alors que ce ne sont généralement pas les Grecs qui sont touchés, 
mais une certaine partie de la population, les travailleurs vivant en 
Grèce, les indigènes et les immigrants.

Le recours à la dette publique pour définir la crise grecque a joué a servi 
de prétexte à l'Etat et au capital: ce n’est pas la cause réelle des mesures 
d'austérité. 

Une grande partie de la dette grecque se trouve  dans les banques, les 
entreprises locales et les caisses d'assurance,  dont les dirigeants ont 
investi l'argent des travailleurs sur le marché boursier dans des obligations: 
c'est la spéculation. L'état fait un effort pour sauver les banques de la 
faillite en volant de l'argent dans la poche des travailleurs, et en 
contraignant le prolétariat à ne plus croire à son avenir, en agitant la peur 
de la banqueroute.

L'assaut des patrons sur les acquis des travailleurs n'a pas commencé 
historiquement avec le Mémorandum. 
Dans les dernières décennies la classe de travailleurs de la Grèce a déjà 
accepté l'attaque des patrons via l'imposition de travail au noir, flexible et 
précaire, la suppression des huit heures journalières, les conventions 
individuelles pour les salaires sous le salaire principal, les licenciements 
illégaux, les heures supplémentaires non rémunérées, etc . 

Jusqu'à présent, cette attaque était réalisée en contournant la législation. 
Avec la législation de ces trois dernières années, cette situation est 
désormais établie par les institutions, elle est reconnue légalement, et 
s'étend à de plus larges sections de travailleurs, quelque chose qui va  
probablement aggraver les conditions de travail en plus.

C’ est remarquable, qu'au nom de la compétitivité des entreprises grecques, 
pendant qu'ils operent toutes ces coupes budgétaires, ils font des lois sur un 
certain nombre d'avantages pour les patrons (petits et grands), tels que les 
réductions d'impôt, la réduction des cotisations sociales (pendant que l'Etat 
se lamente du fait que les caisses d’assurances socialisées sont vides), la 
libération des licenciements etc... 

Cette attaque de l'état et du capital dans nos vies, qui tire la valeur de 
notre travail vers le  fond, était  basé sur l'incapacité de notre classe 
populaire à mettre en place collectivement et à mener des luttes organisées, 
due à la dégénérescence du syndicalisme par les “pères”  des travailleurs  
professionnels qui ont transformé le  mot syndical en une insulte insulte  et 
à l'indifférence des forces anticapitalistes organisées de  reprendre en main 
sérieusement le syndicalisme, et retrouver un syndicalisme de classe. 

Un autre facteur est aussi la prévalence de l'individualisme dans la mentalité 
de la majorité de la société grecque.

L'attaque des patrons avec le prétexte de la crise économique a profité d'un 
mouvement social des travailleurs désorganisé. La société grecque après le 
premier choc a marché à  une série de réactions populaires (indignés, collisions 
aux manifestations etc.) lesquelles sont restées sans organisation ni objectif 
précis et ne se sont pas étendues au champ de bataille principal de la classe 
ouvrière : celui du travail.

La bureaucratie syndicale déclare des grèves générales par la télévision, sans 
préparer aucune défense contre les briseurs de grève et sans plan d'amplification. 
Par conséquence, les grèves sont condamnées  à l'échec et le mouvement est affaibli.   

L'indignation de la société et des travailleurs ne conduit pas forcément à la 
révolution sociale. 

Avec l’augmentation des réactions sociales nous assistons à l’augmentation du 
nationalisme et à la légitimisation du discours raciste au sein des partis de 
gauche. Parallèlement, le système tente de tourner la colère du monde vers des 
faux choix en organisant des élections.

Toutefois, dans ces dernières années il y a le développement  d’une série de 
structures horizontales, dont beaucoup ont des caractéristiques  contre 
capitalisme comme les syndicats de base, les assemblées de quartier, des étudiants 
auto-organisés, les occupations, comités de chômeurs, les éco-communautés de 
producteurs agricoles, coopératives etc . 

Il est important de noter que dans la Grèce, contrairement à la France il y a 
seulement 2 confédérations syndicales des travailleurs (une pour les employés 
du secteur privé et l'autre pour les employés en public) impliquant toutes les 
tendances politiques du mouvement syndical. 

Ce monopole syndical veut  briser les syndicats de  base et ils ont commencé 
des discussions sur une plus grande organisation de syndicats et collectifs du 
travail du syndicalisme révolutionnaire, cherchant à intégrer, à assimiler les 
militants révolutionnaires. 

De même, les assemblées de quartier ont créé une assemblée pour coordonner les 
mouvements de quartiers individuels d'une manière durable.

Ce qui est certain, est que l'intensification des combat de classe en Grèce, 
peut aider à tirer des conclusions pour la classe des travailleurs, sur la 
nécessité de redéfinir le syndicalisme vers une orientation contre la 
bureaucratie, une orientation révolutionnaire.

ESE (Union Syndicale libertaire, Ελευθεριακή Συνδικαλιστική Ένωση


Jusque à maintenant lors d'un conflit avec quelque patron (le non payement de 
travail par exemple) le syndicat pouvait faire des recours à  l’organisme de 
médiation et d'arbitrage. Maintenant il ne peut pas sans l’accord du patron ! 
Ce changement de loi fait disparaître complètement l’organisme de médiation.


