

.. index::
   pair: 2008; ESE
   
   
.. _luttes_ese_2008:
   
===============================
Luttes ESE 2008
===============================

Interview de Yannis, secrétaire international de l’ESE grecque (anarcho-syndicaliste)
=====================================================================================

.. seealso:: http://www.cnt-f.org/spip.php?article863


Yannis Androulidakis est le secrétaire international de l’ESE grecque (2008) 
(anarcho-syndicaliste). 

Il revient sur la révolte qui secoue la Grèce depuis l’assassinat d’Alexis.
