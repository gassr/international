
.. index::
   pair: Suède; SUF
   
   
.. _suede_SUF:
   
=============================================
SUF 
=============================================

.. seealso:: 

   - http://www.suf.cc/


.. figure:: suf_logo.png
   :align: center
   
   *SUF logo*
