
.. index::
   pair: Suède; SAC
   
   
.. _suede_SAC:
   
=============================================
SAC (Sveriges Arbetares Centralorganisation)
=============================================

.. seealso:: 

   - https://www.sac.se/
   - https://en.wikipedia.org/wiki/Sveriges_Arbetares_Centralorganisation
   - https://twitter.com/syndikalisterna


Central Organisation of the Workers of Sweden (in Swedish: Sveriges Arbetares 
Centralorganisation) is an anarcho-syndicalist trade union federation in Sweden. 

Unlike other Swedish unions, SAC organizes people from all occupations, 
including the unemployed, students, and the retired.[1] 

SAC also publishes the weekly newspaper Arbetaren ("the Worker"), owns the 
publishing house Federativs and runs the unemployment fund Sveriges Arbetares 
Arbetslöshetskassa (SAAK).

Its long-term goal is to realize libertarian socialism, a society without 
classes and hierarchies, where the means of production are owned commonly and 
administrated by the workers: in effect, abolition of capitalism, wage slavery, 
and sexism. 

SAC has thus decided to declare itself to be anti-sexist, anti-militarist, 
and, as the first trade union in Sweden, feminist (1998). 

Short-term goals are improved salaries and working environments. It frequently 
cooperates with other libertarian socialist organizations, such as the 
Swedish Anarcho-syndicalist Youth Federation (SUF), although SUF is 
not a part of SAC.

SAC was formed in 1910 by former members of the mainstream trade union movement 
LO who were influenced by the revolutionary syndicalism of the French 
Confédération Générale du Travail. 

It was also a founding member of the anarcho-syndicalist International Workers 
Association, but came into conflict with the IWA in the 1950s when SAC entered 
into a state-supported unemployment fund, which the IWA regarded as state 
collaboration and reformist. 

In 1956, the SAC withdrew from the IWA.


SAC Members
===========

.. toctree::
   :maxdepth: 4
   
   members/index
   
   



