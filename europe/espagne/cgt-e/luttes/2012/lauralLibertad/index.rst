

.. index::
   pair: CGT-E; LauraLLibertad 
   
   
.. _laurallibertad_2012:
   
==============================================
Laura Gómez, #LauraLlibertat
==============================================

.. seealso:: 

   - http://laurallibertat.wordpress.com/
   - https://twitter.com/#!/LauraLlibertat
   - https://twitter.com/#!/cgtcatalunya
   - http://www.youtube.com/watch?v=48holCB05Gk&feature=youtu.be
   - http://www.youtube.com/watch?feature=player_embedded&v=YlooCD7mI-8
   - http://bellaciao.org/fr/spip.php?article127379
   - http://www.cnt-f.org/video/videos/44-international/350-liberte-pour-laura-de-la-cgt-barcelone
   - http://www.cnt-f.org/cnt66/spip.php?article779
   - http://www.cnt-f.org/cnt66/spip.php?article778
   - http://www.cnt-f.org/cnt66/spip.php?article792



Laura Gómez, secrétaire de la CGT (syndicat se réclamant de l'anarchosyndicalisme) 
de Barcelone est emprisonnée depuis le 26 avril. Ses torts ? 

Avoir mis le feu en compagnie d'autres manifestantEs, symboliquement, à des 
cartons remplis de faux-billets devant la bourse de Barcelone pendant la grève 
générale du 29 mars dernier. Lors de cette grève contre les nouvelles réformes 
libérales imposées par le gouvernement (baisse des salaires, des retraites, 
coupes dans les remboursements médicaux, fermeture d'hopitaux...) les 
anarchosyndicalistes catalans ont réunis plus de 50000 manifestantEs. 

Et la grève générale a été un succès. La réponse des autorités n'a alors pas
tardé : menaces, répression, criminilisation. Que ce soit en Grèce, en Algérie, 
au Maroc ou en Espagne, les autorités prouvent une fois de plus qu'en dehors 
de la répression elles n'ont pas de salut. 

Alors aujourd'hui c'est le tour de Laura Gomez. Demain, qui ? Demain où ?
Parce qu'un coup contre l'unE d'entre nous est un coup contre tous, toutes, 
parce que le capitalisme n'a pas de frontière, Solidarité Internationale !


Pétition
========

.. seealso:: http://actuable.es/peticiones/libertad-laura-g-y-todos-pres-s-del-29m


Laura a été libéré contre le versement d'une rançon de 6000 euros
==================================================================


- http://www.cgtandalucia.org/Laura-en-libertad,3729 
- http://www.rojoynegro.info/galeria/accion-sindical/fotos-la-liberacion-laura
- http://www.diagonalperiodico.net/En-libertad-Laura-detenida-en-la.html
- http://www.cnt-f.org/video/videos/44-international/366-liberation-de-laura-gomez-cgt-barcelone
- http://www.cnt-f.org/cnt66/spip.php?article792

Traduction par google traduction
--------------------------------

Depuis la CGT nous nous félicitons de la libération immédiate de notre collègue 
et après 23 jours de prison avec une peine d'emprisonnement tout à fait 
arbitraire et la souffrance d'une peine totalement disproportionnée, en dépit 
d'avoir des centaines d'images qui démolit l'argument des Mossos, les poursuite 
et de décision ultérieure du tribunal en prison sans possibilité de caution.

Du 24 Avril Laura Gomez a été arrêté Avril 25 et s'est retrouvé en prison pour 
avoir participé à un spectacle où ils ont brûlé une boîte en carton en face de 
la Bourse de Barcelone Droits. 

Combien dure a voulu montrer le Puig conseiller, faire tomber tous les moyens 
de répression, les forces armées, les procureurs et les juges contre quelqu'un 
qui ne se cache pas, agissant ouvertement, qui n'a pas causé de préjudice 
personnel et dispose d'une routine quotidienne il est facile à localiser.

Depuis CGT crois que le brûlage des boîtes en carton avec des papiers dans un 
endroit sûr ouverte pour quiconque n'est pas un crime, mais une protestation 
symbolique et pacifique, et si continue de sorte que le critère qui a conduit 
à la prison de Laura et comment être traitée une résistance passive aux 
agissements de la police, devront augmenter beaucoup les prisons.

Syndicats Statewide: CGT, CCOO, UGT, USO, CNT-AIT, la Confédération Inter, 
Workers Solidarity, le SCC ont signé une pétition demandant la libération de 
Laura et contre la répression.

Aussi Comissió Défense Col • legi d'Advocats de Barcelone a fait une déclaration 
dans laquelle il considéré comme excessif de prison ferme, parmi d'autres 
évaluations de l'intérêt légal grande.

Que faire si nous croyons qu'il devrait y avoir une infraction au code de 
l'argent public qui est fabriqué à partir de l'état aux banques. N'oubliez pas 
que les banques sont des entreprises privées et leur avidité pour faire des 
affaires dans les produits immobiliers et financiers plus que douteux, a créé 
cette crise que nous subissons, étant considéré comme privilégié par tous les 
politiciens, peut-être pour puis les écrire leurs dettes dans les millions.

Y at-il pas de juges et procureurs à considérer qu'il peut y avoir des 
infractions de corruption, la corruption et détournement de fonds? Combien 
de ceux qui sont responsables de la crise sont en prison?, Combien de ceux 
qui ont créé des troubles sociaux dans des millions de foyers qui prennent 
les gens à la précarité et l'exclusion, sont en détention provisoire et envoyé 
en prison en dépit de détruire les illusions de millions de personnes. 

C'est pourquoi nous disons que la justice n'est pas égale pour tous les 
procureurs et les juges se comportent très différemment selon la classe 
sociale des personnes devant eux.

La CGT estime que leur portent leurs fruits pression politique, à la fois le 
tribunal et le parquet, comme une personne ayant un casier judiciaire vierge, 
mis en place fixe, emploi stable, une fille qui vit avec elle, dit-on par 
l'accusation qui est à risque les fuites et les rechutes avant même d'être 
jugés et condamnés. Ces échelles ne s'appliquent pas aux banquiers, 
politiciens ou d'une enquête anti-corruption des procureurs qui ont été 
libérés. Nous avons vraiment remettre en question l '«indépendance judiciaire» 
du reste de la puissance dans un soi-disant «règle de droit." Et si non, nous 
expliquer et nous pouvons comprendre comment que Millet avait seulement 13 
jours de prison et Urdangarín et Torres n'ont pas foulé.

La CGT, nous demandons, lors d'un procureur spécial pour enquêter sur 
les banquiers, les politiciens corrompus et des hommes d'affaires qui ont 
créé une crise qui pousse des millions de personnes à l'exclusion sociale et 
la pauvreté. Au moment où un site web avec toutes les photos de criminels en 
cols blancs sont traités comme des gentlemen par les autorités. Au moment où 
un site web avec des photos des Mossos qui ont été mutilées avec des balles 
de caoutchouc et du martèlement des citoyens coups. M. Puig est la violence 
et oui vous avez toujours justifiée.

Nous considérons que l'emprisonnement est une vendetta par le gouvernement de 
la Generalitat et de sa force armée. Ils n'ont pas réussi à comprendre que 
la journée de grève générale dans la matinée, la CGT avec d'autres organisations, 
aglutináramos plus de 15.000 personnes visitent le centre-ville et dans 
l'après-midi plus de 50.000. L'intention est de provoquer la peur parmi les 
citoyens, de peur que nous mobiliser et nous nous conformons aux politiques 
économiques sans poser de question et pour cela ils ont besoin pour créer 
une image de violence entièrement fabriqués à partir d'un bureau.




