

.. index::
   pair: Militants; Valeriano Orobón Fernández
   
   
.. _valeriano_orobon_fernandez:
.. _orobon:
   
===============================================
Valeriano Orobón Fernández (1901-28 juin 1936)
===============================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Valeriano_Orob%C3%B3n_Fern%C3%A1ndez


Introduction
============

Valeriano Orobón Fernández (La Cistérniga, 1901 - Madrid, 1936) est le 
théoricien espagnol de l'anarcho-syndicalisme et militant syndical qui écrivit 
les paroles  du chant révolutionnaire :ref:`A las barricadas <a_las_barricadas>`.

Biographie
===========

Valeriano Orobón Fernández est né en 1901 à La Cistérniga, dans la province de 
Valladolid. 
Fils d'un militant socialiste, il a été impliqué dans le mouvement ouvrier 
très tôt. En 1920, il adhère à la CNT. Doué pour les langues, il traduisit la 
biographie de l'anarchiste allemand Max Nettlau de Rudolf Rocker.

Entre les écrits théoriques, Orobón était profondément impliqué dans les 
tentatives pour former une alliance révolutionnaire destinée à endiguer 
la marée montante du fascisme et à préparer la voie vers la révolution. 

Pour la période post-révolutionnaire, il envisageait que les syndicats auraient 
un rôle majeur dans une société décentralisée économiquement et politiquement.

Il soutenait que l'anarcho-syndicalisme était la première force révolutionnaire 
en Espagne. Il lutta contre les tendances communistes, qu'il jugeait comme trop 
étatiste, au sein de la CNT, le plus grand syndicat et principale organisation 
anarchiste en Espagne (en).

Peu avant sa mort en 1936 à Madrid, pendant la guerre d'Espagne, il écrivit les 
paroles de A las barricadas — sur la musique de La Varsovienne, chant 
révolutionnaire polonais — dans lesquelles il exhortait les travailleurs à 
combattre le fascisme. 

A las barricadas est devenu l'hymne de la CNT et l'un des plus célèbres chants 
anarchistes de la guerre d'Espagne.

