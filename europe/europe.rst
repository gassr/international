
.. index::
   pair: International ; Europe


.. _europe:

===============================
Europe
===============================

.. seealso:: http://www.cnt-f.org/international/Europe,51.html

.. toctree::
   :maxdepth: 4

   allemagne/index
   angleterre/index
   espagne/index
   france/france
   grece/index
   italie/index
   pologne/index
   suede/index
   suisse/index
