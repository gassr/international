

.. index::
   pair: Angleterre; IWW
   
   
.. _angleterre_iww:
   
===========
IWW  
===========

.. seealso:: 

   - http://iww.org.uk/
   - http://iww.org.uk/about/main

.. figure:: logo_iww.png
   :align: center
   
   *IWW logo*
    

British Isles region of the Industrial Workers of the World 

The IWW's unique approach to unionism is based on grassroots action and real 
democracy from the bottom up. 

The IWW is the union for all workers!

Why the IWW ?
=============

We are a grassroots and democratic union helping to organise all workers in 
all workplaces. The IWW differs from traditional trade unions. We believe that 
workers have greater voice if we are organised within our own industries. 

For example, teachers, cleaners and secretaries who work in a school should be 
classed as education workers and all be in the same union. 

Furthermore, unions in one industry are far stronger if they are in the same 
organisation as all other industrial unions. 

Our aim is to see society re-organised to meet the interests of all people, and 
not just shareholders and corporations.

We are NOT
==========

- Full of stifling bureaucracy or linked to any political party or group.
- Led by fat cat salary earners who carry out deals with bosses behind 
  your back.
- Going to sell you services, life insurance or credit cards.

We are
======

- Led by membership. We make all decisions and we all have the final say.
- For uniting all workers across trades, industries and countries.
- Able to offer practical support for members in their workplace.
- Flexible so you are still a member even when you change job or contract.

Who is the IWW for ?
====================

We are for ALL workers who do not have the power to hire or fire. 

This also includes workers who are retired, students, unemployed, part-time, 
temporary or those working at home. Workers who are members of other unions are 
also welcome.

