

.. index::
   pair: Angleterre; Equality trust
   pair: Equality ; trust   
   
.. _equkaity_trust:
   
================
Equality trust 
================

.. seealso:: 

   - http://www.equalitytrust.org.uk/why/evidence
   - http://www.equalitytrust.org.uk/why/francais

.. contents::
   :depth: 3
   

Social networks
================

.. seealso:: https://twitter.com/equalitytrust
