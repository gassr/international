
.. index::
   pair: Campagne FAU; travail intérimaire
   
.. _FAU_campagne_2011:
   
======================================================================
Leiharbeit abschaffen ! Campagne de la FAU sur le travail intérimaire
======================================================================

.. seealso:: http://www.cnt-f.org/international/Leiharbeit-abschaffen.html

Abolition du travail intérimaire ! C'est le slogan d'un campagne nationale
menée depuis quelques années par la Freie Arbeiterinnen- und Arbeiter
Union (FAU). 

Dans un contexte où l'Allemagne a du mal à cacher une
précarisation endémique du marché du travail, le fameux modèle social
allemand a du plomb dans l'aile. C'est le gouvernement « rouge-vert » de
Schröder, au cours des années 2002-2004, qui s'attela à la tâche des
réformes avec la collaboration des centrales syndicales. Ces réformes ont
été prises en concertation avec les nouvelles stratégies européennes de
l'emploi, notamment avec les travaillistes (le fameux « papier
Schröder-Blair). Dans son discours adressé au Bundestag le 14 mars 2003,
Schröder souligna l'impérieuse nécessité des réformes sociales : Nous
devons à travers les réformes engagées par le gouvernement fédéral adapter
l'économie sociale de marché aux conditions entièrement nouvelles de la
mondialisation de l'économie. Ces réformes ont transformé en profondeur le
paysage législatif et le marché du travail allemands, ouvrant ainsi le
service public de l'emploi aux agences intérimaires.


Le travail intérimaire, « la cession de salarié », fut autorisée en
Allemagne par une loi datant de 1972. Cette loi limita la durée des
missions et le nombre de contrats dans une même entreprise, mais ne
réglementa pas le statut de l'intérimaire. Puis, en 1982, un amendement
interdit l'intérim dans le BTP. Mais avec la montée du chômage, la
rigidité du droit du travail fut mise en cause. De nouvelles mesures
assouplirent le cadre réglementaire de l'intérim alors que l'écart
salarial se creusait entre intérimaires et embauchés. A la fin des années
90, quelques conventions collectives furent conclues à l'échelle régionale
par les syndicats avec les sociétés d'intérim, prévoyant la constitution
de conseils d'entreprises au sein des boites intérims. Ces conventions
permettaient aux syndicats de poser les jalons d'une nouvelle branche et
aux agences de s'introduire dans les secteurs fortement syndiqués. C'est à
cette époque que s’opéra un changement dans la perception du travail
intérimaire : il devint un instrument de la politique publique pour
l'emploi.

Plus tard, le travail intérimaire se libéralisa encore. En 2001, la loi
dite job-AQTIV, releva à deux ans la durée maximale d'une mission. L'année
suivante, ce sont les intérimaires étrangers qui furent touchés, par la
suppression de l'obligation de traitement égal avec les nationaux. Ce
furent surtout les lois Hartz I à IV qui sonnèrent le glas des
restrictions relatives au travail intérimaire en faisant du travail
intérimaire une arme pour combattre le chômage. Les agences locales
d'emploi converties en job centers, purent avoir recours à des
prestataires extérieurs : les « agences de services aux personnes »
(Personal-Service-Agenturen). Ces PSA peuvent désormais trouver des
missions d'intérim aux chômeurs de longue durée, rémunérées à hauteur de
l'allocation chômage durant 6 semaines. Ces lois sont extrêmement
coercitives pour les chômeurs. Ils ne peuvent pas refuser des offres mêmes
à bas salaires, la mobilité géographique est imposée aux célibataires, les
contrôles sont renforcés. Les réformes du marché du travail prennent la
forme d'une déréglementation du travail temporaire. Le développement de la
flexibilité externe fait la part belle aux agences d'intérim.
Le travail intérimaire et les agences se développent en Allemagne

Au cours de ces dernières années, le nombre d'intérimaires s'est sans
cesse accru. L'Agence fédérale pour l'emploi (Bundesagentur für Arbeit)
recensait 900.000 travailleurs intérimaires pour l'année 2010, un nombre
qui avait presque triplé depuis janvier 2003. Les deux tiers des contrats
d'intérim sont conclus avec des personnes au chômage dans la période
précédant immédiatement leur embauche. Ces contrats ont représenté environ
40% des emplois créés au cours de l'année 2010. L'accroissement
exponentiel du nombre d'embauchés en intérim va de pair avec celui des
agences d'intérim. Pour l'année 2010, l'Agence fédérale pour l'emploi
comptabilisait environ 23 400 agences agréées, soit une augmentation de
60% par rapport à 2003. Les marchands d'esclaves, pour reprendre les
termes de la FAU et utilisés jadis par les centrales syndicales,
développent leur business en Allemagne.

L'expansion du travail intérimaire développe un secteur à bas salaires et
contribue aussi à « flexibiliser » le marché du travail. Les entreprises
peuvent disposer, soit d'intérimaires qualifiés le temps de répondre à un
besoin de main d’œuvre accru, soit de travailleurs non-qualifiés à des
coûts très bas. Ainsi, les salaires horaires minimum, définis par des
conventions de branche [1], se situent autour de 7,80 euros à l'Ouest de
l'Allemagne et de 6,65 euros à l'Est. La moitié des contrats d'intérim
durent moins de trois mois. Ils débouchent rarement sur des embauches et
créent des disparités de traitement. Le fossé se creuse entre intérimaires
et permanents de l'entreprise : les écarts de rémunération sont passés de
30% en 1990, à désormais 40 %. Malgré le discours de l'Agence fédérale
pour l'emploi selon lequel « le travail temporaire offre une perspective
aux chômeurs, aux salariés menacés de chômage, aux personnes en quête d’un
premier emploi ou souhaitant retourner en emploi  », l'intérim n'est pas
un tremplin vers l'emploi permanent mais bien une variable d'ajustement
sur le marché du travail. Il crée une concurrence accrue et permet aux
entreprises de passer outre les conventions collectives par branche, le
tout avec la complicité des syndicats.

Les partenaires sociaux signent des conventions

La loi Hartz I avait posé le principe de traitement égal (conditions de
travail et salaires) entre travailleurs intérimaires et salariés fixes de
l’entreprise d’accueil. Ce principe avait été salué par la DGB [2] en
contrepartie de la dérégulation du travail intérimaire. Mais le principe
d'égalité de traitement pouvait être dérogé en cas de conventions
collectives signées entre syndicats d'employés et associations
d'employeurs. Les syndicats ne se privèrent pas de le faire. Pour prendre
de vitesse la petite confédération chrétienne CGB (Christlicher
Gewerkschaftsbund) prête à signer des accords de dumping, les syndicats du
DGB négocièrent des conventions collectives avec les deux plus grandes
associations patronales de l’intérim, BZA (Bundesverband Zeitarbeit) et
IGZ (Interessenverband deutscher Zeitarbeitsunternehmen). Cette course à
la convention collective au rabais conduisit à la mise en vigueur de
dispositions dérogatoires, à la satisfaction des associations patronales
et des agences d'intérim.

Des syndicats se retrouvèrent en concurrence. Celle-ci fut dénoncée par
les organisations de la DGB qui la jugèrent « illégale », voire « déloyale
», en raison de la faible représentativité des syndicats chrétiens. De
plus, la DGB accusa le coup, voyant la fin du monopole bilatéral
traditionnel (une fédération patronale de branche signe avec un syndicat
unitaire). La DGB saisit la justice allemande et appela le gouvernement à
instaurer un salaire minimum légal. Le 14 décembre 2010, le tribunal
fédéral du travail invalida les conventions collectives du secteur de
l'intérim passées avec les syndicats chrétiens, mais laissa ouverte la
question de la rétroactivité. Fallait-il redonner de l'argent aux
intérimaires qui avaient travaillé plusieurs années sous des conventions
tarifaires maintenant invalidées ?

Depuis 2009 s'est ouvert un débat politique autour de la question d'un
salaire minimum légal pour l'ensemble des intérimaires (déjà appliqué dans
le BTP et dans la branche postale). Loin d'être voté, ce projet est
contesté à droite car cela signifiait la fin du modèle social contractuel
allemand. Il est contesté à gauche car un salaire minimum légal, qui
serait en dessous des minimas salariaux de certaines branches,
légaliserait la concurrence entre travailleurs par la reconnaissance des
disparités de traitements. Cette question est remise sur la table en 2011,
avec « l'ouverture » du marché du travail allemand, laissant planer la
menace d'une concurrence accrue des travailleurs des pays de l'Est.
L'instauration de ce salaire minimum légal resterait un ajustement
conjoncturel, c'est l'interdiction du travail intérimaire qui est scandée
par la FAU.
La campagne menée par la FAU

L'organisation de cette campagne est une réponse au phénomène de
déréglementation et de développement du travail intérimaire, ainsi qu'aux
problèmes que cela soulève en termes de conditions salariales et de
protection sociale. Depuis les lois Hartz, les procédures menées contre
les agences d'intérim ont quadruplé pour non-paiement des arrêts maladies
ou des congés payés. Si le slogan de la campagne prône l'interdiction du
travail intérimaire, d'autres revendications sont mises en avant. La FAU
réclame l'embauche des intérimaires, pour ceux qui le veulent, dans les
entreprises où ils bossent ; les mêmes conditions de travail, de salaire
et de protection sociale. Concrètement, il est n'est pas aisé de militer
contre le travail intérimaire du fait même de la volatilité des
travailleurs et de disparité des conditions. Les premières étapes sont la
propagande et l'agitation. Des tables de presses et des marches sont
organisées devant des agences de l'emploi et des boites d'intérim. À
Düsseldorf, des militants se sont introduits dans une réunion du BZA,
association patronale qui avait justifié l'inégalité de traitement des
intérimaires. Des usines embauchant des intérimaires sont aussi des
cibles, telle que Airbus à Hambourg.

Ces activités débouchent parfois par un soutien plus spécifique. Dans la
firme SICK-AG de Freiberg, un militant est intervenu auprès de son conseil
d’entreprise afin que les intérimaires soient payés comme les employés.
Cette intervention payante a pu empêcher SICK-AG de pratiquer le dumping
salarial. La FAU a également organisé des manifestations de solidarité
avec des intérimaires de l’usine Volkswagen de Hanovre, en litige avec la
direction. Mais la question du travail intérimaire est aussi liée à la
question de la mondialisation. 

Au vu du contexte international, la FAU songe à organiser une campagne à 
l’échelle européenne. Des rencontres avec les anarcho-syndicalistes grecs de 
l’ESE, ainsi qu’avec des anti-autoritaires ont été déjà organisées.

Notes
======

[1] La négociation collective allemande ne s’appuie pas sur les
conventions nationales interprofessionnelles mais sur des conventions de
branche (Tarifvertrag) contractées entre syndicats et associations
patronales. Les conventions de branche peuvent différer selon les régions.
Des conventions d’entreprise peuvent se substituer aux conventions de
branche.

[2] La Deutsche Gewerkschaftsbund est la centrale syndicale majoritaire en
Allemagne forte de plus de six millions d’adhérents.
