
.. index::
   pair: Allemagne; FAU
   pair: Syndicat anarcho-syndicaliste; FAU
      
.. _FAU:
   
=============================================
Freie Arbeiterinnen- und Arbeiter Union (FAU) 
=============================================

.. seealso::
   
   - http://www.fau.org/
   - https://fr.wikipedia.org/wiki/Freie_Arbeiter-Union
   
   
.. figure:: Fau_logo_250px.png
   :align: center
   
   *Freie Arbeiterinnen- und Arbeiter-Union*

.. figure:: fau.gif
   :align: center
   
   *Freie Arbeiterinnen- und Arbeiter-Union*
   
   
La Freie Arbeiter-Union ou FAU (en français ``Union Libre des Travailleurs``), 
de son nom complet Freie Arbeiterinnen- und Arbeiter-Union, est une 
organisation anarcho-syndicaliste allemande fondée en 1977.

La FAU se revendique de la tradition de la FAUD (Freie Arbeiter-Union 
Deutschlands, « Union Libre des Travailleurs Allemands »), puissante 
organisation anarcho-syndicaliste allemande de l'entre-deux-guerres qui fut 
dissoute par les nazis en 1933.


.. toctree::
   :maxdepth: 4
   
   2011/index
