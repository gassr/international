
.. index::
   pair: CNT ; Solidarité Ouvrière
   pair: Solidarité; Ouvrière
   ! CNT-SO

.. _cntso:

======================================
CNT-SO
======================================

.. seealso::

   - http://www.cnt-so.org

.. contents::
   :depth: 3


.. figure:: logo_cntso.png
   :align: center


Adresses de contact
===================

Siège au 4, rue de la Martinique, à Paris 18e. 

Publie le périodique Solidarité ouvrière. 

Principales fédérations revendiquées : 

- Nettoyage
- Restauration-hôtellerie
- Culture-communication. 


Actions CNT-SO 
==============

.. toctree::
   :maxdepth: 3
   
   actions/index
   
   
