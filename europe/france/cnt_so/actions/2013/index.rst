

.. _cntso_2013:

======================================
Actions CNT-SO 2013
======================================


.. contents::
   :depth: 3


Résumé de la scission
=====================
::

    De : <a-infos-fr@ainfos.ca>
    Date : 13 mars 2013 12:33
    Objet : (fr) France, Alternative Libertaire #224 - CNT : Après la scission, quel futur ? (en)
    À : fr <a-infos-fr@ainfos.ca>


Quel est l’avenir immédiat des deux organisations se réclamant du 
célèbre sigle CNT ? Cruels déchirements ? Réconciliation inespérée ? 
Ignorance mutuelle ? 

Quelques éléments d’explication, et l’avis des protagonistes. 

La scission était jusqu’ici morale. Depuis l’automne 2012, elle est 
effective : il y a à présent deux CNT en France, auxquelles il faut 
ajouter une poignée de groupes locaux non confédérés mais arborant 
le sigle [1].

D’un côté, la CNT française (CNT-F) ; de l’autre, la 
CNT-Solidarité ouvrière (CNT-SO), un nom tiré de la riche histoire de 
l’anarchisme espagnol [2]. 

Déjà, le 1er mai 2011, les habitué-e-s de la manifestation parisienne 
avaient eu la surprise de voir le cortège CNT scindé en deux tronçons 
arborant chacun ses propres drapeaux. 

La distribution d’un petit bulletin intitulé Autre Futur confirmait la 
naissance d’une tendance dissidente au sein de la CNT.

Généalogie d’une rupture
=========================

À l’origine de ce divorce : un désaccord remontant au début des années 
2000, sur l’opportunité ou non, pour une organisation anarcho-syndicaliste, 
d’avoir des permanents. « Risque de bureaucratisa-tion » disent les anti ; 
« nécessité pour se développer » répondent les autres. 

Le débat est vieux comme le mouvement syndical, d’excellents arguments 
existent de part et d’autre, et cet article n’est pas le lieu de trancher 
cette question. Toujours est-il que le désaccord s’est cristallisé autour 
du syndicat du Nettoyage, dont la confédération CNT a « découvert » 
qu’il s’était doté d’un conseiller juridique rémunéré à plein temps.

La situation était épineuse, vu le fort symbole que représentait ce syndicat.

Constituée en 1988, la CNT-Nettoyage syndique des centaines d’ouvrières 
et d’ouvriers issus des couches les plus précarisées du prolétariat, 
essentiellement immigrés, parfois sans papiers. 
Combinant suivi juridique et animation de grèves, elle s’est donnée les 
moyens d’être une des actrices qui compte à Paris dans ce secteur 
industriel très dur. Et cela, personne ne le conteste.

Ce qui est contesté, c’est le rôle de son conseiller juridique, 
Étienne Deschamps. Pour les uns, c’est un « manitou » pesant bien plus 
lourd que les secrétaires réguliers du syndicat. 
Pour les autres, il joue un rôle nécessaire auprès de migrants possédant 
mal la langue française. À ce hiatus originel se sont progressivement 
greffés d’autres griefs. 

Les défenseurs de la CNT-Nettoyage ont reproché à l’union régionale 
(UR) parisienne d’être devenue un milieu contre-culturel et affinitaire. 

En retour, l’UR a déploré une politique du fait accompli et des pratiques 
fractionnistes. Un contentieux est également né autour de la 
labellisation CNT ou non d’un syndicat de l’Hôtellerie-restauration 
en région parisienne.

Le Nettoyage s’en va
=====================

Au fil des ans, le clivage a pris des proportions ingérables. Après le 
1er mai 2011 et la naissance de la tendance Autre Futur, la Foire à 
l’autogestion de juin 2012 a été le théâtre d’un règlement de comptes 
en public, une dizaine de cénétistes venant interdire de parole 
Étienne Deschamps dans un forum de débat. 

Malgré la condamnation, par le bureau confédéral, d’« actes malveillants » 
[3], cet épisode a signé la fin de la cohabitation. 

Le syndicat du Nettoyage a quitté la CNT-F peu après, bientôt suivi par 
d’autres syndicats.

Les portes de sortie n’ont cependant pas été les mêmes pour tout le monde.

Si certains sont passés à Sud, la plupart ont choisi de créer une nouvelle 
confédération : la CNT-SO, officialisée par une déclaration le 12 novembre. 
Quant à Autre Futur, elle a été transformée en « espace intersyndical » 
rassemblant des syndicalistes autogestionnaires de Sud, de la CGT et 
des CNT [4].

Pourquoi créer la CNT-SO plutôt que de rejoindre l’union syndicale 
Solidaires ? D’abord, explique Bernard, du bureau confédéral provisoire, 
par fidélité à une histoire, celle de la CNT, que certains scissionnistes 
estiment avoir «  portée à bout de bras pendant des décennies ». 

Mais surtout par attachement à un syndicalisme porteur d’un « projet 
révolutionnaire global ». L’action quotidienne, juge-t-il, doit être 
«le creuset, la matrice d’un mouvement d’émancipation plus vaste qui a 
pour nom le communisme libertaire ». 
**Ce qui n’est pas la visée de Solidaires**.

Où va la CNT-SO à présent ? « Il ne s’agit pas de se développer en 
“piquant” des adhérents aux syndicats existants, explique Bernard. 
Nous voulons avant tout mettre en pratique ce qui était resté au 
stade des pieuses résolutions au sein de la CNT-F et nous comptons 
essentiellement sur les luttes et un travail de proximité pour assurer 
notre développement à la base. »

Le congrès enfonce le clou
==========================

Du côté de la rue des Vignoles, cette scission est vécue comme un 
immense gâchis. Fouad, du bureau confédéral, regrette principalement 
que les gens d’Autre Futur n’aient jamais présenté une «
motion d’orientation » ou un « texte de congrès permettant de débattre » 
avant de commettre l’irréparable.

Ainsi, l’absence des « syndicats fractionnaires » au congrès de début 
novembre a certes permis à la CNT-F de se retrouver sur des « choix 
d’orientations clairs, cohérents, et largement majoritaires », mais elle 
a également privé les congressistes d’explications franches, et cela 
est ressenti comme un « déni de démocratie ». 
Quoi qu’il en soit, le congrès a enfoncé le clou, en réaffirmant son 
« refus des permanents syndicaux » [5].

Que va faire la CNT-F à présent ? « Contrairement à ce qui s’était passé 
en 1993, on ne va pas rester là-dessus pendant cent sept ans, balaie 
Fouad. 
Le constat, c’est qu’aucune fédération n’a quitté la CNT-F, et à présent, 
nous voulons passer à autre chose, avancer, construire. » 
Et ne pas couper les ponts, comme le laisse entendre, le récent 
communiqué qui évoque « la nécessaire unité » de l’anarcho-syndicalisme [6].

Guillaume Davranche (AL Montreuil)

Deux tronçons pour l’anarcho-syndicalisme
=========================================

CNT française : Siège historique du 33, rue des Vignoles, à Paris 20e. 
Publie le mensuel Le Combat syndicaliste. Principales fédérations 
revendiquées : Éducation, Santé-social, BTP, Culture-spectacle. 
www.cnt-f.org

CNT-Solidarité ouvrière : Siège au 4, rue de la Martinique, à Paris 18e. 
Publie le périodique Solidarité ouvrière. Principales fédérations 
revendiquées : Nettoyage, Restauration-hôtellerie, Culture-communication. 
www.cnt-so.org

[1] Il s’agit notamment des groupes épars se revendiquant directement 
    de l’Association internationale des travailleurs (AIT, l’internationale 
    anarcho-syndicaliste)

[2] Fondée en 1907, l’organisation syndicale catalane Solidaridad Obrera 
    fut à l’origine de la CNT en 1910

[3] Communiqué confédéral CNT-F du 28 juillet 2012.

[4] www.autrefutur.net

[5] Communiqué confédéral CNT-F du 21 décembre 2012.

[6] Communiqué confédéral CNT-F du 21 décembre 2012.

