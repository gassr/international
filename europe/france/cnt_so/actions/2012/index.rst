

.. _cntso_2012:

======================================
Actions CNT-SO 2012
======================================


.. seealso:: 

   - http://www.cnt-so.org/Statuts-de-la-CNT-Solidarite
   - :ref:`statuts_CNT` 

CONFÉDÉRATION NATIONALE DES TRAVAILLEURS ADOPTÉS AU CONGRÈS CONSTITUTIF 
DE DÉCEMBRE 1946, MODIFIÉS AU TROISIÈME CONGRÈS DE NOVEMBRE 1949, ET EN 2012.

TITRE PREMIER
==============

BUT
---

Article 1. 
++++++++++ 

La Confédération nationale des travailleurs dite C.N.T. Solidarité ouvrière 
a pour but :

- De grouper, sur le terrain spécifiquement économique, pour la défense 
  de leurs intérêts matériels et moraux, tous les salariés, à l’exception 
  des forces répressives de l’État, considérées comme des ennemies des 
  travailleurs.
