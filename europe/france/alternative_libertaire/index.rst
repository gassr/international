
.. index::
   pair: Alternative ; Libertaire
   ! AL

.. _alternative_libertaire:
.. _al:

===========================================
Alternative Libertaire
===========================================

.. seealso::

   - http://www.alternativelibertaire.org/
   - https://fr.wikipedia.org/wiki/Alternative_libertaire_%28France%29


.. contents::
   :depth: 3



Qu'est-ce que Alternative Libertaire ?
=======================================

.. seealso::

   - http://www.alternativelibertaire.org/spip.php?rubrique6

Adresses de contact
===================

Si vous ne trouvez pas de collectif AL près de chez vous, contactez 
directement la fédération en écrivant à contacts@alternativelibertaire.org



Collectifs AL
==============

.. toctree::
   :maxdepth: 3
   
   collectifs/index
   
   
