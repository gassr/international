
.. index::
   pair: Alternative ; Libertaire
   ! CGA



.. _collectifs_AL:

==============
Collectifs AL
==============

.. seealso::

   - http://www.alternativelibertaire.org/spip.php?article9

.. toctree::
   :maxdepth: 3
   
   rhone_alpes/index
   
   
