
.. index::
   pair: Appel; 2011


.. _appel_2011_rcr:

=========================================
Appel de Thorens-Glières de novembre 2011
=========================================

.. seealso:: http://www.appel-de-thorens-glieres.fr/


Merci Patrick d'adhérer aux valeurs de l'appel de Thorens-Glières.
Votre signature est désormais validée.

Retrouvez plus d'informations sur les actions menées par l'association Citoyens
Résistants d'Hier et d'Aujourd'hui sur le site www.citoyens-resistants.fr


Vidéo
======

.. seealso:: http://www.appel-de-thorens-glieres.fr/video


