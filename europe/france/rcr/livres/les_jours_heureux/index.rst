
.. index::
   pair: Livres; Les jours heureux   
   
.. _les_jours_heureux_livre_rcr:
   
==================================
Les jours heureux, le livre du RCR
==================================

.. seealso::
 
   - :ref:`les_jours_heureux_film_rcr`
   - http://www.citoyens-resistants.fr/spip.php?article3
   - http://www.editionsladecouverte.fr/catalogue/index-Les_jours_heureux-9782707160164.html
   - http://www.editionsladecouverte.fr/auteur/index.php?id=40607
   - http://www.citoyens-resistants.fr/spip.php?article60
   - http://www.citoyens-resistants.fr/IMG/pdf/apppel_des_resistants_2004.pdf
	

.. figure:: les_jours_heureux.png

   Les jours heureux
   

Présentation
============

Le 4 mai 2007, le candidat Nicolas Sarkozy se rend aux Glières (Haute-Savoie), 
pour y saluer la mémoire des maquisards massacrés en mars 1944 par les nazis 
et les miliciens français. 
Élu président, il renouvelle l'opération en mai 2008 et avril 2009. Et cette 
année-là, il prétend que son action se situerait dans le droit fil « du Conseil 
national de la Résistance, qui, dans les heures les plus sombres de notre 
histoire, a su rassembler toutes les forces politiques pour forger le pacte 
social qui allait permettre la renaissance française ».

Pure imposture ! Publié en mars 1944 sous le titre Les Jours heureux, le 
programme du CNR annonçait un ensemble ambitieux de réformes économiques 
et sociales, auquel le fameux « modèle social français » doit tout, notamment 
la Sécurité sociale, les retraites par répartition et la liberté de la presse. 

Or, depuis son élection, Nicolas Sarkozy s'applique à démanteler ce programme, 
comme s'en réjouissait en 2007 Denis Kessler, l'un des idéologues du Medef : 
« Le programme du gouvernement est clair, il s'agit de défaire méthodiquement 
le programme du CNR. » D'où la contre-offensive de l'association 
« Citoyens résistants d'hier et d'aujourd'hui », créée par ceux qui ont réagi 
dès mai 2007 à l'imposture sarkozyenne.

En republiant ce texte fondateur exemplaire par sa concision, ils ont choisi 
de le compléter par une série d'articles sur son histoire et son actualité, 
expliquant d'abord comment il fut conçu puis mis en oeuvre après la Libération. 

Puis comment, dès les années 1990, mais surtout depuis la présidence de 
Nicolas Sarkozy, cet édifice a fait l'objet d'une démolition en règle. 

En évoquant la mobilisation citoyenne qu'ils ont initiée, ils révèlent la 
puissance du discours d'hier pour nourrir les résistances d'aujourd'hui.
