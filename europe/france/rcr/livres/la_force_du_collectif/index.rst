
.. index::
   pair: RCR; La force du collectif    
  
.. _la_force_du_collectif_rcr:

===============================
La force du collectif
===============================

.. seealso:: 

   - http://editionslibertalia.com/La-Force-du-collectif.html

.. figure:: la_force_du_collectif.png

   La force du collectif
   
   
Entretien avec Charles Piaget.  Par le Réseau citoyens résistants.

Lip, grande fabrique de montres à Besançon, vécut en 1973 une lutte exemplaire.
 
Celle-ci déboucha sur une autogestion innovante. Ce conflit fut très médiatisé 
à l’époque. 

En revanche, la minutieuse construction du collectif au sein de l’entreprise 
est peu connue.

Le Réseau citoyens résistants, émanation de CRHA (organisateur du rassemblement 
annuel des Glières), structuré en groupes locaux, s’investit dans la rédaction 
d’un projet de société tout en expérimentant une méthode de travail collectif. 

À la recherche de méthodes démocratiques, il a voulu faire connaître et mettre 
en valeur l’expérience de Charles Piaget, qui a puisé dans ses souvenirs et ses 
notes pour nous expliquer ce que peut permettre la force du collectif.

