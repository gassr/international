
.. index::
   pair: Livres; Indignez-vous   
   pair: Stéphane Hessel; Indignez-vous 
   pair: Glières; Indignez-vous   
      
.. _indignez_vous_rcr:
   
===============================================
Indignez-vous, 20 octobre 2010, Stéphane Hessel 
===============================================

.. seealso::
 
   - `http://fr.wikipedia.org/wiki/Indignez-vous_!`


En librairie de 20 octobre 2010
(32 pages, 3€, Indigène éditions, diffusion Harmonia Mundi)

.. figure:: indignez-vous_2010.jpg

   Indignez-vous.
   
   
Genèse de l'ouvrage
===================

La rédaction de l'ouvrage a été proposée à Stéphane Hessel par deux journalistes
politiquement engagés, fondateurs de la maison d'édition, après avoir entendu le
discours que celui-ci avait prononcé sur le plateau des Glières, lieu de
mémoire de la Résistance, pour dénoncer la trahison des principes du Conseil
national de la Résistance dont il accuse le chef de l'État Nicolas Sarkozy.

Le discours que celui-ci, alors candidat à l'élection présidentielle, avait
prononcé entre les deux tours de cette élection le 4 mai 2007 au plateau des
Glières avait en effet provoqué l'indignation de l'ancien déporté Walter
Bassan et de ses amis, indignation partagée par un certain nombre d'autres
résistants qui organisèrent une contre-commémoration annuelle aux Glières.

Ils se sont appuyés sur l'esprit de l'Appel à la commémoration du soixantième
anniversaire du Programme du Conseil national de la Résistance du 15 mars 1944
signé trois ans plus tôt, le 8 mars 2004, par les grandes figures survivantes
de la Résistance, Lucie Aubrac, Raymond Aubrac, Henri Bartoli, Daniel Cordier,
Philippe Dechartre, Georges Guingouin, Stéphane Hessel, Maurice Kriegel-Valrimont,
Lise London, Georges Séguy, Germaine Tillion, Jean-Pierre Vernant, Maurice Voutey.

Les amis de Walter Bassan se sont constitués en 2008 en une association,
Citoyens Résistants d'Hier et d'Aujourd'hui, association dont Stéphane Hessel
est l'un des parrains, avec Raymond Aubrac et John Berger.

L'appel du 8 mars 2004 (déjà lu lors du 1er rassemblement le 13 mai 2007) a été
diffusé à nouveau le 17 mai 2009 aux Glières, et a été suivi par un discours,
prononcé en présence de Raymond Aubrac et de quatre mille sympathisants, dans
lequel **Stéphane Hessel appelait à un devoir d'indignation**.

Ce sont ces prises de positions, initiées par les mêmes anciens résistants à
l'occasion de l'affaire des « sans papiers » de Saint-Ambroise et Saint-Bernard,
réaffirmées depuis 2004 qui ont présidé à la rédaction de l'ouvrage.

Le texte de l'ouvrage a été élaboré au cours de trois entretiens, durant le
printemps 2010. Mis en forme par Sylvie Crossman, il a été approuvé par
Stéphane Hessel1.
