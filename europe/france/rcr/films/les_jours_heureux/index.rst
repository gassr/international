
.. index::
   pair: Films; Les jours heureux



.. _les_jours_heureux_film_rcr:

=====================================================================
"Les jours heureux" le film de Gilles Perret : appel à souscription
=====================================================================

.. seealso::

   - :ref:`gilles_perret`
   - :ref:`les_jours_heureux_film`   
   - http://www.lavaka.fr
   - http://www.lavaka.fr/Contact.html
   - http://www.citoyens-resistants.fr/spip.php?article247
   - http://www.reseau-citoyens-resistants.fr/spip.php?article114
   - http://fr.wikipedia.org/wiki/Programme_du_Conseil_national_de_la_R%C3%A9sistance

.. toctree::
   :maxdepth: 4
   
   site/index
   appel_a_souscription
   presse/index
   projections/index
   
   
   
