
.. _projections_les_jours_heureux_5_octobre_2013:

=====================================================================
Projections "Les jours heureux" 5 octobre 2013 à Cran-Gevrier
=====================================================================

.. seealso::

   - http://lesjoursheureux.net/ai1ec_event/cran-gevrier-74-cinema-la-turbine/?instance_id=   



::

    Salle pleine ce soir à la Turbine (74) sur les terres de @Gilles_Perret 
    pour l'avant 1ère #LesJoursHeureux sur #CNR pic.twitter.com/x9cZCInobD
