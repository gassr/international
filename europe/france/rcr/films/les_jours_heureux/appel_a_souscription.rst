

============================================================
"Les jours heureux" de Gilles Perret : appel à souscription
============================================================

.. seealso:: 

   - http://www.lavaka.fr
   - Contact: reseau.citoyens.resistants@gmail.com


Depuis plusieurs mois, le réalisateur Gilles Perret filme et recueille les  
témoignages de plusieurs acteurs ayant participé à l’élaboration de programme 
du Conseil National de la Résistance. 

Ce documentaire intitulé « Les jours heureux » veut raconter l’histoire de ces 
quelques hommes sans qui la sécurité sociale, les retraites par répartition, 
le vote des femmes, les comités d’entreprise et bien d’autres choses 
n’existeraient pas aujourd’hui en France.

L'occasion de rencontrer des historiens, des journalistes, des analystes 
spécialistes de la déconstruction de ce programme par quelqques uns et de voir
que, pour tous, le constat est unanime : le programme du Conseil National de la
Résistance est d'une actualité criante et il y a urgence à le rendre visible en
le remettant sur le devant de la scène.

Ce documentaire a été proposé à plusieurs chaînes de télévision. 
Aucune n'a souhaité "prendre le risque" de raconter une telle histoire.

Trop compliqué, trop risqué, Trop quoi !

Alors pour que ce film existe, qu'il soit vu et que cette histoire belle et 
singulière puisse être raconté à tous.

NOUS AVONS BESOIN DE VOUS ! 
Gilles Perret - Fabrice Ferrari

LA VAKA PRODUCTIONS 
Le revenette 74360 VACHERESSE France 
lavaka@wanadoo.fr


.. figure:: souscription.jpg

   Souscription

:download:`Télécharger la présentation <souscription.jpg>`

.. figure:: souscription_2.jpg

   Souscription

:download:`Télécharger la souscription <souscription_2.jpg>`
