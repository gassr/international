

============================================================
Commentaires presse 2012
============================================================


http://www.rougeprod.fr/les-jours-heureux/
==========================================

1 x 52 min – Scénario: Gilles Perret – Réalisation : Gilles Perret

Imaginez une réunion rassemblant tous les représentants de toutes les 
tendances politiques républicaines ainsi que l’ensemble des syndicats 
ouvriers qui décideraient de signer ensemble un même programme politique 
d’une ambition sociale, humaniste et politique folle ! 

Impossible, vous allez me répondre. Eh bien c’est pourtant ce qui s’est passé 
entre mai 1943 et mars 1944 sur le territoire français encore occupé. 

Ce programme est celui du Conseil National de la Résistance.
