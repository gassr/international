

======================================================================================================
"Les jours heureux", un documentaire consacré au Conseil National de la Résistance signé Gilles Perret
======================================================================================================

.. seealso::

   - http://alpes.france3.fr/2013/05/19/les-jours-heureux-un-documentaire-consacre-au-conseil-national-de-la-resistance-signe-gilles-perret-254187.html


Ce week-end de Pentecôte, au plateau des Glières, se tenait le rendez-vous 
des "citoyens résistants d'hier et d'aujourd'hui". 

A cette occasion, le réalisateur Gilles Perret a présenté son nouveau 
documentaire consacré au Conseil national de la Résistance dont c'est le 
70e anniversaire.

"Les jours heureux", c'est avant tout le titre du programme rédigé par 
le Conseil National de la Résistance. 
C'était entre mai 1943 et mars 1944, sur le territoire français encore 
occupé, 16 hommes, appartenant à tous les partis politiques, tous les 
syndicats et tous les mouvements de résistance, voulaient changer 
durablement le visage de la France, et rédigeaient, dans la clandestinité, 
ce texte qui sera au cœur du système social français d'après guerre 
puisqu’il donnera naissance:

- à la sécurité sociale, 
- aux retraites par répartition, 
- aux comités d’entreprises.


Les jours heureux", c'est aussi le titre du nouveau documentaire signé 
par Gilles Perret."Ce film", explique le réalisateur haut-savoyard, 
"vise à retracer le parcours de ces lois, pour en réhabiliter l’origine 
qui a aujourd’hui sombré dans l’oubli. 

Il vise aussi à raconter comment une utopie folle dans cette période 
sombre devint réalité à la Libération. Ce film raconte également comment 
ce programme est démantelé et collent les valeurs universelles portées 
par ce programme pourraient irriguer le monde demain"

Alors que cette année marque le 70e anniversaire du CNR, "Les jours heureux" 
viennent d'être présentés à Paris, au Forum des Images. 

Vous avez pu le découvrir en version 52 minutes sur l'antenne de France 
3 Rhône Alpes Auvergne ce samedi 18 mai à 15 heures. 

Une grande projection en avant-première a aussi eu lieu ce samedi à 
Thorens-Glières, lors du rassemblement "Paroles de Résistance".

Le film, produit grâce à une souscription, a aussi été sélectionné dans 
le cadre de "Visions Sociales" lors du festival de Cannes. 
Gilles Perret n'en est pas à son premier film sur l'histoire contemporaine, 
il est également l'auteur de "Walter, retour en Résistance", consacré à 
Walter Bassan, grand résistant et ancien déporté, sorti en salles en 
novembre 2009.




