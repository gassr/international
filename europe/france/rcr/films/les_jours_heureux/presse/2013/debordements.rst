

======================================================================================================
Gilles Perret mardi 22 janvier 2013, par Florent Le Demazel 
======================================================================================================

.. seealso::

   - http://www.debordements.fr/spip.php?article142


::

    Oui. Pour le prochain, sur le CNR, il s’agit aussi de rappeler l’origine de 
    ce programme : ce qui a été acquis à la Commune, à la Révolution. 

    C’est toujours la même histoire, toujours : on se bat pour acquérir des choses, 
    et peu à peu, les forces en face grignotent, jusqu’à ce que ça pète. 

    Il faut alors repartir pour reconstruire collectivement… Je pense que si on 
    prend le temps d’expliquer ça, on sera mieux armé pour comprendre les choses 
    et faire face.
