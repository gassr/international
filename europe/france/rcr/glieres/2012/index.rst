
.. index::
   pair: Glières; 2012
   pair: Paroles; Glières 2012
   pair: Programme; Glières 2012


.. _resistance_glieres_2012:

===============================
Résistance Glières 2012
===============================

.. seealso::

   - http://reseau-citoyens-resistants.fr/
   - http://www.dailymotion.com/leimal74#video=xr63bc
   - http://www.dailymotion.com/user/leimal74/1
   - http://www.citoyens-resistants.fr/spip.php?article247

.. contents::
   :depth: 3


Programme
=========

:download:`Télécharger le programme <Programme_Glieres_2012.pdf>`.

.. seealso::

   - http://www.fsd74.org/spip.php?article3591
   - http://www.fsd74.org/IMG/pdf/Programme_A4.pdf


Photos
======

.. seealso::

   - http://www.citoyens-resistants.fr/IMG/pdf/diaporama.pdf


Blogs, Presse
==============

.. seealso::

   - http://www.fsd74.org
   - http://www.librinfo74.fr


Paroles et vidéos Glières 2012
==============================

.. seealso::

   - http://www.citoyens-resistants.fr/spip.php?article247


Programme des 26 et 27 mai 2012
================================

.. toctree::
   :maxdepth: 4

   forum_des_resistances_26_mai/index
   discours_27_mai/index

