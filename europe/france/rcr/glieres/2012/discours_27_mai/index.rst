
.. index::
   pair: Glières; 2012

    
.. _discours_glieres_27_mai_2012:
   
========================================
Discours  et vidéos  Glières 27 mai 2012
========================================

.. seealso:: 

   - http://www.dailymotion.com/leimal74#video=xr63bc
   - http://www.dailymotion.com/user/leimal74/1
   - http://www.citoyens-resistants.fr/spip.php?article247
   - http://www.reseau-citoyens-resistants.fr/spip.php?article114

La liste des discours est ici: http://www.citoyens-resistants.fr/spip.php?article247

Projet de société
==================

.. seealso:: http://www.reseau-citoyens-resistants.fr/spip.php?article114


Discours de Charles Palant "Soyons fiers d'être des Hommes"
============================================================

.. seealso::

   - http://www.dailymotion.com/leimal74#video=xr63bc
   - http://www.citoyens-resistants.fr/spip.php?article247
   - :ref:`charles_palant`

Discours de Xavier Mathieu
==========================

.. seealso::
   
   - http://www.dailymotion.com/video/xr63u7_xavier-mathieu_news
   - http://www.citoyens-resistants.fr/spip.php?article247
   - :ref:`xavier_mathieu`
   
Discours de Catherine Tounier (RESF)
=====================================

.. toctree::
   :maxdepth: 3
   
   catherine_tournier   
   
Discours de Walter Bassan 
==========================

.. seealso::
   
   - :ref:`walter_bassan`
   - http://www.citoyens-resistants.fr/spip.php?article247
      
      
Discours de Panos ANGELOPOULOS
==============================


   
