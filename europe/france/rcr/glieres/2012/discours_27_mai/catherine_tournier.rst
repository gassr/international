
Discours de Catherine Tournier
==============================


.. seealso::

   - http://www.citoyens-resistants.fr/IMG/pdf/Catherine_TOURIER.pdf
   - http://www.dailymotion.com/video/xrazvu_catherine-tourier-resf-au-plateau-des-glieres_news (vidéo)


Les horreurs ne sont supportables que quand elles restent invisibles.
Rendues visibles, elles deviennent insupportables pour peu que des citoyens 
les combattent.

Depuis 2004, RESF ne cesse de mettre sous le nez de l’opinion publique, de mettre 
en lumière les horreurs que nos gouvernements font vivre aux migrants depuis des 
décennies.

50 000 personnes par an sont enfermées dans des prisons pour étrangers pour la 
simple raison qu’ils sont étrangers en situation administrative irrégulière, 
elle prononce 356 enfants dont 57 nourrissons, chiffres de 2010, sont en centres 
de rétention comme dit l’administration, c’est moins parlant, moins choquant que 
prison réservée aux étrangers. Il y a aussi des pères qu’on veut séparer de leur 
famille en les expulsant, des jeunes lycéens majeurs, des infirmes.

50 000 personnes par an, on arrive à un demi-million d’êtres humains enfermés 
en 10 ans. Parmi ceux là, plusieurs dizaines de milliers sont expulsés. 
C'est-à-dire expédiés à l’autre bout du monde,sans préavis, sans pouvoir saluer 
sa famille, ses amis, sans pouvoir organiser quoi que ce soit, parfois sans rien, 
sans un sou en poche, tel que vous avez été arrêté, en chaussons, en chemisette.

Mais cette réalité reste largement invisible, ce n’est pas sensible, on ne les 
connaît pas, on réduit leur sort à un chiffre, une idée.

Pourtant la veille, ils étaient là au milieu de nous tous, ils travaillaient 
sur un chantier, ils emmenaient les enfants à l’école, ils faisaient la plonge 
ou la cuisine dans des restaurants connus.

Ils étaient lycéens, jeunes parmi les autres de leur âge, indiscernables. 
RESF s’est créé pour soutenir des lycéens qui ont osé révéler leur situation à 
leurs profs et à leurs copains provoquant en général la stupeur de leur milieu. 
Et oui, le sans papier est un être comme les autres, on ne le reconnaît pas au 
premier coup d’œil, il peut avoir toute origine, toute couleur, toute religion. 

Non ce n’est pas un « bronzé », un « islamiste » ou tout autre fantasme. C’est 
mon voisin, le parent du copain de mon enfant, le lycéen que j’ai dans ma classe, 
c’est le parent du petit de la maternelle.

Devant cette situation concrète, révoltante, la mobilisation est possible, non 
seulement possible mais souvent spontanée. Comment peut-on penser à expulser 
ces gens là ? Qui vivent paisiblement ici depuis des années, qui sont à l’école 
pour les lycéens, dont les enfants sont nés en France et scolarisés pour les 
familles. C’est intolérable et ce n’est pas accepté par l’opinion publique. 

RESF a simplement permis à des citoyens de toute opinion philosophique, religieuse,
politique, de s’organiser pour protéger des étrangers menacés d’expulsion, pour 
les défendre. Des centaines, des milliers de collectifs se sont créés autour 
d’une situation concrète, ils ont inventé des actions, pétitionné, demandé des 
audiences aux Préfets, sollicités les élus, été voir des journalistes, ils ont 
prévenu les passagers des avions qui transportaient des expulsés. Avec les sans
papiers, ils ont fait du théâtre, des expos de photos, des chorales, des 
manifestations, des rassemblements, des occupations d’école, de gymnase, ils 
ont pris les choses en main et empêché des expulsions, obtenus des régularisations. 

Cette activité nous redonne à tous avec ou sans papier, de la dignité, le sens 
de l’humain et de la fraternité. Pas dans le discours mais dans la vie réelle. 
Etre actif ensemble nous montre que tout est possible, pour peu qu’on le veuille 
vraiment.

Cette activité a modifié l’image que l’opinion se fait du sans papier (l’image 
qu’on lui donne) et donc du sort qu’on doit lui réserver.

Pourtant face à ce mouvement de fond de l’opinion, plus important que la plupart 
des observateurs veulent bien le dire, la droite et l’extrême droite n’ont pas 
désarmé. Pour des motifs électoraux principalement, elles agitent toutes deux 
les peurs et les préjugés xénophobes faute d’avoir une politique face aux maux 
de notre société. C’est plus facile de désigner l’immigration comme un bouc 
émissaire de tous les malheurs du monde, c’est commode ça évite de réfléchir ou
de proposer des solutions. Nous avons eu une telle propagande depuis des années 
que la xénophobie et le racisme sont redevenus légitimes aux yeux de certains.

Mais dit la gauche ? Les gauches ? C’est mieux que le discours xénophobe du 
gouvernement précédent, bien sûr. Mais des engagements concrets, hein ? On rêve 
d’un discours précis, incisif disant que la migration c’est la vie et que nous 
sommes tous liés à des migrants ou à leurs enfants, petits enfants. Certains 
d’entre nous sont des enfants de migrants comme Sarkozy par exemple ou
Mariani, ou mon voisin de palier, comme une bonne part de mes élèves si j’en 
crois leurs noms de famille ; nous attendons un discours offensif face à ceux 
qui vont tenter d’exploiter ce qu’ils pensent être le sentiment xénophobe du 
Français moyen en attribuant aux autres leur propre xénophobie enracinée.

Nous attendons des actes du nouveau gouvernement et pas la prolongation de la 
politique précédente. Le moins qu’on puisse dire c’est que pour l’instant la 
gauche rase les murs sur le thème de l’immigration, elle a peur de son ombre. 
Elle n’a aucune confiance dans la population française qui dans les faits est 
déjà métissée, qui vit, travaille, habite, se marie, a des enfants avec
des migrants ou avec des descendants de migrants. Aujourd’hui nous sommes 
nombreux à avoir un parent, un beau frère, un gendre, une belle fille, issu de 
l’immigration ou migrant soi-même.
Cette réalité concrète doit être acceptée et regarder en face.

Des actes tout de suite.

Nous attendons aujourd’hui un moratoire sur les expulsions en attendant une 
refonte complète des lois sur l’immigration. Nous attendons que les promesses 
du candidat à la présidentielle soient tenues :

- pas d’enfants en rétention
- les parents d'enfants scolarisés ou les sans papiers ayant un travail déclaré 
  ou au noir doivent être régularisés
- les jeunes majeurs scolarisés sans papiers doivent être régularisés
- Le démembrement des familles (l'expulsion d'un père ou d'une mère tandis que 
  le conjoint et les enfants restent en France), mesure inhumaine s'il en est, 
  doit être interdit.
- Les taxes prohibitives perçues sur la délivrance des titres de séjour doivent 
  être réduites, ramenées, par exemples à celles exigées pour la délivrance d'un 
  passeport français (80 €)

De toute façon nous serons toujours là, aujourd’hui comme hier, nous prenons 
sous notre protection les jeunes et les familles qui attendent leur 
régularisation. 

On continue !
 
