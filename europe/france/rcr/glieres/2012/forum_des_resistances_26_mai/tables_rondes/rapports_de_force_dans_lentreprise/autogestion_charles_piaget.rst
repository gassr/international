
.. index::
   pair: Charles Piaget; Autogestion

.. _charles_piaget_autogestion:
   

================================
Charles Piaget et l'autogestion
================================

.. seealso:: http://www.librinfo74.fr/2012/06/le-rassemblement-citoyen-des-glieres%C2%A0-laboratoire-d%E2%80%99un-projet-de-societe-pour-le-21eme-siecle/


Le rassemblement citoyen des Glières : laboratoire d’un projet de société pour le 21ème siècle
==============================================================================================

::

    Vendredi 8 juin 2012
    Par myriam benoit
    
Au Forum des résistances le 26 mai dernier à Thorens-Glières, plusieurs 
tables-rondes ont évoqué les combats du monde ouvrier et salarié de ces 
dernières décennies. Parmi les courants de pensée qui ont irrigué ces combats, 
celui incarné par Charles PIAGET dans les années 70 inspire aujourd’hui 
le Réseau Citoyens Résistants.

Ouvrier, militant syndicaliste particulièrement actif en 1973 à Besançon lors 
du conflit social né au sein de l’entreprise d’horlogerie Lip, Charles PIAGET 
est devenu une figure emblématique du mouvement autogestionnaire français. 

Aujourd’hui âgé de 84 ans, il reste attaché à cet idéal d’inspiration marxiste 
qui prône notamment la suppression de toutes distinctions entre dirigeants et 
dirigés, et la non-appropriation des richesses produites par la société.

Rester égaux et privilégier le collectif, telles sont les fondamentaux que 
Charles PIAGET a réaffirmés à Thorens-Glières. 

Gérard FUMEX a recueilli ses propos:

Loin d’être pessimiste dans le contexte de crise que nous connaissons, 
Charles PIAGET compte sur l’avènement d’un nouveau modèle économique qui marque 
une rupture avec le libéralisme. Et pour qu’un véritable projet de société 
répondant à ses attentes puisse voir le jour, il en appelle au Réseau Citoyens 
Résistants créé avec le concours de l’association CRHA (Citoyens Résistants 
d’Hier et d’Aujourd’hui), organisatrice du rassemblement des Glières depuis 6 ans.

Pour Charles PIAGET, il faut être là où les gens luttent, loin des logiques d’appareil...

Au sein du Réseau Citoyens Résistants, chacun adhère à titre individuel, non 
au titre d’un parti ou d’un syndicat. Les idées sont débattues jusqu’à ce que 
tous tombent d’accord. C’est en tout cas le principe avancé. Quant au projet, 
il est de « réactiver » le programme du CNR (Conseil National de la Résistance) 
qui, au sortir de la Seconde guerre mondiale, avait permis d’initier des 
actions concrètes au bénéfice des Français, dans un esprit de liberté et de 
solidarité.

Michel DUBOIS, membre du RCR69, fait le point sur cette démarche se voulant 
«hyper démocratique» :

Fort du témoignage reçu de l’ancien porte-parole des Lip, les sympathisants 
du RCR (Réseau Citoyens Résistants) ne veulent pas brûler les étapes. A leur 
rythme, ils espèrent développer une certaine influence, sans visée électorale 
immédiate.

Leur volonté est plutôt pédagogique : « éveiller les gens et leur faire 
comprendre que c’est à eux de se saisir des problèmes » précise Michel DUBOIS. 

D’ailleurs, le RCR vient de publier un livre d’entretiens avec Charles PIAGET 
intitulé « la force du collectif », aux éditions Libertalia.

Myriam BENOIT

Tags: autogestion, Charles Piaget, CNR, Conseil National de la Résistance, 
la force du collectif, Lip, RCR, Réseau Citoyens Résistants, syndicalisme, 
Thorens-Glières
