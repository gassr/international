
.. index::
   pair: Bernard Friot; Vidéo Glières

.. _table_ronde_rapports_de_force_dans_lentreprise_2012:

====================================================================
Table ronde Rapports de Force dans l'Entreprise Thorens-Glières 2012
====================================================================


.. contents::
   :depth: 3

Vidéos
======

.. seealso::

   - http://www.cnt-f.org/video/videos/52-interpro-retraites-salaires-secu-rtt/373-forum-des-resistances-paroles-de-resistance-thorens-glieres-mai-2012
   - http://www.librinfo74.fr/2012/06/le-rassemblement-citoyen-des-glieres%C2%A0-laboratoire-d%E2%80%99un-projet-de-societe-pour-le-21eme-siecle/


Discours de Bernard Friot
=========================


.. toctree::
   :maxdepth: 3

   sortir_du_capitalisme_bernard_friot

Interview de Charles Piaget
===========================

.. toctree::
   :maxdepth: 3

   autogestion_charles_piaget


Discours de Gérard Filoche
==========================

.. seealso::   :ref:`discours_gerard_filoche_26_mai_2012`
