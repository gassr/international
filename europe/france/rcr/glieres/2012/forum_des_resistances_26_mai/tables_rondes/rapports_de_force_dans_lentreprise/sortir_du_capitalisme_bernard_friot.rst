

=====================
Sortir du capitalisme
=====================

.. seealso::

   - :ref:`discours_bernard_friot_26_mai_2012`
   - http://www.cnt-f.org/video/videos/52-interpro-retraites-salaires-secu-rtt/373-forum-des-resistances-paroles-de-resistance-thorens-glieres-mai-2012



Vidéo de Bernard Friot
======================

::

    Sujet:  Re: Vidéo de Bernard Friot à Thorens-Glières
    Date :  Thu, 14 Jun 2012 23:48:35 +0200
    De :    Nicolas Schaller <nicolas.schaller@gmail.com>
    Pour :  noamsw@pm.me
    Copie à :       coordinateur@reseau-salariat.info



Merci Patrick et merci au secteur vidéo de la CNT d'avoir fait passé le
message si promptement.

Les prises que tu as faites aux Glières sont très bonnes (la prise de
son et l'ambiance des gens qui rigolent). De ce que j'ai pu voir via les
liens que tu as fourni, elles me semblent toutefois écourtées.

Si c'est effectivement le cas, je suis intéressé pour voir le reste (ou
entendre, s'il n'y a pas de vidéo) dans l'idée de faire un autre montage
(à imaginer : illustration d'article ou autre).

Bonne soirée et à plus tard,

Nicolas

::


   Je m'appelle Patrick, je suis un militant de la CNT et le secteur
   vidéo de la CNT m'a demandé de vous contacter. Je suis évidemment
   tout à fait d'accord pour que vous exploitiez les images et photos
   situés ici:
   http://www.cnt-f.org/video/videos/52-interpro-retraites-salaires-secu-rtt/373-forum-des-resistances-paroles-de-resistance-thorens-glieres-mai-2012

   J'en profite pour remercier mes camarades d secteur vidéo de la CNT
   qui ont fait le montage et bien sûr un grand merci à Bernard Friot
   que je n'avais encore jamais vu en conférence.

   De quoi avez-vous besoin ?

   J'ai mis des morceaux de video ici: http://www.youtube.com/user/assr38
   et des images du plateau des Glières ici :
   https://picasaweb.google.com/100193447668627219452/Glieres_2012_26_mai?authkey=Gv1sRgCMqC_9m-7KS2LA#

   Mon adresse courriel est: noamsw@pm.me
   <mailto:noamsw@pm.me>

   Amitiés.
