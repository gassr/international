
.. index::
   pair: Glières; 2011
   pair: Paroles; Glières 2011
   pair: Programme; Glières 2011


.. _resistance_glieres_2011:

===============================
Résistance Glières 2011
===============================

.. seealso::

   - http://reseau-citoyens-resistants.fr/
   - http://www.citoyens-resistants.fr/spip.php?article106

.. contents::
   :depth: 3


Paroles et vidéos Glières 2011
==============================

::

	Bravo et merci, Grâce à ces vidéos, j’ai pu réaliser l’ambiance de cette journée 
	à laquelle je n’ai pu participer cause maladie. La haute tenue et la présence 
	de tous les participants. malgré les conditions météo, défavorable, m’ont touché. 
	J’ai la conscience de faire parti de cette famille de résistant, qui ensemble, 
	allons écraser la bête et retrouver espoir en la vie. Tout à été dit dans cette 
	journée encore une fois merci Amitiés.


.. seealso::

   - http://www.citoyens-resistants.fr/spip.php?article106
   - http://www.citoyens-resistants.fr/spip.php?article195 (vidéos)




