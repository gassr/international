
.. index::
   pair: Videos; Résistance
   pair: Audio; Glières (2013)
   pair: Glières; 2013

.. _videos_resistance_glieres_2013:

==================================
Vidéos Résistance Glières 2013
==================================

.. seealso::

   - http://www.librinfo74.fr/2013/05/des-debats-participatifs-a-thorens-a-la-recherche-dengagements-citoyens/
   - http://www.citoyens-resistants.fr/spip.php?article273
   
   
Audio
=====

.. seealso::

   - http://www.citoyens-resistants.fr/spip.php?article273

::

    Citoyen.2008 <citoyen.2008@yahoo.fr> via rezo.net 
    répondre à:  "Citoyen.2008" <citoyen.2008@yahoo.fr>
    à:   "Crha-diffusion@rezo.net" <Crha-diffusion@rezo.net>
    date:    6 juin 2013 22:42
    objet:   [CRHA-diffusion] Vidéos
    liste de diffusion:  crha-diffusion.rezo.net 
    
    

Bonsoir toutes et tous
 
Sur le site de CRHA se trouve les vidéos des interventions prisent lors 
de Paroles de Résistance sur le plateau des Glières le dimanche 
18 mai 2013.
 
voici le lien : http://www.citoyens-resistants.fr/spip.php?article273
 
Cordiales salutations
 
CRHA    
