

============================================================
Programme du dimanche 19 mai 2013 à 11h Plateau des GliÈres
============================================================

- René Vauthier, ancien résistant et cinéaste
- Julien Lauprêtre, ancien résistant, Président du Secours Populaire
- Geneviève Coiffard Grosdoy militante d’Attac
  et Marcel Thebault, agriculteur, Notre Dame des Landes
- Olivier Leberquier Fralib CGT
- Michel Warschawski, journaliste et militant pacifique israëlien
- Robert Créange, secrétaire général de la FNDIRP


Projection de films (Cinéma Le Parnal)
======================================

16h0-18h0 Acier trompé de Denis Robert 
--------------------------------------

17h30-19h30 Notre monde de Thomas Lacoste
-------------------------------------------










