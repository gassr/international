

.. index::
   pair: Glières; 18 mai 2013

.. _programme_18mai_2013:

================================================
Programme du Samedi 18 mai 2013 Thorens-Glières
================================================

.. contents::
   :depth: 3

Conférences
===========

14h-16h30 Construire un nouveau CNR en intégrant une dimension écologique
-------------------------------------------------------------------------

animée par Thierry Brun, journaliste à Politis
Avec:

- Hervé Kempf, journaliste et écrivain
- Corine Morel Darleux, secrétaire national à l’écosocialisme au
  Front de gauche
- Jérôme Gleizes militant écologiste, EELV.


.. _lutte_contre_fachos_18_mai_2013:

17h-19h30 Lutter contre la montée de l’extrême droite notamment au sein du monde ouvrier
-----------------------------------------------------------------------------------------

Animée par Thierry Leclere, journaliste.

Avec:

- Alexis Corbière, écrivain en charge de la lutte contre l’extrême droite
  au PG (Parti de Gauche)
- Annie Lacroix Riz, historienne
- Jean Paul Ravaux, président de VISA (Vigilances initiatives Syndicales
  Antifascistes)


Projection de films salle Tom Morel
===================================

14h-16h30 Les jours heureux de Gilles Perret
---------------------------------------------

Puis débat avec Léon Landini ancien résistant
Suivie de:

17h-19h30 Forum ouvert "un CNR aujourd’hui ?"
----------------------------------------------

de François Ruffin journaliste journal Fakir.


Projection de films au cinema le parnal
========================================

10h-12h30 Les jours heureux de Gilles Perret
---------------------------------------------


13h30-15h30 Le grand retournement de Gérard Mordillat
-----------------------------------------------------

15h30-17h30 FTP M.O.I
---------------------

Suivie d’une intervention des réalisateurs,
Laurence Karsznia et Mourad Laffitte.

17h30-19h30 Notre Dame des landes Opération Astérix
----------------------------------------------------

suivie d’un débat avec Geneviève Coiffard Grosdoy et Marcel Thebault


Les débats participatifs organisés par le RCR (Réseau Citoyens Résistants)
===========================================================================


14h-16h30 Autour des thèmes du programme du CNR
------------------------------------------------

- économie,
- santé,
- éducation,
- droit de l’homme,
- presse
- médias


Autour des thèmes non mentionnés dans le programme du CNR
----------------------------------------------------------

- droit des femmes,
- droit des étrangers,
- urbanisme,
- droit de la terre et des paysans


Café de la construction citoyenne à la MJC
-------------------------------------------

Réflexion autour de la rédaction d’un projet de société pour le XXIème
siècle

21h Soirée musicale
====================

Les Maris d’Thérèse et Les chiens d’talus, salle Tom Morel
-----------------------------------------------------------


21h Spectacle de théâtre
========================

Malgré la peur Cie traction Avant, cinéma Le Parnal
----------------------------------------------------
