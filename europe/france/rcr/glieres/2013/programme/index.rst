

=================================================
Programmes des Samedi 18 et dimanche 19 mai 2013
=================================================

.. seealso::

   - http://www.librinfo74.fr/2013/05/rassemblement-des-glieres-le-gouvernement-socialiste-sur-la-sellette/
   - http://www.citoyens-resistants.fr/spip.php?article266



Bonjour
 
A 36 heures du Forum des Résistances et de Paroles de Résistance voici 
la dernière version du programme: http://www.citoyens-resistants.fr/spip.php?article266
 
Nous sommes à la recherche de quelques hébergements de dernière minute, 
les personnes qui sollicitent un hébergement militant sont sans conteste 
comme nous, comme vous,  sympas et responsables.

C'est souvent l'occasion de belles rencontres et permet de créer du lien 
avec des personnes venant d'autres départements et qui luttent dans de 
petites associations originales.

Vous pouvez faire vos offres sur le site à la rubrique:
http://www.citoyens-resistants.fr/spip.php?article263
 
cordiales salutations
 
et à tout bientôt
 
CRHA  


.. toctree::
   :maxdepth: 3
   
   samedi_18_mai_2013
   dimanche_19_mai_2013




