
.. index::
   pair: Glières; 2013


.. _resistance_glieres_2013:
.. _glieres_2013:

===============================
Résistance Glières 2013
===============================

.. seealso::

   - http://reseau-citoyens-resistants.fr/
   - http://www.librinfo74.fr/2013/05/rassemblement-des-glieres-le-gouvernement-socialiste-sur-la-sellette/

.. contents::
   :depth: 3


Introduction
============


Cette année marque le 70ème anniversaire du Conseil National de la 
Résistance qui s’est réuni pour la première fois à Paris, dans la 
clandestinité, le 27 mai 1943.

Ce sont ces hommes qui ont défini les valeurs et les bases d’une société 
à construire après la guerre, en rédigeant le Programme intitulé 
« les jours heureux ».

Ce fut un moment déterminant dans la mise en place de l’état social tel 
que nous le connaissons aujourd’hui.

N’en doutons pas, les hommages aux héros de ce Conseil, à ses morts, vont 
se succéder en mentionnant les faits d’armes mais en occultant la pensée 
politique.

Force est de constater que, même avec un gouvernement de « gauche », 
l’application de ses idées est loin d’être une réalité.

Ce programme mettait systématiquement l’intérêt général avant les 
intérêts particuliers.

Ce programme régulait la finance et contraignait le capitalisme.

Ce programme établissait l’équité sociale et renforçait le droit du travail.

Une nouvelle république devait voir le jour.

Alors que les responsables politiques restent les bras ballants devant 
le diktat des marchés financiers, des agences de notation ou des directives 
libérales européennes qui s’acharnent à détruire l’état social, il y a 
urgence à retrouver toute la force de ce projet, le compléter pour les 
années à venir et réfléchir ensemble à la construction d’un monde meilleur.

C’est ce que nous vous proposons le samedi 18 mai à Thorens-Glières lors 
du « Forum des Résistances » et le dimanche 19 mai au Plateau des Glières 
lors des « Paroles de Résistances ».

Débats participatifs, films, conférences, prises de paroles solennelles, 
contribueront à cette démarche.

La culture, espace de résistance, aura aussi sa place avec du théâtre et 
des concerts.

Tous ensemble ne commémorons pas mais célébrons le CNR en faisant vivre 
ses idées et son utopie les 18 et 19 mai aux Glières !


Articles
==============

.. seealso::

   - http://www.fsd74.org
   - http://www.librinfo74.fr


.. toctree::
   :maxdepth: 3
   
   articles/index
   

Courriels
==============

.. toctree::
   :maxdepth: 3
   
   courriels/index
   

Programme des 18 et 19 mai 2013
================================


.. toctree::
   :maxdepth: 3
   
   programme/index


Interventions sur le plateau des Glières 2013
=============================================


.. toctree::
   :maxdepth: 3
   
   interventions/index
   
   

Comptes-rendus 2013
====================

.. toctree::
   :maxdepth: 3
   
   comptes_rendus/index


Photos, Vidéos
==============

.. toctree::
   :maxdepth: 3
   
   videos/index
   




