
.. index::
   pair: Michel Warschawski; 2013 (Glières)


.. _intervention_warschawski_2013:

=====================================================
Intervention de Michel Warschawski aux Glières 2013
=====================================================

.. seealso::

   - http://www.citoyens-resistants.fr/spip.php?article273
   - :ref:`michel_warschawski`
   - http://www.dailymotion.com/video/x10moum_michel-warschawski-au-plateau-des-glieres-en-2013_news#.UbBo882n6hk
