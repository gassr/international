


.. _rcr_6_janvier_2013:

==================================
Demande d'aide 6 janvier 2013
==================================

.. seealso::

   - http://reseau-citoyens-resistants.fr/

::

    De : Citoyen.2008 <citoyen.2008@yahoo.fr>
    Date : 6 janvier 2013 18:15
    Objet : [CRHA-diffusion] courrier
    À : CRHA diffusion <crha-diffusion@rezo.net>



Bonjour

En mai 2012, vous avez été nombreux- ses à inciter CRHA à continuer à 
organiser les témoignages du dimanche, Paroles de Résistances  ainsi 
que le forum des Résistances du samedi.

D’aucuns-unes nous ont dit que le rassemblement ne nous appartenait 
plus vraiment, qu’il était un « bien commun » et que nous avions le 
devoir impérieux de continuer.

Nous en sommes d’accord, cependant nous manquons de force surtout pour 
la journée du samedi.

Vous l’aurez compris, nous avons besoin de vous.

Cela va des démarches en direction des invité es (billetterie des 
déplacements/hébergements/voiturages/ repas/ souci du confort et 
respect des horaires etc..).

Aussi de bras vigoureux pour monter les chapiteaux, aller chercher 
podiums/ bancs/ mettre en place  et ce pour le montage le vendredi et 
le démontage le lundi.

Bien sûr le samedi il faut de nombreuses personnes pour toutes les tâches 
et permettre à chacun-une de pouvoir  assister à une conférence/débat/ 
film ou autre.

La tenue de la buvette/sandwicherie consomme bien du monde.

Nous avons aussi besoin de personnes sachant monter et gérer des sonos, 
d’autres pour la vente de tickets.

D’autres, plus en amont, qui soient des « pros » dans le montage de 
programmes, affiches, dans la mise en page, des as du Doodle pour 
établir des tableaux

Nous nous devons d’être bien plus performant en matière de communication 
locale.

Un véritable accueil devra être mis en place pour renseigner et orienter 
les participants.

Pour le dimanche si les équipes habituelles sont là, le parcage sera 
parfaitement fait comme d’habitude.

En bref nous avons bien besoin de vous,  votre contribution doit nous 
permettre de mener à bien une nouvelle édition qui sera un peu différente 
des autres années, vous recevrez un autre courrier en ce sens.

N’hésitez pas, d’ores et déjà, à nous donner vos préférences et 
disponibilités et nous faire part de vos réflexions sur ces deux journées.

Le rassemblement aura lieu les 18/19 mai 2013.

Cette année 2013, ce sera le 70ième anniversaire de la création du 
Conseil National de la Résistance.

Citoyen.2008@yahoo.fr
Citoyens Résistants d’Hier et d’Aujourd’hui    
