

===================================================================
[CRHA-diffusion] Programme Rassemblement des Glières 18-19 Mai 2013
===================================================================

::

    Citoyen.2008 <citoyen.2008@yahoo.fr> via rezo.net 
    répondre à:  "Citoyen.2008" <citoyen.2008@yahoo.fr>
    à:   CRHA diffusion <crha-diffusion@rezo.net>
    date:    23 avril 2013 21:14
    objet:   [CRHA-diffusion] Programme Rassemblement des Glières 18-19 Mai 2013
    liste de diffusion:  crha-diffusion.rezo.net 

Bonjour

Voici ci-joint le programme quasi définitif du prochain Rassemblement 
des 18 et 19 mai 2013.

- Samedi, le Forum des Résistances à Thorens-Glières
- Dimanche, les Paroles de Résistances au Plateau des Glières.

Nous vous rappellons que vous pouvez vous rendre sur le site de CRHA pour 
les demandes et propositions d'hébergement ainsi que pour le co voiturage

- http://www.citoyens-resistants.fr/

Vous y trouverez aussi d'autres informations pratiques.

A lire ci-dessous l'édito du site 
=================================

Cette année marque le 70ème anniversaire du Conseil National de la 
Résistance qui s’est réunit pour la première fois à Paris, dans la 
clandestinité, le 27 mai 1943.

Ce sont ces hommes qui ont défini les valeurs et les bases d’une société 
à construire après la guerre, en rédigeant le Programme intitulé 
«les jours heureux».

Ce fut un moment déterminant dans la mise en place de l’état social tel 
que nous le connaissons aujourd’hui.
N’en doutons pas, les hommages aux héros de ce Conseil, à ses morts, vont 
se succéder en mentionnant les faits d’armes mais en occultant la pensée 
politique.

Force est de constater que, même avec un gouvernement de « gauche », 
l’application de ses idées est loin d’être une réalité.
Ce programme mettait systématiquement l’intérêt général avant les intérêts 
particuliers.
Ce programme régulait la finance et contraignait le capitalisme.
Ce programme établissait l’équité sociale et renforçait le droit du travail.
Une nouvelle république devait voir le jour.

Alors que les responsables politiques restent les bras ballants devant 
le diktat des marchés financiers, des agences de notation ou des 
directives libérales européennes qui s’acharnent à détruire l’état social, 
il y a urgence à retrouver toute la force de ce projet, le compléter pour 
les années à venir et réfléchir ensemble à la construction d’un monde 
meilleur.

C’est ce que nous vous proposons le samedi 18 mai à Thorens-Glières 
lors du « Forum des Résistances » et le dimanche 19 mai au Plateau des 
Glières lors des « Paroles de Résistances ».

Débats participatifs, films, conférences, prises de paroles solennelles, 
contribueront à cette démarche.

La culture, espace de résistance, aura aussi sa place avec du théâtre et 
des concerts.
Tous ensemble ne commémorons pas mais célébrons le CNR en faisant vivre 
ses idées et son utopie les 18 et 19 mai aux Glières !

Amicalement

CRHA (Citoyens Résistants d'Hier et d'Aujourd'hui)


Appel au bénévolat
==================

- http://www.citoyens-resistants.fr/spip.php?article152

Programme
=========

- http://www.citoyens-resistants.fr/IMG/pdf/programmes_du_samedi_18_mai_1.pdf

