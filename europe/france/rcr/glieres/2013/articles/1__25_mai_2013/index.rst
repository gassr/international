


.. _glieres_25_mai_2013:

=======================================
Articles Résistance Glières 25 mai 2013
=======================================


.. seealso::

   - http://www.librinfo74.fr/2013/05/citoyens-resistants-dhier-et-daujourdhui-repond-au-general-bachelet-president-de-lassociation-des-glieres/
   - http://www.librinfo74.fr/2013/05/edito-le-plateau-des-glieres-nest-pas-un-sanctuaire-il-appartient-a-tous-les-citoyens/
   
 
   
**Le plateau des Glières n’est pas un sanctuaire, il appartient à tous les 
citoyens**

Le Général Bachelet s’est insurgé contre le rassemblement citoyen sur le 
plateau des Glières, considérant que ce haut lieu de la résistance a été 
profané par des citoyens venus honorer les anciens résistants et montrer 
que la résistance se conjugue au présent.

Le Général Bachelet, garant de le sanctuarisation du Plateau, conjugue 
la résistance au passé alors qu’à l’époque la poignée de citoyens résistants 
la conjuguait au présent.

Pendant l’occupation, ces résistants avaient choisi leur camp, celui des 
combattants contre le nazisme pour construire une société juste et 
fraternelle, garante de la paix et de la solidarité entre tous les peuples.

N’oublions pas que la majorité des français de l’époque était pétainistes, 
celle que l’on appelle « silencieuse » et qui « se lève tôt ».

Ces pétainistes de l’époque, nous les retrouvons majoritairement dans 
l’électorat convoité par les forces conservatrices de droite et 
d’extrêmes droite, liées aux ultras du Medef et à la finance.

Aujourd’hui, où sont les défenseurs de cette société juste et fraternelle ?

Certains étaient réunis au Plateau des Glières, en ce dimanche froid et 
neigeux.
   
   
