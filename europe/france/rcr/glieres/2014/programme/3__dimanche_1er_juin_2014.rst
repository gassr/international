
.. index::
   pair: Glières; 1er juin 2014


.. _glieres_1er_juin_2014:

==========================================
Résistance Glières dimanche 1er juin 2014
==========================================

.. contents::
   :depth: 3

10 h 30 sur le plateau de Glières 
==================================

PAROLES DE RESISTANCES, suivi d'un pique-nique « citoyen » avec, à la tribune, 
(par ordre alphabétique) :

Christian GARRETTE
------------------

Agent du Centre de Distribution de la Poste, et militant syndical CGT,
poursuivi pour ses activités syndicales.

Massa KONÉ
-----------

.. seealso::

   - http://www.no-vox.org/

paysan sans terre du Mali, association `No Vox`_

.. _`No Vox`:  http://www.no-vox.org/

Julien LAUPRETRE 
-----------------

.. seealso::

   - http://fr.wikipedia.org/wiki/Affiche_rouge
   - http://fr.wikipedia.org/wiki/Affiche_rouge#Le_r.C3.A9seau_Manouchian
   - http://fr.wikipedia.org/wiki/Julien_Laupr%C3%AAtre

Le réseau Manouchian était constitué de 23 résistants communistes, dont 20 
étrangers, des Espagnols rescapés de Franco, enfermés dans les camps français 
des Pyrénées, des Italiens résistant au fascisme, Arméniens, Juifs surtout 
échappés à la rafle du Vel'd'Hiv de juillet 1942 et dirigé par un Arménien, 
Missak Manouchian. 

Il faisait partie des Francs-tireurs et partisans - Main-d'œuvre immigrée.

Ils sont arrêtés en novembre 1943 et jugés en février 1944, condamnés à mort 
le 21 février 1944.Les 22 hommes sont fusillés le même jour au fort du 
Mont-Valérien. 
La plupart d'entre eux sont enterrés dans le cimetière d'Ivry-sur-Seine, dans 
le Val-de-Marne, où une stèle a été érigée en leur mémoire. Olga Bancic, la 
seule femme du groupe, est décapitée le 10 mai de la même année à Stuttgart, 
en application du manuel de droit criminel de la Wehrmacht5 interdisant alors 
de fusiller les femmes.


Ancien résistant et président du Secours Populaire Français.
Il est arrêté le 20 novembre 1943 pour faits de résistance et incarcéré à la 
Prison de la Santé à Paris. Il y côtoie Missak Manouchian, le chef du groupe 
de l’Affiche rouge, qui lui dit : « Moi je suis foutu, je vais être fusillé, 
mais toi il faut que tu fasses quelque chose d’utile et que tu rendes la société 
moins injuste... ».

Des paroles qui l’ont marqué à jamais et qui vont conditionner son engagement 
futur.




Laurent PINATEL
---------------

.. seealso::

   - https://fr.wikipedia.org/wiki/Laurent_Pinatel
   - https://twitter.com/LaurentPinatel
   - https://twitter.com/ConfPaysanne


Eleveur bovin, porte-parole de la Confédération Paysanne (sous réserve qu'il
soit libéré, il a été mis en garde à vue le 28/05 suite à son opposition aux 1000 vaches)


Jamal HWEIL 
------------

.. seealso::

   - http://www.counterpunch.org/2006/02/03/a-parliament-of-prisoners/
   
Palestinien né au camp de Jennine, très actif puis clandestin dans ce camp, arrêté
et emprisonné ...

Jamal Hweil: Held the #26 slot on the Fateh slate. Born in Jenin Refugee Camp 
in 1970, Hweil is the twin brother of Najib Hweil, who was one of the founders 
of the Black Panthers in Jenin, Fateh’s first paramilitary in the 1987 Intifada. 
(Najib was eventually assassinated by an Israeli death squad in March 1991.) 

Jamal graduated from Jordan University in 1995 from the college of Economics 
and Social Sciences, and was a manager in an economic body within the Legislative 
Council. 
He is the Secretary General of the Youth and Social Services Center in Jenin 
Refugee Camp, and lived for years as a wanted activist at the beginning of this 
Intifada. Hweil played an active role in organizing the defense of Jenin Refugee 
camp in April 2002 which resulted in 23 Israeli soldiers killed, and the 
destruction of over 400 Palestinian refugee homes. He was arrested in that invasion.
 
 

Cécile ROL-TANGUY
-----------------

.. seealso::

   - http://www.toutelhistoire.com/guide-programmes/Portrait-Cecile-Rol-Tanguy,-une-combattante-pour-la-liberte-674.aspx
   - http://www.acer-aver.fr/index.php/actions/evenements/299-cle-rol-tanguy-promue-u-grade-de-commandeur-de-la-lon-dhonneur

Résistante.


Germain SARHY
-------------

communauté d’Emmaüs de Lescar-Pau

Jean Jacques TANQUEREL 
-----------------------

Médecin et auteur de «Le serment d'Hypocrite Secret médical, le grand naufrage »


Après-midi, au cinéma LE PARNAL 
=================================


15h : « Cause commune » de Sophie AVERTY
--------------------------------------------

.. seealso::

   - http://www.film-documentaire.fr/Cause_commune.html,film,38148

Débat animé par Jean-Luc et Madeleine
GUYBRETEAU, membre de l'association ROMSI à INDRE

17h : ``5 Caméras Brisées`` de Emad BURNAT et Guy DAVIDI
----------------------------------------------------------

.. seealso::

   - http://fr.wikipedia.org/wiki/5_cam%C3%A9ras_bris%C3%A9es
   - http://www.kinolorber.com/5brokencameras/
   - https://twitter.com/5brokencams
     

en présence de Jamal HWEIL
