
.. index::
   pair: Programme; Glières 31 mai 2014


.. _glieres_31_mai_2014:

======================================
Résistance Glières samedi 31 mai 2014
======================================

.. contents::
   :depth: 3

Conférences débats
===================

10 h Grand chapiteau : « Accaparement des terres: Organisation mondiale de violation des droits humains »
==========================================================================================================


Massa KONÉ, paysan sans terre du Mali, association No Vox
----------------------------------------------------------


Laurent PINATEL, porte-parole de la Confédération Paysanne
-----------------------------------------------------------

Débat animé par Fanny METRAT (paysanne, porte-parole de la Confédération Paysanne de
l'Ardèche)

10 h salle Tom MOREL : « Le Grand Marché Transatlantique (TAFTA ou TTIP) »
===========================================================================

Conférence de Raoul-Marc JENNAR, politologue et membre du Conseil scientifique 
d’ATTAC et de la Ligue des Droits de l’Homme
Débat animé par Jacques CAMBON, membre d’ATTAC, Animateur du Collectif Stop GMT 74

14 h Grand chapiteau : « Les étrangers : résistance et résidence »
===================================================================

Conférence de Philippe HANUS, historien et écrivain, avec Xavier POUSSET d'ARTAG 
de Lyon (Association Régionale des Tsiganes et de leurs Amis Gadjé), 
Jacques PICHON, de la CIMADE (association de solidarité avec les migrants, 
les réfugiés et les demandeurs d'asile), Jeanne GANTIER, de 
ROMSI (Rencontre Ouverture Métissage Solidarité d'Indre).

P. HANUS animera les débats.

14 h salle Tom MOREL : « Faire reculer le coût du capital pour une nouvelle expansion sociale »
================================================================================================

Conférence de Frédéric BOCCARA, économiste atterré, et débat avec la salle.

Débat animé par Patrick PIRO, de POLITIS

2 Débats participatifs
=======================

14 – 16 h MJC : « L'image comme moyen de résistance »
------------------------------------------------------

avec Gérard FUMEX, rédacteur en chef de « Le Journal » et journaliste à 
Librinfo74, Florent LABRE, cinéaste et directeur de LABEL VIE D'ANGE et 
Michel CARÉ, directeur du cinéma La Turbine.

Débat animé par Yann CAMARET (RCR 16)

14 – 16 h Petit chapiteau : « Les Jours Heureux à Rennes : 4 000 propositions
------------------------------------------------------------------------------

pour résister » avec François ASTOLFI, créateur et fédérateur du collectif de Rennes.


16 h-16 h 30 : départ de la marche de soutien vers Notre-Dame des Landes
=========================================================================


Un Grand Forum en deux parties, "Pour aller vers de nouveaux Jours heureux" 
============================================================================

16 h 30 : Forum animé par François RUFFIN, journaliste 
------------------------------------------------------

Des responsables nationaux de mouvements, partis et syndicats s'exprimeront 
comme s'ils étaient ministres d'un gouvernement issu d'un CNR 2.0. et qu'ils 
venaient proposer des projets de lois allant dans le sens des « Jours Heureux
pour demain». 

Ils s'exprimeront sur des thèmes imposés par les citoyens résistants.

A ce jour, participeront au Grand Forum (par ordre alphabétique):

- Paul ARIES_, « objecteur de croissance », rédacteur en chef des `Z'indigné(e)s`_
- Pauline ARRIGHI, porte parole nationale de `Osez le Féminisme`_
- François AUGUSTE, PCF
- Julien BAYOU, Europe Ecologie Les Verts
- Vincent BORDENAVE, UNEF
- Max BRUNET, ATTAC_
- Eric COQUEREL, FDG_ / `Parti de Gauche`_
- Gérard FILOCHE, PS
- Julien GONTHIER, membre du secrétariat national de l’Union syndicale SOLIDAIRES
- Emmanuel GUICHARDAZ, FSU
- Pierre LARROUTUROU, Nouvelle Donne
- Antoine MANESSIS, PRCF
- Jacques NIKONOFF, `M'PEP`_
- `Laurent PINATEL`_ , porte-parole de la `Confédération Paysanne`_
- `Serge PORTELLI`_ , syndicat de la magistrature
- Pierre ZARKA, de Ensemble_

.. _ARIES:  http://paularies.canalblog.com/
.. _`Z'indigné(e)s`:  http://www.les-indignes-revue.fr/
.. _`Osez le Féminisme`: http://www.osezlefeminisme.fr/
.. _ATTAC:  http://www.attac.org/fr
.. _FDG:  http://www.placeaupeuple.fr/
.. _`Parti de Gauche`: http://www.lepartidegauche.fr/
.. _`M'PEP`:  http://www.m-pep.org/
.. _`Confédération Paysanne`: http://www.confederationpaysanne.fr/  
.. _`Serge PORTELLI`: http://chroniquedelhumaniteordinaire.blogs.nouvelobs.com/
.. _Ensemble:  https://www.ensemble-fdg.org/
.. _`Laurent PINATEL`:  _https://fr.wikipedia.org/wiki/Laurent_Pinatel


16 h 30 en parallèle, et sous un autre chapiteau 
-------------------------------------------------

Les « députés » (= les membres de l'assistance) pourront interpeller, en leur 
posant des questions, les « ministres » (cités ci-dessus) qui ne seront pas 
occupés à présenter et défendre leur « projet de loi CNR 2,0 ». 

Ceux-ci répondront aux questions posées et un débat pourra s'engager.

Forum animé par Raymond MACHEREL


5 films au cinéma LE PARNAL à THORENS
=====================================

10h00 « Les Jours Heureux » 
------------------------------

.. seealso::

   - https://twitter.com/Gilles_Perret
   - http://lesjoursheureux.net/

En présence du réalisateur Gilles PERRET et de Léon LANDINI,
Résistant FTP-MOI et protagoniste du film

"Ils ont eu la graisse, ils n'auront pas la peau" , de Jean-Baptiste PELLERIN 12 h 30 Documentaire sur Raymond GUREME 
----------------------------------------------------------------------------------------------------------------------

.. seealso::

   - http://centre-medem.org/spip.php?article587

Forain français interné par les autorités françaises avec les tziganes en 1940. 
En présence de Raymond GUREME et d'Isabelle LIGNER

14h00 « Se battre » de Jean-Pierre DURET et Andrea SANTANA
-------------------------------------------------------------- 

.. seealso::

   - http://www.sebattre.com/projections
   - https://www.facebook.com/sebattre.lefilm
   - http://www.ldh-france.org/La-LDH-soutient-le-film,5300.html

Et en présence de Germain SARHY (Emmaüs Lescar Pau) et Julien LAUPRETRE 
(Secours Populaire Français)

16h30 « Ondes, sciences et manigances » De Jean HECHES et Nancy de MERITENS
------------------------------------------------------------------------------

.. seealso::

   - http://www.priartem.fr/Projections-du-film-ONDES-SCIENCE.html
   - http://ondesscienceetmanigances.fr/
   
Sélection du Parnal


18h30 « My sweet pepper land » de Hiner SALEEM
-------------------------------------------------

.. seealso::

   - http://fr.wikipedia.org/wiki/My_Sweet_Pepper_Land
   - http://www.rohfilm.de/



20h30 « 5 Caméras Brisées » de Emad BURNAT et Guy DAVIDI
-----------------------------------------------------------

.. seealso::

   - http://fr.wikipedia.org/wiki/5_cam%C3%A9ras_bris%C3%A9es
   - http://www.kinolorber.com/5brokencameras/
   - https://twitter.com/5brokencams
   
   

En présence de Jamal HWEIL


