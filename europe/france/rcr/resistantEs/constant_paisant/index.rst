
.. index::
   pair: MilitantEs; Constant Paisant
   pair: Constant ; Paisant


    
.. _constant_paisant:
   
===============================
Constant Paisant 
===============================

.. seealso::

   - http://reseau-citoyens-resistants.fr/
   - http://www.citoyens-resistants.fr/spip.php?article26


.. figure:: constant_paisant.jpg

   Contant Paisant
   
   
.. contents::
   :depth: 3
   
Dimanche 13 mai 2007
====================
   
Un des résistants signataires de l’appel, Constant Paisant , nous livre 
aujourd’hui ses réflexions sur cette très belle journée :

"J’ai été surpris par l’affluence. On m’avait posé la question deux jours avant 
et j’avais répondu que quelques centaines de personnes, ce serait déjà beaucoup. 
Mais là, une telle affluence,des gens de partout, alors que ça n’avait même pas 
été annoncé dans la presse locale, c’est extraordinaire !" 

Les délais très courts d’organisation, le bouche-à-oreille, la diffusion de 
l’information "par la bande" le laissent songeur :

"J’ai retrouvé des tas d’amis que je ne m’attendais pas à voir ; Je ne sais 
pas comment ils ont su, mais enfin, ils étaient là, c’était vraiment très 
agréable pour moi de voir cette mobilisation tranquille et sympathique"

Constant, FTP aux Glières Sur le geste de Nicolas Sarkozy-candidat 
("et puis, comme ça, la veille des élections ! " ), cet ancien FTP du plateau
des Glières affirme simplement que " La résistance appartient à tous : elle 
est la mémoire nationale. Mais elle doit être préservée de tout coup médiatique 
et électoraliste".

Constant Paisant n’en fait pas un sujet à polémique. Il s’en tient simplement 
à la notion de dignité et de respect pour ses camarades tombés si jeunes autour 
de lui.

Article publié le mardi 15 mai 2007 sur le site du fsd74
