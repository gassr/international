
.. index::
   pair: MilitantEs; Stéphane Hessel
   pair: Stéphane Hessel ; Indignez-vous !


.. _stephane_hessel:

===================================
Stéphane Hessel (20 octobre 1917-
===================================

.. seealso::

   - http://fr.wikipedia.org/wiki/St%C3%A9phane_Hessel
   - http://reseau-citoyens-resistants.fr/
   - http://fr.wikipedia.org/wiki/Indignez-vous_!
   - :ref:`gilles_perret`
   - http://www.citoyens-resistants.fr/spip.php?article247


.. contents::
   :depth: 3


Introduction
============

Stéphane Frédéric Hessel, né le 20 octobre 1917 à Berlin, est un diplomate,
ambassadeur, résistant, écrivain et militant politique français.

Né allemand, d’origine juive du côté paternel, Stéphane Hessel arrive en France
à l’âge de 8 ans. Naturalisé français en 1937, normalien, il rejoint les forces
françaises libres en 1941 à Londres. Résistant, il est arrêté et déporté à
Buchenwald puis à Dora et ne doit la vie qu’à une substitution d’identité avec
un prisonnier mort du typhus et à son évasion.

Il entre au Quai d’Orsay en 1945 et fait une partie de sa carrière diplomatique
auprès des Nations unies où il assiste comme témoin privilégié à la constitution
de la charte des droits de l’homme et du citoyen. Homme de gauche et européen
convaincu, il est ami de Pierre Mendès France et Michel Rocard.

Stéphane Hessel est connu du grand public pour ses prises de position concernant
les droits de l’homme, le problème des « sans-papiers » et le conflit
israélo-palestinien ainsi que pour son manifeste Indignez-vous ! paru en 2010
et succès international.


Indignez-vous
=============

.. seealso:: :ref:`indignez_vous_rcr` 
