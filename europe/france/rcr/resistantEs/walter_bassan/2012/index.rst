
.. index::
   pair: Walter ; 2012

    
.. _walter_bassan_2012:
   
===============================
Walter Bassan 2012
===============================

.. seealso::

   - :ref:`walter_bassan`
   - http://www.citoyens-resistants.fr/spip.php?article247
   


.. contents::
   :depth: 3
   
Walter, une vie de résistances (avril 2012)
============================================
   
- http://www.neva-editions.fr/produit.php?prod=199&PHPSESSID=c63bd64fe4d386b2b875fbff28a7e4cd   

Discours du dimanche 27 mai 2012 au plateau des Glières 
========================================================

.. seealso:: http://www.citoyens-resistants.fr/spip.php?article247

Pour la Sixième fois, nous nous réunissons sur ce site, haut lieu de la Résistance.

Sur ce plateau, dans les premiers mois de 1944, se sont regroupés toutes les forces
militantes d’origines différentes mais toutes unies pour résister à l’oppression des
nazis et de leurs complices.

Ainsi ce Plateau des Glières, où sont honorés si souvent les faits d’armes des
combattants, porte aussi le symbole du retour de la République après la sinistre
séquence des quatre années d’occupation où la formule « travail, famille, patrie » 
avait remplacé la devise « Liberté, égalité, fraternité ».

**Nous devons rappeler que si mes camarades et moi-même nous nous sommes engagés
dans la Résistance, c’était non seulement pour la libération du pays mais aussi 
pour la mise en place d’une société plus juste et plus fraternelle.**

C’est dans cet esprit que le programme du Conseil National de la Résistance fut 
écrit et mis en œuvre à la libération. Nous lui devons encore aujourd’hui les 
grandes mesures sociales dont nous bénéficions quotidiennement.

**Avec l’association CRHA, au cours de ces 5 dernières années, nous pensons avoir
permis d’ouvrir les yeux à beaucoup et d’avoir réarmé les esprits par notre 
constance à parler du cœur du programme du CNR.**

Cela s’est concrétisé par le succès de nos rassemblements successifs en ce lieu. 
Ce travail de fond a contribué à la déstabilisation et au départ du précédent 
chef de l’état.
Ce chef de l’état dont les amis comme Denis Kessler, ex-numéro 2 du Medef, décrivait
fièrement la ligne de conduite à suivre en affirmant dès octobre 2007: « Il s’agit
maintenant de sortir de 1945 et de défaire méthodiquement le programme du Conseil
National de la Résistance ».

Après tant d’atteintes à l’état social, tant d’atteintes aux libertés, après 
toutes les manipulations des symboles historiques, le 6 mai 2012, une majorité 
de citoyens a élu un nouveau président de la République.

L’air devient plus respirable. Nous nous en réjouissons. 

Pour autant, rien n’est réglé. 

**Nous estimons qu’il faudra être ambitieux notamment pour lutter contre la dictature
internationale des marchés qui menacent les démocraties.**

Il faudra encore se battre pour garantir les valeurs fondamentales laïques,
démocratiques et sociales de la république car, comme le rappelaient les grandes
figures de la résistance en 2004 lors du 60ème anniversaire du Conseil National de la
Résistance::

	« le fascisme se nourrit toujours du racisme et de l'intolérance, qui eux-mêmes se
	nourrissent des injustices sociales ».
	
Le rassemblement des Glières exprime avec détermination cette vigilance citoyenne.

Soyez assuré que l’association « Citoyens résistants d’Hier et d’Aujourd’hui » 
et ses réseaux s’emploieront à contribuer à cette démarche pour les années 
à venir.


