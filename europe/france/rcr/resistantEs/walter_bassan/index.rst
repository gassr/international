
.. index::
   pair: MilitantEs; Walter Bassan
   pair: Walter ; Bassan
   pair: Walter ; Une vie de résistances

    
.. _walter_bassan:
   
===============================
Walter Bassan (5/11/1926-
===============================

.. seealso::

   - http://reseau-citoyens-resistants.fr/
   - :ref:`gilles_perret`


.. figure:: walter_bassan.jpg

   Walter Bassan


.. toctree::
   :maxdepth: 3
   
   2012/index
   2007/index
   
   
