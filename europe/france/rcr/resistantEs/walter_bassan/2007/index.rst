
.. index::
   pair: Walter ; 2007

    
.. _walter_bassan_2007:
   
===============================
Walter Bassan 2007
===============================

.. seealso::

   - :ref:`walter_bassan`
   - http://www.citoyens-resistants.fr/spip.php?article58
   - http://www.citoyens-resistants.fr/spip.php?article247
   - http://contreinfo.info/article.php3?id_article=993
   

.. contents::
   :depth: 3
   

Discours du dimanche 13 mai 2007 au plateau des Glières 
========================================================

Le 13 mai dernier s’est tenu sur le plateau des Glières un rassemblement citoyen, 
là même où Nicolas Sarkozy avait fait sa dernière apparition publique lors de la 
campagne électorale. 

Un millier de personnes se sont rassemblées pour dénoncer ce qu’elles ont 
appelé la « récupération médiatique » de ce haut-lieu de la Résistance française. 

Inteviewé à cette occasion,Walter Bassan, l’un des initiateurs de cette 
manifestation, ancien résistant et rescapé de Dachau, dénonce « l’usurpation » 
d’un symbole par le candidat qui « tourne le dos au programme de la résistance ». 


« [Il s’agit du] comportement d’un candidat - le président de la République 
n’a rien à voir dans l’affaire - c’est un candidat qui usurpe certaines choses, 
certaines idées, certains comportements pour “piquer” des voix, un point c’est tout.

Non seulement il usurpe des situations, mais il a un programme politique qui 
tourne le dos au programme de la résistance, au programme du Conseil National 
de la Résistance.

Rassemblement du 13 mai En plus, la cerise sur le gâteau, c’est que le 4 mai 
il est là pour honorer les résistants, parait-il, et le 8 on célèbre la victoire 
dont les résistants sont en partie participants, et lui, au lieu de venir se 
recueillir à l’Arc de Triomphe, il est sur son yacht.

Pour ceux qui connaissent bien les rescapés qui sont encore en vie, lorsque les 
médias ont passé les images de cette visite, il n’y avait aucun résistant à 
coté des Sénateurs, des Députés de la majorité.

Sa venue, je l’ai apprise le matin à 7 heures sur France Inter. »

Le Programme du Conseil National de la Résistance

Le CNR a été créé à l’initiative du Général De Gaulle, et rassemblait l’ensemble 
des mouvements de résistance, quelque soient leurs obédiences politiques. 
Son premier président fut Jean Moulin.

Longuement discuté dans la clandestinité, puis adopté à l’unanimité le 15 mars 
1944, le programme défini par le CNR a jeté les bases du « Modèle Social français ». 
Parmi les principales mesures, il prescrivait :

- la pleine liberté de pensée, de conscience et d’expression ; 
- la liberté de la  presse, son honneur et son indépendance à l’égard de l’État, 
  des puissances  d’argent et des influences étrangères
- l’instauration d’une véritable démocratie économique et sociale, impliquant 
  l’éviction des grandes féodalités économiques et financières de la direction 
  de l’économie le retour à la nation des grands moyens de production monopolisée, 
  fruits du travail commun, des sources d’énergie, des richesses du sous-sol, 
  des compagnies d’assurances et des grandes banques
- un plan complet de sécurité sociale, visant à assurer à tous les citoyens des 
  moyens d’existence, dans tous les cas où ils sont incapables de se le procurer 
  par le travail, avec gestion appartenant aux représentants des intéressés et de l’État

Dans les faits, son application a donné lieu aux mesures suivantes :

- En 1944 (dés la libération) : Vote des femmes,
- En 1945 : Création des Comités d’entreprise, Création de la Sécurité Sociale 
  Nationalisation des crédits, Accords de Bretton Woods (donnant FMI, Banque 
  mondiale, Gatt puis OMC)
- En 1946 : Semaine de 40 heures, Nationalisation du gaz et de l’électricité, 
  Augmentation de +18% des salaires.
- En 1947 : Création du SMIG, Prélèvement exceptionnel des hauts revenus.
