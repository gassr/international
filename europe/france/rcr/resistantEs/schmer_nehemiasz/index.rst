
.. index::
   pair: MilitantEs; Schmer Nehemiasz
   pair: Liberté ; Carmagnole
   pair: FTP; MOI

    
.. _schmer_nehemiasz:
   
===============================
Schmer Nehemiasz (1927-
===============================

.. seealso::

   - http://comite-pour-une-nouvelle-resistance.over-blog.com/article-plaidoyer-pour-une-legion-d-honneur-a-andre-schmer-juif-et-resistant-118727806.html



.. figure:: schmer_nehemiasz.jpg
   :align: center

.. contents::
   :depth: 3
   
Histoire
=========
   
Schmer Nehemiasz de son vrai nom, Étienne Dumon dans la résistance

 
De son vrai nom Schmer Nehemiasz, il est né le 18 mai 1927 et il n'a 
qu'un an lorsqu'il se réfugie avec sa famille en France en 1928 pour 
fuir l'antisémitisme en Pologne. 

Il vit à Paris de 1928 à 1939, il n'a que 13 ans lorsque Pétain décrète 
en octobre 40 les lois anti-juives. 
Il échappe à une première rafle en mai 1941 dans le 11e arrondissement. 
Cette même année, le gouvernement de Vichy impose à tous les juifs le 
port de l'étoile jaune sur lequel le mot "juif" est indiqué. 
Il ne la portera jamais mais la conserve encore à ce jour.

En septembre 1942, à même pas 16 ans, il rejoint la résistance. 

Il prend le nom Étienne Dumon, il va à Lyon, puis à Grenoble où il 
rejoint le bataillon "Liberté carmagnole" de la MOI FTP.


Il est démobilisé en novembre 1944 à l'âge de 17 ans et demi et avec le 
grade de caporal. Il obtient la nationalité française le 4 avril 1950 et 
va effectuer son service militaire. 
