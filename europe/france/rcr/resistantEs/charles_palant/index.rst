
.. index::
   pair: MilitantEs; Charles Palant
   pair: Discours; Soyons fiers d'être des Hommes


.. _charles_palant:

==========================================
Charles_Palant (1922-09-27 -> 2016-02-26)
==========================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Charles_Palant
   - http://www.dailymotion.com/leimal74#video=xr63bc
   - http://reseau-citoyens-resistants.fr/


.. contents::
   :depth: 3

Histoire
=========

Charles Palant est un militant français des droits de l'homme né en 1922 à
Paris. Il fut Secrétaire général du Mouvement contre le racisme, l'antisémitisme
et pour la paix entre 1950 et 1971.


Charles Palant (1949), ancien Président du comité des jeunes de la LICA,
devenu un des fondateurs du MRAP en 1949. Secrétaire général du MRAP de 1950 à 1971.

Charles Palant, de parents juifs polonais qui avaient immigré en France depuis
peu, a grandi dans le quartier de Belleville à Paris. Son père décède le 2 avril
1933 et laisse une famille de quatre enfants (dont Charles est le second) qui
doit survivre avec le seul salaire de l'aîné. Le 12 février 1934, il manque
l'école pour participer à la grève générale. En juillet de la même année fut
signé le pacte d'unité d'action entre les communistes et les socialistes.
L'un des signataires, le socialiste André Blumel, sera quinze ans plus tard le
premier président du MRAP. En 1935 Charles Palant quitte l’école à
douze ans : le certificat d’études primaires, obtenu six mois plus tôt, restera
son seul diplôme. Il devint alors maroquinier chez un petit patron du voisinage.

En mai 1936, le triomphe du Front populaire lui doubla son salaire, son temps
de travail fut ramené de cinquante cinq heures à quarante. En septembre 1939,
il perdit son travail, et fut engagé dans une petite fabrique de maroquinerie.
Il milite alors à la Ligue internationale contre l'antisémitisme (LICRA), mais
dû se résoudre à fuir Paris et à y revenir après avoir parcouru, à pieds, six
cents kilomètres. « A l'âge des plus exaltantes promesses de la vie, écrit-il,
j'ai du pour survivre apprendre les dures lois de l'illégalité, devenir faussaire,
franchir des frontières dans mon propre pays. ».

En août 1943, dénoncé, il fut arrêté par la Gestapo à son domicile lyonnais.
Sa mère et sa jeune sœur, qui avait dix sept ans, furent arrêtées avec lui.
Elles périrent en déportation. « On comprend que si on cède au chagrin, dit-il,
on est mort ». Il est détenu au Fort-Montluc à Lyon, au camp de Drancy près
de Paris, puis dans les camps d'Auschwitz et de Buchenwald d'où il revint en 1945,
pesant trente-huit kilos. Il avait vingt trois ans.

C'est avec les communistes, dit-il, qu'il a appris à refuser l'injustice et
qu'il a résisté dans les prisons et dans les camps. En 1949, il participe à
la création du MRAP, dont il a été le secrétaire général pendant vingt et un an,
et un des Président d'honneur depuis. Il représente également le MRAP à la
Commission nationale consultative des droits de l'homme (CNCDH).

« Je ne me souviens plus quand je suis devenu communiste, écrit-il, mais c'est
à Buchenwald que j'ai adhéré au PCF. (...) Je ne crois pas avoir jamais cédé
aux fallacieuses promesses du grand soir. Je ne cesserai jamais de croire au matin. »


Je crois au matin
=================

Charles Palant a été arrêté à Lyon en août 1943, par la Gestapo, avec sa mère
et sa soeur Lily âgée de 17 ans. Internés au Fort Montluc, ils sont déportés
début octobre vers Auschwitz via Drancy ; lui seul est revenu en 1945 après
avoir connu la « marche de la mort » et la libération à Buchenwald.
Dans son récit, Charles Palant, né en 1922 à Paris, raconte son parcours depuis
son enfance dans le quartier populaire de Belleville où, comme sa famille, les
Juifs immigrés vivaient alors nombreux.

Le fil directeur de l’exposé lucide qu’il nous livre ici tient dans sa foi
inébranlable en l’Homme, cette foi qui ne le quitta jamais, même au cœur des
plus terribles épreuves.

Dès les années 1930, Charles Palant exprime ses convictions et son engagement
au service des autres. Délégué syndical en 1936, membre de la Ligue
internationale contre l’antisémitisme, il participera après la guerre à la
fondation du Mouvement contre le racisme, l’antisémitisme et pour la paix (Mrap) –
dont il sera par la suite secrétaire général durant vingt ans.
Depuis 1983, il est membre de la Commission nationale consultative des droits de l’Homme.

Également très tôt impliqué dans la transmission de la mémoire, il a participé
à la création de l’Amicale des déportés de Buna-Monowitz. Il sera également
vice-président de l’Union des déportés d’Auschwitz et administrateur de la
Fondation pour la mémoire de la déportation.

Depuis de nombreuses années, Charles Palant met ses talents d’orateur et son
intelligence émaillée d’humour, au service des jeunes pour leur transmettre
par le récit de son expérience de déporté et de militant des droits de l’Homme,
son indéfectible espoir en des matins meilleurs.

Préface de Stéphane Hessel
Récit recueilli par Karine Mauvilly-Graton

Discours du dimanche 27 mai 2012 au plateau des Glières "Soyons fiers d'être des Hommes"
========================================================================================

.. seealso:: http://www.dailymotion.com/leimal74#video=xr63bc


