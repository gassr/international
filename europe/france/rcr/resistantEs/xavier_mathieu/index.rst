
.. index::
   pair: MilitantEs; Xavier Mathieu
   pair: Xavier; Mathieu

    
.. _xavier_mathieu:
   
===============================
Xavier Mathieu (1965-
===============================

.. seealso::

   - http://fr.wikipedia.org/wiki/Xavier_Mathieu
   - http://www.dailymotion.com/video/xr63u7_xavier-mathieu_news
   - http://reseau-citoyens-resistants.fr/


.. contents::
   :depth: 3
   
Histoire
=========
   
Xavier Mathieu est un délégué de la Confédération générale du travail (CGT) de 
l'usine Continental AG de Clairoix qui a été condamné en appel à 1200 euros 
d'amende pour avoir refusé un prélèvement de son ADN.

Parcours syndicaliste
----------------------

En compagnie de nombreux collègues de travail, il refuse la fermeture de son 
usine annoncée le 11 mars 2009 dans le cadre d'une délocalisation1, et engage 
une lutte radicale au cours de laquelle il est poursuivi pour faits de 
dégradation dans la sous-préfecture de Compiègne.

Le 7 février 2010 Xavier Mathieu est condamné à 4000€ d'amende alors même 
qu'il ne reconnaît pas les faits. Refusant de donner ses empreintes génétiques, 
un nouveau procès à lieu le 3 juin 2010 au tribunal de Compiègne concernant 
son refus de se laisser prélever son ADN. Il est relaxé car la prise d'ADN pour 
une affaire de dégradation paraît disproportionnée. 
Le parquet fait cependant appel de la décision de relaxe et un nouveau procès 
a lieu le 3 février 2012 où Xavier Mathieu est condamné à 1300 euros d'amende 
pour avoir refusé de donner son ADN.

Discours du diamnche 27 mai 2012 au plateau des Glières 
========================================================================================

.. seealso:: http://www.dailymotion.com/leimal74#video=xr63u7


