
.. index::
   pair: Militante; Annette Beaumanoir


.. _annette_beaumanoir:

===============================
Annette Beaumanoir (1923-
===============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Anne_Beaumanoir
   - http://reseau-citoyens-resistants.fr/


.. figure:: twitt_2018.png
   :align: center

   https://twitter.com/CRHA_glieres/status/1059133544696594432


.. contents::
   :depth: 3

Histoire
=========

Anne Beaumanoir, née le 30 octobre 1923 dans les Côtes-du-Nord, est un médecin
neurophysiologiste français. Pour son aide aux Juifs en Bretagne pendant la
Seconde Guerre mondiale, elle est reconnue Juste parmi les nations par le
Yad Vashem.

Militante communiste, elle est également connue pour sa lutte en Algérie avec
le FLN et pour ses divers engagements.


Mai 2018
=========

.. seealso::

   - https://vimeo.com/273714996
   - https://www.citoyens-resistants.fr/IMG/pdf/2018_annette_beaumanoir.pdf


Mai 2015
========================================================================================

.. seealso::

   - https://vimeo.com/136931744


