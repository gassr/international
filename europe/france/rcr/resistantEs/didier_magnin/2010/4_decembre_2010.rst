
.. index::
   pair: MilitantEs; Didier Magnin
   pair: Projet de société ; 21ème siècle


.. _didier_magnin_4_decembre_2010:

==========================================================================================
Intervention au forum des résistances dans les services publics, Paris, le 4 décembre 2010
==========================================================================================

.. seealso:: http://forum-4-dec-2010.resistancepedagogique.org/articles.php?lng=fr&pg=17

.. contents::
   :depth: 3


Tout d’abord un grand merci aux organisatrices et organisateurs de ce forum 
pour m’avoir invité, c’est pour moi un grand plaisir et un honneur d’être avec 
vous aujourd’hui.
Je me présente en quelques mots.
Au plan professionnel, je suis à la retraite depuis 3 mois, j’étais 
masseur-kinésithérapeute dans un centre médico-social, un établissement de la 
fonction publique hospitalière. Je m’occupais d’enfants et d’adultes handicapés 
mentaux profonds ou polyhandicapés, plus de 300 agents travaillent dans ce 
centre où j’assurais aussi la fonction de secrétaire du syndicat CGT de 
l’établissement.

Par ailleurs, je suis militant à ATTAC, à RESF et dans des comités de soutien 
à des « sans papiers ».
J’interviens ici en tant que président de l’association 
« citoyens résistants d’hier et d’aujourd’hui » (CRHA).


Citoyens résistants d’hier et d’aujourd’hui : 
rassemblements citoyens aux Glières.   

L'association CRHA est née d’un collectif qui s’est constitué après la venue de 
SARKOZY sur le plateau des Glières, le 4 mai 2007, deux jours avant le deuxième 
tour des présidentielles. 
Des résistants et des citoyens ont été indignés, révoltés par la récupération 
de la Résistance à des fins bassement électoralistes par le candidat SARKOZY, 
venu terminer sa campagne électorale à Glières.

Le collectif organise un 1er rassemblement citoyen à Glières, le 13 mai 2007 où 
près de 1500 citoyens ont répondu à cet appel.

Le 4 mai 2008, le même collectif initie un nouveau rassemblement citoyen avec 
la venue pour la 1ère fois à Glières de Stéphane HESSEL qui a prononcé un très 
beau et émouvant discours.

Le 3 décembre 2008, le collectif se constitue en association et ainsi est née 
« citoyens résistants d’hier et d’aujourd’hui ».

Le 17 mai 2009, à l’appel de CRHA, près de 4000 personnes sont montées à 
Glières, pour un nouveau rassemblement citoyen. Elles y ont vu et entendu 
Raymond AUBRAC, Stéphane HESSEL et les résistants d’aujourd’hui, l’enseignant 
désobéisseur Alain REFALO et le médecin psychiatre Michael GUYADER.

Le 16 mai 2010, nouvel appel de CRHA pour un rassemblement citoyen à Glières 
où 3000 personnes, dans le froid et les giboulées de neige, ont vu et entendu 
Odette NILES, grande résistante et petite amie de Guy MOQUET, le résistant 
FTP Léon LANDINI et les résistants d’aujourd’hui, le Dr Didier POUPARDIN de 
Vitry sur Seine, le «robin des bois» d’EDF, que nous venons d’entendre, 
Dominique LIOT, le journaliste du Fakir et de Là bas si j’y suis François RUFFIN 
et le magistrat Serge PORTELLI.

CRHA est parrainée par 3 résistants : Raymond AUBRAC, Stéphane HESSEL, 
Henri BOUVIER (résistant de Haute-Savoie) et par l’écrivain John BERGER.



Objectifs 
=========
 
Dès la création de CRHA, nous nous sommes fixés comme objectifs :
de faire connaître le programme du CNR et ses valeurs de solidarité, de 
fraternité, de justice, de vivre ensemble... de faire connaître 
l’appel du 8 mars 2004, où 13 résistants à l’occasion du 60ème anniversaire 
de la parution du programme du CNR, invitaient les organisations politiques, 
syndicales, les associations, les institutions, les mouvements héritiers de 
la Résistance à dépasser leurs enjeux sectoriels et à se consacrer en priorité 
aux causes des injustices et des conflits sociaux et non seulement à leurs 
conséquences...à définir ensemble un nouveau programme de résistance de ce 
siècle...(A ce propos, j’ai été très heureux de rencontrer, ce matin, 
Luc DOUILLARD qui a rédigé cet appel), de faire se rencontrer les résistants 
d’hier qui, par leur dynamisme, leur optimisme, apportent soutien et énergie 
aux résistants d’aujourd’hui


Rencontres et publications 
==========================

Pour réaliser ces objectifs, nous avons décidé : d'organiser des rassemblements 
citoyens sur le plateau des Glières, le prochain aura lieu les 14 et 15 mai 2011,
d'organiser des conférences-débats, comme la conférence que nous avons organisé 
à Annecy le 12 mars dernier sur l’instrumentalisation de l’histoire avec 
l’historienne Sophie WAHNICH et Suzette BLOCH, petite fille de Marc BLOCH,
de rédiger des écrits, avec la parution de notre livre « Les jours heureux », 
édité à la Découverte, et dont la rédaction a été coordonnée par Jean-Luc PORQUET, 
journaliste au Canard enchaîné. (Je rappelle que « Les jours heureux » était 
le titre du programme du CNR.)


Projets 
=======

Je ne reviendrai pas sur les destructions, les massacres des services publics, 
si bien exposés ce matin par des personnes concernées, je voudrais vous faire 
part de deux projets que nous menons à CRHA depuis notre dernier rassemblement 
de mai 2010. 

Pour répondre aux demandes de plus en plus nombreuses, provenant de toutes régions, 
de personnes voulant adhérer, nous soutenir ou participer activement à notre 
association, nous avons décidé, comme nous l’avait proposé Stéphane HESSEL 
en mai 2009, de constituer un réseau citoyens résistants. Les citoyens qui le 
souhaitent pourront en créer un dans leur ville, dans leur département.


Réseau citoyens résistants
==========================

Chaque réseau citoyens résistants pourrait ainsi, dans le respect de la charte 
proposée :

- impulser et fédérer les résistances citoyennes,
- développer l’éducation populaire,
- diffuser les cahiers de doléances contemporains, par le biais d'un collectif 
  qui s’est constitué à l’initiative de l’historienne Sophie WAHNICH et de 
  ses amis, pour la création et la diffusion de ces cahiers de doléances 
  contemporains. Notre réseau pourra servir de relais pour les récupérer et les 
  diffuser (voir site : http//www.letambourdesdoleances.org).
- participer à la rédaction du projet de société (voir ci-après).


Pour plus d’informations sur ce réseau : http://www.reseau-citoyens-resistants.fr


Projet de société pour le 21ème siècle : étape 1
=================================================


Ce n’est pas notre modeste association qui va, seule, rédiger un projet si 
ambitieux. Nous travaillons par étapes :

La 1ère étape a eu lieu les 6 et 7 novembre, dans un centre de vacances à 
Thorens-Glières, au pied du plateau.

Nous étions une cinquantaine pour cette première séance de travail. Ont répondu 
présents à cette séance de travail, des intervenants des rassemblements à Glières : 
Dominique LIOT, Alain REFALO, le Dr Didier POUPARDIN et son épouse, des 
contributeurs du livre « Les jours heureux » : Jean-Luc PORQUET, 
l’historien Olivier VALLADE, petit-fils de Raymond AUBRAC, des experts proches 
de notre association : Sophie WAHNICH, historienne ; Paul ARIES du Sarkophage 
et de la Décroissance ; Charles PIAGET, le leader syndical des LIP dans les 
années 70, qui milite à Agir ensemble contre le chômage ; Maurice CHAUVET, 
élu d’Orly, coordinateur de la défense des services publics...des citoyens 
rencontrés au cours de nos déplacements dans les régions.

Les débats et les échanges ont été très riches. Ils se sont déroulés dans une 
ambiance chaleureuse et conviviale. 

Nous avons débattu en réunions plénières et dans des ateliers sur trois thèmes:

- le processus et la méthodologie de travail,
- les besoins sociaux fondamentaux
- les valeurs fondamentales.

Nous avons désigné un comité de rédaction, un comité de coordination et un 
comité d’arbitrage. Un compte-rendu complet des débats a été rédigé et un 
texte d’amorce est en cours de rédaction.


Projet de société pour le 21ème siècle : les rendez-vous
========================================================

La 2ème séance de travail est prévue pour le samedi 15 et le dimanche 16 janvier 
2011 en Haute-Savoie, dans un lycée agricole non loin des gares SNCF d’Annemasse 
et Bonneville.

Une 3ème séance de travail est prévue les 26 et 27 mars, peut-être dans le Jura.

Nous ferons une communication sur l’avancée de ce projet, au cours du 
rassemblement citoyen des 14 et 15 mai à Glières.

N’hésitez pas à nous contacter si vous voulez participer à la rédaction de ce 
projet de société.

Pour plus d’informations sur les activités de CRHA : www.citoyens-resistants.fr.




