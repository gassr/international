
.. index::
   pair: MilitantEs; Didier Magnin
   pair: Didier ; Magnin 
   pair: Didier Magnin ; RESF


==========================================================================================
Intervention au forum des résistances dans les services publics, Paris, le 4 décembre 2010
==========================================================================================

.. toctree::
   :maxdepth: 3
   
   4_decembre_2010
