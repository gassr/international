
.. index::
   pair: MilitantEs; Raymond Aubrac
   pair: Raymond ; Aubrac 
   pair: Raymond ; Samuel 
   pair: Raymond Aubrac ; UJFP

.. _raymond_aubrac:

===============================================
Raymond Aubrac (31 juillet 1914-10 avril 2012)
===============================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Raymond_Aubrac
   - http://reseau-citoyens-resistants.fr/


.. contents::
   :depth: 3


Historique
============

Raymond Aubrac, de son vrai nom Raymond Samuel, né le 31 juillet 1914 à Vesoul 
et mort le 10 avril 2012 à Paris, est un résistant français à 
l'Occupation allemande et au régime de Vichy pendant la Seconde Guerre mondiale.

Ingénieur civil des Ponts et Chaussées (promotion 1937), il est spécialement 
connu pour s'être engagé avec son épouse Lucie dès 1940 dans la Résistance 
intérieure française. Sous le pseudonyme Aubrac, aux côtés d'Emmanuel d'Astier 
de La Vigerie, il participe, dans la région lyonnaise, à la création du 
mouvement Libération-Sud, plus tard intégré dans les mouvements unis de la 
Résistance (MUR) dont le bras armé fut l'Armée secrète : Aubrac y secondera le 
général Delestraint.

À la Libération, il est nommé commissaire de la République à Marseille, puis 
responsable du déminage au ministère de la Reconstruction. 

Compagnon de route du PCF, il crée ensuite BERIM, un bureau d'études investi 
dans les échanges Est-Ouest avant de devenir conseiller technique au Maroc et 
fonctionnaire de la FAO.

Ami d'Hô Chi Minh depuis 1946, il est sollicité par Henry Kissinger pour 
établir des contacts avec le Nord Vietnam, pendant la guerre du Viêt Nam 
entre 1967 et 1972.
À la fin de sa vie, il s'engage en faveur des droits du peuple palestinien 
et adhère à l'`Union juive française pour la paix`_.


.. _`Union juive française pour la paix`: http://fr.wikipedia.org/wiki/Union_juive_fran%C3%A7aise_pour_la_paix


Communiqué CRHA pour sa mort 
=============================

Raymond Aubrac, parrain de notre association, vient de nous quitter. 
Nous tenons à rendre hommage à son optimisme, sa disponibilité, sa perspicacité 
et à son engagement. A aucun moment, il ne sera resté figé dans le passé tant 
les valeurs qu’il défendait ont une résonance dans la France d’aujourd’hui. 

C’est aussi ce qu’il avait exprimé en 2009 au Plateau des Glières, lors de son 
intervention écoutée avec beaucoup de ferveur.

Nous l’avions rencontré le mois dernier, il tenait absolument à venir lors de 
notre prochain rassemblement aux Glières.

Celui-ci aura lieu les 26 et 27 mai prochain. Ce sera l’occasion de lui rendre 
hommage, à lui et à ses idées.
