
.. index::
   Réseau des Citoyens Résistants
   pair: Réseau ; Citoyens Résistants
   ! RCR
   ! Réseau des Citoyens Résistants

.. _rcr:

======================================
Réseau des Citoyens Résistants
======================================

.. seealso::

   - http://www.citoyens-resistants.fr/spip.php?article247
   - http://www.citoyens-resistants.fr
   - http://www.reseau-citoyens-resistants.fr/spip.php?article114
   - http://www.librinfo74.fr




.. contents::
   :depth: 3


Adresses de contact
===================

:Contact 1: reseau.citoyens.resistants@gmail.com
:Contact 2: citoyen.2008@yahoo.fr


Liste de diffusion
==================

.. seealso:: http://listes.rezo.net/mailman/listinfo/crha-diffusion


Appels
=======

.. toctree::
   :maxdepth: 4

   appels/index

Projet de société pour le XXIe siècle
=====================================

.. toctree::
   :maxdepth: 4

   projet_de_societe/index


Films
==========

.. toctree::
   :maxdepth: 4

   films/index

Groupes du réseau
=================

.. toctree::
   :maxdepth: 4

   groupes/index

Livres
==========

.. toctree::
   :maxdepth: 4

   livres/index



Résistants
==========

.. toctree::
   :maxdepth: 4

   resistantEs/resistantEs


Rassemblement des Glières
=========================

.. toctree::
   :maxdepth: 4

   glieres/index

