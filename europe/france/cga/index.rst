
.. index::
   pair: Coordination ; Groupes Anarchistes
   ! CGA

.. _cga:

===========================================
Coordination des Groupes Anarchistes (CGA)
===========================================

.. seealso::

   - http://www.c-g-a.org/


.. contents::
   :depth: 3



Qu'est-ce que la CGA ?
======================

.. seealso::

   - http://www.c-g-a.org/content/quest-ce-que-la-cga-0

Adresses de contact
===================

::

    SECRETARIAT CGA
    c/o La Mauvaise Réputation
    20, rue Terral
    34000 Montpellier
    Mail : secretariat@c-g-a.org

Résistances libertaires (infos CGA)
===================================

.. toctree::
   :maxdepth: 3
   
   infos/index
   
   
