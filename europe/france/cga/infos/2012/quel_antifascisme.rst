
.. index::
   pair: CGA; Antifascisme
   pair: 2012; Antifascisme

.. _cga_quel_antifascisme_2013:

============================================
Quel antifascisme aujourd'hui (mai 2013) ?
============================================


.. contents::
   :depth: 5

Courriel
========

    Sujet:  (fr) Coordination des Groupes Anarchistes - Positions politiques de la CGA Ii. (2/4)
    Date :  Tue, 28 May 2013 18:12:25 +0300
    De :    a-infos-fr@ainfos.ca
    Répondre à :    a-infos-fr@ainfos.ca
    Pour :  fr <a-infos-fr@ainfos.ca>


Analyse
========

Quel antifascisme aujourd'hui ? 

Le nécessaire bilan de deux décennies «d'antifascisme».

La lutte contre le fascisme a, jusqu'à son effondrement temporaire, été 
souvent amalgamée en France avec la lutte contre le Front National et 
les idées racistes et réactionnaires qu'il véhicule. 

Or le Front national n'est pas à proprement parler un parti fasciste, 
même s'il comporte une composante fasciste. C'est un parti d'extrême-droite
nationaliste, qui a pendant longtemps fait coexister des tendances 
idéologiques différentes, depuis les nostalgiques de l'Algérie française, 
jusqu'aux catholiques intégristes, et pour un certain temps, les nationalistes 
révolutionnaires. Il a représenté pendant longtemps la face visible la 
plus nette d'un nationalisme qui irrigue la classe politique en France 
de la droite à la gauche, voire une partie de l'extrême-gauche.

Il a représenté la formulation explicite des conséquences idéologiques 
de ce nationalisme: un discours raciste et xénophobe, mais aussi sexiste 
et homophobe. Il correspond au choix de la bourgeoisie française, dans 
le contexte de crise liée au second choc pétrolier, de promouvoir une 
grille de lecture raciste et xénophobe pour masquer les antagonistes de 
classe, et ainsi combattre le développement de luttes populaires. 

Il a également bénéficié des facilités accordées par la social-démocratie, 
dans une perspective politicienne, afin de briser sur le plan électoral 
l'influence de la droite.

La principale stratégie de riposte antifasciste pendant ces 20 dernières 
années a été la création de fronts antifascistes spécifiques, larges et 
unitaires, dontla dominante idéologique a été un discours moral fondé sur 
les valeurs humanistes dans lesquelles les références de classe et la 
nature du nationalisme comme outils des classes dominantes ont été diluées, 
voire complètement masquées.

Même si les libertaires, ou des groupes d'extrême-gauche, ont tenté de 
rendre visible cette dimension au sein de ces fronts, ils n'ont pu se 
faire entendre de manière audible, ce qui a conduit à une prédominance 
du discours républicain en matière «d'antifascisme ». 

Cela a souvent amené les libertaires à faire les « petites mains » de
fronts antifascistes qui promouvaient une approche «l’assiste» du fascisme.

**«L'antifascisme radical» n'échappe pas à ce constat**. 

Malgré la volonté et les tentatives de relier la lutte contre le fascisme 
à la lutte contre le capitalisme, la dimension spécifique de ce courant 
a souvent évolué vers une tendance à réduire la lutte contre le fascisme 
à la lutte contre les fascistes, à se contenter d'une « riposte » qui 
plaçait nécessairement la lutte sur le terrain même du fascisme, lui 
laissant l'initiative politique, voire dans certaines de ses expressions, 
relevant davantage du folklore plus que de l'action politique. 

Une chose est sûre, c'est que le développement des idées nationalistes, 
racistes et xénophobes n'a pas été stoppé, il a même été donné crédit à 
la rhétorique du fascisme qui a ainsi pu se présenter comme «antisystème», 
comme «révolutionnaire».

Un autre aspect de ce bilan est qu'une telle approche focalisée sur une 
organisation, le FN, est passée à côté de la réalité idéologique du 
fascisme,à savoir sa stratégie d'implantation «métapolitique», c'est à 
dire en conquérant une influence idéologique par la culture. 

Elle a également eu pour effet d'empêcher de saisir les« nouvelles »
formes et tendances du fascisme, celles de la réorganisation d'un courant
fasciste authentique, alliant racisme, antisémitisme et rhétorique 
« anticapitaliste », alliant discours social et national, et se développant 
hors de la sphère classique et identifiée du fascisme français, puisqu'il 
s'enracine et se développe également au sein des minorités nationales.

Analyser le fascisme comme tendance, définir le fascisme
=========================================================

Qu'est-ce que le fascisme, historiquement ? 

C'est l'alliance entre discours social et national, c’est la formation 
d'une « droite révolutionnaire » qui remet en cause l'idéologie 
démocratique bourgeoise, qui se vit comme « révolutionnaire », mais sert
les intérêts de la bourgeoisie en brisant les luttes populaires et toute 
perspective révolutionnaire. 

C'est aussi un discours voyant la société (amalgamée à une mythique
«nation») comme un «organisme» qu'il faut purifier (des «ennemis intérieurs» 
que sont les minorités nationales et les étrangers, mais aussi les subversifs), 
diriger et défendre contre elle-même, en la guidant d'une main de fer. 

C'est un discours idéologique qui se fonde sur une vision raciste ou 
ethno-différencialiste identitaire (racisme biologique ou culturel) 
qui divise l'espèce humaine en groupes auxquels il assigne une « race », 
une identité essentialisée, c'est à dire unensemble de caractéristiques 
qui ne dépendent pas de leur construction sociale, mais de ce qu'ils
sont, de leur prétendue «nature». 

C'est enfin un discours assignant ces identités à un territoire, autour 
d'une mystique de la terre et des morts (cf. Maurras, l'un des théoriciens 
français du fascisme).

C'est une idéologie qui oppose le capitalisme industriel, corporatiste, 
considéré comme «authentique», au capitalisme financier, arbitrairement 
séparé et amalgamé aux juifs par le discours antisémite, ce qui permet
de protéger la classe capitaliste par une stratégie de bouc émissaire.

Le fascisme et la crise
=========================


Dans une période de crise d'adaptation capitaliste, le fascisme est 
l'ultime recours du capitalisme et de la bourgeoisie : pour briser toute 
résistance des classes populaires à ses offensives, mais aussi pour 
« mettre de l'ordre » en son sein. 
Tant que son pouvoir n'est pas remis en cause, la bourgeoisie a intérêt 
à préserver le cadre de la démocratie représentative, car le pouvoir 
d'influence est la forme de pouvoir la plus efficace et la plus économique. 
Même pour les très riches, il est aussi certainement plus confortable
de vivre dans un cadre de relative liberté d'expression. Mais dès lors 
que ce pouvoir est fragilisé, la tentation fasciste suscite rapidement 
l'adhésion de larges secteurs de la bourgeoisie.

Dans la période actuelle, la crise économique et sociale capitaliste a 
suscité un certain nombre de résistances populaires qui inquiètent la 
bourgeoisie. Cependant, elle bénéficie depuis plus de dix ans d'un 
avantage certain dans la lutte des classes, lié à la désorganisation 
du mouvement ouvrier à l'échelle internationale, et à l'absence de
perspectives révolutionnaires émancipatrices. 
Dans le même temps, le fatalisme qui en découle conduit une partie des 
classes populaires à se tourner vers les mouvements populistes d'extrême 
droite en renforçant d'autant leur influence.

La situation sur le plan international
=======================================

Cette situation est visible clairement sur le plan international. 

On assiste à un triple mouvement : le renforcement des outils de 
coercition des états et des régimes autoritaires, qui visent à réprimer 
les mouvements liés à la révolte des classes populaires contre leurs 
conditions de vie, le développement de ces mouvements populaires 
poussés par la nécessité, qui se heurtent aux privilèges de la 
bourgeoisie et des États en défendant leurs intérêts, enfin le 
développement de courants idéologiques qui s'inscrivent dans la défense 
des intérêts de la bourgeoisie confrontée à ces soulèvements et qui se 
présentent comme «révolutionnaires» et «anticapitalistes».

Ces tendances correspondent aux différentes formes que prend le fascisme.

En Europe
---------

On constate le développement de mouvements nationalistes, et notamment 
«nationalistes révolutionnaires», qui se traduit à la fois par des 
violences contre les minorités nationales (arabes, noirs, juifs, Rroms...), 
et contre les  militant-e-s antifascistes et progressistes (agressions 
de camarades en Russie, en Serbie, etc.)

Aux États-Unis
--------------

On constate le développement de groupes nationalistes et racialistes,
depuis les suprématistes blancs jusqu'aux «minute men» servant 
d’auxiliaires à la politique de répression de l'immigration américaine. 

En Amérique du sud
------------------

Le développement de groupes paramilitaires de type nationaliste et de 
groupes néo-nazis répond aux mêmes dynamiques.


En Turquie 
-----------

Les groupes fascistes tels que les loups gris mènent une politique de
violence et de terreur fasciste contre les minorités nationales kurdes, 
arméniennes, les minorités religieuses (halevis), et les militant-e-s 
révolutionnaires.

Pays musulmans
--------------

Dans un certain nombre de pays où la religion musulmane est majoritaire, 
les groupes qui assument ce type de politique se cachent derrière le 
masque de la religion: 

- nervis fascistes iraniens se réclamant de l'islam qui répriment et 
  attaquent les militant-e-s ouvriers et féministes iranien-ne-s, 
- fascistes ou réactionnaires religieux tels que les frères musulmans, 
  les salafistes, les militant-e-s du FIS en Algérie, qui servent de 
  supplétifs à la répression anti-ouvrière et antiféministe, ainsi que 
  d'une «fausse opposition» et d'une «fausse alternative» à des pouvoirs 
  nationalistes discrédités, qui mènent eux aussi une répression directe 
  des luttes populaires. 
  
Ce type de mouvement existe également dans bon nombre de pays catholiques 
ou orthodoxes, à travers notamment des mouvements réactionnaires religieux 
qui assument ce type de politique.

En France
----------

La période récente se traduit par une montée en puissance du nationalisme, 
entretenue notamment par le pouvoir politique, mais aussi les relais 
médiatiques et idéologiques de la bourgeoisie. 
Si ce nationalisme irrigue la quasi-totalité des courants politiques, 
depuis la gauche coloniale jusqu'à l'extrême-droite, les courants 
fascistes sont le fer de lance de sa diffusion en milieu populaire, au 
moyen d'une rhétorique «sociale» pseudo-anticapitaliste.

Au sein des catégories de population désignées par l'idéologie nationale 
comme constituant le «corps national», le fascisme joue un rôle 
mobilisateur pour les intérêts de la bourgeoisie, en présentant la 
violence sociale non pour ce qu'elle est, le résultat du capitalisme, 
mais pour l'effet de l'action «d'ennemis intérieurs» ou «d'ennemis
extérieurs». 
Ces «ennemis intérieurs» et «extérieurs» sont désignés comme étant les
membres de minorités nationales, religieuses, sexuelles du pays, ou les 
étrangers. En contexte de crise, c'est la tendance «socialiste-nationale» 
qui se développe le plus rapidement, autour notamment d'un antisémitisme 
virulent (qui se masque derrière un discours prétendument antisioniste) 
réactivant la figure de bouc émissaire du juif, d'une islamophobie 
virulente (substituant ou le plus souvent ajoutant à la figure bouc-
émissaire du juif celle du musulman), et plus largement d'un racisme 
«décomplexé».

Cette tendance «socialiste-nationale» est représentée par plusieurs 
organisations se réclamant plus ou moins ouvertement du nationalisme 
révolutionnaire:

Égalité et réconciliation et ses alliés 
+++++++++++++++++++++++++++++++++++++++

(Dieudonné et les relais de l’État d'Iran en France que sont les militants 
du centre Zahra), qui privilégient un front antisémite visant à mobiliser 
au côté des nationalistes français une partie des personnes appartenant 
à la minorité nationale arabe.

Les identitaires 
+++++++++++++++++

qui privilégient un front «antimusulman» qui vise à mobiliser aux côtés 
des nationalistes révolutionnaires européens les courants racistes qui 
se cachent derrière une « laïcité » à deux vitesses, et une partie des 
personnes appartenant à la minorité nationale juive (notamment la frange 
fasciste du sionisme, comme en témoigne l'organisation d'une manifestation 
commune identitaires-LDJ devant l'ambassade d’Israël).

Fraction marxiste du FN
+++++++++++++++++++++++

Enfin, s'ajoute à cela la fraction mariniste du FN qui tente de développer 
un discours «national et social» proche de celui des identitaires, mais 
qui diffère en privilégiant un cadre nationaliste français au cadre 
nationaliste européen (suprématiste blanc) des identitaires.

Dévier la révolte sociale
++++++++++++++++++++++++++

Toutes ces tendances tentent de dévier la révolte sociale vers une 
approche nationaliste, xénophobe et raciste, en se présentant comme 
«révolutionnaires». 

Leur radicalité formelle leur permet d'amener aux thèses nationalistes 
une partie des travailleuses et des travailleurs en révolte contre le 
système capitaliste, à travers un «anticapitalisme» qui se réduit à la 
défense du corporatisme contre le «capital financier», de présenter la 
nation comme un recours contre la «finance internationale», à une 
critique des valeurs consuméristes, sans contenu de classe, sans lien 
avec la réalité des luttes populaires. 

C'est en ce sens que ces courants diffèrent des courants nationalistes 
de la droite classique : en période de crise ceux-ci apparaissent trop
ouvertement comme les représentants de la classe bourgeoise (en 
témoignent les affaires Bettencourt, etc.), et suscitent donc la 
méfiance au sein des classes populaires.
Alors que la radicalité de postures des nationalistes révolutionnaires 
et leur conviction d'être «révolutionnaires», leur permettent d'attirer 
aux thèses nationalistes des individus appartenant aux classes populaires, 
en mobilisant les valeurs réactionnaires largement présentes dans la 
société (sexisme, homophobie, chauvinisme...).

Soral a ainsi d'abord construit son image de « rebelle» sur un discours
antiféministe et homophobe, présenté comme un «refus du politiquement 
correct», puis sur un antisionisme antisémite qui a visé à instrumentaliser 
la question palestinienne pour relégitimer l'antisémitisme historique 
des fascistes français.

Premièrement, l'outil internet a permis aux sympathisants et aux 
militants d'extrême-droite de pouvoir s'exprimer et diffuser leurs idées 
beaucoup plus librement qu'auparavant. 
En effet, la lutte sur le plan moral de l'extrême-droite aau moins permis 
de faire en sorte que le racisme n'était pas une opinion comme une autre. 

De plus, l'outil internet a donné une caisse de résonance importante à 
des courants au départ confidentiels, qui ont su utiliser les nouvelles 
technologies (vidéos sur dailymotion, youtube), pour diffuser leur pensée. 
Ils ont également su utiliser des passerelles, sous la forme de sites 
internet relayant en lien leur discours ou des personnes cautionnant leur 
discours au nom d'un «anti-impérialisme» hérité du stalinisme ou du 
tiers-mondisme, les pseudos laïcs relayant un discours raciste (par
exemple l'officine raciste «riposte laïque») derrière une prétendue 
critique de l'islam.

Sur internet par exemple, de nombreux sites diffusent l'idée d'un 
« nouvel ordre mondial » (expression qui provient à l'origine de la 
droite radicale américaine) dirigé par les « sionistes » et les 
«illuminatis». 
Il ne s'agit de rien d'autre que du bon vieux discours national-socialiste 
et fasciste sur le «complot juif et franc maçon mondial», qui a adopté 
une nouvelle forme pour contourner le discours antifasciste et la
législation de l’État sur le racisme. 
Cette nouvelle forme du discours sur le «complot judéo-maçonnique» a des 
succès inattendus, au sens où de telles approches sont reprises par des 
musiciens de rap, y compris ceux qui affichent des sympathies libertaires, 
qui en ignorent peut-être l'origine, mais qui les banalisent et 
contribuent à leur diffusion dans la jeunesse populaire.

On retrouve ces influences dans les courants fascistes ou nationalistes 
spécifiques aux minorités nationales : ainsi, les sionistes de tendance 
fasciste de la Ligue de défense juive reprennent le discours raciste 
anti-arabe des identitaires ou la théorie du «choc des civilisations» 
et du danger islamique. 
À Belleville, des nationalistes chinois ont organisé une manifestation 
«contre l'insécurité» au cours de laquelle des passants noirs ou arabes 
ont été pris pour cibles, désignés comme des «voleurs» sur critères 
racistes, ce qui a provoqué les applaudissements des réseaux identitaires 
français (par exemple sur le site internet «français de souche»)

De même, une partie des courants fascistes panarabes et des courants 
fascistes se réclamant de l'islam politique reprennent la rhétorique 
antisémite issue du nationalisme français. 
Ces convergences expliquent le développement de fronts communs entre 
nationalistes français et nationalistes se revendiquant des minorités
nationales, qui peut apparaître surprenante au premier abord, puisque 
c'est le nationalisme français qui, en excluant juifs et arabes du corps 
national,a créé de toute pièce les minorités nationales et, dans le même 
temps, les conditions de l'oppression raciste des individus qui y sont 
alors assignés par leur origine et/ou leur couleur de peau. 

Mais cela traduit au contraire la profonde parenté idéologique entre ces
différents courants, et le fait qu'ils se nourrissent les uns des autres,
au détriment des classes populaires, et particulièrement des individus 
victimes de l'oppression raciste parce qu'assignés à une «minorité nationale».

Cela montre qu'il n'existe pas d'alternative au racisme dans le 
développement d'un nationalisme au sein des minorités nationales, puisque 
celui-ci reproduit le discours raciste dominant et converge parfois avec 
le nationalisme dominant. 
Au contraire, l'alternative se trouve dans le développement d'un 
antiracisme populaire qui combat toutes les formes de racismes, sur le 
plan idéologique comme sur le plan pratique. 
Les différents courants fascistes ont progressé sur le plan organisationnel
comme sur le plan de leur influence idéologique et culturelle : ils ont 
ainsi réussi à imposer leurs «sujets», leurs «approches» dans le débat 
politique : une approche ethno-différentialiste des questions politiques 
et économiques au détriment d'une approche de classe, une rhétorique 
fondée sur la menace « intérieure» ou «extérieure» que représenteraient 
les minorités nationales ou religieuses, au détriment de l'affirmation
de la question sociale, etc.

L'influence de l'idéologie nationaliste a progressé, et celle de l'idée 
de la « guerre du tous contre tous » également. 
Dans le même temps, les discours ouvertement sexistes ou homophobes, qui 
constituent également une partie du corpus fasciste, ont gagné du
terrain. 
L'influence de l'idéologie fasciste dépasse de loin celle des groupes 
constitués, mais ceux-ci progressent quantitativement et 
organisationnellement, notamment dans les campagnes, mais aussi en 
ouvrant des locaux pignon sur rue dans plusieurs grandes villes. 

Il est également significatif que des discours reprenant les canons de
l'idéologie fasciste ne soient pas considérés comme tels y compris au
sein de la gauche et de l'extrême-gauche, voire d'une partie du courant 
anarchiste. 
Ce qui explique par exemple la tolérance dont a longtemps bénéficié 
Dieudonné au seinde l'extrême gauche au nom d'une posture «rebelle», 
certains groupes le trouvant fréquentable jusqu'à ce que celui-ci invite 
Faurisson sur scène.

On peut trouver des éléments d'explication dans la faiblesse de réflexion 
sur le fascisme de «l'antifascisme des années 90», qui s'est focalisé 
sur les groupes fascistes plutôt que sur leurs idéologies (quand dans sa 
version gauchiste ou social-démocrate il ne s'est pas contenté d'une 
dénonciation du FN), qui a négligé la lutte idéologique antifasciste 
pour se consacrer exclusivement à la nécessaire (mais pas suffisante) 
lutte contre les groupes fascistes constitués et à l’autodéfense. 

On peutaussi trouver une explication à cela dans l'amalgame fréquent 
entre nationalisme, fascisme et racisme.
Or si le fascisme se nourrit et fait la promotion du racisme et du 
nationalisme, il ne s'y résume pas, et réciproquement : on retrouve 
l'idéologie nationaliste dans une grande partie du spectre politique, 
comme le discours raciste. 
La spécificité du fascisme réside dans le développement d'un discours 
social «antisystème» qui permet, en période de crise, de recruter au 
sein des milieux populaires des personnes qui auraient pu être attirées 
par un réel discours révolutionnaire.

Alternative anarchiste et riposte
==================================

**La nécessité d’une contre-offensive idéologique ne fait pas question.**

Une réponse politique anarchiste est une évidence qui doit privilégier 
l’autoformation des militant-e-s au sein du Mouvement libertaire et, 
plus largement au sein du « Mouvement social», sur les formes prises 
par les discours racistes et fascistes.

Aussi, faut-il insister sur le fait que le fascisme n’a jamais été 
éradiqué par le biais  des consultations électorales, ces dernières 
lui ayant même conféré une certaine dose de «légitimité».

La lutte contre le fascisme constitue un axe important du combat des 
prolétaires en même temps qu'une absolue nécessité, pour autant 
l’antifascisme ne doit pas constituer l’unique combat pour la défense 
des intérêts de classe du prolétariat face à la bourgeoisie.

Le développement de luttes populaires, qui reste l’unique moyen d'imposer
la lutte de classe, la solidarité, le refus de la domination masculine 
et de l'homophobie dans les débats politiques, doit nous permettre de 
briser les tentatives d’hégémonie culturelle des nationalistes et des 
fascistes.

Il s'agit donc pour nous de combattre le fascisme et la bourgeoisie qui 
le sous- tend, quel que soit son ancrage : une bourgeoisie qui se pare 
des vertus démocratiques ou pas, qui se réfère au libéralisme ou à la 
social-démocratie...

L’antifascisme, en s’attelant à dénoncer les discours autour des
«souverainetés nationales», ne peut se concevoir que par le biais d’une 
Humanité sans frontières et d’une solidarité internationale sans 
concession !

La lutte contre les «souverainetés nationales» ne doit en aucun cas se 
faire le relais de l’ultra libéralisme économique et/ou idéologie, lequel 
ne se préoccupe que de la satisfaction de ses propres intérêts, intérêts 
opposés à ceux de la grande majorité des individus...

Nous devons développer un antifascisme qui puisse faire tomber les 
barrières humaines tout en contrecarrant les plans économiques globaux 
du capitalisme à l'échelon national et/ou à l'échelon mondial.

Combattre le fascisme et l'extrémisme nécessite alors de le faire :

- au plan idéologique,
- au plan social,
- et quand cela est inévitable (voire souhaitable), au quotidien, dans 
  les quartiers, les usines etc.
  
Sur le plan idéologique, l'organisation anarchiste qui préconise des 
valeurs d’entraide, de solidarité, d’égalité, d’autonomie individuelle 
et collective est armée pour regrouper les individus et les structures 
qui ne fondent pas leur horizon de société autour de la défense des 
hiérarchies, de la lutte pour le Pouvoir, de la force brute et de
la croyance en des êtres supérieurs qui seraient appelés à diriger les 
masses.

L’anarchisme peut offrir les références et les outils nécessaires à la 
constitution d'un «regroupement antifasciste» autour des valeurs 
égalitaires et libertaires qui sont les siennes.

Si les anarchistes ne sont pas les seuls susceptibles de regrouper 
individus et groupes afin de lutter efficacement contre le fascisme, 
elles et ils garantiront à ce regroupement et la lutte qu’il sous-tend 
son entière autonomie. 
Il ne s’agira à aucun moment d'en prendre le contrôle. 

À contrario, au travers de cette lutte, ils œuvreront pour bâtir, toutes 
et tous ensemble, une société débarrassée du fascisme et de ses 
fondements : le capitalisme, le libéralisme et l'étatisme.

Au plan des agencements sociétaires, l'égalité économique et sociale 
pour laquelle milite l'organisation anarchiste se traduit par la mise 
en œuvre de l'autogestion généralisée. 
La participation des individus et des collectifs en tant qu'acteurs 
et non plus simples spectateurs supposent :

- une conscience aiguë des problèmes qui traversent la société,
- une responsabilisation face à ces problèmes,
- le rejet de toutes les idéologies qui aspirent à diriger les individus 
  et à décider en  leur nom, le fascisme étant de ce point de vue 
  exemplaire au travers de ses pratiques autoritaires...

Concernant la lutte contre l’idéologie fasciste et sa traduction en actes, 
il ne peut être question pour l'organisation anarchiste d’être désignée 
comme l'avant-garde d'une armée antifasciste prête à faire le coup de 
poing avec la vermine extrémiste.

Ainsi, en vue du développement d'une autodéfense antifasciste, nous 
devons éviter le piège d'un tête à tête anarchistes contre fascistes, 
qui placerait l’État, et les courants politiques institutionnels dans 
le rôle d'arbitres, usant tour à tour de la répression  pour les uns et 
pour les autres, ce qui n'empêche pas l’État par ailleurs de soutenir
ponctuellement les fascistes (en leur garantissant l'immunité ou en les 
protégeant).

Les méthodes employées pour barrer la route aux fascistes doivent 
rencontrer l'assentiment de tous les individus et collectifs épris de 
justice sociale et de liberté. 
Il ne pourra être question de s'abriter derrière les Institutions, 
Justice, Police,  décideurs et élus, pour éradiquer les idéologies 
rétrogrades.
L’autodéfense antifasciste que nous préconisons relève d’une culture 
d'autodéfense à développer dans les quartiers, sur les lieux de travail, 
les associations, les syndicats.
Elle ne pourra se résumer à la seule dimension physique, dimension 
nécessaire et inévitable à certains moments, mais cette autodéfense 
devra faire sens et s’employer à aborder dans le champ idéologique la 
critique et la dénonciation des offensives fascistes masquées derrière 
des «passerelles».

Pour l’essentiel, le message que nous tenons à faire passer est celui-ci:
Nous devons tout mettre en œuvre pour barrer la route à la vermine 
fasciste et à toutes les dérives autoritaires.
Pour cela, nous devons nous mobiliser, le plus largement possible, afin 
de créer les conditions favorables à l’éradication des idées et des 
actes fascistes et à leur installation au sein de nos sociétés.
La lutte sociale nous fournit le terrain privilégié du nécessaire 
affrontement aux «fascismes», dès lors que s’engagent en même temps, 
le combat contre la barbarie capitaliste, l’autorité étatique et la lutte 
pour l’émancipation des individu-e-s.

::

    Relations Extérieures – Coordination des Groupes Anarchistes (novembre 2012)

