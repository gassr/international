
.. index::
   pair: Tribune Syndicaliste ; Libertaire
   pair: Tribune ; Syndicaliste 
   ! CGA

.. _tsl:

===========================================
Tribune Syndicaliste Libertaire (TSL)
===========================================

.. seealso::

   - http://tribune-syndicaliste-libertaire.over-blog.com/


.. contents::
   :depth: 3


Qui sommes nous ?
==================

Ce blog est animé par des militant-e-s syndicalistes libertaires lyonnais, 
organisés à la CGA et adhérents de différentes organisations syndicales. 

Il a pour objet la diffusion des propositions que font les militant-e-s 
syndicalistes libertaires, dans le respect de l'indépendance et de la démocratie 
syndicale,contribuant au débat en cours dans le mouvement syndical, ayant pour 
but le renforcement du rapport de force en faveur des travailleuses et des 
travailleurs, par le développement des lutte.

Les textes publiés n'engagent pas l'ensemble du groupe de Lyon de la CGA,sauf lorsque 
cela est explicitement mentionné.
   

Adresses de contact
===================

::

    Mail : tribunesyndicalistelibertaire@gmail.com

Tracts
===================================

.. toctree::
   :maxdepth: 3
   
   tracts/index
   
   
