
.. index::
   pair: Retraites; 2013



.. _tracts_tsl_18_septembre_2013:

==================================================
Tract du 18 septembre sur les retraites (TSL)
==================================================



::

    Sujet:  (fr) Tribune Syndicaliste Libertaire*: Appel pour la défense des retraites (en)
    Date :  Wed, 18 Sep 2013 11:22:21 +0300
    De :    a-infos-fr@ainfos.ca
    Pour :  fr <a-infos-fr@ainfos.ca>


La réforme des retraites en cours s'inscrit dans la droite ligne des précédentes 
casses du système de retraite. 

Depuis l'adoption du système par répartition, le patronat n'a eu de  cesse de 
chercher à le liquider par différents mécanismes : 

- assèchement des recettes par  les exonérations, 
- transfert de financement du salaire à la fiscalité,
- baisse des pensions par l'allongement des durées de cotisation et le report 
  de l'âge à la retraite.
  
Cet allongement aggrave la situation des travailleuses qui sont particulièrement 
touchées par ces réformes du fait de la surprécarité, des salaires inférieurs, 
des temps partiels et des carrière incomplètes imposées par le patronat comme 
par le patriarcat. 

Dans la réforme actuelle, les mesures présentées comme favorables aux femmes,
avantagent dans les faits ceux qui ont les meilleurs revenus, les hommes.

Ce qui est insupportable au patronat, c'est tout ce qui dans le mécanisme actuel 
repose sur le fonctionnement collectif et solidaire, c'est tout ce qui échappe 
à la plus-value, donc à l'exploitation. 

C'est aussi tout ce qui échappe au marché, à la capitalisation.

Ces éléments ont été le résultat d'un rapport de force continu, et imposé par la 
mobilisation collective et les outils du mouvement ouvrier et syndical : grèves 
interprofessionnelles, boycott, blocage de la production et de la distribution, 
sabotage des cadences, manifestations de masse...

L'Etat a été le fer de lance du démantèlement du système de répartition. 

L'Etat a :

- imposé d'abord le paritarisme pour briser le contrôle syndical, 
- supprimé les élections de la sécurité sociale pour liquider tout embryon 
  de contrôle ouvrier, permettre la gestion bureaucratique des caisse sur fond 
  d'accord tacite entre MEDEF et organisation syndicale jaune. 
- utilisé la loi pour assécher les recettes, baisser les pensions, faire passer 
  du capital au travail le financement par le biais de la fiscalité et des 
  exonérations patronales.

Le fait que le gouvernement actuel s'inscrit dans la continuité de cette 
politique de casse menée aussi bien par la gauche que par la droite n'a rien 
de surprenant : il s'inscrit dans la longue liste des reniements politiques 
dont les partis de gauche ont fait preuve au cours de l'histoire du mouvement 
ouvrier. 

Cela après avoir prétendu, chaque fois qu'ils avaient besoin de voix, défendre 
les intérêts des travailleuses et des travailleurs. 

Rappelons que l'un des architectes de la stratégie de casse en cours est 
Michel Rocard, qui a préconisé dans son « livre blanc sur les retraites » 
l'essentiel des tactiques mises en oeuvre depuis pour cette liquidation 
progressive.

A l'inverse, tous les acquis sociaux ont toujours été le résultat d'un rapport 
de force de haut niveau, fondé sur la lutte dans les lieux de travail et la rue.

Dans ce contexte, seule la mobilisation la plus large, et le blocage collectif 
le plus ferme de la production et de la distribution, dans les entreprises et 
les services, sera à même de faire reculer l'Etat et le patronat.

Il n'y a rien à attendre de bon d'une stratégie de lobbying, ou d'une position 
attentiste qui consiste à espérer le moins pire, pas plus de mots d'ordres 
radicaux lancé sans se soucier de leur mise en oeuvre concrète.

Nous pensons que nous devons tout mettre en oeuvre pour que les structures 
syndicales organisent cette lutte de masse.

Néanmoins, nous pensons également:

- que c'est à chaque syndiquéEs, à toutes les travailleuses et travailleurs 
  avec ou sans emploi, de prendre en charge l'extension de la grève et son 
  organisation.
- qu'il est fondamental de créer des outils permettant de faire circuler 
  efficacement les informations nécessaire à la mobilisation
- qu'il est nécessaire que tous les syndicalistes conscients de la nécessité de 
  construire un rapport de force se mettent en rapport, s'organisent pour 
  réaliser l'unité syndicale à  la base.

Pour cela il est nécessaire de mettre à distance toute tentative 
d'instrumentalisation politique par des avant-gardes autoproclamées, par 
quiconque souhaite faire du mouvement syndical un marche pied pour les élections. 

Cela passe par une attention toute particulière à la démocratie syndicale, aux 
décisions collectives des grévistes, au contrôle des mandats. 

Cela seulement permettra de créer l'unité syndicale et l'unité des travailleuses 
et travailleurs autour des objectifs propres de la lutte.
Une victoire sur le terrain social reste le meilleur moyen de faire reculer
fascistes, nationalistes et réactionnaires qui se nourrissent aussi bien de 
nos échecs que du contexte de crise.

Nous invitons toutes celles et ceux qui se retrouvent dans cette approche, à 
nous contacter afin de contribuer ensemble à construire un outil de résistance 
intersyndical à la base.

Tribune syndicaliste libertaire
:contact: tribunesyndicalistelibertaire@gmail.com
   
