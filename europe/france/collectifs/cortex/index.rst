
.. index::
   pair: Collectif; Cortex
   pair: Esprit; Critique
   ! Cortex
    
.. _cortex:
   
==============================================================================
CorteX (COllectif de Recherche Transdisciplinaire Esprit Critique & Sciences)
==============================================================================

.. contents::
   :depth: 3
   

Présentation
============

Bienvenue sur le site du CorteX


COllectif de Recherche Transdisciplinaire Esprit Critique & Sciences) 
est  un collectif né en 2010 à Grenoble, Marseille et Montpellier. 

Son but est de réunir tous les acteurs, enseignants, chercheurs, étudiants 
travaillant sur un sujet développant l'esprit critique, quelle que soit 
leur origine disciplinaire. 

Il s'agit d'un pont transdisciplinaire et inter-universités. 

Lire la suite 

Contact
========

Pour nous contacter : contact @ cortecs.org


Réunion
========

Pour nous rencontrer :  le mercredi

Permanence sur rendez-vous le matin, et sans rendez-vous de 14h à 18h30 
bureau CorteX, 1er étage, Bibliothèque des sciences, Grenoble. Plan_.

.. _Plan: http://sicd1.ujf-grenoble.fr/-Informations-pratiques,10-


   

   
      
   
   
   
   

