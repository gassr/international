
.. index::
   pair: BDS 38; Juin 2014
    
.. _bds38_juin_2014:
   
==================================================================================================
Pas de coopération avec les universités israéliennes sans respect du droit internationtional 
==================================================================================================

.. seealso::

   - http://fr.slideshare.net/assr38/bds38-2014-06technion
   


La visite d’une délégation israélienne du Technion (institut polytechnique 
israélien) et de l’Institut Weizmann est attendue à l’Université Joseph Fourier 
(UJF) de Grenoble du 8 au 10 juin prochain. 

Le Groupe de coordination « Boycott, Désinvestissement, Sanctions »(BDS) de 
Grenoble s’est adressé au Président de l’UJF dans une lettre ouverte dont 
voici les grandes lignes. 

L’UJF, ainsi que d’autres établissements grenoblois, entretient des relations 
étroites avec le Technion. Les échanges ont été nombreux, particulièrement en 
2012, dans le cadre d’une collaboration inter-universités et dans celui du 
jumelage Grenoble-Rehovot. 

De par certaines de ses activités, le Technion participe à des programmes dont 
les applications sont directement liées à l’occupation et à la colonisation de 
la Palestine : Conception de véhicules blindés sans pilote destinés à être 
utilisés en territoire palestinien lors des opérations contre des populations 
civiles, comme le **bulldozer D9** utilisé pour détruire habitations et cultures, 
conception avec la Société Elbit de drones utilisés pour assassiner des 
militants présumés, toujours avec la société Elbit, conception de caméras de 
surveillance qui équipent le mur de séparation.... 

Le Technion semble ainsi très lié à l’occupation des territoires palestiniens, 
à la colonisation, à l’enfermement de la population de Gaza. 

De par ses activités de développement au service de l’armée israélienne, 
l’Institut Weizmann, est également impliqué dans le soutien direct à la politique 
israélienne de colonisation et d’occupation. 

Les valeurs universitaires sont fondées sur la liberté, la justice et l’égalité 
===============================================================================

L’État israélien bafoue tous les jours ces valeurs, ainsi que la liberté 
académique et la vie culturelle du peuple palestinien. 

La science est ainsi mise au service de la colonisation et de l’occupation de 
la Palestine. 

Tant que l’État d’Israël ne respecte pas le droit international et les 
résolutions des Nations Unies, nous demandons à l’Université Joseph Fourier, 
comme nous le demandons au gouvernement français, de s’assurer qu’aucun des 
accords qui seraient signés avec le Technion ou avec l’Institut Weizmann 
n’implique ni les entreprises israéliennes ni les institutions qui jouent 
un rôle dans l’industrie militaire, la défense, et la sécurité, ainsi que 
dans l’occupation et la colonisation illégales des terres palestiniennes.  

Le BDS est un mouvement international créé à l’appel de plusieurs personnalités 
palestiniennes visant à imposer à l’État d’Israël le respect des droits du 
peuple palestinien qu’il bafoue depuis sa création. 
À Grenoble, une dizaine d’associations, syndicats et partis politiques prennent 
part à ce mouvement. Groupe de coordination BDS de Grenoble : 

- Association France Palestine Solidarité Grenoble-Isère, 
- Attac 38, 
- Centre d’Information Inter Peuples, 
- Cercle Juif pour une Paix Juste, 
- CNT, 
- Échirolles Palestine Solidarité, 
- Fédération Syndicale Unitaire, 
- Les Alternatifs-Isère, 
- Nouveau Parti Anticapitaliste, 
- Pour une Alternative à Gauche 38, 
- Solidaires Étudiants, 
- Union Juive Française pour la Paix 

bds.gre.38@gmail.com 

Dans sa réponse en date du 5 juin, le Président de l’UJF nous indique que l’UJF, 
« soucieuse du respect de la réglementation européenne en vigueur et des valeurs 
fondamentales notamment le respect des Droits de l’Homme, restera vigilante 
lors des discussions engagées, et des potentiels accords qui en découleraient 
avec ses partenaires israéliens, et ce pour une totale application des valeurs 
et textes applicables » 

Depuis le 1er janvier 2014, l’Union Européenne exclut de la coopération avec 
Israël les institutions et entreprises israéliennes ayant des activités dans 
les territoires occupés par Israël depuis 1967 (Cisjordanie, Jérusalem-Est, 
bande de Gaza, Golan). 

Toute subvention ou investissement financier, attribution de salaire, de bourse 
ou de prix par des agences ou fondations européennes à des groupes israéliens 
liés directement ou indirectement aux colonies est désormais prohibé. 

Lignes Directrices publiées au JO de l’EU, 19.07.2013 



