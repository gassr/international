
.. index::
   pair: Libertaires; Faranches
   ! Faranches
    
.. _faranches:
   
=================================
Faranches
=================================

.. seealso:: http://www.les-renseignements-genereux.org/collections/faranches/

.. contents::
   :depth: 3
   

Contact : faranches@gresille.org
================================

:courriel: faranches@gresille.org

Présentation
============

.. seealso::

   - http://www.les-renseignements-genereux.org/ressources/14712?themeId=6342

::   

	Sujet: 	[Fac38-enlutte] Apéro de rentrée Faranches 25/09
	Date : 	Fri, 21 Sep 2012 17:49:11 +0200
	De : 	Faranches <faranches@gresille.org>
	Répondre à : 	S'organiser pour le mouvement étudiant/social à Grenoble <fac38-enlutte@boum.org>
	Pour : 	<fac38-enlutte@boum.org>


Bonjour !

Faranches est une coopérative libertaire d'idées et d'actions, basée à
Grenoble.

Pour raconter le bilan de l'année écoulée, présenter nos projets de l'année 
qui commence, et partager un moment convivial,

Nous vous invitons à un apéro-dînatoire de rentrée (et oui c'est la période)

LE MARDI 25 SEPTEMBRE 2012 A 20H A ANTIGONE !


.. toctree::
   :maxdepth: 3
   
   presentation

   
Activités
=========

.. toctree::
   :maxdepth: 3
   
   2012/index
   
   
   
   
   

