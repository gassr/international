
===========================
Qu'est­ce qu'une faranche ?
===========================

En 1343, une cinquantaine de communes du Haut­Dauphiné (grosso modo le 
Briançonnais,  le Queyras, le Val Varache, le haut Val de Suse et 
le Val Cluson) ont acheté leur liberté au  Dauphin  et   se   sont  fédérées.   

Elles   ont  contracté   des  Chartes  garantissant   leurs  droits  et 
limitant   l’arbitraire   féodal,   se   sont   administrées   elles­mêmes   
et   ont   mis   en   place   des  institutions originales pour l’époque. 

La Révolution française sonnera leur fin.

Ces communautés n'incarnent certes pas les modèles de société auxquels nous 
aspirons  aujourd'hui   :   ce   ne   sont   pas   à   proprement   parler   
des   communes   autonomes   ou  autogérées et la liberté n’est à entendre 
qu’au sens fiscal et foncier. Tout n’était pas idéal  dans le fonctionnement, 
tout de même hiérarchisé.

Aussi imparfaites qu’elles furent, ces communes naquirent de la prise de conscience de 
leurs intérêts propres, permirent la libération du servage et furent porteuses de valeurs 
de solidarité et de mutualisation des moyens matériels. L’habitude séculaire d’assemblées 
générales et de débats sur la place publique, convocables par n’importe qui, est peut­être 
un prélude à la démocratie directe...

Nous avons choisi de nous réapproprier le nom de l’une des plus fameuses et 
constantes  d’entre elles, la « Faranche » de Villar d’Arène , qui signifie 
« affranchie », « libre ». 

Nous  l’accordons   au   pluriel,   comme   autant   de   modèles   de   
sociétés   qui   sont   encore   à  imaginer.... Il est en effet important 
pour nous de puiser partout, y compris dans le passé  de notre région, y 
compris dans les mêmes montagnes qui nous escortent, les volontés et 
les   exemples   dynamiques   d'autonomie   sociale.   

Alors   que   de   ces   longs   siècles,   nos  manuels scolaires ne nous 
content que les Rois et les seigneurs. 

Que fleurissent les Faranches libertaires !

Grandes orientations
====================

(texte adopté à l'AG du 2 février 2010)
A l'automne 2008, ça faisait déjà plusieurs mois que certain.e.s d'entre nous, 
activistes de la nébuleuse  libertaire et féministe, échangions en privé des 
frustrations sur notre manière de lutter. Nous avions le  sentiment de tourner 
un peu en rond. « Engagements fragiles et éphémères ; militantisme compulsif, 
défensif et morcelé, dicté par le calendrier des puissants ; manque de 
stratégies pensées sur le long terme, d’où un essoufflement ; idées ­slogans 
brandies comme des évidences, doutes interdits au risque de se 
sentir traitre.sse.s ; 

difficultés pour les sympathisant.es à percer la coquille d’un « milieu » fermé 
et parfois  perçu comme prétentieux ; tendance au pessimisme voire au fatalisme ; 
déprimes récurrentes... » 

Un peu  démoralisé.e.s,   nous   avions   l'impression   que   ces   critiques   
étaient   partagées   par   beaucoup   de   nos  camarades,   mais   personne   
évidemment   n'avait   de   recettes   lumineuses   pour   sortir   de   ces   
pièges   de  l'activisme. On ne trouve pas de recettes lumineuses tout.e seul.e.
 
Néanmoins nous avons continué à en parler, dans des cercles de plus en plus 
grands. Des textes ont  commencé à circuler là ­dessus, plus ou moins adroits, 
plus ou moins bien reçus. Et nous nous sommes  finalement décidé.e.s à lancer 
un groupe qui prenne ces questions au sérieux. Soit nous nous fossilisions, 
soit nous nous donnions les moyens de nous mettre en recherche. Et puis, faire 
exister un tel groupe  c'était aussi lancer un appel à nos camarades : ces 
questionnements n'allaient plus être partagés dans le  cynisme d'une bière au 
bar alternatif, mais inscrits au fronton d'un collectif, et nous pouvions nous 
fédérer  sur ces bases, inventer ensemble. 

Bienvenue. 

Pendant  plusieurs  mois,   cette  intention  a   donné  lieu  à   une  sorte  d'ambiguïté.  Certes,  nous   étions  un 
groupe de réflexion, qui voulait enfin donner du temps à l'analyse de nos modes de lutte et des raisons de 
nos essoufflements, à l'invention de nouvelles pistes possibles. Mais nous ne voulions pas être juste des « 
consultant.e.s ès luttes » : fidèles au principe de praxis révolutionnaire, nous voulions aussi mettre nos 
idées en pratique, vérifier ce qu'elles donnaient, les modifier en retour, et ainsi de suite.  Nous allions 
réfléchir ensemble sur nos manières de nous organiser... et simultanément, de fait, nous allions 
nous organiser. 
Nous étions réuni.e.s par le besoin de nous organiser « autrement », « mieux ». Pas dans un parti pour 
autant... Nous ne partageons pas, faut­il le rappeler, l'objectif principal des partis politiques : la prise du 
pouvoir   institutionnel,   le   pouvoir   d’Etat   (pays,   région,   département,   mairie,...).   Nous   sommes 
nombreux.ses à avoir été échaudé.e.s par ces militant.e.s d'un parti qui débarquent comme des vautours 
dans des luttes au moment où elles deviennent à la mode, en briguent les postes  importants, puis les 
quittent après un ou deux recrutements, quand une autre « urgence sociale » et surtout médiatique (par 
exemple,   les   élections)   pointe   le   bout   de   son   nez   ailleurs.   Nous   sommes   nombreux.ses   à   avoir   été 
dégouté.e.s par le zèle des centrales syndicales à dégonfler les grandes manifestations « unitaires » plutôt 
qu'à renforcer les solidarités entre des travailleurs et travailleuses qui, pour beaucoup, bouillent d'envie de 
sortir des retranchements « corpo ». 
Par   contre,   la   question   des   organisations   politiques,   libertaires   notamment   (FA,   OLS,   OCL...),   restait 
ouverte parmi nous. En fait nous ne voulions pas reproduire la distinction binaire entre les partisan.e.s de 
ces organisations (« les organisé.e.s ») et les autres (« les autonomes »). Nous commençons à comprendre 
ce que, de chacune de ces expériences, nous voulons garder ou abandonner. 

Dépasser les orgas 
==================

Plusieurs d'entre nous se sont déjà investi.e.s dans des organisations anarchistes. Pour cultiver des liens 
stables entre camarades, assurer un ancrage durable là où l'on vit, construire des pratiques à une échelle 
géographique élargie, mettre des outils en commun et les coordonner  (journal,  librairie, radio)... Les 
orgas semblaient être des formes plus puissantes, plus efficaces pour intervenir dans la société. 
Mais d'autres parmi nous, qu'on a nommé.e.s « autonomes », se sont plutôt investi­e­s dans des collectifs 
affinitaires, des lieux alternatifs de vie et d'activités, qui fonctionnaient en réseaux : en solidarité avec des 
sans­papiers, en s'opposant au « Grand stade », en intersquats... Ils et elles avaient l'impression que les 
orgas, y compris anarchistes, étaient prises dans des enjeux de pouvoir et de compétition, préférant trop 
souvent se renforcer plutôt que de se mettre au service des luttes.

Dans Faranches, nous gardons notamment des réseaux autonomes  ce penchant pour les connexions 
plutôt que pour les querelles de chapelle. Nos toutes premières actions en tant que groupe étaient 
commandées par le besoin de convergence des luttes. D'emblée, nos premières commissions (alternatives, 
centre   social   autogéré,   en­quête)   se   formaient   dans   l'espoir   d'aider   la   mutualisation   des   forces   d'une 
peuplade rebelle bien plus large que notre propre groupe, et la circulation de la parole au­dessus des 
murs des ghettos sociaux et militants. Notre première intervention réfléchie dans un mouvement social, 
l'an dernier, a essayé d'alimenter la dynamique « inter­luttes » qui soutenait toutes les velléités opposées 
aux divisions maintenues par les directions syndicales. 

Aujourd'hui Faranches regroupe par ailleurs des militant.e.s investi.e.s sur diverses problématiques : luttes 
féministes,   du   logement,   anti­coloniales,   de   soutien   aux   sans­papiers,   syndicales,   paysannes,   médias 
alternatifs, lieux autogérés, squats,... 

Ses   membres   vivent   seul.e.s,   en   collectifs   ou   en   famille,   
sont   étudiantes,   charpentières,   animateurs,  randonneuses   passionnées,   
chômeurs,   profs   et   chercheuses,   pions,   éboueurs,   rédactrices   de   fanzines, 
speakers dans des radios associatives, agriculteurs, factrices... 

Un tel alliage de terrains de vie et de lutte nous aide à résister à l'idée 
d'un modèle unique du bon militant  et  de  la   bonne  stratégie.   Nous   
croyons  dans  la  diversité  des   tactiques  et  nous  voulons   penser  leur 
coalition dans une visée politique commune. 
­
L'autre point fort des dynamiques autonomes que nous avons connues dans notre 
ville, c'est l'attention  qu'elles donnent à des questions habituellement 
considérées comme  « personnelles ». Nos éventuels soucis d'argent, psychiatriques, 
familiaux ou médicaux ont timidement  commencé à être divulgués pour que cesse 
l'illusion que ces affaires étaient individuelles alors qu'elles  sont souvent 
l'expression la plus douloureuse d'un ordre social. 

Nous avons commencé à considérer que la 
solidarité entre nous n'avait pas à s'arrêter au seuil de nos appartements. Les projets de squats ont été une 
manière parmi d'autres de tenter de briser ces séparations, d'expérimenter des vies moins privatisées. Sur 
cette même lancée, la commission alternatives de Faranches cherche aujourd'hui à renforcer toutes les 
propositions d'entraide concrète, matérielle, alimentaire, hors des circuits du profit et du pouvoir. 

Dans   le   même   ordre   d'idées,   si   des   personnes   dans   les   collectifs   autonomes   ont   des   comportements 
écrasants, humiliants, paternalistes, sexistes, bref si les rapports de pouvoir et d'oppression existent « 
entre camarades », nous avons cessé de fuir la question : nous avons commencé à répondre. Il s'agit 
d'avoir des actes et des attitudes cohérentes avec nos valeurs, d'oser se remettre en question, d'oser se « 
dé/construire », c'est­à­dire faire un travail personnel pour modifier les normes que nous avons tous et 
toutes plus ou moins intégrées dans nos imaginaires et  nos  «  spontanéités ». Beaucoup d'écrits et de 
tentatives concrètes se sont accumulées autour de tous ces questionnements dits « internes ». Nous avons 
des acquis, nous sommes en chemin, mais nous sommes également lucides : la route est longue. Nous ne 
perdons pas de vue le travail encore nécessaire à abattre sur ces questions. 

Divers courants féministes ont eu un rôle majeur dans ce souci­là, et leurs victoires les plus visibles sont 
sans doute l'existence de groupes de femmes et la proportion plutôt équivalente de femmes et d'hommes 
dans les collectifs autonomes grenoblois aujourd'hui. Alors que dans les organisations même libertaires, 
les femmes, trop souvent réduites à des rôles de « copines de » ou de petites mains, ne constituent en 
général   qu'un   quart   des   effectifs   au   mieux,   et   que   plus   globalement   dans   notre   société   elles   sont 
largement découragées de prendre part aux affaires publiques et politiques. 

De tout ce travail des autonomes et des féministes sur les rapports sociaux, Faranches hérite des axes 
politiques précieux. A notre avis, aucune organisation anarchiste en France n'accorde suffisamment de 
place à ces questions­là. La confiance, l'écoute, la solidarité concrète, la manière dont la parole circule 
entre nous, sont au centre d’un projet politique où la qualité des rapports sociaux est en jeu, et pas 
seulement « après la révolution ». Au sein de Faranches, différents outils tentent de répondre à cette 
nécessité : notre charte de parole, le fait qu'une réunion sur quatre soit en non­mixité, le projet d'une 
nouvelle commission sur la gestion des conflits. Chacun de ces outils ou presque a été mis en place suite à 
des difficultés internes que le groupe a rencontrées concrètement, et qu'il a tentées de prendre à bras­le­
corps plutôt que de les enfouir. 


Dépasser les réseaux 
====================

Ce   n'est   donc   pas   un   hasard   si   une   bonne   partie   d'entre   nous   ont   grandi   plutôt   dans   les   réseaux 
autonomes. Nous savons qu'ils ne fonctionnent pas avec « moins » d'organisation que les orgas politiques 
traditionnelles.  Le temps qu'ils  ne gaspillent pas dans  le marketing militant, ils l'emploient souvent  à 
approfondir les domaines relationnels et théoriques, ou à imaginer des actions ingénieuses. 
Si aujourd'hui nous remettons en cause certains aspects de ces « marmites autonomes », ce n'est sûrement 
pas dans une envie de revenir en arrière mais plutôt de continuer à chercher. Nous voulons continuer nos 
explorations politiques, garder le meilleur de ce que nous avons appris et aller plus loin, porté.e.s par la 
même sincérité qui avait pu amener certain.e.s d'entre nous dans ces réseaux jusqu'à présent. 

Nous   avons   notamment   besoin   de   liens   plus   solides   entre   nous,   envisagés   sur   le   long­terme   :   des 
engagements. Parce que les réseaux entretiennent l'idée d'une pseudo­liberté individuelle, qui aboutit à 
une forme d'union tellement évanescente qu'elle finit par nous déstabiliser. Ensuite, il nous semble y avoir 
une contradiction entre le côté identitaire de notre « milieu » (codes de langage, normes vestimentaires et 
idéologiques) et en même temps notre répugnance à nous voir défini.e.s de l'extérieur justement comme 
un milieu homogène (« on n'est pas tous pareils ! »). Difficile d'admettre qu'il existe entre nous une forme 
de pression psychologique,  souvent inconsciente, pour correspondre  à un même  modèle.  Face à cette 
contradiction, l'une de nos hypothèses, c'est qu'il faut rendre les liens entre nous plus conscients, plus 
clairs, pour être à la fois prêt.e.s à les assumer, à la fois capables de les transformer davantage. Des liens 
plus   clairs   nous   paraissent   suffisamment   rassurants   pour   que,   paradoxalement,   nos   individualités   se 
sentent, à partir de là, libres de s'épanouir dans toute leur singularité : plus besoin de vérifier à chaque 
instant qu'on est bien « dans le coup », on relâche la pression et on se marre. 

Nous donnons donc dans Faranches de la place à ce besoin de liens politiques forts et clairs. Ca change 
des choses à nos habitudes militantes, à commencer par l'impression d'être moins les consommateurs et 
consommatrices atomisé.e.s et frénétiques du débordant agenda des luttes : pas besoin d'être partout à la 
fois quand nous sommes plusieurs et quand nous pouvons nous relayer auprès des faits politiques qui 
grouillent   dans   notre   ville.   Nous   aspirons   à   redécouvrir   l'intérêt   de   se   sentir   membres   d'une   force 
collective. 

­D'ailleurs, l'engagement dans la durée au sein du groupe est important. Mais là encore, plus qu'un effort, 
il s'agit sans doute d'un besoin. Nombre de collectifs auxquels nous avons participé dans le passé, fidèles 
aux principes flexibles des réseaux, se sont effilochés dès que les moments chauds de la lutte étaient 
terminés, et nous n'en sommes pas sorti.e.s indemnes. Beaucoup d'entre nous ont alors éprouvé le désir 
d'une structure qui sache se maintenir en place y compris dans le froid des temps ordinaires. Parce que ces 
moments plus calmes sont en réalité des respirations à saisir pleinement pour se préparer aux prochains 
mouvements   sociaux   :   si   nous   ne   les   saisissons   pas,   nous   sommes   perdu.e.s,   toujours   surpris.es   et 
dépassé.e.s par la vitesse des événements. Nous faisons donc le pari de tirer profit des moments froids. 
Par exemple en travaillant l'auto­formation, que ce soit en partageant nos expériences, en analysant un 
fait d’actualité, en revisitant l’histoire des luttes. 
­
Une dernière limite que nous avons éprouvée dans les milieux autonomes, c'est la réticence à se projeter 
au­delà des échelles affinitaires, marginales. Trop souvent par exemple, le regard exigeant, « radical » 
(qu'est­ce que la radicalité ?), posé sur les pratiques et les discours de chacun­e nous a paru prendre le 
pas sur la volonté de rencontrer d’autres milieux et personnes. Il est important pour nous de nous frotter 
davantage   à  une   dimension   publique,   et   de   pouvoir   ouvrir   nos   portes   à   n'importe   qui,   sur   la   base 
d'énoncés politiques partagés. Nous voulons apprendre à nous organiser à plus de 15, chiffre auquel la 
plupart de nos collectifs ont plafonné. Dans les rares moments où nous avons été impliqué.e.s dans des 
dynamiques de plus grande ampleur, contre­sommets, mouvements sociaux comme l'entre­deux tours ou 
le CPE, nous avons entrevu des questions d'organisation que nous voulons maintenant creuser. Qu'est­ce 
qu'une   réelle   autogestion   à   plus   grande   échelle   ?  Nous   avons   besoin   de   nous   confronter   à   cette 
question pour avancer sur nos projets de société, et pour avoir en stock des propositions fortes pour les 
prochains mouvements sociaux. 

Car nous tenons à une visée politique globale. Encore un point qui nous a laissé.e.s sur notre faim dans 
certains   collectifs,   où   l'on   avait   le   mérite   de   créer   des   refuges   et   des   foyers   de   liberté,   mais   où   la 
construction d'un changement social était au pire un tabou, au mieux une question secondaire. Nous 
avons   besoin   de   remettre   ces   questions­là   sur   la   table   :   quelles   stratégies   ?   Et   comment   nourrir 
aujourd'hui, après les échecs des expériences révolutionnaires, les débats et les ambitions d'un projet de 
société révolutionnaire qui tienne la route ? Nous avons choisi d'avoir de l'espoir. Nous voulons renforcer 
notre capacité à intervenir, et peser sur la vie sociale, ne serait­ce qu'au niveau local. Nous ne voulons 
pas nous habituer à ce que les libertaires soient seulement le poil à gratter de la gauche de la 
gauche. 

Fonder une organisation ? 
=========================

Quand nous parlons alors de nous organiser autrement qu'en réseau, de manière plus conforme à nos 
objectifs,   sans   pour   autant   reproduire   les   défauts   des   organisations   militantes   traditionnelles,   nous 
sommes en pleine mer. En pleine création politique. Aucune boussole, aucun modèle à l'horizon ne nous 
semble réunir ce que nous souhaitons, et quand nous en discutons avec d'autres camarades libertaires 
organisé.e.s, nous voyons bien que nous sommes des OVNIs. Parfois nous nous sentons perdu.e.s. Mais 
quand nous prenons patience, nous pouvons aussi ressentir le frisson de l'aventure. 

Allons­ nous fonder une nouvelle, une énième « organisation » ? Dans une ville comme Grenoble où il 
n'existe plus aucune autre organisation que des partis, des syndicats et des collectifs concentrés sur un 
seul thème (sans­papiers, transports gratuits, logement, féminisme, Minatec, etc.)... 

Dans une ville où le  dernier bout d'orga anar, le groupe local de la 
Fédération Anarchiste, est mort de sa belle mort il y a trois  ans... 

Où un mouvement autonome tonitruant a fleuri pendant le CPE, semblant installer 
pour de bon une  (saine) méfiance pour les appareils politiques 
institutionnalisés... Si notre but est d'être aussi ouvert.e.s  que possible, 
ne devons­ nous pas trouver une formule revisitée et plus appropriée que ces 
bannières  militantes communément discréditées ? 

Quand on nous dit « je veux m'engager politiquement, mais je 
veux pas faire partie d'un truc trop structuré, genre petit parti... », que pouvons­nous répondre ? « T'en 
fais pas, chez nous tu seras plus libre qu'ailleurs, parce que notre orga elle est plus mieux que les autres 
» ? Ces interrogations sont toujours en suspens pour nous. Ce qui est sûr, c'est que nous cherchons à 
construire une structure plus précise et plus stable qu'un réseau. 
Après, quelle pourrait être exactement cette structure ? Une « organisation », une « fédération », une « 
coopérative », une « assemblée » ? C'est le sujet de nos recherches actuelles, qui nous amènent à nous 
renseigner sur différents modèles existants ou possibles. De toutes façons nous ne comptons pas trouver 
une forme d'organisation « finale », parce que pour nous toute expérience libertaire repose justement sur 
ce processus infini de questionnements et d'exploration : sur cette question de l'organisation sociale qui 
n'est jamais bouclée, toujours ouverte. Nous nous trouvons donc devant une tension inévitable et vitale : 
entre  la   nécessité   de   rester   toujours   en   mouvement,   en   chantier,   et  celle   d'instituer  une   forme 
d'organisation claire, ne serait­ce que provisoirement, pour arriver à exister et à être accessibles. 


Fonctionnement
==============

(en résumé)
Le fonctionnement de Faranches est évolutif. Un document détaille ce 
fonctionnement : il présente la liste des tâches   qu'on   a   
repérées   comme   nécessaires   dans   le   collectif.   Les  visibiliser   
aide   à   les   prendre   en   compte   réellement   et   à   penser   leur   
rotation   entre   les  membres.

Faranches a aussi sa « charte de parole ». Ce texte propose un cadre pour avancer vers des 
rapports égalitaires et une ambiance sécurisante dans la circulation de la parole entre nous. 

Ce travail­ là exige une disponibilité qui entre souvent en contradiction avec les urgences et 
les productivismes militants : nous voulons apprendre à lui donner plus de place. Ainsi, par 
exemple, les formes de nos réunions sont l'objet d'attentions et d'expérimentations. 
Toute personne qui est d'accord avec nos grandes orientations, notre charte de parole et les 
détails de notre fonctionnement actuel est la bienvenue dans Faranches. 

Les réunions de Faranches sont hebdomadaires. 

Elles comportent en général au moins un  tour   d'information   générale   et   
locale.   Puis   elles   se   transforment,   selon   les   soirs,   en   AG 
décisionnelle, en réunions de commissions, ou en réunions non­mixtes.

Les   commissions   sont   une   instance   de   travail   :   quand   l'envie   
d'agir   dans   telle   ou   telle  direction est évoquée dans Faranches et 
adoptée, une commission se crée pour concrétiser  cette   intention.   

Pour   le   moment   il   existe   cinq   commissions:   

- stratégies,   
- enquêtes, 
- alternatives, 
- revue de presse 
- et groupe de parole.

Chaque mois ont lieu une ou deux journées de discussions approfondies, 
d'auto­formation,  et/ou de débat sur un projet de société.

Les   personnes   membres   de   Faranches   sont   libres   de   choisir   
leur   niveau   d'implication  (présence   aux   réunions,   participation   
aux   commissions...),   du   moment   qu'elles   en  préviennent le groupe.

Les membres assurent de manière volontaire et tournante différentes tâches: 

- préparation  des   AGs,   trésorerie,   
- communication   interne   et   externe,   
- soin   du   climat   général   dans   le  groupe,...
 
Nous aspirons à nous organiser: 

- sur le long terme
- de manière visible et ouverte à tout.e un.e chacun.e sur la base d’énoncés 
  politiques clairs
  
  
- pour construire un projet de société et le vivre dès maintenant
- dans une dynamique positive, offensive et choisie


− en assumant de lutter contre le système dans sa globalité sans viser la  prise du  pouvoir



− en donnant de la vie et de la place à l’activité de la pensée, pour renouveler nos
  stratégies, nos perspectives politiques et notre projet de société, en interaction avec nos actions
  
  
− en   rencontrant   d’autres   milieux   sociaux,   d’autres   groupes,   collectifs,   et 
  organisations, en se donnant la peine de se confronter à leurs arguments, pour
  travailler ensemble, de manière ponctuelle ou durable
  
  
  
− en   accordant   de   l’importance   au   développement   d’outils   et   de   méthodes   de
  travail,   utilisés   en   interne   et   diffusés   vers   l’extérieur,   qui   permettent   la
  participation de tou.te.s, quel que soit son bagage militant.
