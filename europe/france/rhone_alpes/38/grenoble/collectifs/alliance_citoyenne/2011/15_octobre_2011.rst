


.. _alliance_citoyenne_15_octobre_2011:

==================================================================================================
15 octobre 2011 action de l'Alliance Citoyenne devant l'ESPACE 600, la Villeneuve, FR 3 JT Local
==================================================================================================

.. seealso:: http://www.youtube.com/watch?v=zbCyu-hYhz4

A la suite d'une négociation avec l'ESPACE 600 qui a aboutit à la prise en 
compte des 12 revendications de la campagne "se réapproprier les équipements 
et les espaces publics", une action a rassemblé une trentaine de personnes au 
PATIO de la Villeneuve alternant discours sur la campagne citoyenne, musiques, 
percussions...
