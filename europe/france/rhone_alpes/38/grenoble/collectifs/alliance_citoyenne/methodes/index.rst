
.. index::
   pair: community ; organizing
   pair: Saul Alinsky  ; Alliance citoyenne
   
.. _ac_methode_saul_alinsky:

=================================================
Alliance citoyenne : Les méthodes de Saul Alinsky 
=================================================

.. seealso::
 
   - :ref:`saul_alinsky`
   - http://projet-echo.org/node/6
   - http://www.lesinfluences.fr/Saul-Alinsky-de-la-sociologie-d-Al.html
   - :ref:`alliance_citoyenne_11_mai_2012`
   - :ref:`alliance_citoyenne_4_decembre_2012`
   - http://www.citizensuk.org/
   - :ref:`festival_entropie_14_octobre_2012_atelier_14h`


.. contents::
   :depth: 3

Introduction
============

.. seealso:: http:://www.citizensuk.org/


Pour mener à bien son projet, Echo s'inspire des expériences américaines et 
londoniennes (London Citizens ) de **community organizing** héritées du 
sociologue américain :ref:`Saul Alinsky <saul_alinsky>`. 

L'un des défis de notre projet est d'adapter ces méthodes d'organisation 
citoyenne au contexte français.



L'approche relationnelle
=========================

Le cœur de ce travail consiste pour les organisateurs de cette association à 
aller à la rencontre des personnes impliquées dans la vie de leur quartier 
faisant partie, ou non, de groupes formels ou informels (tel un syndicat de 
locataires, un groupe de parents d’élèves, les habitants d’un quartier...) 
et d'échanger avec elles individuellement. 

Ces échanges facilitent la création de liens de confiance pour construire 
ensemble une Alliance Citoyenne. Ils permettent de faire émerger les 
préoccupations et les indignations des personnes rencontrées. 

Il s'agit de repérer les problèmes que rencontrent les habitants dans leur 
quotidien sur lesquels il est nécessaire d'agir collectivement si l'on souhaite 
les résoudre.

A partir de ces multiples rencontres individuelles, les organisateurs ont 
pour fonction de mettre en relation les personnes aux motivations et intérêts 
proches, de les aider à s'organiser afin de traiter les problèmes et les 
indignations qu'ils ont en commun. 

Des rencontres collectives rassemblant des habitants et des associations de la 
métropole grenobloise sont organisées régulièrement. Ces rencontres permettent 
également de créer des relations entre les habitants de l'agglomération qui 
malgré leurs intérêts et préoccupations communs n'avaient jusqu'à là, pas 
toujours eu les espaces pour se rencontrer et échanger. 

Une grande assemblée a eu lieu le 21 avril 2011 réunissant une centaine de 
personnes. 

Le 1er décembre 2011, une seconde assemblée avec 200 personnes a permis 
de lancer le processus de création de l'Alliance Citoyenne et de présenter 
les campagnes en cours. 

Celle :ref:`du mois de mai 2012 <alliance_citoyenne_11_mai_2012>` a permis de 
construire et valider collectivement  les mode de fonctionnement d'une telle Alliance. 

A venir, la grande Assemblée Fondatrice de l'Alliance Citoyenne :ref:`en décembre 2012 <alliance_citoyenne_4_decembre_2012>`.

La naissance de l'Alliance Citoyenne s'appuie sur des personnes repérées comme 
moteurs (leaders) dans leur quartier ou leur association, capables de porter 
la dynamique de campagnes et d'actions collectives. 

A cet effet, l'association Echo propose à ces personnes des formations de 
leadership permettant de leur faire acquérir des méthodes d'organisation, de 
stratégie d'actions collectives et de négociation ainsi que des moyens de 
renforcer les liens et la participation des personnes au sein de leurs propres 
collectifs et ainsi faire émerger, à leur tour, d’autres leaders.

Les campagnes de l'Alliance
===========================

Echo s'appuie sur une conviction importante : la justice et la transformation 
sociale passent par de l'action collective et la capacité du plus grand nombre 
à défendre les intérêts collectifs. 

L'alliance en construction cherche ainsi à mener des campagnes sur des 
problèmes soulevés lors des rencontres individuelles et détermine les 
responsables qui ont un pouvoir de décision sur ce problème. 

Les rencontres collectives d'habitants et d'associations ont pour objectif de 
présenter des campagnes d'actions collectives possibles. 

Ensemble, nous décidons sur quoi agir, quelles actions mener et quand. 

L'association se propose alors comme un soutien et support stratégique à la 
mise en œuvre de ces campagnes et de ces actions non violentes afin d'obtenir 
la résolution des problèmes grâce à l'obtention de temps de négociation avec 
les responsables des institutions décisionnelles. 

Autres explication de la méthode dimanche 14 octobre 2012
=========================================================

.. seealso:: :ref:`festival_entropie_14_octobre_2012_atelier_14h`




