



.. _alliance_citoyenne_4_decembre_2012:

======================================================================
Assemblée fondatrice de l'Alliance Citoyenne le mardi 4 décembre 2012
======================================================================

L'Assemblée fondatrice de l'Alliance Citoyenne le mardi 4 décembre 2012 !

Pour continuer et pérenniser cette dynamique, cette année 2012 verra la 
création effective de l’Alliance Citoyenne de l’agglomération de Grenoble.  

Les statuts et le mode de fonctionnement sont en cours de construction. 

Plusieurs associations nous ont déjà dit leur envie de devenir les alliés 
fondateurs de cette Alliance Citoyenne allant de l’association des Guinéens 
de l’Isère aux Associations Familiales Protestantes.

Lors de cette assemblée du mois de décembre seront présentées les premières 
organisations alliées. Nous attendons environ 300 invités. Elle sera l’occasion 
de fêter la création d’une telle Alliance inédite en France et fêter l’avancée 
des différentes campagnes.


