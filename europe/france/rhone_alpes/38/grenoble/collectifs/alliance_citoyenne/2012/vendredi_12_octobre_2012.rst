



.. _alliance_citoyenne_12_octobre_2012:

===========================================================
Vendredi 12 octobre 2012 14h Atelier // Mouvements Citoyens
===========================================================

.. seealso:: 

   - :ref:`festival_entropie_12_octobre_2012_atelier_14h`

**5 tables de discussion** autour d’expériences d’organisation citoyenne.

Rencontrons ses représentants et découvrons les processus suivis par:

- l’Alliance Citoyenne  (Grenoble)
- Antigone (Grenoble)
- ReAct
- Reclaim the Streets, 
- l’Armée des clowns,
- le Laboratoire de l’imagination Insurrectionnelle, 
- le CEIMSA, le SNESup et les IATOS des Universités Grenobloises.

- Pourquoi se sont-ils organisés ?
- Comment l’ont-ils fait ?
- Comment prennent-ils leurs décisions ?
- Quels sont les bénéfices de cette démarche ?
