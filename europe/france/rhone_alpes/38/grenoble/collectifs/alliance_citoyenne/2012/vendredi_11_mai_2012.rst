



.. _alliance_citoyenne_11_mai_2012:

===============================================================
Assemblée constituante du 11 Mai 2012 : un défi démocratique !
===============================================================

.. seealso:: http://projet-echo.org/node/60

Tags 
====

alliance citoyenne, Saul Alinsky, Grenoble, echo, démocratie participative, 
community organizing

Une centaine de personnes, représentant une cinquantaine d’associations 
différentes, étaient réunies à l’assemblée citoyenne du vendredi 11 mai 2012 
pour définir collectivement le fonctionnement de la future Alliance Citoyenne. 

Ce projet est initié par l’association ECHO depuis septembre 2010 à partir des 
méthodes du « community organizing » de Saul Alinsky.

Pour ce faire, un groupe « les Sherpas » avait été mandaté à l’assemblée 
du 1er décembre 2011 pour élaborer des propositions de statuts pour 
l'Alliance Citoyenne. 

Tour à tour, Mayliss, Mathieu, Pierre, Aliou et Nathalie se sont saisis du 
micro pour exposer les propositions qui avaient fait consensus au sein de 
leur groupe. 

L’assemblée allait-elle valider les suggestions avec la même approbation ? 

Un véritable challenge démocratique a été tenté ce soir là !

Les participants, reparti autour de tables colorées, ont discuté les 
propositions concernant les finalités de l’Alliance, le mode d’adhésion des 
organisations, le pilotage de l’organisation et les futures ressources 
financières. Le débat a ensuite laissé place au vote : un carton vert pour 
valider la proposition ou un carton rouge en cas de désaccord. 

Pour compléter et préciser ce vote, les participants étaient invité à écrire 
derrière leurs cartons, les raisons de leur choix.

L’ensemble des propositions a été validé par au moins 90% des participants. 
Les finalités de l’Alliance à 100% ; le mode d’adhésion des organisations et 
leur pouvoir à 94% ; le pilotage de la stratégie globale de l'Alliance et des 
campagnes à 90% ; et les ressources financières de l'Alliance à 90%. 

Un plébiscite de l'important travail mené par le groupe des Sherpas entre 
janvier et mai 2012.

Mais un autre exercice démocratique, plus difficile, attendait la centaine de 
participants pour la seconde partie. Il s’agissait de définir un mode de 
désignation des administrateurs en préservant la diversité de 
l’Alliance Citoyenne. 

Sur fond sonore de la bande originale de la guerre des étoiles, un dispositif 
d’animation original « en galaxie » était prévu pour créer les conditions 
d’un consensus. Une première proposition a été construite et amendée 
par 83% des votants pour la liste des candidats. 

Mais la question complexe du mode de désignation des administrateurs n’a 
pas abouti à un consensus. A défaut d’un choix définitif, 3 propositions 
concrètes ont émergé qui devront être retravaillées d'ici l'assemblée 
fondatrice.

Pour finir, un calendrier a été présenté pour les mois à venir. 

Les participants ont été invité à discuter les futurs statuts de l’Alliance 
Citoyenne au sein du CA et de l’AG de leur organisation en vue de l’assemblée 
fondatrice de l’Alliance Citoyenne de Grenoble en novembre 2012. 

D’ici là un conseil d’administration mixte regroupant des membres d’Echo 
et des volontaires a été constitué. La conclusion de l'assemblée s'est 
faite par la présentation de campagnes en cours, le cœur de l’Alliance, 
avec Ibrahima qui nous a annoncé les victoires de la campagne pour les 
étudiants étrangers et Amèle qui a appelé à participer à l'action MC3 de 
la campagne de réappropriation des équipements publics.

Les volontaires pour le conseil d’administration intermédiaire (non définitif): 

- Habby Coulibaly, 
- Laura Perez Medina, 
- Agnès Helio Poulos, 
- Marion Grosset-Janin, 
- Pierre Lavigne, 
- Mathieu Ilunga, 
- Haitem Belgacem, 
- Sylvie Geoffray, 
- Nicolas Vanwingerden, 
- Nathalie Dubois, 
- Hélène Balazard (Echo), 
- Manuel Bodinier (Echo), 
- et avec voix consultative les salariés d’Echo : Adrien Roux, David Bodinier, 
  Solène Compingt.


