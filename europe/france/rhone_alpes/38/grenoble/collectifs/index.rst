
.. index::
   pair: Collectifs; Grenoble
   ! Collectifs Grenoble
    
   
.. _collectifs_grenoble:
   
=================================
Collectifs sur Grenoble
=================================


.. toctree::
   :maxdepth: 6
   
   alliance_citoyenne/index
   bds38/index
   faranches/index
   gregrecs/index
   
   
   

