
.. index::
   pair: Gregrecs; Grenoble
   pair: Gregrecs; Collectifs
   ! Gregrecs
    
.. _gregrecs:
   
=================================
Gregrecs
=================================

.. seealso:: 

   - http://gregrecs.wordpress.com/
   - :ref:`grece`

.. contents::
   :depth: 3
   


Courriel
========

- gregrecs@gmail.com

Présentation
============

Les Gregrecs est une nouvelle initiative des Grecs et Grecques de Grenoble.

Le groupe est indépendant des partis politiques et fonctionne de manière 
collective et horizontale sans représentant.

Notre objectif est l’information et sensibilisation des citoyens de la 
ville que nous habitons sur les événements en Grèce qui touchent non 
seulement le pays, mais l’Europe entière et qu’ils sont d’ailleurs 
souvent présentés de manière fragmentée dans les média. 

Par le biais de soirées thématiques et de débats nous souhaitons partager 
une autre réalité qui ne passe pas à la télé et encourager la 
solidarité internationale qui dépasse les frontières des pays.

   
Activités
=========

.. toctree::
   :maxdepth: 3
   
   2013/index
   
   
   
   
   

