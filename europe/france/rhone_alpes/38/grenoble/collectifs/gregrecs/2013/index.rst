

.. _gregrecs_2013:
   
=================================
Gregrecs 2013
=================================

.. seealso:: 

   - http://gregrecs.wordpress.com/

.. contents::
   :depth: 3
   

22 Janvier 2013: Le néofascisme aux temps de crise
===================================================

Notre première soirée aborde le sujet de la hausse des attaques racistes 
en Grèce comme phénomène résultant de l’austérité continue et de la 
montée du parti néo-nazi de l’Aube Dorée. 

La soirée commence avec une présentation de la situation actuelle, un 
historique et aboutit dans une discussion ouverte avec les participants.
   
   

