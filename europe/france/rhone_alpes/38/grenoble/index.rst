
.. index::
   ! Grenoble

.. _grenoble:

=======================================
Grenoble
=======================================


.. toctree::
   :maxdepth: 4

   associations/index
   collectifs/index
   festival_anarchiste/index
   medias/index
   quartiers/index
   squats/index
   
   
