
.. index::
   pair: Squats; 102
 
.. _le_102:
.. _le102:
.. _102:
   
=============================================
Squat Le 102 (rue d'Alembert 38000 Grenoble)
=============================================


.. contents::
   :depth: 3

Présentation
=============

En mai 1983, des hurluberlus ouvraient au pied-de-biche les bâtiments
situés au 102 rue d’Alembert. Trente ans plus tard, après avoir connu trois
maires de Grenoble, survécu à trois procès et vu se faire détruire une bonne
centaine d’autres squats, le 102 est toujours là, sur les mêmes principes
d’autogestion, de bénévolat, de refus de subventions et sur l’envie de faire
découvrir autre chose, autrement.

Autre chose. Le travail de recherche qui a lieu au 102 se concentre sur des
formes artistiques et politiques marginales, ou plutôt marginalisées. 
Musique improvisée ou électroacoustique, cinéma expérimental ou documentaire,
poésie sonore, formes graphiques DIY, critique sociale, cris de rage ou de
désespoir, expérimentations dans la gélatine de la pellicule ou l’émulsion de
la sérigraphie : ici on travaille le propos et sa mise en forme. 

Signe distinctif : une attirance pour le côté ludique et pratique, pour 
l’expérimentation et les manières de faire.
Autrement. Avec pour seuls carburants la passion des bénévoles et la
sueur de la récupe, le 102 rappelle cet idéal libertaire d’autogestion,
qu’on pourrait traduire localement par «  faire soi-même sans attendre les
institutions », comme par « tenter de produire ensemble quelque chose qui
nous ressemble » à moins que ce ne soit par « trouver un fonctionnement
non-commercial ouvert sur le monde ».

Trente ans plus tard, le 102 est-il devenu la caution « alternative » de la Ville
de Grenoble ? Quelle place reste-t-il pour les lieux qui ne rentrent pas dans
les cadres de l’institution ? Il faut dire que la pérennité du 102 ne s’est pas
faite sans anicroche : trois procès (1983, 1991, 1994), des flics, des huissiers,
des discussions, des banderoles, de la concertation. La force du lieu a aussi
été de trouver un terrain d’entente avec l’administration, qui a dû (pour une
fois) reconnaître le travail effectué sur le lieu. Les institutions apprendront-
elles un jour à oublier quelques instants ces formes de vie atypiques, en
décalage ou antagonistes avec la norme, en leur laissant une chance
d’expérimenter ?

Rendez-vous dans trente ans. En 2043, les soixante ans du 102 seront une
sacrée fête. « Soixante ans, soixante semaines » en sera le slogan mégalo.

Le lieu programmera du cinéma expérimental (en pellicule) et de la musique
improvisée (avec des instruments analogiques, voire acoustiques). 
La sérigraphie aura été rejointe par la typographie (et les peintures rupestres),
et les tentatives de normalisation du lieu auront été vaines. 
Avec le Pathé Chavant, le 102 restera l’un des deux lieux culturels de 
Grenoble.

En attendant, deux questions qui nous concernent. Est-on spectateur au
102 comme ailleurs ? Et en miroir : programme-t-on au 102 comme on le fait
ailleurs ? Les réponses sont entre nos mains et les vôtres. Nous espérons
qu’elles resteront négatives le plus longtemps possible.


Activités
=========

.. toctree::
   :maxdepth: 3
   
   2013/index
   
   
   
   

