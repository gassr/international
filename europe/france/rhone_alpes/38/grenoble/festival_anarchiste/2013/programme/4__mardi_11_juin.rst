
================================================================
Festival de l'anarchie,  Mardi 11 juin 2013, MJC Nelson Mandela
================================================================

.. contents::
   :depth: 3


14h Qu'apprendre de Mai 68 pour les luttes d'aujourd'hui ?
===========================================================

Un soixant-huitard vous parle. 

Il va vous pas vous expliquer, mais vous raconter le formidable tournant 
politique que ça a été pour lui et pour la société. 
Et aussi comment les portes que ce mouvement a ouvertes ont été brutalement 
refermées.


16h30 La 1ère internationale
=============================

L’Association Internationale des Travailleurs devait être le genre
humain selon la chanson d'Eugène Pottier. 

A travers cette conférence, nous vous proposons de parcourir son histoire, 
de croiser ses acteurs, de découvrir ses buts et voir quelles influences 
elle a pu avoir sur le mouvement anarchiste naissant.

20h30 Les hommes anarchistes et le patriarcat
==============================================

Exposé et discussion sur l'éventail de réactions des militants face à la 
critique féministe : antiféminisme, masculinisme, pro-féminisme... 
Et quelques pistes pour la suite.


