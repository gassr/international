
.. _festival_anarchie_9_juin_2013:

=========================================================
Festival de l'anarchie, dimanche 9 juin 2013, Antigone
=========================================================

.. contents::
   :depth: 3

14h Francisco Ferrer, une éducation libertaire en héritage
===========================================================

Une conférence/débat avec l'auteur, Sylvain Wagnon, pour redécouvrir la 
pensée libre de Francisco Ferrer, replacer sa réflexion et son action 
dans son contexte, au croisement de plusieurs histoires : celle de
l’anarchisme, de l’éducation libertaire mais aussi de l’éducation nouvelle.


.. _cnt38_9_juin_2013:

Dimanche 9 juin, 16H30 Se syndiquer, une stratégie révolutionnaire (CNT38)
==========================================================================

.. seealso::

   - :ref:`cnt` 
   
L'anarchosyndicalisme et le syndicalisme révolutionnaire aujourd'hui.
 
Autogestion des luttes, luttes pour l'autogestion, etc. 

Discussion, mêlant choix stratégiques, théoriques et pratiques, avec 
des militant-e-s de la :ref:`CNT38 <cnt38>`.


19h Apérock : Djoey Start et le Bouzouki Magique
================================================

Le Temps des Griottes, Politic Lovers in a Rêvbêtiko Style !


20h30 Transport et mobilité: (Im)Mobilisation générale !
=========================================================

Contre l’idéologie libérale de la circulation massive des marchandises, 
y compris humaines, contre l’étalement urbain et le triomphe de la bagnole, 
contre la grande vitesse et ses destructions, il est temps de réfléchir 
à l'impact de nos mobilités.

Une soirée proposée par la revue Offensive.

