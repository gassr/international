
==========================================================================
Festival de l'anarchie, Vendredi 14 juin 2013, Lokal autogéré et Antigone
==========================================================================

.. contents::
   :depth: 3


14h Local Autogéré - Atelier d'écriture
=========================================

Oubliez l’angoisse des dissertations et venez partager un moment
ludique pour exercer votre plume grâce à des contraintes simples et
accessibles. Une autre manière de dire et de penser ensemble le
monde que nous voulons.

20h30 Antigone - Soirée sans chef-fes
======================================

Sans chef-fes ? Chiche ! Un joyeux débat sur les théories et les 
pratiques anarchistes actuelles.
