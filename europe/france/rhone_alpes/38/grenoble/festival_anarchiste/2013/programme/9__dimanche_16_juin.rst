
.. _festival_anarchie_16_juin_2013:

==================================================
Dimanche 16 juin 2013, La malapprise et La BAF
==================================================

.. contents::
   :depth: 3


14H La Malaprise (30 rue Marbeuf, quartier des Eaux Claires) 
=============================================================

Balade féministe à Vélo

Deux guides improvisées et leurs complices vous escortent sur les routes
sinueuses d’un certain féminisme grenoblois pour re-connaître les lieux 
qui ont marqué leurs parcours... A vos pédales !

La BAF – Faisons tous ensemble
==============================

- Assemblées de quartier; 
- cabinets médicaux sociaux : l'autogestion comme alternative au niveau 
  de l'organisation de la société.

