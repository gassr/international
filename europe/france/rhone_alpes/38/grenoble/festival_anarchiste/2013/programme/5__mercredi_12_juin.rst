
================================================================================
Festival de l'anarchie Mercredi 12 juin 2013, Antigone et Parc de la Villeneuve
================================================================================

.. contents::
   :depth: 3


14h Le travail captif de l'emploi capitaliste : comment sortir de cette tenaille ?
====================================================================================

Comment transformer nos rapports d'échange sociaux : ceux où nous 
travaillons pour les autres et vivons du travail des autres ? 
Pourquoi aussi partout ce repli sur le chacun pour soi et le 
"on ne peut rien faire ?"

16h30 Antigone - Une histoire critique de la Justice
====================================================

Qui est sujet de droit?  Qui se cache derrière la notion de citoyen
soi-disant neutre et sans caractéristiques matérielles? 
Qui est protégé-e et de quoi?

20h30 La Villeneuve - Atelier chant, Concert participatif
==========================================================




