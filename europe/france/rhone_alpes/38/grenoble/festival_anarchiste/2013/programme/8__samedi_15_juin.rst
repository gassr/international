
.. index::
   pair: Alliance citoyenne; 15 juin 2013

.. _festival_anarchie_15_juin_2013:

==============================================================
Festival de l'anarchie, samedi 15 juin 2013, Chemin de Pinal
==============================================================

.. contents::
   :depth: 3

14h Chemin Pinal - Expériences anarchistes à grande echelle
===========================================================

Ukraine 1918 vs Catalone 1936
------------------------------

Les expériences anarchistes à grande échelle ne sont pas légions, nous
allons assister à un battle entre la Makhnovtchina de 1918 en Ukraine
méridionale et l'expérience catalane de 1936.

14h Col de Fau (Monestier de Clermont) - Vélorution
====================================================

Contre le projet inutile et néfaste de l'A51 venez participer à la 
« masse critique » en vélo ou roues libres pour occuper en toute sécurité 
une route, excluant ainsi les usagers motorisés.

Ensemble et par le nombre, nous pourrons nous faire entendre ! 

Nous ne bloquerons pas la circulation mais nous serons la circulation. 

Contre l'autoroute, c'est en vélorution que nous manifesterons ! 
Plus d'info: www.stopa51.org

16h30 Chemin Pinal – La méthode Alinsky
========================================

Rencontre discussion avec l'Alliance citoyenne de Grenoble.


20h30 Chemin Pinal - Projet de société Faranche
================================================

Vous êtes contre tout... mais qu'est ce que vous proposez? » 

Quel  interêt de penser un projet social et politique en tant 
qu'anarchistes aujourd'hui ? Un débat proposé par Faranches.


