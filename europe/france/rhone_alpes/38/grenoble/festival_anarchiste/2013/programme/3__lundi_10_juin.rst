
==================================================
Festival de l'anarchie, lundi 10 juin 2013, La BAF
==================================================

.. contents::
   :depth: 3
   
   
13h30 Être Anarchiste et revendiquer des droits sociaux ....
=============================================================

Pourquoi lutter pour des droits sociaux quand on n'espère rien de ce
système de droit ? Venez discuter avec nous des problématiques,
questionnements, remises en question et du sens que l'on trouve à
s'investir dans ces luttes.

16h Permanence de la bibliothèque féministe En Non mixité Meufs Gouines Trans
=============================================================================

Un moment et un endroit pour découvrir des livres féministes, des
romans, des essais, des bédés, des fanzines... et puis discuter de
littérature, se conseiller et emprunter des ouvrages !


17h30 Écoute collective de l'émission de Dégenré-e « Féminisme et luttes transpédégouines »
=============================================================================================

En Non mixité Meufs Gouines Trans.

Quels lien entre luttes féministes et luttes transpédégouines ? 
Pourquoi ces thématiques sont-elles imbriquées ? Quelles alliances 
effectives ou à renforcer ? Écoute suivie d'un débat.


20h30 Atelier chants féministes En Non mixité Meufs Gouines Trans
==================================================================

Dalidark (collectif féministe) invite les copines des Barricades 
(chorale révolutionnaire) pour un atelier chants féministes!

A propos de la non-mixite...
=============================

Durant cette journée, des évènements sont organisés en non-mixité et destinés
aux meufs, aux gouines et aux trans'. C'est en tant que personnes cibles
d'oppressions et d'exploitation communes que nous voulons nous réunir
ensemble, pour partager, réfléchir ou s'amuser. Concrètement durant ces
moments, il y aura des meufs (en tous genres), des gouines (tout le monde 
n'est pas hétérosexuelle), des trans' (tout le monde n'a pas gardé le 
genre qui lui a été assigné à la naissance, qu'il soit masculin ou féminin). 
Car nous voulons nous retrouver, nous organiser ensemble, dans la
même logique que des ouvriers ne s'organiseraient pas avec des patrons !
