
.. _festival_anarchie_8_juin_2013:

===================================================================================
Festival de l'anarchie,  Samedi 8 juin 2013, Maison des  habitants chorier berriat
===================================================================================

.. contents::
   :depth: 3

Analyse croisée des rapports de domination
==========================================

L'analyse croisée des rapports de domination... ou "l'intersectionnalité" 
comme outil d'analyse des rapports de domination et leur articulation 
entre-eux. 

Présentation-lecture-discussion, ouverte à tou.te.s les anarchistes en 
herbe ou confirmé.e.s !

17h Les leçons de l'autonomie Italienne
========================================

Italie 1968-1978 : "un mai 68 qui a duré dix ans", un mouvement social 
d'une radicalité et d'une ampleur rarement égalées. 
Résumé  des faits et exposé-débat sur les leçons que nous pouvons en 
tirer pour les luttes d'aujourd'hui.


19H Apérock & repas : Martin Del Compàs
========================================

Des chansons punk, d'autres pas du tout, des chansons militantes, d'autres 
pas du tout ... De la tristesse et de la joie, pas de mélancolie !

20h30 ZAD à la grecque 
======================

La lutte des habitants contre la création des mines d'or à Chalkidik
