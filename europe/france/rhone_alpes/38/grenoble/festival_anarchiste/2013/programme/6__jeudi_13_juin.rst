
==================================================
Festival de l'anarchie, jeudi 13 juin 2013, La BAF
==================================================

.. contents::
   :depth: 3


14H Atelier d'économie féministe En non mixité Meufs Trans Pédés Gouines
=========================================================================

Pour un autre regard sur le travail.

Ecoute collective de l'émission de On est pas des Cadeaux sur Bash Back !

16H30 En non mixité Meufs Trans Pédés Gouines
=============================================

L’histoire du mouvement anarcho-transpédégouines étasunien Bash Back ! 
nous est racontée par un pédé de Millwaukee qui a participé à son 
émergence. 

*On est pas des Cadeaux* est une émission de radio transpédégouine 
féministe sur Radio Canut à Lyon.

20H30 L'info en lutte(s): créer et faire vivre des médias autonomes...
=======================================================================

L'information est un enjeu de lutte en soi mais aussi un enjeu au
sein des luttes. Comment faire vivre des médias autonomes ?

Qu'attend-on de ceux-ci ? Comment rester au contact des luttes mais 
aussi diffuser au delà des ghettos militants ?

Une soirée discussion en présence de personnes qui participent à différents
médias indépendants.


A propos de la non-mixite...
=============================

la ballade à vélo se fera en non-mixité, c'est à dire entre entre meufs, gouines,
trans' et pédés. 
L'idée c'est de se retrouver en tant que personnes cibles d'oppressions 
communes. Nous serons donc des meufs (en tous genres), des gouines et des 
pédés (pour se ré-approprier les insultes de manière positive), et des 
trans' (tout le monde n'a pas gardé le genre qui lui a été assigné à la
naissance, qu'il soit masculin ou féminin).

Car nous voulons nous retrouver, nous organiser ensemble, dans la même 
logique que des ouvriers ne s'organiseraient pas avec des patrons !

