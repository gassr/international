
.. index::
   pair: Association; La BAF
   ! La BAF
    
   
.. _labaf:
   
=================================
La BAF 
=================================

.. seealso:: 

   - http://labaf.org
   

.. contents::
   :depth: 2
   


Présentation
============

.. seealso:: 

   - http://labaf.org/documents/charte/tractbaf.pdf

La BAF , centre social autogéré - 2 chemin des
Alpins, Grenoble

La BAF, c’est un centre social autogéré, qui met en place diverses activités
autour de valeurs communes : solidarités, autogestion, convivialité, luttes contre
les oppressions, horizontalité. La BAF fonctionne sans sponsor, ni subvention,
les dons réguliers garantissant la bonne marche et l’indépendance du lieu.


    Présentation : le tract « Bienvenue à la BAF » (au format pdf)
    Fonctionnement : la charte détaillée (au format pdf)


