


.. _rg_traverse_2:

======================================
La Traverse 2 (8 mars 2011)
======================================

  
.. contents::
   :depth: 3


Voici le second numéro de La Traverse, la revue des Renseignements Généreux...

Dans ce second numéro vous trouverez :

Pompiers pyromanes en Côte d'Ivoire, un décryptage de la crise ivoirienne avec 
Rafik Houra de l'association Survie.

Peut-on changer une société qu'on hait ?, une rencontre autour de l'atelier 
"Amour, liberté et politique" de Paris Saint Denis.

L'effet Pangloss, un petit cours d'autodéfense intellectuelle proposé par 
Richard Monvoisin.

"Créons des espaces de convergence des luttes !", une rencontre avec Christel 
et Hugo de l'association et du lieu Antigone, à Grenoble.

Et si nous cultivions notre autonomie paysanne ?, une réflexion politique 
(et des pistes concrètes) vers l'agriculture biolocale.


