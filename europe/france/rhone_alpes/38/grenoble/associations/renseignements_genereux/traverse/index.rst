
.. index::
   pair: Renseignements Généreux; Traverse

.. _rg_traverse:

======================================
La Traverse 
======================================

  
.. contents::
   :depth: 3


La Traverse est la revue des Renseignements Généreux.

À travers des entretiens, des analyses, des exposés, La Traverse s'efforce de 
tendre vers deux directions : 

- forger des outils d'autodéfense intellectuelle ; 
- imaginer, construire et faire découvrir des actions politiques ou des 
  alternatives qui nous semblent pertinentes.

La périodicité de La Traverse est irrégulière. 

Gratuite sous format électronique, cette revue peut également être commandée 
en version papier recyclé, à prix coûtant ou à prix libre.

Comme pour tous nos travaux, nous comptons sur le bouche-à-oreille, 
c'est-à-dire sur vous, pour élargir la diffusion de La Traverse. 

N'hésitez pas à faire découvrir cette revue ou les articles qui vous ont plu 
autour de vous. 

N'hésitez pas également à nous faire part de vos réactions.

.. toctree::
   :maxdepth: 3

   traverse_3/index
   traverse_2/index   
   traverse_1/index

   



