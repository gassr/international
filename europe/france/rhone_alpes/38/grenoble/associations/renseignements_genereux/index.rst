
.. index::
   pair:  Renseignements ; Généreux
   ! Renseignements Généreux
   pair: Associations; Renseignements Généreux
   pair: Grenoble; Renseignements Généreux
   
   
.. _rg_grenoble:

======================================
Les Renseignements Généreux (Grenoble)
======================================

.. seealso::
 
   - http://www.les-renseignements-genereux.org/
     
.. contents::
   :depth: 3


Site web http://www.les-renseignements-genereux.org/
====================================================

- http://www.les-renseignements-genereux.org/


Courrier électronique : rengen (à) no-log.org 
==============================================

Le courrier électronique a quelque chose de magique en ce qu'il permet à un 
grand nombre de personnes de nous joindre facilement. 

Cependant, il nous place dans une position complètement asymétrique : nous 
recevons une telle quantité de messages qu'il nous faudrait une plus grande 
organisation pour pouvoir répondre à toutes et à tous de manière satisfaisante. 

Il n'est donc pas certain que votre message fasse suite à une réponse de 
notre part. Ce n'est pas par mépris ou autres signes d'antipathie mais par 
choix stratégique, car le manque de temps et de moyens fait que nous fixons 
parfois nos priorités ailleurs.

Cela dit, nous lisons tous les messages que nous recevons et nous sommes 
toujours contents d'avoir des nouvelles des personnes qui lisent nos brochures, 
qui les diffusent, etc.

Voici notre adresse rengen (à) no-log.org 

Liste de diffusion
==================

.. seealso:: http://www.les-renseignements-genereux.org/liste-de-diffusion/

Nous vous proposons de vous inscrire à notre liste de diffusion.

Le but de cette liste est de :

- signaler les nouvelles brochures produites ;
- partager quelques informations qui nous semblent très importantes (textes, 
  livres, appels, dates...)
- signaler la sortie d'un nouveau numéro de notre revue, La Traverse.

Pour ne pas encombrer vos boîtes-mails, nous nous limiterons à un courriel par 
mois maximum.

Bien sûr, nous ne diffuserons pas votre adresse, vous pouvez vous désinscrire 
à tout moment.

Adresse  
========

::

    Les Renseignements Généreux
    chez CAP Berriat
    15 rue Georges Jacquet
    38000 Grenoble


Traverse
========

.. toctree::
   :maxdepth: 3
   
   traverse/index

Actions
========

.. toctree::
   :maxdepth: 3
   
   2012/index
   
   
