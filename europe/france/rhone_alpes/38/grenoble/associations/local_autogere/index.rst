
.. index::
   pair: Association; Local autogéré
   ! Local autogéré
    
   
.. _local_autogere:
   
=================================
Local autogéré
=================================

.. seealso:: 

   - http://www.lustucrust.org
   

.. contents::
   :depth: 2
   


Présentation
============

Le local autogéré - 7 rue Pierre Dupont , Grenoble

Le local autogéré est un lieu d'activité, de rencontres, de luttes, 
d'échange et de diffusion d'information. 

Nous proposons diverses soirées et activités mais vous pouvez passer 
pendant les permanences pour lire des brochures et des bouquins,
aller sur Internet, faire une affiche, écouter des disques en buvant un 
café, etc.
