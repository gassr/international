
.. index::
   pair: Historique ; Antigone  
   
.. _historique_antigone:
   
=================================
Historique Antigone
=================================

.. seealso:: 

   - http://www.les-renseignements-genereux.org/fichiers/14990
      

Créons des espaces de convergence des luttes ! 
==============================================

Rencontre avec Christel et Hugo d'Antigone, une bibliothèque, une librairie, 
un lieu d'évènements publics et d'activités contre la pensée unique, à Grenoble

Comment encourager dans une ville la convergence entre les luttes sociales ? Comment participer à la construction d'une culture de résistance et d'esprit critique dans un quartier ? Peut-on ouvrir des espaces d'éducation et d'expérimentation politiques tournés vers la population, autogérés et inscrits dans la durée ? Avec ces questions en tête, Les Renseignements Généreux ont rencontré Christel et Hugo d'Antigone, à Grenoble.

Pouvez-vous vous présenter en quelques mots ?

Je m'appelle Christel, je suis cofondatrice d'Antigone, qui est à la fois une bibliothèque, une librairie, un lieu d'évènements publics et d'activités contre la pensée unique, à Grenoble. Nous existons depuis 2002.

Je m'appelle Hugo, je suis également membre fondateur d'Antigone. Je fais actuellement partie du Conseil d'Administration, comme Christel d'ailleurs.

Comment est née Antigone ?

Christel : Après plusieurs années d'expériences militantes ! J'ai milité d'abord dans le mouvement punk et la lutte antifasciste. La première structure politique à laquelle j'ai participé était le SCALP1, en 1989. Depuis vingt ans je participe à la création et à la vie d'associations d'expressions, de résistances : des fanzines, des émissions de radio libre, des actions politiques, l'organisation de fêtes... Cet esprit de résistance, je le dois en grande partie à mon père, militant socialiste proche des idées de Jaurès. Je l'ai toujours vu manifester et protester. Il m'a élevée avec un sentiment de révolte, l'idée que les classes populaires n'auront que ce qu'elles auront réussi à défendre et à conquérir, que la vie est faite de rapports de force entre dominants et dominés, entre pauvres et riches...

Une éducation à la lutte des classes !

Christel : Exactement ! Ça peut paraître manichéen de parler de luttes des classes, et pourtant c'est une réalité très concrète et très actuelle. Mais je reviens à l'histoire d'Antigone : après pas mal d'années à expérimenter de nombreuses luttes, avec ma meilleure amie, Aurélie, nous sommes arrivées à la conclusion que le plus important, pour qu'une opposition puisse se construire, c'est de donner aux gens des outils de réflexion. A la fin des années 90, nous avions l'impression d'un recul social dans plein de domaines. Nous avions le sentiment que la conscience politique et la capacité de résister de la population étaient en recul, qu'il fallait reconstruire une culture de lutte et de résistance. D'où la nécessité de créer un lieu et une bibliothèque autogérés, pour que les gens puissent s'y retrouver, s'y mélanger, expérimenter et partager des expériences politiques, quel que soit leur milieu social. C'est ainsi que l'idée d'Antigone est née. On voulait créer un espace politique qui ne soit pas un nid pour une certaine catégorie de gens, mais que ce soit un lieu très ouvert, pour tous les âges, tous les milieux, tous les genres, du moment qu'il s'agit de personnes en résistance, un minimum critique vis-à-vis de la société actuelle. Nous voulions aussi redonner le goût des livres, le goût de la pensée engagée. Pendant plusieurs années, avec Aurélie, nous avons étudié ce projet, tout en commençant à constituer un fond de livres, à rassembler du matériel et du financement. Le processus s'est accéléré fin 2001, quand nous avons rencontré des jeunes, très motivés, impatients de commencer tout de suite, avec encore moins de doutes que nous sur la pertinence d'un tel lieu.

Hugo : Je fais partie de ces jeunes ! Quand j'ai rencontré Christel et Aurélie, je n'avais pas encore vingt ans, j'étais tout jeune étudiant avec un ami, Simon. Tous les deux on se cherchait politiquement. Nous étions engagés depuis le Lycée, mais nous ne savions pas trop où nous situer, nous nous reconnaissions plutôt dans les luttes altermondialistes. Le projet de Christel et Aurélie nous a énormément plu, du coup nous avons fait pression pour qu'il se réalise vite. En avril 2002, Antigone a ouvert.

Comment expliques-tu que ce projet t'aie interpellé ?

Hugo : A l'époque j'étais à Greenpeace, et je ressentais le besoin d'une réflexion plus globale sur la société, moins sectorisée. J'avais besoin de faire le lien entre toutes les luttes que je voyais. Christel et Aurélie avaient une certaine expérience de ces luttes. Elles m'ont fait découvrir la pensée libertaire, avec laquelle j'ai beaucoup accroché. J'avais envie de lire, de comprendre, d'aller à l'essentiel. Cette rencontre, c'était comme découvrir une mine d'or.

Comment vous êtes-vous rencontrés ?

Hugo : C'était sur le campus, à l'occasion d'un colloque sur les multinationales nord-américaines, avec Michael Albert2. Christel tenait le stand d'Attac Isère, en tant que vice-présidente. Et nous, en tant qu'agitateurs, nous étions venus la taquiner sur l'altermondialisme mou et le réformisme d'Attac, la nécessité d'une pensée plus révolutionnaire. Nous ne pensions pas tomber sur une personne avec autant de répondant... Nous avons beaucoup discuté, le courant est très bien passé.

Et vous avez ouvert Antigone ensemble...

Hugo : Oui, en avril 2002, à Fontaine3, près de l'arrêt Louis-Maisonnat. Nous avons trouvé un local pas trop cher. Nous souhaitions ouvrir un local en banlieue, dans un quartier populaire.

Combien étiez-vous au début ?

Christel : Au moment de l'ouverture, le conseil d'administration d'Antigone regroupait une dizaine de personnes, dont des ami-e-s et nos conjoints respectifs.

Comment s'est passée cette ouverture ?

Hugo : C'est parti sur les chapeaux de roue ! Au début nous ne faisions que café-bibliothèque, avec en plus une programmation artistique et politique. Nous faisions pas mal de concerts, avec un public nombreux. Il y avait souvent une ambiance de fête.

Christel : On avait la soif de s'ouvrir sur plein de mondes différents. On essayait d'être présents dans la rue, dans les écoles, dans les associations, d'où notamment nos liens avec Greenpeace, la Confédération Paysanne, Attac Isère. A ce propos, tout à l'heure, Hugo parlait du réformisme mou d'Attac. Il faut savoir qu'Attac Isère a été créé en 1997, suite à une rencontre politique autour de la précarité sur le campus, avec la venue de Pierre Bourdieu. Initialement, celles et ceux qui ont créés Attac Isère pensaient construire une réelle association de contre-pouvoir, un mouvement de résistance, pas du tout réformiste. C'était un groupe très radical, qui a d'ailleurs mené de nombreuses batailles au sein d'Attac France pour pointer du doigt ses contradictions et ses dysfonctionnements4.

Comment fonctionnait Antigone, à ses débuts, au niveau de son organisation interne ?

Christel : Avec Aurélie, depuis plusieurs années, on bossait à plein-temps. Personnellement je suis institutrice, pour une raison politique, parce que je souhaite transmettre le goût de la révolte et de la critique. Quand Antigone a ouvert, on s'est mises à travailler à mi-temps, pour être un maximum disponibles. On était épaulées par toute une équipe de bénévoles. Avec Aurélie, on était à fond. En plus d'assurer une présence à Antigone, j'animais des ateliers-théâtre auprès d'écoles, notamment sur les rapports filles-garçons.

Hugo : Tu animais d'ailleurs ces ateliers au nom d'Antigone, ce qui permettait de payer le loyer.

Christel : Oui, et je me demande comment physiquement on a fait pour tenir ce rythme effréné. On s'est vraiment mises en danger... Il y a d'ailleurs eu des moments de crise ! Bref, c'était du grand n'importe quoi, c'était épique, et ça a duré deux ans.

Quels ont été les moments les plus marquants ?

Hugo : J'ai adoré l'énergie politique du collectif de l'époque. On brassait des luttes dans tous les sens, on montait des collectifs, on produisait des textes, des tracts, on intervenait dans la rue. Antigone servait de lieu de relais pour les mouvements sociaux, on était force de proposition. Pendant le mouvement social autour des retraites, on a créé une vraie-fausse organisation politique, baptisée « Mais encore ». On débarquait en manif avec des autocollants, des banderolles, à 25 ou 30, ça bougeait.

Christel : Et puis il y a eu le grand rassemblement du Larzac, l'été 2003. Nous avons constitué une équipe pour tenir un grand stand de livres. C'est à ce moment-là que nous avons commencé à devenir une librairie, suite à la demande d'éditeurs indépendants, en particulier les éditions Agone, qui avaient besoin de librairies indépendantes pour être diffusées. Du coup on a créé une librairie alternative itinérante, c'était passionnant. On a également beaucoup aidé pour le procès des Dix de Valence5, l'automne 2003, au tribunal de Grenoble.

Hugo : A cette occasion, pour accueillir les personnes qui venaient soutenir pour le procès, nous avons proposé des passerelles entre les milieux politiques grenoblois. Les gens étaient accueillis dans des squats politiques, ou dans des appartements de militants plus classiques, et étaient invités à naviguer entre les différents lieux alternatifs en ville. Il y a eu à ce moment-là une sorte d'alliance entre les milieux radicaux et les militants plus classiques de Grenoble, ce qui est aussi le but d'Antigone. C'étaient des moments forts.

Christel : Et puis il y a eu le vote Le Pen, en 2002. Nous avons officiellement appelé à ne pas voter, ce qui a déclenché de grosses polémiques dans le groupe, entraîné même des départs. Cette position politique nous a aussi définitivement grillé auprès du Parti Communiste de Fontaine. Quand nous avons ouvert Antigone, les élus PC de Fontaine sont venus, alors qu'on ne les avait pas invités. Ils sont venus nous encourager, en nous incitant à déposer un dossier de subvention. Après de longs débats au sein du CA, on a fini par décider que nous avions la légitimité de demander des subventions à la mairie de Fontaine, que l'argent public, l'argent du peuple, pouvait servir à faire vivre des projets de résistance. C'est moi qui suis allée à la mairie demander la subvention, mais ça s'est très mal passé, justement à propos de notre positionnement par rapport au vote.

Comment s'est passée l'interaction avec les personnes du quartier de Fontaine ?

Christel : Les échanges ont été intéressants mais difficiles... Des associations de quartier et des habitants venaient de temps en temps. A l'époque nous faisions de la petite restauration sur place, des gens du coin venaient pour manger un bout entre ami-e-s, faire un petit repas de quartier. Certains ont adhéré à Antigone. Mais au final, il y a eu peu d'emprunts de livres, et la plupart de ceux qui ont empruntés n'ont jamais rendu les livres. La plus grande difficulté au quotidien, c'étaient les lascars du quartier. Ils venaient tout le temps, ils avaient compris qu'Antigone était un lieu chaud et accueillant, avec du thé à la menthe, des filles, et ils venaient perturber le projet. Ils nous demandaient une attention de fou, ils nous provoquaient, cherchaient des embrouilles. Physiquement, il y a eu des affrontements forts. En face d'Antigone vivait un facho, un ancien boxeur qui nous a menacé physiquement dès le premier jour avec une batte de baseball. Les lascars cherchaient aussi la bagarre, surtout en fin de soirée. Or nous avions décidé que la sécurité d'Antigone serait assurée par des filles, et qu'il n'y aurait pas de garçons pour s'interposer au moment des affrontements. Ce choix a eu évidemment des conséquences : plein de filles sont parties, avaient peur, ne voulaient plus faire les permanences. Cela a créé de grosses tensions.

Hugo : Au final, sur le lien avec le quartier, c'est un bilan un peu négatif. Les habitants ne se sont pas retrouvés dans ce lieu, la radicalité portée par Antigone ne leur convenait pas. Un autre bilan négatif de cette expérience à Fontaine, c'est la perte de livres. Pendant toute cette période, l'adhésion à Antigone permettait d'emprunter des livres sans chèque de caution : en deux ans on a perdu environ 200 livres, que des gens n'ont jamais rendu, pas seulement par vol, mais aussi par négligence, par oubli.

Comment avez-vous initialement constitué ce stock de livres ?

Christel : D'abord en mettant en commun nos bibliothèques. Quel est le sens d'avoir dans sa maison des centaines d'ouvrages, que le plus souvent on ne relit jamais, alors que d'autres n'ont pas accès aux livres ? Et puis nous avons acheté des livres, avec nos salaires, Aurélie et moi. Puisque nous sommes issues de milieux prolétaires, nous avons besoin du salariat pour vivre, mais nous pensons que le salariat a du sens quand nous utilisons une part importante de notre salaire à soutenir des projets de transformation sociale. Avec Aurélie nous avons mis en commun une partie de nos salaires pour acheter des livres, aux puces, dans les brocantes, dans les bouquineries.

Hugo : Il faut savoir qu'Aurélie et Christel, par le passé, organisaient des fêtes technos, donc elles avaient l'expérience de la débrouille, de la logistique, de la gestion de matériel.

Christel : Oui, d'ailleurs en arrivant à Antigone à Fontaine, nous avons posé nos enceintes, nos platines, nos tables de mixage, de la décoration, bref tout notre patrimoine des années passées. Nous avons été cambriolés, presque tout a été volé...

Avant Antigone vous organisiez des fêtes technos ?

Christel : Mon père, comme celui d'Aurélie, étaient des fans de la fête, ils adoraient les moments de rassemblement. Quand on s'est rencontrées, on est parties à fond sur des projets de fêtes subversives. On voulait créer des alternatives aux lieux où il faut payer des entrées très chères, où si tu n'as pas la tête qu'il faut on ne te laisse pas entrer. On est parties dans cette idée avec plein de bonne volonté, comme deux prunes, avec de fortes envies politiques, dans un milieu de drogues, de défonce et d'abus... On a pris une grande gifle, une grosse désillusion. Nous faisions des fêtes magnifiques, mais il y avait des jours où il était plus intéressant de discuter avec les gendarmes au petit jour lorsqu'ils venaient pour nous empêcher de faire la fête, qu'avec les gens venus faire la fête dans la nuit. Avec Aurélie, on s'est dit qu'on ne pouvait plus continuer comme ça. Le décalage entre nos envies et celles de la majorité des gens qui participaient aux fêtes était insupportable.

Pourquoi la techno ?

Christel : Avec Aurélie on vient du punk rock. Quand la techno est arrivée, on était assez curieuses, mitigées, mais on a trouvé ça assez drôle. On y voyait une résurgence des mouvements hippies, la libération par la fête, tout ça. Du coup on s'est engouffrées dans la techno. Mais au final on a pris peur, certains jours on se demandait même si la techno n'était pas un délire organisé par l'État pour lobotomiser les jeunes générations. On t'encourage à faire des rave party pour t'éclater, sans aucune réflexion humaine, sociale ou politique. Bref, on a fait la fête un moment, parce qu'on aime ça, mais on est finalement parties de ce milieu. Nous, on voulait créer un autre rapport à la musique, à la fête, même à la drogue. Mais on ne voulait plus participer à un processus de « décérébration ». On a plutôt eu envie de donner de la place au goût de la réflexion, de la politique, tout en faisant la fête, parce que la fête permet de recréer du lien, des espaces où se retrouver.

Quand avez-vous finalement quitté Fontaine, avec Antigone ?

Hugo : En 2004. On était épuisé-e-s. Comme il fallait de l'argent pour faire fonctionner le lieu, environ 1000 euros par mois, on ne pouvait jamais s'arrêter de faire des évènements, sinon le lieu coulait financièrement. On trouvait que nos forces ne se renouvelaient pas, on était un peu dans une impasse. On a convoqué une assemblée générale extraordinaire, pour expliquer la situation, dire combien on était sur les rotules. On n'avait pas de perspectives, on s'est demandé que faire, collectivement. On était une quarantaine, des gens très divers.

Christel : Nous avions un constat d'échec par rapport à notre objectif de toucher des classes populaires. Depuis deux ans nous étions au bord d'un quartier, on avait de grandes idées sur le peuple, et quand le peuple était devant notre porte, nous crachant dessus, nous insultant, ça nous a montré nos limites. Nous étions épuisé-e-s moralement et physiquement. Il y avait plein de gens qui participaient à Antigone, mais cela représentait une grosse charge de travail pour le noyau dur. Nous étions au bord de la rupture nerveuse. Et nous avions plein de questionnements sur le fond. Étions-nous vraiment utiles ? Parce que si c'est pour créer un lieu qui ne rassemble que les potes, ça n'a pas de sens.

Quel a été le résultat de cette Assemblée Générale ?

Christel : Tout le monde voulait qu'Antigone continue. Certains ont proposé de squatter un lieu, ce qui nous semblait moyennement possible, en particulier avec le stock de bouquins.

Hugo : Et c'est là qu'un adhérent s'est levé et a dit « Je trouve super ce que vous faites. Je suis anarchiste, je viens de faire un héritage de ma grand-mère, mais je ne veux pas de cet argent, j'en ai pas besoin. Je le mets à la disposition de ce projet. Nous allons chercher un local ensemble, je vais l'acheter, et vous n'aurez plus de loyer, ce sera plus simple. »

Quel coup de théâtre !

Christel : Oui, c'était le délire ! Tout le monde a applaudi, c'était un grand moment, qui rappelait l'époque de certains anars aristocrates et généreux.

Que s'est-il passé ensuite ?

Christel : On s'est mis à chercher un lieu. On a mis un an et demi à trouver ce qui nous convenait.

Hugo : Pendant cette période on était itinérants, c'était « Antigone hors les murs ». Nous étions accueillis à droite et à gauche, à la maison du Bénin6, un peu aux 400 Couverts7, ou avec les Comédiennes du possible8. On voulait assurer une permanence par semaine, pour ne pas perdre tout contact avec nos adhérent-e-s.

Christel : Finalement on a appris que la mairie vendait un lieu, une ancienne usine de 300 mètres carrés, au 22 rue des Violettes, pour un projet culturel et associatif. En tant que fille d'ouvrier, petite-fille d'un maçon et d'un mineur, c'était un rêve de ma vie de transformer un lieu d'oppression en lieu d'expression. Le local, par contre, était dans un sale état, on était découragé-e-s. C'était décomposé, sans chauffage, crasseux.

Pourquoi la mairie vendait-elle ce lieu ? Un local près du tram, c'est un jackpot financier !

Hugo : On ne sait pas vraiment pourquoi. On pense que ce lieu n'était pas si simple à transformer en logements.

Christel : La ville voulait développer des lieux de culture de proximité, pour dynamiser des quartiers. En fait, c'est un cadeau empoisonné, parce qu'ici c'est un quartier résidentiel, où la grande majorité des gens sont propriétaires. Au début les gens étaient assez hostiles au projet d'Antigone. Dans la rue, certains habitants voulaient racheter collectivement l'usine, pour en faire des garages.

Comment votre dossier est-il passé à la mairie ?

Christel : Certainement parce que nous étions les seuls à avoir l'argent tout de suite. La mairie a lancé un appel d'offres, il y avait une soixantaine de dossiers, et nous étions, je crois, les seuls à ne pas avoir besoin de faire d'emprunt, à pouvoir poser 130 000 euros sur la table. En tout cas, c'est ce que je pense.

Il y avait beaucoup de travaux pour transformer ce local ?

Hugo : Oui, beaucoup. Au début, nous avons campé dans Antigone, tout était sale. Le collectif du trou dans le mur nous a prêté leur structure, une tente métallique avec des bâches et un poêle, c'était rock'n roll. Pendant plus d'un an nous étions paralysé-e-s face à l'ampleur des travaux à mener. Aucun de nous n'avait d'expérience dans les travaux.

Christel : Finalement nous avons décidé de faire appel à des artisans du bâtiment, on voulait des gens compétents. La première chose à faire, c'était la création d'un mur de séparation, pour diviser l'espace en deux zones de 150 mètres carrés. Ça s'est très mal passé. L'entreprise n'a pas fini le mur, les artisans nous ont piqué des outils, bref c'étaient des truands. Après ça, on s'est dit que désormais on ne ferait plus jamais appel à des entreprises, que ce serait l'autogestion, sauf pour la sortie de secours.

Pourquoi avoir divisé le lieu en deux espaces de 150 mètres carrés ?

Hugo : Pour des espaces de 150 mètres carrés, les normes de sécurité sont moins sévères. Les normes, c'est un sacré problème. On a dû prendre un architecte, nous avons mis en place plein de systèmes de sécurité, mais nous n'avions pas les moyens de respecter toutes les normes dans le détail. De ce point de vue, on est dans le ''trancher-souple'' : on bétonne au niveau de la sécurité, pour qu'Antigone ne soit pas en danger, tout en sachant que si l'État ou la mairie veulent nous faire disparaître, de toute façon ce sera facile pour eux, il suffira de nous demander de respecter les normes dans le détail. On sait très bien qu'actuellement nous sommes tolérés. Ceci dit, notre fonctionnement est bien cadré : c'est une association, avec des membres, toute personne qui rentre est membre temporaire, et nos travaux sont annoncés.

Avec les voisins, ça va ?

Christel : Au début oui. Et puis de nouveaux voisins sont arrivés juste au dessus du local, ils nous ont reproché de faire trop de bruit, ce qui a enclenché une longue guerre psychologique et juridique. Finalement on a gagné le procès. Mais on a décidé malgré tout de faire de gros travaux d'isolation.

Et les gens du quartier ?

Hugo : Ça marche mieux qu'à Fontaine, mais ça reste difficile. Nous avons beaucoup de mal à intéresser les personnes qui sont apparemment indifférentes aux questions de transformation sociale. On essaie pourtant régulièrement d'interpeller les gens du quartier. Parfois ça marche, sur des ateliers très ponctuels, par exemple l'atelier mosaïque.

Christel : Notre rêve, c'est de devenir un centre social populaire, où peuvent venir les gens du quartier pour apprendre à réparer son vélo ou faire du théâtre, ou des gens de tout bord pour discuter de l'écologie, de la transformation de la société. Et surtout, que ce ne soit pas un lieu anonyme, que ce soit un lieu humain, avec des échanges, des paroles, un lieu accueillant et chaleureux.

Hugo : Ce qui manque ici, et qu'on avait à Fontaine, c'est de pouvoir prendre des pots entre ami-e-s, boire un coup. Ici, on doit faire attention au bruit, du coup c'est moins convivial.

Combien avez-vous d'adhérent-e-s actuellement ?

Hugo : Environ 150.

Et combien de personnes fréquentent Antigone ?

Christel : Environ 1500 par an. C'est grosso modo le nombre de nos cartes d'adhérents temporaires.

Quel est le profil du public qui vient ?

Christel : C'est un public plutôt blanc, ce qui est l'une de nos grandes tristesses. Par contre, au niveau des âges, des modes de vie, des opinions politiques et des classes sociales, c'est diversifié. A Antigone, nous recevons beaucoup de « rien-du-tout-istes », des gens mal à l'aise avec toutes les formes d'organisation politique actuelles, de la réforme à la révolution, mais qui sont intéressés par la pensée libertaire, tout en ne se retrouvant pas dans des lieux trop radicaux, des squats ou des groupes libertaires existants. C'est un public qui nous soutient souvent, qui nous donne du matériel, des livres, des coups de main. Il y a des rencontres extraordinaires.

Hugo : Nous avons un noyau dur d'étudiants, de professeurs, d'instituteurs. Mais il y aussi des ouvriers, des ingénieurs, des chômeurs, des squatteurs, un peu de tout quoi.

Comment se fait le choix de la programmation publique ?

Hugo : Nous recevons de nombreuses propositions. On en discute et on décide en CA. Le CA est actuellement composé de six personnes.

Christel : C'est une programmation très diversifiée. Depuis le début, Antigone est dans une démarche de ''service'' pour le milieu militant : nous essayons d'accueillir et d'accompagner des initiatives plurielles, de la manière la plus ouverte qui soit. Mais nous souhaitons également porter ce qui nous plaît, donc il y a un équilibre à trouver. De temps en temps, il y a des ratés, il arrive que certaines soirées ne nous plaisent pas du tout. Il y a aussi des thèmes que nous souhaitons porter à long terme, comme le féminisme, la réflexion sur la consommation et la production.

Est-ce que la bibliothèque tourne bien ?

Christel : Plutôt bien, même s'il y a plus d'emprunts que d'emprunteurs. Certaines personnes viennent beaucoup à Antigone, et empruntent beaucoup et régulièrement. Ceux qui empruntent le moins, ce sont les milieux radicaux.

Hugo : Dans l'époque actuelle, les bibliothèques ne rencontrent pas de succès fulgurant... Le livre n'a pas le vent en poupe.

Christel : Les films en revanche sont très demandés, notre médiathèque est bien utilisée. La librairie fonctionne bien également, c'est la source principale de financement d'Antigone, notamment grâce à la participation au Marché équitable à Noël, ou au stand dans la rue le 1er mai. Mais encore trop peu de monde réalise qu'Antigone est une librairie. Quand on entend que des camarades militants achètent leurs livres à la FNAC, on désespère un peu... On aimerait qu'à Grenoble les militants aient l'envie et le réflexe d'utiliser davantage les lieux alternatifs. Si j'ai envie de manger entre amis, je vais aux Bas-Côtés9 ; si je veux voir un film, je vais au 10210 ; si j'ai envie de lire ou d'acheter un livre, je vais à Antigone !

Au fait, pourquoi le nom Antigone ?

Christel : Parce que le projet est parti, à l'origine, de deux filles qui voulaient se battre, résister et opposer la raison du coeur à la raison d'Etat. On veut montrer que les petits individus peuvent être aussi forts que les institutions. Pour nous, la symbolique de cette bataille, perdue au sens strict, cette guerre entre le pot de fer et le pot de terre, le personnage d'Antigone le symbolise complètement. Il nous a beaucoup marquées, Aurélie et moi. C'est un personnage féminin, et nous trouvons qu'il manque cruellement de représentation féminine dans la lutte, dans le militantisme. Souvent, les présidents d'associations, ceux qui parlent, ceux qui mettent officiellement leur nom pour appeler à manifester, ce sont des hommes, ce qui nous gêne beaucoup. Nous voulons que la dimension féminine pose la base d'Antigone, tout en remettant en question le concept de genre. Nous sommes parties du postulat que nous vivons dans un monde avec une certaine construction du genre et des personnalités, et dans ce cadre, les femmes ne proposent pas le même genre d'inventions, de résistances et d'oppositions que les hommes, et il est important qu'Antigone soit pensé et mis en place par des femmes. C'est ce qui explique le côté ''mère à Titi'', au niveau esthétique, à Antigone. On n'a jamais voulu être dans l'imaginaire ou l'esthétisme ''crapouillou'', on veut que ce soit un lieu chaleureux, joli, coquet. On veut que, lorsque tu rentres dans Antigone, tu ressentes quelque chose de l'ordre du ventre de la Baleine dans Pinocchio, ou la caverne d'Ali Baba. Extérieurement Antigone est moche, ne ressemble à rien, et puis tu pousses la porte et paf, il y a des petits coussins, des petites loupiotes, plein de couleurs. On a envie que les gens se sentent bien à Antigone, comme chez eux, comme chez leur grand-mère.

Est-ce que vous pensez que les évènements que vous organisez changent les gens, participent à la transformation de la société ?

Christel : Pour ma part je dirais oui. Par exemple, j'ai rencontré dans ma vie des gens comme Aurélie, comme Hugo ou d'autres, qui avec le temps m'ont dit : « si on ne s'était pas rencontrés, si on n'avait pas rencontré la pensée libertaire, s'il n'y avait pas eu Antigone, je n'aurais pas fait ça, je n'aurais pas été dans cette direction de vie ». Notre seule prétention à Antigone, c'est de nourrir les pratiques et les imaginaires de la population, nous permettre de grandir et de nous développer. Le fait de rencontrer des lieux alternatifs, des gens qui font des expériences radicales, ça oriente le sens de ta vie. Le fait qu'Antigone existe, comme les 400 Couverts, le 102 ou d'autres lieux, change le rapport au monde et à la vie. Mais au niveau individuel plus qu'au niveau global.

Hugo : Je dirais oui aussi, parce que les soirées que nous proposons à Antigone sont des outils qui doivent aller vers un processus de transformation sociale. Mais on ne se raconte pas trop d'histoires. On sait qu'Antigone n'est pas le fin du fin, et que faire des soirées ne suffira pas à changer le monde... C'est un outil parmi d'autres ! C'est fondamental de créer des espaces de discussion, qu'il y aie au moins cette possibilité-là, ce refuge d'idées, ce petit lieu libéré pour des idées subversives.

Christel : On ne transforme pas les gens, on transforme des gens, c'est une nuance importante. Quand j'étais à Attac Isère, j'ai rencontré une femme de 50 ans qui était professeure, qui n'avait jamais entendu parler d'anarchie dans sa vie. On s'est rencontré, on a parlé, je lui ai prêté des livres, et quelques temps plus tard elle a quitté Attac avec pertes et fracas en faisant une lettre où elle remerciait Attac de lui avoir permis, chemin faisant, de découvrir la pensée libertaire, et en expliquant qu'elle rejoignait désormais la CNT. Du coup, cette femme s'est engagée à la CNT, à la Fédération Anarchiste, j'ai trouvé ça magique.

Antigone, c'est un lieu anarchiste ?

Christel : Attention, non non, je précise qu'Antigone n'est pas et ne revendique pas l'anarchie. Ça me fait mal aux dents de le dire, mais par respect pour mes camarades d'Antigone, je suis obligée de le souligner fortement : Antigone n'est pas anarchiste. Elle fonctionne de manière autogérée, ou en tout cas elle essaie, elle défend la pensée libertaire, mais elle est composée de personnes très diverses. Certaines viennent du mouvement communiste, d'autres du mouvement radical, d'autres républicains, d'autres de rien du tout. Mais par contre on se retrouve toutes et tous sur la démarche d'Antigone. Evidemment, notre bibliothèque a un fond important d'ouvrages anarchistes, et nous accueillons régulièrement des soirées sur des thèmes anarchistes, ce qui est pour nous fondamental, tant ce courant de pensées a peu de place pour s'exprimer, tant il est stigmatisé, assimilé au terrorisme. L'anarchie, ce n'est pas ce qu'en disent les journaux, les dominants. L'anarchie est bien différente de l'imaginaire social majoritaire, et nous avons envie de le faire savoir, de faire connaître les anarchistes et leurs expériences. Nous voulons rendre accessible cette pensée pour les personnes qui, dans leur milieu social, ne la rencontrent jamais.

Et toi, es-tu anarchiste ?

Christel : Personnellement, je prétends à être anarchiste, même si c'est extrêmement compliqué dans cette société, dans le système dans lequel on nous fait vivre. Mais j'ai envie qu'on remette cette idée-là au centre de la rue, montrer qu'il y a d'autres manières de vivre, de penser, d'agir.

Puisqu'on parle d'anarchie : en 2007, Antigone a soutenu José Bové, non ?

Christel : Pas Antigone ! Antigone a accueilli les collectifs anti-libéraux pour qu'ils puissent s'exprimer et s'organiser, ce qui est l'un des objectifs de ce lieu. Après, certains d'entre nous, en tant qu'individus, ont soutenu la candidature de Bové, et l'ont aidé logistiquement. Mais Antigone n'a pas soutenu officiellement José Bové.

Quel regard portez-vous sur le milieu libertaire grenoblois, les 300 personnes qui gravitent autour de ces idées ?

Hugo : J'y vois beaucoup de choses positives, je suis très content qu'Antigone y participe. Je suis notamment très enthousiasmé par les réflexions stratégiques qui s'y développent, la volonté de prendre du recul, ne pas être tout le temps dans l'activisme forcené, questionner les limites et les espoirs du militantisme. J'aime bien l'autocritique, je trouve que ça manque très souvent dans nos milieux militants, où les gens sont souvent bornés. Parmi les limites que je perçois dans le milieu libertaire, c'est le fait qu'extérieurement ce milieu n'est pas considéré comme un mouvement ouvert, où chacun peut y entrer, c'est plutôt vécu comme un milieu affinitaire, assez fermé. En ce sens Antigone me permet de garder les pieds sur terre, d'avoir à la fois un pied dans le milieu radical et un pied dans un milieu plus ouvert, plus proche de la population ''normale''. Il faut préciser qu'il y a quelques années, le milieu libertaire boudait Antigone. Nous avions une étiquette de réformistes. Il a fallu du temps pour que nous apprenions à nous connaître. Maintenant ça va plutôt bien.

Christel : Les dynamiques stratégiques dont parle Hugo ne sont portées que par quelques personnes, et c'est pour cela qu'à mes yeux c'est très fragile. Personnellement j'ai du mal avec le milieu libertaire. Voilà vingt ans que je brasse dans les milieux anarchistes, et je n'ai jamais été compatible avec des groupes officiels, la FA, les syndicats, parce qu'à chaque fois il fallait adhérer globalement à un certain nombre de postulats et d'idées. J'ai toujours constaté un manque d'ouverture de la part de ces milieux, un mépris plus ou moins affiché pour le reste du monde de la part de certain-e-s. Trop de libertaires veulent bien changer le monde, mais ils aimeraient surtout enlever les autres gens du reste du monde, parce qu'au fond ils n'ont pas envie d'être avec eux, ils n'ont pas envie de leur parler. Et puis, le machisme est aussi très fort dans les milieux anars officiels, c'est insupportable. J'ai, de manière générale, une grande angoisse des milieux, ces ambiances sclérosantes où tout le monde parle de tout le monde, où il y a des embrouilles affectives en permanence. Ceci étant dit, je trouve le milieu libertaire grenoblois bien meilleur et bien plus chouette que dans les autres villes que je connais. Si je vais dans une manifestation, je rejoins le cortège noir, parce que symboliquement je le reconnais comme ma famille.

Combien d'années vous projetez-vous dans Antigone ?

Hugo : Pour ma part, je me vois bien dans Antigone longtemps, à condition de ne pas être un élément indispensable au fonctionnement de la structure. Je ne veux pas être un pilier, je veux pouvoir partir plusieurs mois sans mettre en danger le projet. Actuellement, la plus grande fragilité d'Antigone est humaine : pour fonctionner, ce lieu nécessite quasiment deux permanent-e-s à plein temps, et une dizaine de bénévoles, sinon ça ne tourne pas.

Christel : C'est pour cette raison qu'on aimerait davantage d'aide. C'est dur quand on constate que des militant-e-s utilisent Antigone de manière consumériste, sans vraiment de contrepartie. On a envie que les gens puissent utiliser Antigone pour leurs besoins, mais dans une logique commune. Certaines personnes ne se souviennent qu'on existe que quand elles ont besoin de nous. Certains jours on en a marre de laver les toilettes, de faire les courses, de faire le ménage pour des gens qui ne nous disent même pas bonjour, ou qui se foutent de nous.

Hugo : Ceci étant dit, on a reçu plein d'aide pour notre dernier gros chantier, des dizaines de personnes ont mis la main à la patte, c'était génial.

Et toi Christel, combien d'années te projettes-tu dans Antigone ?

Christel : Toute ma vie j'espère ! Antigone changera sans doute de forme, de lieux, de pays, mais toute ma vie j'espère être dans cette aventure. C'est ce qui me donne le plus de bonheur dans la vie. Je ne peux pas vivre dans un monde capitaliste si je ne suis pas concrètement dans des processus d'opposition et résistance. Je ne peux pas m'imaginer travailler, rentrer le soir chez moi dans ma maison, avoir une voiture, des loisirs... Tout cela ne m'intéresse pas. J'aimerais un jour ne plus travailler et ne faire qu'Antigone, pour avoir le temps et les moyens de faire tout ce que j'aimerais, notamment faire davantage le lien avec des lycées, des associations d'habitants, se rapprocher de la population, lancer des AMAP11 dans les milieux populaires, plein de petites choses, des alternatives concrètes. En ce moment je passe l'essentiel de mon énergie à faire en sorte que le lieu vive, je manque terriblement de temps. Et je m'inquiète pour l'avenir de ce pays. Je crains que si la France ne change pas de direction, il va falloir qu'on s'expatrie. Je pense qu'il y a un vrai danger à venir sur les projets et les gens proches d'Antigone. Je pense que nous allons vers un État de type dictatorial, et que les petites latitudes accordées jusqu'ici vont être de moins en moins possibles. Le chef des renseignements généraux de Grenoble me l'a déjà dit en face dans une manif : si je n'ai jamais pris de coup, si on m'a laissé faire ma vie, c'est parce que je suis considérée comme une douce rêveuse et que je ne suis pas dangereuse. Tant que le pouvoir nous considère ainsi, on peut se développer. Mais je ne suis pas sûre que le pouvoir nous considère ainsi longtemps. Si rien ne change au niveau politique, nos espaces de liberté politique vont être de plus en plus restreints. Si demain le Pouvoir veut fermer Antigone, c'est très rapide, il suffit par exemple de nous bloquer sur les normes.

Quels sont vos espoirs pour la suite, au niveau politique ?

Christel 

    La Révolution... J'aimerais voir le monde se transformer vraiment, que le rapport 
    de force s'inverse, que nous soyons en capacité de s'opposer vraiment à ce qui 
    est en train de se mettre en place. 
    Je trouve que ce monde devient complètement désespérant et désespéré. 
    J'aimerais bien ne pas être seulement dans des petites luttes ou des petites 
    résistances, mais qu'à un moment il y aie une vraie opposition, relayée par un 
    maximum de gens, qui disent que nous ne voulons plus de ce monde-là. 

    J'ai encore l'espérance de la Révolution et j'aimerais qu'Antigone devienne un 
    vrai centre social autogéré, où il y aurait toutes sortes de gens pour plein de 
    raisons diverses, avec plein d'échanges, de mélanges, de partage de savoirs par 
    exemple entre un ouvrier qui, par ''culture'' ou pression sociale, fait de la 
    mécanique, et une féministe radicale qui aimerait en faire par goût ou nécessité 
    d'autonomie. 

    J'aimerais que nous retrouvions le goût des autres, l'envie de respecter les 
    limites de chacun-e tout en les repoussant, pour essayer de construire un monde 
    avec nous toutes et tous. 
    Arrêtons de vivre dans nos espaces confinés, avec nos bandes de potes, nos petits 
    milieux politiques !

Hugo 

J'ai l'espoir que la société change. J'ai l'impression que les idées défendues par Antigone montent en puissance. J'ai l'impression que nous sommes de moins en moins minoritaires. Les luttes sur la Fac me semblent mieux organisées, mieux menées. Je rencontre de plus en plus de militant-e-s qui parlent d'auto-organisation, d'autonomie, qui veulent sortir des luttes de chapelle, des stratégies électorales. Je sens une force qui monte. Il va se passer des choses.

Christel : Mais il faut qu'on s'organise davantage ! On manque de réseaux politiques bien structurés à Grenoble ! Quand je vois la taille de ce qui se met en place dans le monde en terme de domination, à quel point les dominants sont forts, ont les moyens et sont prêts à tout pour plus de pouvoir et de fric ; et quand je vois, en face, les gens qui essaient de résister, je trouve que nous sommes des rigolos. Je vois bien nos limites, nos difficultés pour s'organiser ensemble, le fait que je n'ai pas envie de bosser avec telle ou telle personne, ou pas envie de rejoindre tel ou tel parti alternatif de gauche. Mais l'heure est à la convergence des luttes. Si nous ne sommes pas capables de créer de vrais réseaux de résistance, des unions pour des actions fortes, on va se faire écrabouiller les un-e-s après les autres, comme l'histoire l'a toujours montré.

Antigone

22, rue des Violettes, 38 000 Grenoble

www.bibliothequeantigone.org

NOTES :

1La Section carrément anti Le Pen (SCALP) est un réseau antifasciste et libertaire des années 80-90.

2Libertaire nord-américain, auteur notamment de Après le capitalisme, Éléments d’économie participaliste, éditions Agone, 2003.

3Fontaine est une commune de la banlieue Ouest de Grenoble.

4Sur ce sujet lire notamment Attac, encore un effort pour réguler la mondialisation ?!, Michel Barrillon, éditions Climats, 2001.

5Dix personnes accusées d'avoir participé à un fauchage d'OGM en 2001.

6Espace culturel et associatif, à Grenoble.

7Squat grenoblois avec four à pain, salle de spectacle, atelier-vélo, zone de gratuité, occupé en 2001 et expulsé en 2005.

8Association artistique engagée.

9Café-cantine-épicerie-librairie et décroissance, à Grenoble. L'interview du fondateur est disponible sur le site des Renseignements Généreux.

10Squat culturel conventionné, à Grenoble, www.le102.net

11Association pour le Maintien d'une Agriculture Paysanne. Les AMAP ont pour but de maintenir des paysans locaux en leur achetant prioritairement et directement leur production plutôt que de passer par les supermarchés.

