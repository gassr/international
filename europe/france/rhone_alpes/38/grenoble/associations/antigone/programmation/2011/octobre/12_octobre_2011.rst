
  .. index::
   pair: La fabrique du futur; Introduction (12 octobre 2011)
   
.. _programmation_antigone_12_octobre 2011:
   
==================================================
La Fabrique du Futur Soirée introductive du cycle 
==================================================

.. seealso:: http://www.zcommunications.org/znet

Cette première soirée d’introduction permettra de:

- présenter un exemple de modèle économique futuriste
  L’ecopar ou parecon d’Albert et Hanel : http://www.zcommunications.org/znet voir en anglais)
- soulever des questions incontournables de l’économie
- préciser la démarche dans laquelle nous souhaitons nous inscrire

Ce sera l’occasion de faire connaissance et de participer à la mise en route 
du cycle.



   
