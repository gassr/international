
.. index::
   pair: Antigone  ; 2013 (Novembre)
   
.. _programmation_antigone_novembre_2013:
   
======================================
Programmation Antigone Novembre 2013
======================================

.. contents::
   :depth: 3

.. _retraites_antigone_novembre_2013:

Mercredi 13 novembre 2013 à 20h Les retraites: un système émancipé du capital à défendre
=========================================================================================

.. seealso::

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=day&id=20131113&Itemid=18
   

:ref:`Christine Jakse <christine_jakse>`, auteure de "L'enjeu de la cotisation sociale" 
éd. du croquant, et :ref:`Bertrand Bony <bertrand_bony>`, tous deux 
membres du :ref:`Réseau salariat <reseau_salariat>`  aborderont le système des retraites pour 
élargir sur **la cotisation sociale  comme alternative au capitalisme**.

Dans le cadre du cycle économie de la fabrique des futurs La Fabrique 
des futurs reprend du service cette année en collaboration avec le 
réseau salariat, qui viendra une fois par mois à Grenoble pour présenter 
leurs recherches et questionnements:

Bien au-delà des arguments habituels, Réseau salariat défend l’idée que 
la cotisation sociale et, en particulier la retraite, sont des modèles 
d’émancipation du marché du travail et du capital. 

L’une et l’autre prouvent que l’on peut financer du salaire hors de 
l’emploi, massivement et sur le long terme, ainsi que des investissements 
lourds (ex. les hôpitaux). 

En s’appuyant sur l’existant, la cotisation dessine alors les contours 
d’une alternative d’une part au travail soumis au marché du travail, 
et d’autre part, au crédit soumis aux « marchés financiers » : 
une cotisation salaire et une cotisation économique, à l’image de la 
cotisation sociale, permettraient aux salariés et indépendants, 
producteurs exclusifs de la richesse (le PIB), d’en maîtriser totalement 
les moyens de sa production, ses finalités, ses quantités, et de penser 
une autre façon de travailler dans le respect de l’humain et de 
l’environnement. 

Cette expérience réussie de la cotisation sociale explique d’ailleurs 
pourquoi, depuis sa création, et plus encore depuis 40 ans, 
la sécurité sociale fait l’objet d’attaques violentes et incessantes. 

A chacun d’en saisir l’enjeu et de s’émanciper du récit dominant.

- Première soirée le lundi 7 octobre sur le campus pour la 3° édition 
  du festival “Vivre l’Utopie”
- Retour sur la question des retraites le mercredi 13 novembre à Antigone
- Débat le 11 décembre à Antigone :: cotisation sociale vs revenu garanti, 
  quelle revendication choisir? 


 
La Banlieue du « 20 heures »: Ethnographie de la production d’un lieu commun journalistique
============================================================================================

.. seealso::

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=day&id=20131114&Itemid=18
   - http://festival-espritslibres.librest.com/Berthaut-Jerome.html

Plongés dans un collectif de travail régi par des logiques économiques 
(audience, productivité), le poids des sources légitimes et des modèles 
professionnels importés de l’audiovisuel commercial, les journalistes de 
France 2 fabriquent et perpétuent les lieux communs sur les habitants 
des quartiers populaires pour satisfaire dans l’urgence la commande de 
reportages prédéfinis par leur hiérarchie.


À partir d’une enquête menée au plus près des pratiques quotidiennes des 
journalistes, ce livre propose une explication sociologique à la permanence 
des représentations réductrices véhiculées par certains contenus médiatiques.


 
