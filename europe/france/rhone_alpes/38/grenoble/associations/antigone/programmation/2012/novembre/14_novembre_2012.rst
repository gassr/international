
  .. index::
   pair: Programmation Antigone  ; novembre 2012
   pair: Bernard Friot; Antigone
   
.. _programmation_antigone_14_novembre_2012:
   
====================================================
Bernard Friot, "L'enjeu du salaire" 14 Novembre 2012
====================================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_content&view=category&layout=blog&id=13&Itemid=58
   - :ref:`bernard_friot`

.. contents::
   :depth: 3 

Présentation
============ 
  
Entre 1920 et 1980, des conquêtes révolutionnaires ont bouleversé la 
façon dont le capitalisme organise la production, en particulier 
la cotisation, le grade de la fonction publique et la qualification 
du poste dans le privé, la pension de retraite comme continuation 
du salaire.

Nous pouvons les généraliser en remplaçant le marché du travail par la 
qualification universelle (et donc le salaire à vie), et le profit 
(et donc le crédit) par une cotisation économique qui financera 
l’investissement.

Ainsi libérés des employeurs et des actionnaires, et avec eux du 
chantage à l’emploi et à la dette, nous maîtriserons la valeur 
économique et produirons autrement. 
  
   
Vidéo
======

- http://multimedia.bibliothequeantigone.org/videos/20121114%20-%20Bernard%20Friot%20-%20L%27enjeu%20du%20salaire/20121114%20-%20Bernard%20Friot%20-%20L%27enjeu%20du%20salaire.webm
