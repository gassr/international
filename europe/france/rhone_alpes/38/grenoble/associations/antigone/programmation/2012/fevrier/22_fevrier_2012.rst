

   
.. _changer_societe:
   
=========================================================================
Peut-on changer une société qu’on hait ?   (22 février)
=========================================================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=eventlist&Itemid=18
   
 
.. contents::
   :depth: 2
       

Peut-on changer une société qu’on hait ? 
=========================================

Rencontre entre l’interviewer et l’interviewé.

Suite à la lecture de l’article du même nom dans la revue Traverse, Antigone 
avait envie de recevoir Alexandre et Samuel Foutôyet des Renseignements 
généreux. 

Cet article prenait pour départ l’expérience d’Alexandre autour de l’atelier 
« Amour, liberté, politique », atelier qu’il animait alors depuis deux ans à 
l’université de Paris 8, au sein de « l’UFR-zéro ». 

Bon prétexte pour réexaminer avec eux ce qui pourrait nous aider à nous 
comprendre et comprendre le monde tel qu’il va.


