
  .. index::
   pair: Programmation Antigone  ; Leur Grande Trouille
   
.. _leur_grande_trouille:
   
=========================================================================
Leur Grande Trouille avec François Ruffin   (23 février)
=========================================================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=eventlist&Itemid=18
   
 
.. contents::
   :depth: 2
       
Rencontre – débat avec François Ruffinleur_grande_trouille autour de son livre 
« Leur Grande Trouille, Journal intime de mes pulsions protectionnistes »

Depuis dix ans, François Ruffin visite des usines, voit des ouvriers 
manifester, désespérer, et avec, toujours, au bout, la défaite. 
«Ça lasserait le plus vaillant des soldats, écrit-il, tant de défaites 
accumulées. Ça m’a lassé. Le dégoût est monté lentement, comme une marée. 

Mais là, avec les Parisot, il s’est installé, définitif. Il faut préparer 
la contre-offensive…» 

Quelle est leur grande trouille ? Leur peur bleue ? Il suffit de peu d’analyse. 
A chaque intervention du MEDEF, la même rengaine : «Nous attendons des 
responsables politiques qu’ils écartent toute mesure protectionniste» ; 
«nous sommes convaincus que nos économies retrouveront le chemin de la 
croissance à condition que les pays écartent les mesures protectionnistes »… 

Tous copains sur un thème, patrons européens, américains, canadiens, japonais 
repoussent ce spectre par un «refus commun de toute forme de protectionnisme».

Vivement l’avenir ! A condition qu’il ressemble au présent… Plus absurde encore, 
cette lutte est fondamentale «pour nos économies mais aussi nos démocraties.» 

Car taxer les importations, c’est bien connu, voilà le prélude du fascisme… 

Voilà leur talon d’Achille. Contre leur libre-échange, des barrières douanières. 
Des taxes aux frontières. Des quotas d’importation. La grosse artillerie. 
C’est notre dernière arme.

Les seules batteries qui les feront reculer. Sans quoi, tel un hamster dans 
sa cage, nous serons condamnés à faire tourner notre roue, plus vite, toujours 
plus vite, parce que le hamster allemand, le hamster roumain, le hamster 
chinois, pédale bien plus vite – ou pour moins cher. 

Dans cette course mortifère le «travail» est réduit à un coût – qu'il faut 
sans cesse baisser. 

La Sécurité sociale, les retraites décentes, le salaire minimum deviennent des 
boulets qui nous ralentissent. Les normes écologiques sont des «entraves» pour 
nos entreprises. A partir de reportages, de rencontres avec syndicalistes, 
patrons, économistes, douaniers, ce livre explore brillamment cette "hypothèse". 
      

