
  .. index::
   pair: Programmation Antigone  ; novembre 2012
   
.. _programmation_antigone_novembre_2012:
   
=====================================
Programmation Antigone Novembre 2012
=====================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=day&id=20120321&Itemid=48&el_mcal_month=3&el_mcal_year=2012
   
   
.. toctree::
   :maxdepth: 4
   
   14_novembre_2012  
