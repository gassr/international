
.. index::
   pair: Antigone  ; 2013
   
.. _programmation_antigone_avril_2013:
   
=================================
Programmation Antigone Avril 2013
=================================

.. contents::
   :depth: 3

.. _soeuf_elbadawi_2013:

Mercredi 3 avril 2013 Rencontre avec l'auteur, metteur en scène, comédien Soeuf Elbadawi
=========================================================================================

.. seealso:: http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=day&id=20130403&Itemid=48


autour de son ouvrage "Un dhikri pour nos morts"

Dans ce texte, également adapté au théâtre, il est question d'un archipel 
menacé d'effondrement, de l'annexion de Mayotte par la France, du 
prolongement de la relation coloniale, des morts du tristement célèbre 
Visa Balladur et d'une vingtaine de résolutions de l'ONU.


 
