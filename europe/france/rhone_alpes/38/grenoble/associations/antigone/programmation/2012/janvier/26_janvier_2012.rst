
  .. index::
   pair: Programmation Antigone  ; Résister à la chaîne
   
.. _resister_a_la_chaine_antigone_2012:
   
=====================================================================
Résister à la chaîne, Rencontre Christian Corouge, co-auteur du livre
=====================================================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=day&id=20120126&Itemid=48
   - :ref:`resister_a_la_chaine`
   

Pendant quatre jours je t’ai raconté des trucs sur le travail, les lois Auroux, 
les trente-huit heures? Seulement ça, je vais te le dire, ça crée un 
déséquilibre complet, parce qu’une semaine comme ça, c’est pas facile de la 
vivre quand tu travailles en chaîne et que t’as en plus plein de boulot 
syndical à faire. 

C’est pas facile. Alors mes mains, dans tout ça, qu’est-ce qu’elles deviennent, 
mes mains ? On dit : « Bon, en 1974, il avait mal aux mains. Maintenant ça a 
l’air de passer. Il est devenu beaucoup plus intellectuel, il n’a plus mal 
aux mains, il a mal à la tête? » Il est fou, quoi. Seulement, moi, je 
travaille encore avec mes mains ! Et ça, ça me fait toujours mal. 

**Mais maintenant je me tais.** 

Parce que, pendant dix ans, tu en souffres tout seul. Et en même temps, tu as 
l’impression d’être une espèce de cobaye, aussi bien de la part des copains 
qui veulent surtout pas écrire ce genre de truc avec moi, alors qu’en fait, 
à mon avis, leur boulot de militant c’est à eux que je devrais le dire, 
ç’aurait été de faire ce livre avec moi.

Au début des années 1980, le sociologue Michel Pialoux rencontre 
Christian Corouge, ouvrier et syndicaliste chez Peugeot-Sochaux. Ils entament 
un long dialogue sur le travail à la chaîne, l’entraide dans les ateliers et 
la vie quotidienne des familles ouvrières.

À partir de l’histoire singulière d’un ouvrier, devenu porte-parole de son 
atelier sans jamais le quitter, sont abordées les difficultés de la 
constitution d’une résistance syndicale.


