
  .. index::
   pair: Programmation Antigone  ; La Fabrique du Futur
   
.. _fabrique_du_futur:
   
=========================================================================
La Fabrique du Futur / Qu'est-ce que le travail 2ème partie (1er février)
=========================================================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=eventlist&Itemid=18
   
 
.. contents::
   :depth: 2
       

Cycle d’élaboration collective d’un projet économique post-capitaliste

Un autre monde est possible... oui, mais lequel ?
==================================================

ConvaincuE de l’inconsistance des projets de société actuels ? 

LasSE des revendications apathiques portées par les syndicats? D’avis qu’il 
faut s’approprier les problématiques économiques contre le règne des expertEs ? 

Besoin de joindre à la critique du système un travail de proposition? 

Volontaire pour aborder des questions de fond ? MotivéE pour partager, 
étoffer nos points de vue réciproques ?

CurieuxSE d’expérimenter la démocratie ?
========================================

Les groupes Post-capitalisme et Faranches nous invitent à prendre le temps de 
la réflexion pour penser le futur et les chemins qui y mèneront, renforçant 
nos luttes, ici et maintenant.

De quoi s’agit-il ?
====================

D’un cycle d’ateliers pour s’entraîner à la construction du monde économique 
de demain (après le capitalisme).

Qui est concerné-e ?
=====================

Débutant-e-s ou expert-e-s en matière d’économie, nous sommes tou-te-s 
concerné-e-s car nous avons tou-te-s quelque chose à dire sur ce qui régit 
en grande partie notre vie quotidienne. 

Vous êtes les bienvenu-e-s quelque soit votre culture politique.

Quels objectifs ?
=================

- Revisiter l’économie à un moment crucial : le capitalisme s’effondre, par 
  quoi allons nous le remplacer ? Nous tenterons de répondre à cette question 
  autrement que par : on veut le communisme ! ou l’anarchie !, 
  le communisme libertaire ou encore le socialisme !
- Commencer à élaborer une plate-forme de revendications. L’exercice sera de 
  lier nos envies et souhaits pour un monde futur aux réalités concrètes du 
  système actuel.
- Renforcer nos luttes : elles ne seront que plus fortes, plus cohérentes 
  et plus efficaces si l’on sait un peu où on veut aller.

Le principe
===========

Nous fonctionnerons par thème, chaque thème faisant l’objet de deux soirées.

- Les premières soirées seront dédiées à l’expression et à l’approfondissement 
  de nos imaginaires : Des ateliers nous permettront d’approfondir nos points 
  de vue respectifs tout en découvrant ce que les autres participant-e-s ont 
  en tête. Ce sera l’occasion d’apprendre mais aussi d’enseigner, de partager 
  nos réflexions et de clarifier nos idées.
- Les secondes soirées seront axées sur la transition vers une économie 
  post-capitaliste : Nous nous demanderons, à partir du système capitaliste 
  actuel, quelles premières pierres nous pourrons poser afin d’avancer sur la 
  voie du nouveau monde que nous aurons décrit. Pour nous inspirer, nous 
  réfléchirons à partir d’alternatives et revendications existantes, le tout 
  dans une perspective stratégique.  
  
  
