
.. figure:: antigone.png
   :align: center

   *Bibliothèque Antigone*
   
   
.. index::
   pair: Association; Antigone
   ! Antigone
    
   
.. _antigone_grenoble:
.. _antigone:
   
=================================
Bibliothèque Antigone (Grenoble)
=================================

.. seealso:: 

   - http://www.bibliothequeantigone.org/
   

.. figure:: antigone_2.png
   :align: center

   *Antigone*
   
   

.. contents::
   :depth: 2
   

Adresse et permanences
======================

.. seealso:: http://www.bibliothequeantigone.org/index.php?option=com_content&view=article&id=2&Itemid=3

::

    22 rue des violettes 
    38000 Grenoble

Pas facile de se garer dans le quartier, prenez le tram !
Tram C, arrêts "Vallier-Catane" ou "Dr Calmette"


Nos horaires de permanences habituels::

    Mercredi 16h - 21h.
    Jeudi 18h30 - 21h30.
    Vendredi 18h30 22h.


Présentation
============

.. seealso:: http://www.bibliothequeantigone.org/index.php?option=com_content&view=article&id=1&Itemid=2


Antigone, c'est une association d'agitation artistique en milieu populaire ; 
c’est un café autogéré, trait d’union entre la culture et le politique. 

Agiter les esprits et les individus autour des pratiques artistiques et 
culturelles, utiliser les multiples outils permettant l'expression et la 
création, être un élément de contacts, d'ouverture sur la diversité du monde 
contemporain ainsi qu'un lieu d'échange et de débat politique, voilà quelques 
uns de nos desseins.

Depuis septembre 2005, après deux années passées à Fontaine, Antigone s’est 
installée au 22 rue des violettes à Grenoble dans les locaux d’un 
ancien atelier-usine.

La bibliothèque, cœur du projet, met à la disposition des adhérent-es plus 
de 5000 titres enfants et adultes, avec un important fonds sur la pensée 
libertaire. (consultez le fonds ici)

La librairie, sa petite sœur, défend l’édition indépendante et la presse 
alternative.


Pour contacter Antigone
=======================

.. seealso:: http://www.bibliothequeantigone.org/index.php?option=com_contact&view=contact&id=1&Itemid=6


- 04-76-99-93-23
- 06-74-51-29-03
- antigone@ouvaton.org

.. warning:: Le téléphone fixe n'est pas à l'association. Aux heures de 
   permanences, essayer plutôt le téléphone portable ! 
   
      
Quelques mots pour ceux-celles qui voudraient nous faire parvenir une 
proposition de spectacle : Antigone est un café bibliothèque librairie qui 
fonctionne sans subvention, uniquement grâce aux forces de ses bénévoles. 

Toutes les personnes intervenant lors d’une soirée le font en soutien à 
l’association : il y a habituellement un prix libre à l’entrée qui sert au 
financement d’Antigone. 

Les artistes et intervenants sont nourris, éventuellement logés et abreuvés 
fort convenablement. Nous pouvons prendre en charge quelques frais de 
déplacement (dans la limite de nos modestes moyens).

Liste d'information
=======================

Aller sur la page http://www.bibliothequeantigone.org/index.php?option=com_contact&view=contact&id=1&Itemid=6
et remplir le formulaire.


Historique Antigone
===================

.. toctree::
   :maxdepth: 6
   
   historique/index


Programmation Antigone
======================

.. toctree::
   :maxdepth: 6
   
   programmation/index
   
   
   

