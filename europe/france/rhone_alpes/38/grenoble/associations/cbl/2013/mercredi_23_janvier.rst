
.. index::
   pair: Henry; Bulawko 
   pair: Cercle Bernard Lazare; Henry Bulawko 
   
.. _henry_bulawko_2013:

==================================================
Mercredi 23 janvier 2013 Henry Bulawko, un mentsch
==================================================

.. seealso::

   - http://www.cbl-grenoble.org/
   - http://www.cbl-grenoble.org/6-cbl-grenoble-16-action-0-page-1.html
   - :ref:`henry_bulawko`

.. contents::
   :depth: 3


Hommage à une grande figure du Cercle Bernard Lazare
Henry Bulawko, un mentsch 
   
Henry Bulawko (1918-2011)

Henry Bulawko a fondé en 1954, avec ses amis, le Cercle Bernard Lazare 
et, en 1957, sa revue, « Les Cahiers du Cercle Bernard Lazare », doublée 
d'une revue en yiddish, langue qui lui était si chère. 

Il était un fin connaisseur de la littérature de cette langue menacée 
par l'anéantissement du yiddishland, auteur notamment d'une biographie 
de l’écrivain Shalom Aleichem. Agnostique, Henri Bulawko avait conservé 
l'esprit des hassidim du monde perdu de sa jeunesse. 

Rescapé d’Auschwitz, il était habité par une grande joie de vivre qui 
se manifestait par son sens de l'humour (il fut également l'auteur d'une 
« Anthologie de l'humour juif et israélien »).

Journaliste, écrivain, traducteur, Henry Bulawko a été sur tous les 
fronts de la mémoire mais aussi du temps présent pour bâtir un monde 
plus juste, plus fraternel, avec pour ligne d'horizon la paix au Proche-Orient. 

Avec un remarquable esprit synthétique, il savait résumer et conclure 
une discussion. Et aussi, il était toujours capable de trouver le « witz » 
(le mot d’esprit) qui soulignait son propos.

Ses liens avec Grenoble furent nombreux et justifient l'hommage qui lui 
est rendu en Mairie. Pendant des décennies, dès le début des années 1960, 
il ne se passait pas une saison sans une visite de Henry Bulawko dans 
notre ville. Il nous rejoignait à chaque commémoration, célébration, 
événement politique. 

Il analysait l'actualité d'une manière non conformiste, sans manichéisme. 

C'est à cet homme que le Cercle Bernard Lazare – Grenoble doit son existence. 

Mais Henry Bulawko était connu ici bien avant la naissance du Cbl-Grenoble. 

Nous espérons que cette soirée sera l'occasion d'évoquer tous ces 
grenoblois qui, avec lui, ont fait renaître notre communauté après la 
seconde guerre mondiale.

Avec ses compagnons et collaborateurs nous faisons nôtre l'héritage 
culturel, politique et humain que nous laisse Henry Bulawko. 

Organisateur hors pair, tribun défendant les idées de paix et de progrès, 
infatigable combattant contre le racisme et l'antisémitisme, et grand 
défenseur de la mémoire juive, Henry Bulawko portait en lui toutes les 
composantes de la judéité moderne. 

Et il savait faire confiance aux jeunes générations pour poursuivre ses 
combats.

Très jeune, il s'était engagé dans la Résistance et le sauvetage d'enfants, 
avec le comité Amelot. Il fait partie des rares rescapés de la Shoah à 
avoir endossé, dès leur retour des camps nazis, le rôle de porteurs de 
mémoire. Et à en avoir fait l'engagement de toute une vie.

Humaniste, sioniste, Henry Bulawko était un homme de gauche toujours 
impliqué dans la société française, ayant gardé toute sa vie cette 
jeunesse d’esprit qui préservait sa capacité d’indignation.
   
