

.. index::
   pair: Hachomer ; Hatzaïr
   pair: Henry ; Bulawko

.. _henry_bulawko:

==================================================
Henry Bulawko (1918-2011)
==================================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Henry_Bulawko
   - http://fr.wikipedia.org/wiki/Hachomer_Hatza%C3%AFr
   - :ref:`henry_bulawko_2013`
   

.. contents::
   :depth: 3   
   
Introduction
============

Henry Bulawko, né le 25 novembre 1918 à Lida, alors en Lituanie, 
désormais en Biélorussie, et mort le 27 novembre 2011 à Paris, est un 
journaliste1, historien, traducteur et écrivain juif français, déporté 
à Auschwitz, qui présidait l'Union des déportés d'Auschwitz.


Paris
=====

Arrivé à Paris, en 1925, à l'âge de 7 ans, avec sa famille, sa langue 
maternelle est le yiddish. Il en apprécie la richesse, qui inclut sa 
littérature et son humour.

À Paris, son père, Shlomo Zalman Bulawko, est un rabbin orthodoxe non 
consistorial français qui habite rue Le Regrattier, sur l'île Saint-Louis, 
dans le 4e arrondissement de Paris. Auteur de Haschorass Hanefesh 
(L'Éternité de l'âme humaine), publié en 1936, il décède cette année-là. 

Henry Bulawko a alors 18 ans. Il sera toujours discret sur son père, ne 
deviendra pas rabbin, mais comme lui publiera et sera un membre actif 
du Hachomer Hatzaïr, mouvement se revendiquant juif, sioniste, mais 
laïque. Lucien Lazare (1987) a écrit : « Une volonté d'agir au service 
de la population juive poussa l'un des animateurs de ces réunions 
[informelles de jeunes sionistes], Henri [sic] Bulawko, militant du 
`Hashomer Hatzaïr`_, à s'adresser au grand rabbin de Paris, Julien Weill, 
qui l'orienta vers la rue Amelot. Il fut associé au travail social et 
chargé de grouper la jeunesse. »

.. _`Hashomer Hatzaïr`: http://fr.wikipedia.org/wiki/Hachomer_Hatza%C3%AFr


La Résistance
==============

Henry Bulawko fait partie de la Résistance de novembre 1940 au 19 
novembre 194216. Il a 22 ans lorsqu'il entre dans la Résistance.

Reconnu comme un Juste parmi les nations, Joseph Migneret (1888-1949) 
héberge, cache, aide ou sauve des familles dont celles de Ady Steg et 
de Henry Bulawko.

Le Comité Amelot est créé le 15 juin 1940 par des responsables de la 
Fédération des sociétés juives de France (FSJF), du Bund, du Poale Zion 
de gauche et du Poale Zion de droite.

André Kaspi (1991) écrit20 : « […] la Colonie scolaire, qui a son siège 
à Paris, au 36, rue Amelot. On la désigne soit sous le nom de son 
dispensaire “La Mère et l'enfant”, soit tout simplement par sa 
localisation “le Comité de la rue Amelot” »21, fondé par Léo Glaeser 
en juin 1940 et animé jusqu'en 1943 par David Rapoport22 (qu'assiste 
jusqu'en novembre 1942 Henry Bulawko). Le Comité de la rue Amelot 
regroupe la Colonie scolaire et des cantines populaires. 
Il sert 2 000 repas quotidiens en 1941. Ses dispensaires donnent 
1 000 consultations médicales par mois, assurent des soins, disposent 
d'un service dentaire. Les enfants de 300 familles et des orphelins 
sont pris en charge, et dès 1941 certains d'entre eux sont placés à 
la campagne. Le Comité de la rue Amelot n'entre pas à l'UGIF, mais 
tire parti de sa protection

Selon Lucien Lazare : « Une commission animée par Bulawko assurait la 
fabrication de faux titres d'identité et d'alimentation. La Rue Amelot 
était le recours des Juifs traqués n'ayant de chance de salut qu'en se 
camouflant aussi complètement que possible. »

À la suite de la rafle du Vélodrome d'Hiver (16-17 juillet 1942), 
Henry Bulawko raconte : « Nous décidâmes d'accroître le placement 
d'enfants et la diffusion de fausses cartes, avec assistance pour 
passer en zone sud. Nos assistantes aryennes, accompagnées souvent 
de leurs maris non juifs, sillonnent des régions pour trouver des 
“planques”. Des parents, rescapés de la rafle ou non visés ce jour-là, 
qui avaient hésité jusque-là à confier leurs enfants, changèrent d'avis. 
Cela nous obligea à renforcer le travail de planquage et à effectuer 
de nombreux voyages en province. »

Henry Bulawko est arrêté en novembre 1942, au métro Père-Lachaise 
(Père Lachaise (métro de Paris)). Il décrit ainsi l'événement : 
« Un inspecteur m'interpelle. Il voit mon étoile et m'invite à le 
suivre. Quel est mon crime ? Je l'apprends plus tard. Je suis accusé 
de camoufler mon étoile avec un livre et une gabardine que je porte 
sous le bras. J'essaie de prouver l'absurdité de cette accusation, 
mais le flic ne connaît qu'une logique : la sienne. » Ce passage 
continue ainsi : « Il faut que la rafle dont il est chargé soit 
rentable. Je suis son seul “client” de la journée et il n'a pas 
l'intention de le lâcher. »

Lorsqu'il est arrêté le 19 novembre 1942, il porte sur lui de nombreux 
faux papiers. Il réussit à s'en débarrasser avant d'être fouillé. 
Il est interné à Beaune-la-Rolande puis au camp de Drancy jusqu'au 
18 juillet 1943. Il est donc interné huit mois en France, avant d'être 
déporté en Pologne.

...
