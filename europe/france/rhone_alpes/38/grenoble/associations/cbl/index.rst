
.. index::
   pair: Cercle Bernard Lazare; Grenoble
   
   
.. _cbl_grenoble:

================================
Cercle Bernard Lazare (Grenoble)
================================

.. seealso::

   - http://www.cbl-grenoble.org/
   - http://www.bernardlazare.org/

.. contents::
   :depth: 3


Présentation
=============

Sur le plan politique, le Cercle défend les valeurs des Droits de l'Homme, 
telles que les luttes contre le racisme, les exclusions, ... le lien 
consubstantiel entre Israël et les Juifs du monde. 

A ce titre, il est indéfectiblement attaché à  l'existence de l'Etat 
d'Israël. Ceci n'implique pas nécessairement son adhésion aux politiques 
des gouvernements de ce pays.

Il est proche du mouvement `La Paix Maintenant`_

.. _`La Paix Maintenant`: http://www.lapaixmaintenant.org/


Actions
=======

.. toctree::
   :maxdepth: 3
   
   2013/index
  

Paris
=====

.. seealso::
 
   - http://www.bernardlazare.org/
     
   
Militants
==========

.. toctree::
   :maxdepth: 3
   
   militants/index
   
   
   
