
.. index::
   ! Entropie
   pair: Associations; Entropie
   pair: Grenoble; Entropie

.. _entropie:

========
Entropie
========

.. contents::
   :depth: 3

Adresse courriel : entropie.asso@yahoo.fr
=========================================

- entropie.asso@yahoo.fr


Site web
========

- http://www.facebook.com/entropie.laboratoire

Présentation
============


Ce projet est né du constat d’une carence de nos formations. Celles-ci nous
préparent uniquement à nous intégrer au système dominant, et ne nous
proposent pas d’alternatives aux structures économiques et politiques.

L’ambition de cette deuxième édition est de mettre en lumière toute la
richesse de ces expériences alternatives et de proposer un panel d’outils
dont chacun puisse s’emparer pour construire son alternative. 

Nous avons voulu, pour cette édition, mettre l’accent sur la question de 
l’organisation mais aussi sur l’articulation possible entre la construction 
d’alternatives et les luttes sociales.

Pause repas - resto U à proximité !


Le festival
===========

.. toctree::
   :maxdepth: 3
   
   festival/index
   
   
