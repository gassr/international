


.. _entropie_jeudi_11_octobre_2012:

============================================================
Festival Utopie et modèles de société jeudi 11 octobre 2012
============================================================

.. seealso::

   - :ref:`entropie_festival_2012`

.. contents::
   :depth: 3
   

10 heures Présenttaion du festival par Christophe André // Association Entropie
===============================================================================


Ce projet est né du constat d’une carence de nos formations.

Celles-ci nous préparent uniquement à nous intégrer au système dominant, et ne
nous proposent pas d’alternatives aux structures économiques et politiques.

L’ambition de cette deuxième édition est de mettre en lumière toute la richesse
de ces expériences alternatives et de proposer un panel d’outils dont chacun
puisse s’emparer pour construire son alternative.

Nous avons voulu, pour cette édition, mettre l’accent sur la question de
l’organisation mais aussi sur l’articulation possible entre la construction
d’alternatives et les luttes sociales.

11h30 Pause repas - resto U à proximité !
==========================================

12h30 Film (90 min) Videografía Zapatista // Promedios prod. (1998 – 2010)
==========================================================================


L’histoire est rarement racontée par ceux qui la vivent et qui la font.
Projection de 5 films réalisés par des promoteurs de communication au
sein d’une pratique vidéo-collective engageant les communes autonomes
zapatistes du Mexique.


14h Atelier (120 min) // Débat en étoile sur les pratiques de débat dans l’autogestion
======================================================================================


Hélène Blanchard // Association Virus36

Débattre, c’est construire une intelligence collective.

Pour ne pas dissocier nos valeurs de nos pratiques, découvrons une forme de
débat coopérative : le débat en étoile.


16h30 Pause goûter maison
=========================


17h Conférence // Un développement alternatif, le cas de l’Asociacion para el Desarrollo Campesino en Colombie
==============================================================================================================


Laura Pérez Medina // Association Entropie

Au milieu de la guerre en Colombie, un collectif de producteurs autochtones
et paysans confirme la possibilité de vivre autrement.

La solidarité et l’harmonie avec leur communauté et leur environnement, sont
les bases de cette expérience. Découvrons comment un développement alternatif a
pu être possible dans un contexte aussi difficile que celui de la Colombie.


18h30 Pause repas - resto U à proximité !
==========================================



20h Film (80 min) Oaxaca, entre rébellion et utopie // Miriam Fischer, Mexique-Allemagne (2007)
=================================================================================================


Débat animé par Marc Tomsin (site internet La voie du jaguar et éditions
Rue des Cascades).

Nous explorerons avec lui comment l’organisation communautaire indigène a
influencé le chemin parcouru par l’Assemblée populaire des peuples d’Oaxaca
(APPO) en 2006 au Mexique.


