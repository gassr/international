


.. _entropie_samedi_13_octobre_2012:

==============================================================
Festival Utopie et modèles de société samedi 13 octobre 2012
==============================================================

.. seealso::

   - :ref:`entropie_festival_2012`

.. contents::
   :depth: 3
   

10h Témoignage // Ardelaine, une coopérative de développement local
====================================================================

.. seealso::

   - http://www.ardelaine.fr/histoire.html
   - http://www.autogestion.coop/spip.php?article133

Beatrice Barras // SCOP Ardelaine

Béatrice Barras, la directrice, nous explique comment une filière de laine
restructurée est dévenue un nouveau modèle socio-économique.

Une cinquantaine de personnes y réalisent des activités très diversifiées
concernant le développement rural, la coopération, l’écologie et
réinterrogent l’organisation du travail, la hiérarchie, les salaires, 
le rapport au territoire, le mode de développement, le rapport ville/campagne 
et globalement, le rapport au ``vivant``.



11h30 Pause repas - restauration bio et locale sur place!
=========================================================


12h30 Film (85 min) The Yes Men Fix the World // Andy
=====================================================

.. seealso:: 

   - http://en.wikipedia.org/wiki/The_Yes_Men_Fix_the_World
   - http://en.wikipedia.org/wiki/The_Yes_Men

Bichlbaum et Mike Bonanno (2009).

Qui aurait pensé que réparer le monde pourrait être aussi amusant ?

Une histoire vraie au sujet de deux militants politiques qui, se faisant
passer pour des cadres supérieurs de sociétés géantes, se trouvent dans
les conférences des grandes entreprises et y réalisent les bouffonneries
les plus extravagantes.


.. _faranche_13_octobre_2012:

14h Atelier (150 min) : Que proposer quand on est contre les élections ? // Les Faranches
=========================================================================================

.. seealso::

   - http://en.wikipedia.org/wiki/Bookchin
   - http://en.wikipedia.org/wiki/Stephen_Rosskamm_Shalom
   - http://en.wikipedia.org/wiki/Participatory_economics
   - http://en.wikipedia.org/wiki/Ezequiel_Adamovsky
   - http://ebookbrowse.com/adamovsky-pdf-d282748031

Le collectif autogéré Les Faranches propose un atelier participatif sur les
alternatives à la démocratie representative. 

Il traitera les cas de La Catalogne de 1936 et des conseils communaux au 
Venezuela.

Les bases théoriques de leur intervention seront: 

- le municipalisme libertaire de Bookchin, 
- les constributions de Stephen Rosskam Shalom sur le participalisme politique 
- et celles d’Adamovski sur l’Assemblée des mouvements sociaux.


17h Pause goûter maison
=======================


17h30 Témoignage // 25 ans d’autogestion dans une scierie
=========================================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Ambiance_Bois
   - http://www.autogestion.coop/spip.php?article46

Marc Bourgeois // Ambiance Bois

A travers leur présentation nous découvrirons qu’on peut gérer une usine
autrement.

Polyvalence, partage des tâches et des responsabilités, absence de hiérarchie,
salaires égaux, choix du temps de travail, décisions collectives sont les
éléments principaux qui caractérisent leur approche pratique de l’autogestion.


18h30 Pause repas - restauration bio et locale sur place!
=========================================================

20h Soirée musicale
===================


Les Contratakerz, groupe de Hip-Hop grenoblois, viendront nous injecter
de l’énergie pure. 

Laissons nous emporter par le flow de leurs textes et partageons l’univers de 
chacun des six MC, leur vision de la musique et l’énergie qu’ils déploient.



