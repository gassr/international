


.. _entropie_dimanche_14_octobre_2012:

==============================================================
Festival Utopie et modèles de société dimanche 14 octobre 2012
==============================================================

.. contents::
   :depth: 3
   

10h Témoignage // Comment l’utopie est devenue réalité à Eybens
===============================================================

.. seealso::

   - http://www.lelabo-ess.org/?Le-Zeybu-solidaire-cooperer-et
   - http://lesamisduzeybu.free.fr/
   - http://www.alpesolidaires.org/le-principe-du-zeybu-solidaire

Laure Taraud // Zeybu

Découvrons le principe du Zeybu Marché et Zeybu Solidaire. 

Comment la disparition d’une épicerie de quartier a donné naissance à 
une utopie où se mêle solidarité, produits locaux, développement durable, 
faire ensemble, citoyenneté, mixité sociale et intergénérationnelle, 
témoignant d’une volonté commune de vivre ensemble !


11h30 Pause repas - restauration bio et locale sur place!
=========================================================


12h30 Débat // Le projet institutionnel peut rêver. De l’institué à l’instituant et vice-versa, comme une valse
===============================================================================================================

.. seealso:: http://associationlevillage.fr/

Vincent Delahaye // association Le Village (Cavaillon)

Le Village est un lieu de vie qui accueille 28 personnes sur du logement
collectif et 36 personnes sur un chantier d’insertion. 

Dans cet endroit on explore et déconstruit les formats. 

La stratégie : rester à bonne distance des dispositifs institutionnels pour 
pouvoir les réinterroger. 

Les valeurs : construire mieux que réparer, émanciper mieux qu’autonomiser et 
vivre mieux que survivre.


14h Atelier 1 // Partager la parole et éviter les prises de pouvoir dans les collectifs
=======================================================================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Alternative_Libertaire
   - http://www.alternativelibertaire.org/

Adeline Le Pinay et Benjamin Koskas // Alternative Libertaire

Explorons à travers cet atelier le rapport entre la prise de parole dans un 
collectif et les enjeux de pouvoir qui peuvent émerger: 

- soumission/domination, 
- autorité exercée ou consentie, 
- (auto) censure...

Inscriptions par mail entropie.asso@yahoo.fr.

Salle culturelle Berlioz- Campus Universitaire
361, allée Hector Berlioz, St Martin d’Hères (38)


.. _festival_entropie_14_octobre_2012_atelier_14h:

Dimanche 14 octobre 2012 14h Atelier 2 // La co-formation
==========================================================

.. seealso::

   - http://en.wikipedia.org/wiki/Saul_Alinsky
   - http://www.iopsociety.org/france/rhone-alpes/grenoble/fr
   - http://www.iopsociety.org/profile/francis-feeley/fr
   - :ref:`francis_feeley`
   - :ref:`ac_methode_saul_alinsky`
   - :ref:`saul_alinsky`   

Christophe André // Association Entropie
Francis Feeley // Université Stendhal 3

Une co-formation c’est un temps pour apprendre et partager ses connaissances à
travers un dispositif pédagogique et des documentations de référence.

Chaque participant occupe tour à tour une position d’apprentissage puis
d’échange de ses découvertes. 

A cette occasion nous nous intéressons aux sociétés alternatives aussi qu’aux 
idées d’un des fondateurs du ``community organizing``  aux Etats-Unis, 
Saul Alinsky (1909-1972).


16h30 Pause goûter maison
=========================


17h Témoignage // Boulangerie La conquête du Pain.
===================================================

.. seealso::

   - http://laconquetedupainmontreuil.wordpress.com/
   - http://fr.wikipedia.org/wiki/Pierre_Kropotkine

Leur nom fait référence à la théorie du communisme libertaire de Pierre
Kropotkine.

Autogérés, ils veulent mettre en commun et partager. 

Libertaire, parce qu’ils refusent l’idéologie autoritaire et pensent que 
l’égalité sans la liberté n’est rien. 

Et du pain parce que ils sont boulangers !


18h30 Pause repas - restauration bio et locale sur place!
=========================================================


20h Clôture
============

.. seealso::

   - http://fr.wikipedia.org/wiki/%C3%89lis%C3%A9e_Reclus

Toute révolution, si grandiose qu’elle apparraisse par les masses qu’elle a 
soulevées, n’a rien en propre qu’elle puisse apporter au monde. 

Elle se borne à proclamer ce qui était non seulement conçu mais déjà réalisé. 
Elle révèle ce qui existait sous les institutions vieillies, elle montre le 
nouveau vêtement dont s’était revêtue l’humanité sous les hardes que le temps 
déchire. 

Pour que l’anarchie triomphe, il faut qu’elle soit déjà une réalité concrète 
avant les grands jours qui viendront; il faut que nos oeuvres fonctionnent et 
que partout, dans nos journaux, dans nos groupes, dans nos écoles, nul ne 
donne des ordres, nul compagnon ne soit le serviteur des autres, que tous 
soient vraiment des égaux et des camarades, n’ayant d’autre guide et d’autre 
contrôle que leur propre respect et le respect d’autrui, car sans la bonne
solidarité entre les compagnons, il ne saurait y avoir de liberté pour 
personne ! 

Elisée Reclus [1892] «Aux Compagnons rédacteurs des Entretiens», 
Extrait des entretiens politiques et littéraires.


entropie.asso@yahoo.fr
Site du festival http://vivrelutopie.free.fr/




