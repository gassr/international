.. index::
   pair: Entropie; Festival


.. _entropie_grenoble:

========
Entropie
========

.. contents::
   :depth: 3
   

Définition
==========

.. seealso:: http://fr.wikipedia.org/wiki/Entropie

Présentation
============   

Ce projet est né du constat d’une carence de nos formations. Celles-ci nous
préparent uniquement à nous intégrer au système dominant, et ne nous
proposent pas d’alternatives aux structures économiques et politiques.

L’ambition de cette deuxième édition est de mettre en lumière toute la
richesse de ces expériences alternatives et de proposer un panel d’outils
dont chacun puisse s’emparer pour construire son alternative. 

Nous avons voulu, pour cette édition, mettre l’accent sur la question de 
l’organisation mais aussi sur l’articulation possible entre la construction 
d’alternatives et les luttes sociales.


.. toctree::
   :maxdepth: 3
   
   2012/index
   
   
