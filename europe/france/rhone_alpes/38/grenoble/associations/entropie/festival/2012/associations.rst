
.. _associations_entropie_festival_2012:

=============
Associations
=============

Retrouvez tout au long du festival les associations partenaires du Laboratoire 
Entropie.

Antigone
========

.. seealso:: 

   - http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=eventlist&Itemid=18#pg20121011
   - :ref:`antigone_grenoble`

Antigone, qui tiendra un stand avec ses livres, est une association-café 
autogérée défendant l’édition indépendante et la presse alternative, c’est un 
trait d’union entre la culture et le politique.

Les Renseignements Généreux
===========================

.. seealso::
  
   - :ref:`rg_grenoble`
   -  http://www.les-renseignements-genereux.org/liste-de-diffusion

Les Renseignements Généreux auront un stand avec leur textes pédagogiques sur 
des sujets essentiels! 

Constitués en association, ils contribuent à forger des outils d’autodéfense 
intellectuelle, imaginer, construire et faire découvrir des actions politiques 
ou des alternatives pertinentes.

Qu'est-ce qui renforce les luttes collectives ? Qu'est-ce qui les fragilise de l'intérieur ?
--------------------------------------------------------------------------------------------

.. seealso::

   - http://www.les-renseignements-genereux.org/fichiers/15302

Réflexions pratiques sur le militantisme avec Nathalie Dom
Propos recueillis par Les Renseignements Généreux

Repérages
=========

.. seealso::

   - http://www.reperagesvelo.org/
   - http://www.reperagesvelo.org/pr%C3%A9sentation.php
   - http://www.alpesolidaires.org/reperages-valoriser-le-velo-ici-et-la-bas

Repérages, qui proposera une bourse aux vélos, est une association qui fait 
du recyclage de vélo un objet d’insertion et de coopération avec des 
associations en Afrique.

