


.. _entropie_vendredi_12_octobre_2012:

==============================================================
Festival Utopie et modèles de société vendredi 12 octobre 2012
==============================================================

.. seealso::

   - :ref:`entropie_festival_2012`

.. contents::
   :depth: 3
   

10h Atelier : Rencontre autour de l’écriture du Rap // Duval Mc.
================================================================


En atelier, les ``auteurs`` d’un jour, peuvent transcender leurs quotidiens,
nous faire partager leurs humanités et leurs manières d’appréhender les
petits et grands problèmes de l’existence (12 participants au maximum).
Inscriptions par mail ``entropie.asso@yahoo.fr``


10h  Tournoi de beach volley, face à la Résidence Ouest
========================================================

10h  Tournoi de pétanque, autour de la Salle Berlioz
=====================================================


10h Jeux de société collaboratifs sur les tables à l’extérieur de la Salle Berlioz // Maison des jeux de Grenoble
==================================================================================================================

Inscriptions par mail ``entropie.asso@yahoo.fr``


11h30 Pause repas - Amenez votre sandwich pour regarder le film !
=================================================================


11h30 Film (127 min) The Take // Avi Lewis et Naomi Klein (2004)
=================================================================

Tourné en Argentine, ce film se penche sur le phénomène des entreprises 
autogérées par les salariés suite à la crise financière de 2001.

**John Jordan, un des caméramans du film, sera présent lors de la projection.**


.. _festival_entropie_12_octobre_2012_atelier_14h:

Vendredi 12 octobre 2012 14h Atelier // Mouvements Citoyens
===========================================================

.. seealso::
 
   - http://projet-echo.org/
   - :ref:`alliance_citoyenne`

**5 tables de discussion** autour d’expériences d’organisation citoyenne.

Rencontrons ses représentants et découvrons les processus suivis par:

- l’Alliance Citoyenne  (Grenoble)
- Antigone (Grenoble)
- ReAct
- Reclaim the Streets, 
- l’Armée des clowns,
- le Laboratoire de l’imagination Insurrectionnelle, 
- le CEIMSA, le SNESup et les IATOS des Universités Grenobloises.

- Pourquoi se sont-ils organisés ?
- Comment l’ont-ils fait ?
- Comment prennent-ils leurs décisions ?
- Quels sont les bénéfices de cette démarche ?

16h30 Pause goûter maison
=========================

17h Témoignage // Un lieu des co-errances
=========================================

Collectif d’Action des Sans Abris (CASA) // Avignon

Laetitia Home-Ihry, la coordinatrice de Casa et deux personnes accueillies
par le centre confronteront leurs points de vue vis-à-vis de l’expérience
de Casa Avignon. 

En essayant de complexifier les visions, elles tenteront de faire partager 
la notion de «CO», «avec» qui construit leur quotidien :

- coorganisation, 
- coresponsabilité, 
- coproduction, 
- cogestion, 
- coordination,

dans une interdépendance réelle.

18h30 Pause repas - restauration bio et locale sur place!
==========================================================

20h Film (48 min) Vi Vil Danse // en présence de sa réalisatrice, Carole Thibaud
==================================================================================


Projection sur la ville libre de Christiania, au Danemark **Vi Vil Danse** est 
la parole de ceux qui se sont battus et ont participé à l’organisation d’une 
ville libre et autogérée, de 40 hectares et 1000 habitants au cœur d’une 
capitale européenne : Copenhague. 

**Vi Vil Danse** montre la fin d’une utopie, pour l’empêcher de sombrer dans 
l’oubli et donner encore la force de s’indigner et le courage d’en inventer 
d’autres.

