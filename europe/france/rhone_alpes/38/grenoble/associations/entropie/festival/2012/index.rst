
.. index::
   pair: Rethinking ; Society
   pair: Entropie ; Festival 2012
   pair: astroturf ; democracy
   pair: grassroot ; democracy
   
   
   
.. _entropie_festival_2012:

===============================================================
Festival Utopie et modèles de société 2012, rethinking society
===============================================================

.. figure:: rethinking_society.jpg
   :align: center
   
   Rethinking society

.. seealso::

   - http://vivrelutopie.free.fr/
   - http://www.iopsociety.org/events/rethinking-society-festival-vivre-lutopie-2012-/fr
   - http://dimension.ucsd.edu/ceimsa-in-exile/indexE.html
   
   
.. contents::
   :depth: 3
 
 
Programme bilingue à télécharger
================================

:download:`Programme bilingue à télécharger <Programme_11_14_octobre_2012_eng_fr.odt>`   

Re-Think Society Festival
=========================

.. seealso:: 

   - http://www.iopsociety.org/events/rethinking-society-festival-vivre-lutopie-2012-/fr
   - http://www.iopsociety.org/events/no-where-is-now-here-new-paths-in-utopia/fr


The Association Entropie in Grenoble, France invites you to attend its 
four-day International; bilingual “Festival pour repenser la société,” 
(our Re-Think Society Festival), which will be held on the campus of Stendhal 
University from October 11 thru 14, 2012.

This event will attempt to make more visible the actual experiments in society 
that have attempted to introduce grassroots democracy in existing institutions. 

These attempts arose out of the sterile milieu of university studies which do 
no more that prepare us to fit into the existing model of alienation, with no 
hope for alternative life styles.

The Association Entropie attempts to defend and give priority to values which 
to us seem essential, such as mutual assistance, collective cooperation, and 
solidarity –all necessary qualities to construct and give life to “Utopia.” 

We believe that it is not enough to denounce the existing capitalist system, 
the obvious source of endless injustices and inequalities; but above all we 
must support initiatives, organizations, collective movements which operate 
independently of this economic logic, which go against the main stream.

During the four days of October 11-14, 2012, we are proposing a series of 
conferences to provide the opportunity for people to speak about their person 
experiences (such as self-managed food co-ops, self-managed living co-ops, 
collective citizen actions…) as well as participate in theoretical discussions 
on diverse subjects related to this central theme, “Utopia and Social Projects.” 

(Present; for example, will be the “Faranches” citizens from Villar-d’Arêne, 
the commune of Medieval fame in the Alps, not far from Grenoble, and speakers 
from the heroic commune at Oxaca in Mexico.) 

During this four-day series of conferences, you are invited to participate in 
organizing workshops along with others attending specific discussions groups 
and documentary film projections.

At one of these workshops, on Sunday, October 14, Francis Feeley will present 
experienced community organizers from the United States who will recount their 
material experiences with what has been called Astroturf democracy, in contrast 
with grassroots democracy; the difference being that the former concept 
reflects an inauthentic democracy, where the  decision-making process is 
conducted from the top down, by a so-called “elite” representing the interests 
of dominant institutions, usually existing outside the community, whereas the 
term  “grassroots democracy” reflects an authentic bottom-up decision-making 
process, in which free participation in discussions is encouraged by all people 
within the community who are naturally concerned with the consequences of 
decisions governing their individual and collective future.

We are expecting around 600 people to attend the conference, the majority of 
which will be high school and university students, with whom we are in close 
contact. These youth have not yet entered into the workplace (such as it is) 
and as a result they are less conditioned to adapt themselves to the needs of 
the workplace. For many of them, it is still possible to imagine a different 
world, a world in which they are encouraged to use their knowledge and skills 
to construct their own “utopia.” However, our aim, as always, is to make 
contact with as divers a public as possible. 

We have undertaken the task to  inform the most people possible about the
possibilities of living differently, and the positive effects of such behavior 
on society, for individuals as well as for the group and for society in general.

Last year our fist annual conference (Oct. 6-8, 2011) was held on the 
University of Grenoble campus and drew about 500 people (most of whom were 
students). 

By all accounts last year’s three-day conference “Vivre l’Utopie” was a success, 
and we have decided to continue our tradition of “free admission” to make 
this culture available to as many people as possible, this year in our cultural 
center at 15 rue George Jacquet.

We hope to see many of you in Grenoble next October.

In solidarity,
Christophe André
Contact : <entropie.asso@yahoo.fr>


Festival pour repenser la société, Une Conférence internationale des 11 à 14 octobre 2012
==========================================================================================

L’association Entropie vous invite à assister à la conférence internationale 
de quatre jours,  «Festival pour repenser la société», du 11 au 14 octobre 
2012 à Grenoble.

Cet événement est une manifestation qui a pour ambition de mettre en lumière 
des expériences sociétales alternatives qui envisagent des rapports plus 
démocratiques. 

Cette volonté est née du constat de véritables carences de nos formations 
universitaires qui nous préparent à nous insérer dans le modèle dominant, mais 
ne nous proposent pas d’alternatives. 

L’association Entropie entend défendre et valoriser des valeurs qui nous 
semblent essentielles telles que l’entraide, la collaboration et la solidarité, 
pour ensemble construire et « vivre l’utopie ». 

Il s’agit donc non seulement de dénoncer le système capitaliste actuel ; source 
apparemment inépuisable d’injustices et d’inégalités, mais surtout de valoriser 
des initiatives, des organisations ; des mouvements collectifs qui fonctionnent 
indépendamment de cette logique économique, quitte à aller à contre-courant. 

Pendant les quatre jours d’événements les 11, 12, 13, et 14 octobre 2012, nous 
proposerons des conférences de personnes qui viendront témoigner de leurs 
expérience, (scopes autogérés, foyers d'hébergement autogérés, 
collectif citoyen...) ainsi que des conférences théoriques sur différentes 
questions en lien avec le thème central « Utopie et projets de société » 
(Les faranches, la commune d’Oxaca...). 

Il y aura aussi des ateliers, notamment sur la prise de décision en groupe et 
les protocoles de débats, ainsi que des projections de documentaires et de films.  

Dans un de ces ateliers, le dimanches 14 octobre, Francis Feeley est invité à 
présenter des activistes américains et leur expérience concrète du concept 
de "astroturf democracy" par rapport au concept alternatif "grassroots democracy", 
leur expérience de la démocratie inauthentique où les décisions semblent venir 
du peuple quand en réalité elles sont imposées par une soi-disant élite dans 
les institutions dominantes en opposition à une démocratie authentique où les 
décisions sont prises à la suite d'une discussion libre entre les personnes 
concernées qui  par les décisions qui vont déterminer leur avenir collectif et 
individuel.

Sur l’ensemble de ces rencontres, nous attendons près de 600 personnes, avec 
une majorité d’étudiants car nous avons des liens bien établis avec ce public, 
et celui-ci n étant pas encore entré dans la vie active, il lui est plus facile 
de faire le choix d’utiliser ses connaissances dans le but de construire 
sa « propre utopie ». 

Cependant, nous souhaitons toucher un public le plus divers possible, nous 
tenons à informer au plus large la population sur les possibilités 
d’entreprendre « autrement »,  et sur l’impact positif de telles démarches, 
tant pour les individus que pour la collectivité et la société en générale. 

La première édition de « Vivre l’utopie » a rassemblé environ 500 personnes 
(dont une grande majorité d’étudiants) et les retours qui nous ont été faits 
étaient très positifs, ce qui nous donne envie de pérenniser cet événement 
qui sera gratuit, toujours dans l’idée de le rendre accessible à tous.

Nous espérons vous voir en grand nombre à Grenoble des 11 à 14 octobre.

En solidarité,

Christophe André

Contact : <entropie.asso@yahoo.fr>


Lieu : Salle Berlioz - Campus universitaire Grenoble // Saint Martin d’Hères (38)
=================================================================================

::

	du jeudi 11, 12, 13, au dimanche 14 octobre. 2012
	Salle Berlioz - Campus universitaire
	Grenoble // Saint Martin d’Hères (38)
   

Associations présentes 
======================

.. toctree::
   :maxdepth: 3
   
   associations
   


Journées du 11 au dimanche 14 octobre 2012
==========================================

.. toctree::
   :maxdepth: 3
   
   jeudi_11_octobre
   vendredi_12_octobre
   samedi_13_octobre
   dimanche_14_octobre
   
   
   
   
