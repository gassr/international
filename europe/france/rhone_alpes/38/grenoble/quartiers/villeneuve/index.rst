
.. index::
   pair: Villeneuve; Grenoble
   ! Villeneuve Grenoble
 
.. _villeneuve_grenoble:
   
=================================
Villeneuve Grenoble
=================================

.. seealso::

   - http://fr.wikipedia.org/wiki/La_Villeneuve_%28Grenoble%29
   
   
   
.. toctree::
   :maxdepth: 3
   
   lieux/index
   plainte_france2/index
