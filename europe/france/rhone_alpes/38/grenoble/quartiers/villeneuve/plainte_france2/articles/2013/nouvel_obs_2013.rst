

.. _nouvelobs_2013:
   
====================================================================
Article du nouvel obs sur la plainte de Villeneuve contre France 2
====================================================================

.. seealso::

   - http://teleobs.nouvelobs.com/actualites/20130930.OBS9107/entre-la-villeneuve-et-envoye-special-rien-en-va-plus.html
   
   
  

"Quel reportage lamentable", "un reportage inepte, dangereux et mensonger", 
"une caricature, une insulte", "un regard partiel et partial", 
"une stigmatisation injuste"… 

A La Villeneuve, quartier sensible au sud de Grenoble, des voix s’élèvent 
pour critiquer le reportage "La Villeneuve : le rêve brisé" diffusé 
jeudi dernier dans "Envoyé Spécial" (France 2). 

D’abord, retour sur l’objet de la discorde : ce sujet d’une demi-heure 
est signé par Amandine Chambelland, grand reporter dans la société de 
production "Lignes de Mire", journaliste expérimentée qui a également 
collaboré aux émissions "Capital" ou "Enquêtes exclusives" (pour voir 
le reportage, c'est ici). 

L’idée de France 2 et de "Lignes de Mire" était simple : revenir à 
Villeneuve, théâtre d’émeutes urbaines en 2010 et quartier d’où est 
originaire la dizaine de jeunes accusés d’avoir tabassé à mort Kévin 
et Sofiane il y a tout juste un an. 

C'était le 28 septembre 2012 à Échirolles et la presse, à l'époque, 
avait largement fait l'écho de ce fait divers. 


