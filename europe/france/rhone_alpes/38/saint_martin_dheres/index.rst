
.. index::
   ! Saint-Martin d'Hères
    
   
.. _smh_pub:
   
===================================
Saint-Martin d'Hères
===================================

.. seealso::

   - http://fr.wikipedia.org/wiki/Saint-Martin-d%27H%C3%A8res


.. figure:: 60px-Blason_Saint_Martin_dHeres.svg.png
   :align: center
   

Saint-Martin-d'Hères est une commune française située dans le département 
de l'Isère et la région Rhône-Alpes.

La commune se situe à l'est de Grenoble et fait partie de la Communauté 
d'agglomération Grenoble Alpes Métropole. 

Avec Échirolles, elle est la principale ville de la banlieue de Grenoble. 
Elle est principalement connue pour héberger le grand campus de Grenoble.


.. toctree::
   :maxdepth: 6
   
   collectifs/index
   squats/index
   
   
   

