
.. index::
   pair: Squats; Moulissimo
   ! Moulissimo
    
.. _moulissimo:
   
=================================================
Moulissimo
=================================================


.. seealso::

   - http://fr.squat.net/2013/03/23/saint-martin-dheres-38-ouverture-du-squat-moulissimo/
   - :ref:`lapoudriere_pub`

.. toctree::
   :maxdepth: 3
   
  
Création
========

La poudrière s’installe au soleil !

Après moult aventures périlleuses et plus de 25 assemblées pirates, 
ON S’INSTALLE AU SOLEIL ! 

La poudrière et quelques personnes ont ouvert depuis le vendredi 8 mars un 
nouveau repère de crustacés. Est donc occupé, “sans droit ni titre”, l’ancien 
restaurant Moulissimo, au 1 rue du Tour de l’Eau, 38400 à St-Martin-d’Hères, 
pour la joie de tous les bigorneaux, mollusques et autres animaux marins : 
venez poulper avec nous !

À toi étudiante, sioux, prolétaire, opprimée, bandit de grands chemins, sérieux, 
rigolarde, théâtreux, boxeuse, cuisto en galère, jardinier, diablotin, 
soudeuse : Viens quand tu veux ! Pour commencer, une première assemblée générale 
pour décider tous ensemble de ce que l’on va faire de ce lieu et des principes 
de base sur lesquels on veut partir est prévue le jeudi 14 mars à 16h30. 

On a déjà pas mal d’idées dans la caboche comme une cuisine autogérée, une 
zone de kitsch, une salle de boxe, des projections, des concerts, une salle 
de répèt’, un lavomatic, etc. 

Mais on reste preneurs et preneuses de toutes les bonnes idées et de toutes les 
bonnes vibes !

Un peu plus de mordant sur cette fac apolitique, déculturée et depuis trop 
longtemps laissée à la merci des costumes gris !

La poudrière, Collectif de réquisition des espaces vides étudiants

   
   

