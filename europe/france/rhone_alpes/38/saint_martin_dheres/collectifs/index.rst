
.. index::
   pair: Collectifs; Saint-Martin d'Hères 
    
   
.. _collectifs_smh_pub:
   
===================================
Collectifs sur Saint-Martin d'Hères
===================================


.. toctree::
   :maxdepth: 6
   
   la_poudriere/index
   les_jardins_dutopie/index
   
   
   

