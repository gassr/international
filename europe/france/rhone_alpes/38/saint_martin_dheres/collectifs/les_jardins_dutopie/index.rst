
.. index::
   pair: Collectif; Les Jardins d'Utopie
   ! Les Jardins d'Utopie
    
.. _les_hardins_dutopie:
   
=================================================
Les Jardins d'Utopie
=================================================


.. seealso::

   - http://lepetitpotager.over-blog.com/


.. contents::
   :depth: 3
   
Contact
========

:email: jardins.d.utopie@gmail.com



Présentation
=============

.. seealso::

   - http://lepetitpotager.over-blog.com/pages/presentation_de_lassociation-1750503.html
   

Une graine de dissidence ! 

Tondus et entretenu par le service d’aménagement durable, le campus grenoblois 
est occupé par un gazon vert et uniforme.

Tout le campus ?

Non !

Deux petites parcelles de terres résistent vaillamment à l’envahisseur !


Elles se nomment les « Jardins d’utopie »

 
Ces jardins-potagers ont germé à l’occasion de la mobilisation anti-précarité 
qui a eu raison du CPE et du CNE. 
Ce sont aujourd’hui l’une des rares trace visible de ce mouvement sans précédent 
qui monta en puissance de l'hiver au printemps 2006.


Jardins d’utopie est une association qui mélange théorie et pratique.

C’est l’apprentissage et la pratique d’un jardinage collectif en milieu étudiant, 
selon des méthodes respectueuses des sols. 
C’est aussi de la réflexion et du débat sur de futures sociétés désirables. 
Une autre agri-culture est possible !

 
Jardins d’utopie se revendique d’idées écologistes, humanistes et de l’éducation 
populaire.

Pour faire avancer ces valeurs nous recourrons à l’action politique, parfois 
sous la forme de désobéissance civile. Cela a été le cas lors de la création 
de ces jardins-potagers. 
A ce jour, ils n’ont aucune existence officiel ou légale auprès de l’université 
ou de la ville de Saint-Martin-d’Hères.

Notre contribution pèse son poids dans la balance.

Nous cultivons deux potagers, nous assurant une certaine autosubsistance. 
Nous faisons de l’agitation politico-culturelle comme la création de l’AMAP 
du campus, des conférences, un concert de soutien, la participation a divers 
festivals…

 
Et nous avons l’espoir, la folie de croire que cette initiative en appellera 
d’autres.

Il existe sur le campus pas mal d’associations et syndicats œuvrant pour 
l’intérêt collectif et le bien commun. 
Nous faisons en sorte que notre association puisse servir d’exemple, d’amorce, 
de tremplin, de catalyseur vers un mouvement global de transition de notre 
société… Vers une sortie positive du système capitaliste pour un autre système, 
plus humain et plus respectueux envers la nature. Rien que ça !


Plusieurs projets sont en cours, d’autres sont à mettre en chantier. 
Nous ne serons jamais trop nombreux pour les réaliser.

Tu peux nous contacter a jardins.d.utopie@gmail.com    

