

    
.. _le_boulet_2_avril_2013:
   
=================================================
Le boulet N°2 avril 2013
=================================================

.. contents::
   :depth: 3

Agenda
=======

Mercredi 3 avril 2013, 18h
--------------------------

Assemblée Générale 

Mercredi 3 avril 2013, 20h
--------------------------

A :ref:`Antigone <antigone>`, rencontre avec :ref:`Soeuf Elbadawi <soeuf_elbadawi_2013>`







