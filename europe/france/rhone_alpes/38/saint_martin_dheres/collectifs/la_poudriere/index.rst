
.. index::
   pair: Collectif; La Poudrière
   ! La Poudrière
    
.. _lapoudriere_pub:
   
=================================================
La Poudrière
=================================================


.. seealso::

   - :ref:`moulissimo`


.. contents::
   :depth: 3
   
Contact
========

:email: Michel Michel <lesjeudisausoleil@gmail.com>

::

    La Poudrière
    1 rue du Tour de L'eau
    38400 Saint Martin d'Hères


Actions
=======

.. toctree::
   :maxdepth: 3
   
   actions/index
   
Publication le boulet
=====================

.. toctree::
   :maxdepth: 3
   
   le_boulet/index   
