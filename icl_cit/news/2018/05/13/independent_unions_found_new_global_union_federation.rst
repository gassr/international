

.. _independant_unions_2018_05_13:

================================================================================
Sunday May 13th  2018 **Independent unions found new Global Union Federation**
================================================================================

.. seealso::

   - https://en.labournet.tv/independent-unions-found-new-global-union-federation


.. contents::
   :depth: 3


.. figure:: cnt_vignoles_drome.png
   :align: center

   https://en.labournet.tv/independent-unions-found-new-global-union-federation


Interview
===========

In May 2018 the “International Confederation of Labour” was founded in Italy to
unite independent unions and worker organisations across frontiers.

We interviewed delegates at the congress to hear about their union activities
at home and what they hope will come out of the new confederation.

The United Voices of the World union from London, UK wrote a report on their
experience from the conference:

Delegates and observers from Greece, Brazil, Morocco, Bulgaria, Netherlands,
Belgium, Germany, Italy, Catalonia, Spain, Poland, United States, Canada,
France, Austria and Argentina sent solidarity messages or attended the Congress,
which was organised at the headquarters of the Italian independent USI union.

The idea behind the union confederation is to reignite practical international
solidarity between workers in struggle that, in the words of Spanish CNT union
organiser Miguel Pérez, **has to be shown in acts that go beyond issuing communiqués
or holding pickets of solidarity.**

Italian USI delegates emphasised the historical anarcho-syndicalist and
revolutionary-syndicalist origins of many of the participating unions and
collectives: ‘Today, and even more so tomorrow, a combative libertarian
international trade union organisation is needed to defend workers effectively,
and to build a new, free and equal society independently of the traditional
political party and union structures.’

**Anna Baum**, an organiser with the German Free Workers’ Union in Berlin, explained
that the new confederation intended to share resources and information across
frontiers among independent unions, focussing on workers who work for the same
corporation, but in different countries.

‘An emphasis will be made on sharing campaign aims, for instance in fighting
for a collective agreement across sectors that includes protecting migrant workers
from immigration checks,’ Anna reported.

‘Campaigning and organising resources will be shared on a joint website or platform,
and workshops and conferences bringing workers in struggle in other countries
such as **China and Indonesia**, together with Italians, Germans and Polish workers,
for example, will also be held, with unions from the wealthier nations financially
supporting workers from low income countries to attend.’

There was discussion about coordinating research on international labour laws,
and an idea to register the confederation with the International Labour Organisation
so as to be able to take cases of abuse of workers’ rights by multinational
corporations to international tribunals.

Several union activists from Catalonia, Brazil and Greece emphasised how workers
are more frequently organising wildcat strikes, road blockades and other imaginative
acts of resistance to counter the lack of response from official trade unions
and traditional political parties which have failed to defend workers’ interests.

Mass workers’ migration across frontiers, combined with the increasing ease of
communication technology that allows these workers to stay in contact with
struggles back home and connect to struggles in their destination country,
mean that independent unions and collective organisations should step up
their cooperation and, in the words of Bulgarian Autonomous Workers’ union
organisers, ‘look towards building an international resistance movement’.

United Voices of the World organiser Claudia Turbet-Delof concluded that the
congress had been ‘an enriching experience, especially for me as a feminist,
migrant trade unionist, and has stirred profound reflection on what union
organising means to me and what we, as workers, hope to achieve.' "

team: Labournet.tv
