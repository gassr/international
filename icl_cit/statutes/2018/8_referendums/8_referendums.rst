.. index::
   pair: Referendums ; ICL-CIT

.. _icl_referendums:

====================
**8. Referendums**
====================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


8.1 The referendum, or a consultation of the Sections and Friends
===================================================================

The referendum, or a consultation of the Sections and Friends of the ICL, will
be the standard method of reaching agreements in the ICL between Congresses.


8.2 Referendums will be called by the Secretary at the request of any of the Sections or Friends
==================================================================================================


8.2 Referendums will be called by the Secretary at the request of any of the
Sections or Friends, or automatically in the cases stipulated in these Statutes.


8.3 To call a referendum, the Section or Friend who desires it
================================================================

To call a referendum, the Section or Friend who desires it should send a
request to the Secretary including:

- the issue at hand,
- the motives behind the proposal or consultation, and
- the concrete proposal or consultation.


8.4 The Secretary and the Liaison Committee will forward this full request
============================================================================

The Secretary and the Liaison Committee will forward this full request to the
rest of the Sections and Friends within a maximum period of three months,
calling the referendum and asking the Sections and Friends to express their
opinions with respect to the referendum in a period that is no greater than
six months from the date that the original request was submitted.

8.5 The Secretary and the Liaison Committee
==============================================

The Secretary and the Liaison Committee can and should submit to referendum
all those questions, proposals, or consultations that exceed the mandate
they have received from the Congress, or those where they require clarification
from the Sections and Friends, following the procedures outlined above.

8.6 The decision with respect to the referendum will be reached in accordance
===============================================================================

.. seealso::

   - :ref:`icl_decision_making`


The decision with respect to the referendum will be reached in accordance with
the article on :ref:`decision-making <icl_decision_making>`


8.7 For improved internal efficiency, the Secretary can group together
========================================================================

For improved internal efficiency, the Secretary can group together all
proposals or consultations made in a period of six months.
