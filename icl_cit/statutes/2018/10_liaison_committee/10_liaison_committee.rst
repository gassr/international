.. index::
   pair: Liaison Committee ; ICL-CIT

.. _icl_liaison_committee:

==========================
**10. Liaison Committee**
==========================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


10.1 The Liaison Committee of the ICL is to be made up of one representative
==============================================================================

The Liaison Committee of the ICL is to be made up of one representative of each
of the Sections and Friends of the ICL.

10.2 This person is to be selected by the Section or Friend
==============================================================

This person is to be selected by the Section or Friend from among its individual
members and may or may not be its international secretary, if it has one.

The individual member assigned to the Liaison Committee can be replaced at any
time by their own Section or Friend at its own discretion.


10.3 The Liaison Committee can create as many subcommittees and work groups as they consider necessary
=======================================================================================================

10.3 The Liaison Committee can create as many subcommittees and work groups as
they consider necessary, in which case the Sections will decide the best
way to organize them.

The subcommittees and work groups can include individuals who are not part of
the Liaison Committee.


10.4 The members of the Liaison Committee do not represent the organization they belong to
============================================================================================

The members of the Liaison Committee do not represent the organization they
belong to, but rather the whole of the ICL.

All of their actions should be guided by the principle of respect for the
agreements of the ICL, independent of the position of their Section.

They must carry out the internal agreements of the ICL just as they have been
decided on during the decision-making process.

10.5 The duties of the Liaison Committee and its members are as follows
=========================================================================

The duties of the Liaison Committee and its members are as follows:

- To ensure the flow of communication and facilitate collaboration among the
  Sections and Friends of the ICL, both bilaterally (directly between individual
  organizations, autonomously) and between each Section or Friend and the ICL
  as a whole.

- To carry out the agreements of the Congresses and referendums, to develop
  the ICL’s lines of work set out at the Congress, and to achieve the goals
  determined at the ICL’s Congress.

- To ensure all internal documents of the ICL are sent to the Sections and
  Friends in their corresponding language, through translation.
  For this task, the Liaison Committee may appoint as many individual members
  as are deemed necessary.

- Any other task necessary for the day-to-day facilitation of the internal
  affairs and the coordination of the structure of the ICL.

- To report, in general, to the Secretary and to the rest of members of the
  Liaison Committee the general opinions prevalent in their respective
  organizations or opinions expressed or decisions taken by their organizations
  in their internal votes, so that the Secretary and the Liaison Committee
  can act in a manner that is as respectful as possible of the positions of
  all of the member organizations of the ICL.

- To submit to referendum, in accordance with the provisions on referendums,
  all those questions that exceed the mandate of the Congress to carry out the agreements.


10.6 The Liaison Committee is to meet as often as it considers necessary
=========================================================================

The Liaison Committee is to meet as often as it considers necessary in plenary
meetings, either physically or **virtually**.

**Meetings are called by the Secretary or by one third of the participants.**

The agendas of these meetings are to be drafted by the Secretary, as
coordinator of the Liaison Committee.

The agenda will include all of the proposals of the members of the Liaison
Committee and any matters entrusted to them by a prior Congress or referendum,
should there be any.

10.7 Decision-making in the Liaison Committee
================================================

.. seealso::

   - :ref:`icl_decision_making`


Decision-making in the Liaison Committee, in all cases, is to take place
in accordance with the provisions on :ref:`decision-making in the ICL <icl_decision_making>`.
