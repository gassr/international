.. index::
   pair: Congresses ; ICL-CIT

.. _icl_congresses:

====================
**7. Congresses**
====================

.. seealso::

   - https://www.icl-cit.org/statutes/

.. contents::
   :depth: 3


7.1 The Congress of the ICL will be made up of all of the Sections and Friends
=================================================================================

The Congress of the ICL will be made up of all of the Sections and Friends
of the ICL.

**It is the highest decision-making body of the ICL**

7.2 The tasks and functions of the Congress are as follows
==============================================================

The tasks and functions of the Congress are as follows:

- 7.2.1 To plan the strategic line of the ICL.
- 7.2.2 To carry out an analysis of the international situation, in all of its
  relevant aspects.
- 7.2.3 To clearly define the areas and lines of work of the ICL for the
  period between Congresses.
- 7.2.4 To set the general objectives of the ICL in its different work areas
  and to set priorities among them.
- 7.2.5 To provide a mandate to the Secretary and the Liaison Committee to
  carry out the necessary tasks to achieve those objectives.


7.3 The agreements and decisions adopted during the Congress are binding for all
====================================================================================

The agreements and decisions adopted during the Congress are binding for all
the member organizations, the provision included in :ref:`article 6.7 <article_6.7>`
providing the only exception.


7.4 The Secretary will call the Ordinary Congresses of the ICL every five years
================================================================================

The Secretary will call the Ordinary Congresses of the ICL every five years.

The Secretary will announce the Congress early enough to give a period of three
months to submit proposals for the agenda.

These proposals should come from the Sections, and be accompanied by an explanation.

Once this period has passed, the Secretary will set the agenda for the Congress
using these proposals, which will act as base proposals.

A period of six months will follow when counter-proposals, amendments, and
comments to the base proposals can be sent by the Sections.


7.5 In the same first announcement of the Congress, the Secretary will call a referendum
=========================================================================================

In the same first announcement of the Congress, the Secretary will call a
referendum on the date and location for the Congress, in accordance with
the provisions on referendums.


7.6 A Section may request an Extraordinary Congress at any time it sees fit
================================================================================

A Section may request an Extraordinary Congress at any time it sees fit.

Friends, the Secretary, and the Liaison Committee may not request Extraordinary
Congresses. This request should include:

- the reason for the Extraordinary Congress,
- a proposed agenda,
- proposals that develop the points in the proposed agenda, and
- all documentation relevant to the matter.

The Secretary and the Liaison Committee will send this information to all of
the Sections and Friends of the ICL and call a referendum on the necessity of
holding the Extraordinary Congress, in accordance with the provisions on referendums.


7.7 If the request for an Extraordinary Congress is approved
===============================================================

If the request for an Extraordinary Congress is approved, the Secretary will
proceed to organize it, following the process for Ordinary Congresses but
adapting the time line to the urgency of the Extraordinary Congress.


7.8 The Section that made the initial call for an Extraordinary Congress
==========================================================================

The Section that made the initial call for an Extraordinary Congress is
obligated to host it.

7.9 Participation
======================


7.9.1 All of the Sections and Friends of the ICL will participate in the Congress
-----------------------------------------------------------------------------------

All of the Sections and Friends of the ICL will participate in the Congress.

Sections and Friends can present papers, table motions, and participate in
decision-making based on consensus and in the work groups or committees
created in the Congress, according to their capacities.

The Sections may also vote in accordance with the current system of voting
for each Congress.

7.9.2 The Initiatives of the ICL can attend the plenary sessions
------------------------------------------------------------------

The Initiatives of the ICL can attend the plenary sessions of the Congresses
and meetings of work groups or committees created in the Congress, if invited.


7.9.3 All those organizations and/or groups invited as observers
-------------------------------------------------------------------

All those organizations and/or groups invited as observers can attend the
plenary sessions of the Congress.

Invitations to the Congress for observer will be sent by the Secretary at the
initiative of the Sections and Friends.


7.10 The Congress is made up of the delegations sent by the different Sections and Friends
=============================================================================================

The Congress is made up of the delegations sent by the different Sections
and Friends.

The delegations are to come with two copies of the agreements of their
respective Sections in writing (one copy for the delegation and the other
for the chair).

These are the Direct Delegations.

7.11 Sections and Friends can also send their agreements in writing to the Secretary
======================================================================================

Sections and Friends can also send their agreements in writing to the Secretary
before the Congress begins, or give their agreements to a delegation that will
be present.

In this case, they would be Indirect Delegations.


7.12 The Direct Delegations may interpret the agreements of their Sections
============================================================================

The Direct Delegations may interpret the agreements of their Sections based on
their understanding.

They can negotiate with the other delegations present in search of consensus.

To this end, it is recommended that the agreements of the Sections and Friends
for the Congress specify in as much detail as possible the limits of their
delegations’ capacities for negotiation.


7.13 The agreements of the Indirect Delegations may only be read as they are
================================================================================

The agreements of the Indirect Delegations may only be read as they are.

If there are any doubts as to the interpretation of these agreements, they will
not be included in any consensus that may be reached on the point in question.

The Indirect Delegations will also have no voice on questions or votes that
arise during the Congress.

7.14 The Congress agreements are to be reached in accordance with the article on decision-making
==================================================================================================

.. seealso::

   - :ref:`icl_decision_making`

The Congress agreements are to be reached in accordance with the article on :ref:`decision-making <icl_decision_making>`


7.15 The Congress sessions will follow the agenda
===================================================

The Congress sessions will follow the agenda.

At the beginning of each session, the Sections present will elect participants
to chair the Congress.

The opening session of the Congress will be opened by the Secretary who, after
verifying which delegations are present, will open the Congress and pass
immediately to the first item: The election of participants to chair the
meeting (a chairperson, a recorder, and someone to make a speakers list)
from among the delegations present.

7.16 Once the Congress is finished, the Secretary will send the minutes
==========================================================================

Once the Congress is finished, the Secretary will send the minutes of the
sessions and a summary of all agreements reached at the Congress to all of
the Sections within a period of three months.

Once the minutes and agreements have been sent out, Sections will have three
months to challenge those agreements.

These challenges, if they exist, are to be resolved through a referendum, which
is to be called immediately after the period for accepting challenges, in
accordance with the provisions on referendums.

Once any challenges have been resolved, the Secretary will publish the
agreements from the Congress.
