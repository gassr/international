.. index::
   pair: Treasurer ; ICL-CIT

.. _icl_treasurer:

===================
**11. Treasurer**
===================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


11.1 The Treasurer of the ICL will be elected at the Congress
==============================================================

The Treasurer of the ICL will be elected at the Congress.

They will manage the treasury for the period between two Congresses.


11.2 Once this period has passed, the position may not be extended
===================================================================

Once this period has passed, the position may not be extended.

**A new Treasurer shall be elected at each Congress.**


11.3 The Treasurer’s duties include everything relating to the finances of the ICL
=====================================================================================

The Treasurer’s duties include everything relating to the finances of the ICL.

**They will be in charge of its bank accounts.**


11.4 The Treasurer and the Secretary will be the two individuals jointly authorized to disburse the funds of the ICL
=======================================================================================================================

The Treasurer and the Secretary will be the two individuals jointly authorized
to disburse the funds of the ICL, in accordance with the instructions of the
agreements and the directions of the bodies of the ICL at all times.


11.5 Among the Treasurer’s duties is calculating the average numbers of dues-paying
======================================================================================

Among the Treasurer’s duties is calculating the average numbers of dues-paying
individual members of the Sections, in order to allocate votes before any vote
is to be held.

11.6 The Treasurer is responsible for all work related to the dues for the ICL
================================================================================

The Treasurer is responsible for all work related to the dues for the ICL.


11.7 The Treasurer reports to the Secretary and Liaison Committee every six months
=====================================================================================

**The Treasurer reports to the Secretary and Liaison Committee every six months.**

The Secretary or any Liaison Committee member can request a report at any time.

An audit will occur at the midpoint of their term and a final audit will occur
after a Treasurer has completed their five-year term.

It is performed by a commission of at least two people who are not part of the
Liaison Committee and who should belong to different member organizations.


The Section to which the Treasurer belongs may replace a Treasurer
=====================================================================

11.8 The Section to which the Treasurer belongs may replace a Treasurer from
their ranks at any time.

They must provide the replacement.

The Treasurer can be recalled by a referendum, which can be initiated by a
Section or the Liaison Committee.
