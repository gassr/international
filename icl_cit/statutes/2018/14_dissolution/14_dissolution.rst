.. index::
   pair: Dissolution ; ICL-CIT

.. _icl_dissolution:

=====================
**14. Dissolution**
=====================

.. seealso::

   - https://www.icl-cit.org/statutes/

.. contents::
   :depth: 3

14.1 The dissolution of the ICL can only be decided at a Congress
===================================================================

The dissolution of the ICL can only be decided at a Congress, which will
also determine what to do with the assets of the ICL.

The ICL will not dissolve itself as long as at least three Sections stand
against the dissolution.
