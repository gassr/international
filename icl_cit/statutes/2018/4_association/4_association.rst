.. index::
   pair: Statutes ; Association (ICL)

.. _icl_statutes_association:

===================
**4. Association**
===================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


4.1 All those anarcho-syndicalist and revolutionary unions who request membership
===================================================================================

All those anarcho-syndicalist and revolutionary unions who request membership
may form part of the ICL as long as they meet the conditions laid out in the
:ref:`Federative Agreement <icl_agreement>`.


4.2 Because of its federalist and internationalist methodology, the ICL is not governed by the logic of states
===============================================================================================================

Because of its federalist and internationalist methodology, the ICL is not governed
by the logic of states, nor does it recognize in its structure the artificial
divisions of borders.

For this reason, there can be more than one Section per country.
In such cases, the substantiated opinion of an organization already adhering to
the ICL in that country will be taken into consideration, although this will
not constitute a veto.

4.3 In the same way, a Section could have a sphere of activity that includes more than one state
===================================================================================================

In the same way, a Section could have a sphere of activity that includes more
than one state

4.4 The minimum number of individual members in order to be a Section of the ICL is 75 individual members.
==========================================================================================================

The minimum number of individual members in order to be a Section of the ICL
is 75 individual members.


4.5 Anarcho-syndicalist or revolutionary unions that are organized
===================================================================

Anarcho-syndicalist or revolutionary unions that are organized in line with the
:ref:`Federative Agreement <icl_agreement>` but do not have this minimum number of individual members
may join the ICL as Friends.


4.6 Groups that are not established as anarcho-syndicalist or revolutionary unions
=======================================================================================

Groups that are not established as anarcho-syndicalist or revolutionary unions
but express a desire to establish an organization in line with the :ref:`Federative Agreement <icl_agreement>`
and collaborate with the ICL for that purpose may do so as Initiatives, independent
of their number of individual members.

4.7. Sections, Friends, and Initiatives
=========================================

4.7.1 Sections
------------------

The organizations that form part of the ICL as Sections can and should:

- contribute to the effort of expanding the organizing model of the ICL and achieving its goals, based on their capacities;
- provide the number of participants that has been agreed upon at any given moment to the administrative bodies of the ICL;
- take part in the decision-making processes through consensus and, if this is not possible, by voting, in accordance with the current system of voting;
- pay the dues corresponding to their number of individual members at the currently established rate; and
- receive economic aid from the solidarity fund when they request it, in the manner set out for the administration of that fund.

4.7.2 Friends
----------------

The organizations that form part of the ICL as Friends can and should:

- contribute to the effort of expanding the organizing model of the ICL and
  achieving its goals, based on their capacities;
- provide the number of participants that has been agreed upon at any given
  moment to the administrative bodies of the ICL;
- take part in the consensus-based decision-making processes, with a voice
  but without a vote;
- pay the dues corresponding to their number of individual members at the
  currently established rate; and
- receive economic aid from the solidarity fund when they request it, in
  the manner set out for the administration of that fund.

4.7.3 Initiatives
--------------------

The organizations that collaborate with the ICL as Initiatives can and should
contribute to the effort of expanding the organizing model of the ICL and
achieving its goals, basedon their capacities.

As the Initiatives pay no dues, they do not have the right to draw on the
solidarity fund as Sections and Friends do. However, they can request aid
and this will be assessed on a case-by-case basis.

4.8 Admission of new organizations
==========================================

4.8.1 Any organization that adheres to the :ref:`Federative Agreement <icl_agreement>`
---------------------------------------------------------------------------------------------

Any organization that adheres to the :ref:`Federative Agreement <icl_agreement>`
and is interested in participating in the ICL as a Section or Friend should
send their membership application to the Secretary and the Liaison Committee of the ICL.

This application should include the following:

- A request for membership in the ICL, specifying if membership is requested as a
  Section or as a Friend, the number of individual members, and the geographic
  sphere of activity.
- A commitment to accept the principles of the ICL and a declaration of agreement
  with and consent to the :ref:`Federative Agreement <icl_agreement>` of the ICL,
  indicating, if relevant, any aspects of the :ref:`Federative Agreement <icl_agreement>`
  they consider to be inapplicable due to geographic, cultural, legal, or
  labor-related circumstances, and their reasoning.

- A copy of their current statutes.


4.8.2 If the Secretary and/or the Liaison Committee find formal errors
-----------------------------------------------------------------------

If the Secretary and/or the Liaison Committee find formal errors in the application
or deem it incomplete, they will return it to the organization to correct the
errors or provide the requested information within a period of three months;
otherwise, the request for membership will be considered withdrawn.


4.8.3 The Secretary, through the Liaison Committee, will send the applications
--------------------------------------------------------------------------------

The Secretary, through the Liaison Committee, will send the applications received
to the Sections and Friends of the ICL within a period of three months after
they are received, accompanied with a report on the organization’s observance
of all of the membership conditions or, if relavent, any inconsistencies or
anomalies detected.

In particular, the report will make mention of the specific features of the
geographic, cultural, legal, or labor-related circumstances that might be
behind a lack of adherence with particular points of the :ref:`Federative Agreement <icl_agreement>`.

4.8.4 Along with this report, the Secretary will also call for a referendum
----------------------------------------------------------------------------

Along with this report, the Secretary will also call for a referendum among
the Sections and Friends of the ICL, in accordance with the provisions on
referendums, so that they can decide whether to accept the new organization.


4.8.5 The evaluation of the potential member organization’s compliance
-------------------------------------------------------------------------

The evaluation of the potential member organization’s compliance with the points
of the :ref:`Federative Agreement <icl_agreement>` falls exclusively upon the member organizations of
the ICL through referendum.

It cannot be carried out by the Secretary or the Liaison Committee on their own.

When making this evaluation, the criteria should be one of maximizing the
transformative potential of the relevant union system in each case.

That is to say, the extent to which the potential member organization has been
able to develop or put forward an alternative and transformative union model,
in a revolutionary sense, within their sphere of activity.

4.8.6 If the organization is accepted for membership, it will be admitted
---------------------------------------------------------------------------

If the organization is accepted for membership, it will be admitted as a Section
or Friend of the ICL with full rights effective immediately, without the need
for a waiting period.

**The admission has to be ratified at the next Congress**.

During the first six months, it will have a single vote until an average of its
dues paid to the ICL can be calculated, after which time it will have the number
of votes corresponding to its average dues, according to the current system
of voting.
Additionally, it will provide the corresponding participants to the Liaison
Committee and the relevant work groups.


4.8.7 In the case that the organization is not accepted
--------------------------------------------------------

In the case that the organization is not accepted, the Secretary will communicate
this to the applying organization, providing the reasoning behind the refusal.

The applying organization may attempt to correct the reasons for which they were
rejected within a period of six months and re-apply for membership to the ICL,
explaining the corrections.

4.9 Disassociations to the ICL
================================

4.9.1 A Section can be disassociated from the ICL for the following reasons:
-------------------------------------------------------------------------------

- 4.9.1.1 By their own will, in which case they should inform the Secretary and
  the Liaison Committee of their decision. They will leave the ICL immediately
  upon reception of this communication by the Secretary.

- 4.9.1.2 Due to failure to pay dues for more than one year. In this case, once
  this period has expired, and if the organization in question has not submitted
  a request for exemption from dues in accordance with the provisions on exemptions
  from dues, resolved or not, the Secretary will call upon the Section or Friend
  to pay the outstanding dues within a period deemed reasonable by both parties,
  or they will be expelled from the ICL. The Secretary may grant a deferral of
  at most six months.
  Once this deferral has elapsed, the Secretary shall initiate a referendum as
  to whether another six month deferral can be granted.

  The Section in question may pay the outstanding dues, accept expulsion,
  or request an exemption from dues, in accordance with the provisions on
  exemptions from dues.
  In the case that the exemption is not approved, the Section that requested
  it should pay the outstanding dues or accept expulsion.

- 4.9.1.3 By the ICL deciding to expel the Section.
  The motives for expulsion can be:

  - failure to comply with the principles of the ICL and failure to observe
    these Statutes and their subsequent amendments;
  - publicly acting against the interests of the ICL or of any of its Sections
    and/or Friends and claiming to represent the ICL for activities without
    a mandate;
  - acting within the context of the ICL in a manner that harms the interest
    of the ICL or of any of its Sections and/or Friends, or that impedes or
    damages its normal operation, or that spoils the relations among its
    members; or
  - theft, fraud, or expropriation of the assets of the ICL.

4.10 Expulsion process
=========================

4.10.1 Any Section of the ICL may request the expulsion of any other Section
------------------------------------------------------------------------------

Any Section of the ICL may request the expulsion of any other Section or
Friend of the ICL for any of the reasons listed above.

**Friends, the Secretary, and the Liaison Committee may not request an expulsion.**

4.10.2 To do so, the Section requesting expulsion must send the Secretary
----------------------------------------------------------------------------

To do so, the Section requesting expulsion must send the Secretary and the
Liaison Committee a proposal listing the violations that form the basis for
the request, along with any evidence to substantiate this request.

4.10.3 The Secretary and the Liaison Committee will send this request
-----------------------------------------------------------------------

The Secretary and the Liaison Committee will send this request to all of the
Sections and ask the Section that is the object of the request for expulsion
to respond to the request within a period of one month.

This response will be sent to all Sections, and will be the only documentation
sent out with respect to the matter.

4.10.4 Once the response has been received and forwarded
---------------------------------------------------------

Once the response has been received and forwarded, or if it was not received
before the deadline, the Secretary will organize a referendum on the question,
in accordance with the provisions on referendums.

4.10.5 If the expulsion is approved, it will be effective immediately
-----------------------------------------------------------------------

4.10.5 If the expulsion is approved, it will be effective immediately


4.10.6 The expelled Section or Friend may appeal the decision within
-------------------------------------------------------------------------

The expelled Section or Friend may appeal the decision within a period of one
month, as long as the appeal is sufficiently justified.

When the appeal is received, the Secretary and the Liaison Committee will send
it to all Sections and Friends and call a referendum on the question, in
accordance with the provisions on referendums.

**The outcome of this referendum must be ratified by the next Congress**.


4.10.7 After a second referendum, the decision will be final
---------------------------------------------------------------

After a second referendum, the decision will be final and there will be no
room for further appeals.

4.10.8 In the case of a split within a member organization
-----------------------------------------------------------

In the case of a split within a member organization, the next Ordinary Congress
decides whether one, both or none will be a Section of ICL.
