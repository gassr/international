.. index::
   pair: Decision-making ; ICL-CIT

.. _icl_decision_making:

=======================
**6. Decision-making**
=======================

.. seealso::

   - https://www.icl-cit.org/statutes/

.. contents::
   :depth: 3

6.1 In the ICL decisions will be made by consensus of all Sections
=====================================================================

In the ICL decisions will be made by consensus of all Sections and Friends
whenever this is possible.


6.2 To facilitate this objective, when Sections or Friends make two or more different proposals
================================================================================================

To facilitate this objective, when Sections or Friends make two or more different
proposals on the same subject, a Reconciliation Committee will be formed.

This will consist of, at least, delegates from each of the Sections or Friends
that presented the proposals.

The Committee will draft a consensus text that contains all of the points of
agreement among the different proposals, any points of consensus reached by the
delegates in the Committee based on the mandates from their organizations, and
that highlights the irreconcilable differences between the proposals, to be
voted on by the Sections.

6.3 The Sections can then vote on any points
=============================================

The Sections can then vote on any points sent back from the Reconciliation
Committee, in accordance with a system of votes weighted by the number of
individual members of each Section, using the following logarithmic scale:

- From 75 to 424 individual members: 1 Vote
- From 425 to 1,546 individual members: 2 Votes
- From 1,546 to 4,243 individual members: 3 Votes
- From 4,244 to 9,999 individual members: 4 Votes
- Over 10,000 individual members: 5 Votes

6.4 The number of dues-paying individual members
==================================================

The number of dues-paying individual members referred to above will be
calculated by averaging the dues paid by each Section over the 6 months prior
to the vote.

6.5 Once the voting has been carried out
===========================================

Once the voting has been carried out, a resolution is passed if 2/3 of all votes
cast are in favor.

That is to say, abstentions will count toward the total votes.


6.6 At the request of at least three affiliated organizations
===============================================================

At the request of at least three affiliated organizations, an international
agreement can be reviewed through a referendum.

.. _article_6.7_2018:
.. _article_6.7:

6.7 Through a decision of the national congress
===================================================

Through a decision of the national congress or a referendum, a Section can
reject ICL Congress agreements or referendum decisions.

6.8 Friends of the ICL cannot participate
==========================================

6.8 Friends of the ICL cannot participate in the voting.
