.. index::
   pair: Introduction ; ICL-CIT

.. _icl_statutes_introduction_2018:

====================================================================================================================
**0-Introduction**
====================================================================================================================

.. seealso::

   - https://www.icl-cit.org/statutes/

.. contents::
   :depth: 3


Introduction
==============

One hundred years have passed since the beginning of the 20th century, the heyday
for revolutionary unionism.

It is no coincidence that this time was also the high point of workers’ struggles,
both to win basic rights and for the revolutionary transformation of society.
There’s no need to list here the profound changes that our planet has experienced
in these hundred years in every aspect: human, social, and environmental.

Some of these transformations have been especially relevant for anarcho-syndicalism
and revolutionary unionism at a global level.

For one, the vast majority of organizations that described themselves, in some way,
as *revolutionary* ceased to do so.

Most labor unions and political parties as well as organizations of all kinds
adapted themselves to the prevailing political situation after the fall of
the **Berlin Wall**.
The word *revolution* disappeared from their vocabularies.

They dropped from their practice, inconspicuously, the aim of profoundly and
radically changing society. They became, at best, alternative models for managing
the already existing systems.

At the same time, the mere possibility of such a radical transformation disappeared
from the collective imagination to such an extent that, at the beginning of the
twenty-first century, there was a need to remind ourselves that **another world is possible**.
And this self-evident truth seemed then to be the peak of revolutionary radicalism.

More recently, the prevailing political discourse has gone even further in this
direction. Ever since the capitalist system established itself worldwide, it has
been accompanied by and relied upon **patriotism, racism, xenophobia, and isolationism**.

**The global crisis of capitalism has worsened this situation**.

It is true, of course, that these were always present, lurking beneath the surface
of Western societies. But in recent years they have gained respectability and
have become plausible options for government, in a way that would have been
unthinkable a decade ago.
This phenomenon seems to repeat itself.
Unscrupulous politicians and demagogues have periodically used it for their
own ends. But we had not, for many years, seen this kind of discourse gain
such global predominance.

Finally, the unstoppable growth of capitalist structures of production and
consumption has brought the planet to the brink of **ecological collapse**, if indeed
we have not already plummeted over it.

One hundred years ago, anarcho-syndicalism and revolutionary unionism could
place certain hopes in the development of productive forces.
Now, in contrast, they face the prospect of dwindling natural resources.
**Clean air and water are unreachable privileges for millions of people**.
The tensions associated with ecological disasters, those already happening and
the foreseeable ones, spell decades of conflict, of wars for mere survival
with no quarter given, in an increasingly hostile environment.

Of course, we could list more factors.
But **just these three are already enough to present a harrowing prospect**, radically
different from the prevailing expectations when past internationals were born.

**The combination of these elements is explosive**.

It calls up images of a **dystopian world** of sealed borders, wars over resources,
aggressive militaristic nationalism, intolerant conservatism, famines, etc.
Faced with the old slogan *revolution or barbarism*, it appears that once
revolution has been left by the wayside, barbarism is coming.

However, and precisely because of all of this, we, the organizations that have
come together to give shape to this new international, who call ourselves
revolutionary unionists and anarcho-syndicalists, resolutely stand for the need
of revolutionary internationalism, today as much as ever before.

**Revolutionary**, because **we will not give up on solidarity and mutual aid** as
instruments for a radical and profound transformation of society.
And **internationalist** because, while so many countries are withdrawing within
their own confines, we assert yet again the bonds that bring us together, across
borders and nations.
We today refer to these same prerogatives that gave life to the anarcho-syndicalist
international founded in Berlin in 1922 and to its principles.
Only the global development of a strong alternative, today as then based on
these principles, can possibly reverse the destructive course that we find
ourselves on and prevent our descent into absolute barbarism.

Perhaps it is necessary to stress, in this moment in history when this kind of
declarations is not very fashionable, that our commitment to these two principles,
**revolution and internationalism**, is not just empty rhetoric.
Our firm intention is to build and develop a revolutionary international,
starting from the bottom, based on strong local foundations.

That is to say, we understand **internationalist** and **revolutionary** not to
be distinctions that can be granted merely by belonging to some organization.

They are not truths which once existed that someone can administer, but rather
realities that must be practiced daily on the ground.
Local revolutionary struggles coming together across borders is what builds
internationalism, and not the reverse.

This organic, decentralized, and federalist convergence should seek to provide
itself with the tools necessary to be effective, explore new content and forms,
and strengthen the local organizations that participate in it.
From it arises the need for a formal international in which the localized
revolutionary activity of the member organizations shapes internationalism.

It is clear that if this body is not based on the activity of firmly established
member organizations, with clear revolutionary programs, any purported
internationalism could be nothing more than an empty shell, managed from the top down.

We, the member organizations of this international, know very well that unionism
in the sphere of production and reproduction is not the only front that is
necessary to build a revolutionary movement.

**Halting and reversing the course towards disaster** that this system has set us
on is a complex and multi-faceted undertaking.
It can be approached from many angles since social transformation is a goal that
presupposes and also **includes a cultural change**.

From agroecology to self-managed cooperatives, anti-fascism, the struggle against
patriarchy in the sphere of reproduction, and supporting refugees, to the liberation
and sharing of knowledge in the field of education - there are too many approaches
to list here.

**All of them together will create, in a decentralized fashion, an alternative.**

We intend to do our part, contributing some sound and serious work in our area
and remaining open to cooperating and coordinating with other forces working
in the same direction.

**The gamble is great**.

If there ever was a promise of liberty, equality, and solidarity, what remains
of it seems to be rapidly vanishing. It is a terrifying prospect.
The ruling class will not offer an alternative. We, the working class, need to
build one ourselves or there will be no future.

Periods in history are very long, true, but we cannot allow opportunities to
pass us by indefinitely. The time to build a strong consistent revolutionary
internationalism is now.
**There may not be a next time**. For our part, we, each one of the members of the
anarcho-syndicalist and revolutionary unions in this international, are
committed to giving our best efforts to this process.
We are confident that it will be enough for the huge task we are taking on.

The future is ours. We are the future. Long live the international!
