.. index::
   pair: Federative Agreement ; ICL-CIT

.. _icl_federative_agreement:
.. _icl_agreement:

============================
**3. Federative Agreement**
============================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


3.1. In accordance with the above point on federalism, the ICL shall have a Federative Agreement
===================================================================================================

In accordance with the above point on federalism, the ICL shall have a
Federative Agreement, or an agreement on minimum criteria, which lays out a
series of conditions that the member organizations or prospective member
organizations must comply with.


3.2 While the principles are undeniable
===========================================

While the principles are undeniable, and are generally and globally relevant,
the more concrete points in the Federative Agreement can be conditioned by the
specific frameworks for unions and labor law in the areas where the member
organizations or prospective member organizations of the ICL are active.

That is to say, certain points could be redundant and unnecessary, others
inadequate, etc.
It is impossible to adapt the Federative Agreement in advance to every possible
legal and labor context.

For this reason, the context and situation should be taken into account in
detail when requiring and interpreting compliance with the Federative Agreement.


3.3 Interpreting the Federative Agreement, in particular issues of compliance
=================================================================================

Interpreting the Federative Agreement, in particular issues of compliance and
applicability in concrete situations and contexts, is the responsibility of
the member organizations of the ICL in accordance with the decision-making
process described in the corresponding article of these Statutes, either by
referendum or in Congress.

This cannot be done by the Secretary nor by the Liaison Committee on their own.

In every case where the Federative Agreement needs to be interpreted, attention
should be paid to the criteria described in the article about the association
of new organizations, even when the issue does not involve an application
for membership.


3.4 The Federative Agreement that follows is the development of the principles
=================================================================================

The Federative Agreement that follows is the development of the principles listed
in the previous article:

- Any organization that joins or wishes to join the ICL must be an anarcho-syndicalist
  or revolutionary union.
- Its internal structure should be federal and horizontal, and the internal
  decision-making process should start from the bottom.
- It may not provide support as an organization to any electoral political project,
  either individual or party-based.

- With respect to union elections, in countries where these are carried out,
  the member organizations should reject participation in any body, organization,
  election or negotiation process, government entity, or structure which does
  not respect the principle of horizontality.
  That is to say that these are configured as elements of co-management of a
  company and do not act under a mandate and in the interests of workers.

- No individual who voluntarily joins repressive forces, state or private, may
  join or be in any other way included in any member organization of the ICL.

- To maintain the principle of independence, in those countries where this is a
  possibility, no member organization of the ICL may receive economic subsidies,
  either public or private, for union activities that are being realized or developed.
