.. index::
   pair: Relations ; ICL-CIT

.. _icl_relations:

==================
**5. Relations**
==================

.. seealso::

   - https://www.icl-cit.org/statutes/

.. contents::
   :depth: 3

5.1 Autonomy of the Sections
=============================

The member organizations of the ICL are autonomous, and establish their own
goals, strategies, tactics, and plans of action, with no limits beyond respecting
the :ref:`Federative Agreement <icl_agreement>` of the ICL.

5.2 Relations with other organizations
======================================

5.2.1 Out of respect for the federal principle that governs the ICL
---------------------------------------------------------------------

Out of respect for the federal principle that governs the ICL and its
:ref:`Federative Agreement <icl_agreement>` and as an expression of the
strict autonomy of each Section, the Sections can establish tactical or
strategic relations, with whatever scope they deem appropriate, with
whichever organizations outside of the ICL that they deem fit.


5.2.2 In turn, the ICL can make contact, cooperate, and collaborate
-----------------------------------------------------------------------

In turn, the ICL can make contact, cooperate, and collaborate with whichever
organizations in the international context that it deems fit or necessary for
the defense of its interests and to promote its principles, always respecting,
of course, the :ref:`Federative Agreement <icl_agreement>`.

It does not matter if this is proposed by a Section, Friend, or neither, nor
if the organization being contacted is an Initiative or not.
