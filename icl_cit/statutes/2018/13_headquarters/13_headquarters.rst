.. index::
   pair: Headquarters of the federation ; ICL-CIT

.. _icl_headquarters:

=======================================
**13. Headquarters of the federation**
=======================================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


13.1 The ICL will locate its headquarters at the headquarters of the Section
==============================================================================

The ICL will locate its headquarters at the headquarters of the Section
currently acting as the Secretary, notwithstanding the fact that a change of
headquarters can be agreed upon through any internal vote of the ICL.
