.. index::
   pair: Funding and dues ; ICL-CIT

.. _icl_funding:

============================
**12. Funding and dues**
============================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


12.1 The only source of funding and income for the ICL is to be the contributions
==================================================================================

The only source of funding and income for the ICL is to be the contributions in
the form of dues paid by the Sections and Friends of the ICL.


12.2 There shall be three categories for monthly dues
========================================================

There shall be three categories for monthly dues:

- 0.10 euros per individual member
- 0.05 euros per individual member
- 0.02 euros per individual member

The Congress shall decide which category is appropriate for each Section, based
on the economic situation of the workers in the country.

A Section may voluntarily choose to pay higher dues.

The monthly dues are set based on a three tier systems in every Ordinary
Congress for the next five years.

If a Section has a union of incarcerated workers, no monthly dues are owed for
the incarcerated workers.

12.2.1 The Sections and Friends of the ICL are to make their payments to the Treasurer
---------------------------------------------------------------------------------------

The Sections and Friends of the ICL are to make their payments to the Treasurer
of the ICL at periodic intervals of their choice, but at minimum once every year,
based on the realities of their membership.

12.2.2 Partial exemptions from dues can be requested in exceptional cases
---------------------------------------------------------------------------

Partial exemptions from dues can be requested in exceptional cases for a
predetermined and limited period.

In order to do so, a substantiated proposal should be sent to the Secretary or
to the Liaison Committee specifying the exceptional circumstances behind the
request for exemption from dues, the duration of the exemption, and the amount
by which they propose to reduce the dues.

This proposal will be put to referendum, in accordance with the provisions
on referendums.


12.3 A solidarity fund is to be created from a significant proportion of the dues
==================================================================================

A solidarity fund is to be created from a significant proportion of the dues,
to be determined in Congress.


12.3.1 The Sections or Friends who so desire or require may request economic aid
-----------------------------------------------------------------------------------

The Sections or Friends who so desire or require may request economic aid from
the solidarity fund in “ordinary cases” for concrete projects by sending a
substantiated request to the Secretary or Liaison Committee, specifying the
project, the necessity of the economic aid, the amount requested, and any
commitment to repay the aid, partially or fully, should they choose to make one.

The Secretary ask the Treasurer to report on the viability of the proposal and
will send both documents to the member organizations and call a referendum, in
accordance with the provisions on referendums.


12.3.2 In no case may the combined sum of the ordinary economic aid granted exceed
-----------------------------------------------------------------------------------

In no case may the combined sum of the ordinary economic aid granted exceed 50%
of the total solidarity fund at the time it is granted.


12.3.3 In the case of ordinary economic aid, priority will be given to projects
--------------------------------------------------------------------------------

In the case of ordinary economic aid, priority will be given to projects and
requests that contribute to the development of the objectives and the lines
of work of the ICL, in accordance with the decisions made at the Congress.


12.3.4 Any Sections and Friends that so desire or require may request economic aid
-------------------------------------------------------------------------------------

Any Sections and Friends that so desire or require may request economic aid
from the solidarity fund for “extraordinary cases” where funds are required
urgently.

In these cases, the Secretary and the Liaison Committee can decide, in accordance
with the provisions on :ref:`decision-making <icl_decision_making>` in the ICL,
to immediately grant  the sum requested.


12.3.5 With respect to the above, cases of extraordinary economic aid could be
-------------------------------------------------------------------------------

With respect to the above, cases of extraordinary economic aid could be:

- situations of war, civil war, or natural or man-made disaster;
- situations of revolution, uprising, or severe disorder; or
- situations of exceptional repression.


12.3.6 In no case may the combined sum of the extraordinary economic aid disbursed
------------------------------------------------------------------------------------

In no case may the combined sum of the extraordinary economic aid disbursed
exceed 50% of the total solidarity fund at the time it is granted.


12.3.7 In all cases, the requested economic aid can be in the form of one-time payments
-----------------------------------------------------------------------------------------

In all cases, the requested economic aid can be in the form of one-time payments
or periodic payments.

12.3.8 In all cases, the economic aid that is granted can cover the entire quantity
-------------------------------------------------------------------------------------

In all cases, the economic aid that is granted can cover the entire quantity
requested or only a part, depending on the availability of funds, after
consultation with the Treasurer.

12.3.9 Initiatives can also request economic aid from the solidarity fund
---------------------------------------------------------------------------

Initiatives can also request economic aid from the solidarity fund, in the
manner described above.
