.. index::
   pair: Authoritative ; ICL-CIT

.. _icl_authoritative:

============================
**15. Authoritative text**
============================

.. seealso::

   - https://www.icl-cit.org/statutes/

.. contents::
   :depth: 3

15.1 In case of any discrepancy between the official versions of the text
============================================================================

In case of any discrepancy between the official versions of the text of
these Statutes in different languages, the English text will be the authoritative
version.

Also available in: Español (Spanish)
