.. index::
   pair: Secretary ; ICL-CIT

.. _icl_secretary:

======================
**9. Secretary**
======================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3

9.1 The Secretary of the ICL is the legal representative of the ICL
=======================================================================

The Secretary of the ICL is the legal representative of the ICL.


9.2 This office will be held by one of the members of the Liaison Committee
============================================================================

This office will be held by one of the members of the Liaison Committee
from one of the Sections of the ICL.

**It will rotate every two years with no extensions**.

That is, every two years the office will be assumed by an individual member of
the Liaison Committee from a different Section.


9.3 The system of rotation will be established at the Congress
=================================================================

The system of rotation will be established at the Congress.

It will be valid for the period until the next Ordinary Congress.


9.4 Once two years of office have been completed, the Secretary will automatically step down
=============================================================================================

Once two years of office have been completed, the Secretary will automatically
step down and be immediately replaced by the next Section in the rotation.


9.5 The Secretary’s duties are
================================

The Secretary’s duties are:

- to be the public and legal representative of the ICL,
- to call Congresses and referendums when they are required by these Statutes,
- to coordinate the work of the Liaison Committee, and
- any other task assigned by these Statutes.
