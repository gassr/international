.. index::
   pair: Name and definition ; ICL-CIT

.. _icl_statutes_name:

============================
**1. Name and definition**
============================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3

1.1 The International Confederation of Labor (ICL) is an international federation
-------------------------------------------------------------------------------------

The International Confederation of Labor (ICL) is an international federation
with a global scope, composed of anarcho-syndicalist and revolutionary unions,
defined by its principles and its Federative Agreement.


1.2 The ICL is governed by these Statutes
-------------------------------------------

The ICL is governed by these Statutes, in the version approved at its most
recent Congress.
