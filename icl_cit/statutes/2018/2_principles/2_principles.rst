.. index::
   pair: Statutes ; Principles (ICL)

.. _icl_statutes_principles:

===================
**2. Principles**
===================

.. seealso::

   - https://www.icl-cit.org/statutes/


.. contents::
   :depth: 3


2.1. The member organizations of the ICL share a set of undeniable principles
===============================================================================

The member organizations of the ICL share a set of undeniable principles,
**considered to be essential** for creating the possibility for any truly significant
social and economic change, and are committed to uphold these principles in
their actions, both at the local and international levels.

2.2. The principles of the ICL that follow are embodied in its Federative Agreement
=====================================================================================

The principles of the ICL that follow are embodied in its Federative Agreement:

Anarcho syndicalism and revolutionary unionism
-----------------------------------------------

The member organizations of  the ICL define themselves as anarcho-syndicalist unions and/or revolutionary unions,
in consideration of historical and cultural peculiarities of the different
geographic regions. That is to say, they are syndicalist organizations that are
involved principally, but not exclusively, in the realm of labor, with an ultimate
revolutionary goal.
It is thus understood that their activities are directed towards or strive for
the transformation of the dominant economic model of production and consumption,
in order to make it respond to the needs of the population at large, in
accordance with **criteria of equality, sustainability, and inclusion**, among others,
and the rest of the principles set forth in these Statutes.

Each member organization of the ICL will define within their sphere of activity
the tactics and strategies that they consider most suitable to pursuing this goal.

As a consequence of all of the above, the ICL defines itself as revolutionary
unionist and anarcho-syndicalist, because these are precisely the characteristics
of the organizations that make it up.

Solidarity
-----------

The main tool for achieving a revolutionary transformation of society and the
key element of anarcho-syndicalism and revolutionary unionism is solidarity,
understood as **mutual aid among all working people towards each other**.

In the context of the ICL, this translates as the willingness of all of the
member organizations to support the efforts of any of the others in the process
of revolutionary transformation, in the manner and at the time requested by
the organization requiring support.

However, it can sometimes be difficult to express this solidarity in practical
ways, due to barriers of language, geography, or any other kind.

The member organizations of the ICL state their commitment to explore all
possible forms of expressing this solidarity, not merely by carrying out one-time,
occasional acts of solidarity but by developing collaborative work dynamics
that can effectively reinforce the activity of the member unions at the
local level.

Class struggle
---------------

As stated above, with their activities the member organizations of the ICL seek
to end the present economic system of production, reproduction, and consumption.

This is essentially based on exploitation and profit maximization.

It thus stands in the way of the interest of the workers.

Consequently, **employers cannot become members at any level of the organization**.

Internationalism
-----------------

The member organizations of the ICL affirm the need for active solidarity
between all workers, independent of their nationality, place of birth or
residence, legal status, ethnicity, beliefs, gender, or sexual orientation.

Thus, in accordance with the principles already laid out, their desire is to
extend solidarity to all members of the working class wherever they are found,
across the barriers that artificially separate them.

This drive for internationalism is expressed in the very creation of the ICL
as a tool to make this solidarity more effective across borders and in spite of them.

Horizontality
-----------------

The member organizations of the ICL are internally organized in a horizontal,
non-hierarchical manner, with decision-making processes starting from the bottom.

The exact way this is put into practice in each case depends, of course, on the
organization in question.

This **principle of horizontality** is reflected in these Statutes, so that the
decision-making power resides in each person affiliated with the member
organizations of the ICL, through a decision-making process starting from
the bottom.
Thus, all of the administration and facilitation positions considered in
these Statutes are irrevocably subject to a mandate from the corresponding
stage in the decision-making process.

Federalism
-------------

.. seealso::

   - :ref:`icl_federative_agreement`

To respect this horizontality and the autonomy of the member organizations of
the ICL in their respective spheres of activity, the ICL will operate according
to **federalist principles**.

The member organizations will be bound by the :ref:`Federative Agreement <icl_federative_agreement>`, which
defines the minimum criteria that all members must uphold in their actions,
but also **confirms the autonomy of the member organizations** in all other questions
beyond this agreement on minimum standards.

This Federative Agreement can be found :ref:`below <icl_federative_agreement>`.
It is the logical development of the principles that have been laid out here

In consequence, **the ICL will adopt the form and function of a federation**.

Independence
--------------

To ensure scrupulous respect at all times for the principles laid out in this
article, and to ensure that all of the member organizations act only in
response to the interests of their members and with the goal of the
revolutionary transformation of society, it is necessary that these organizations
and the ICL itself maintain their independence from any possible conditioning
by external forces.
The ICL will have no sources of funds beyond the dues paid by each of its
member organizations in the amount set forth in these Statutes.

For the same reason, the ICL does not take part in elections for representative
bodies. Its member organizations should also not take part in such elections,
whether they relate to politics or labor unions.

In order to remain independent from the state, the involvement of individuals
belonging to the repressive forces of the state (unless conscripted) and of
private enterprises is prohibited on all levels of the ICL.

- Anti-fascism
- Anti-militarism
- Environmental protection
- Direct action
