
.. index::
   pair: But ; Confédération Internationale du Travail

.. _but_cit:

====================================================
But/Historique
====================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Conf%C3%A9d%C3%A9ration_internationale_du_travail
   - :ref:`motion_8_2020_ptt95`


.. contents::
   :depth: 3

Historique
===========

La Confédération Internationale du Travail (CIT) (en anglais International
Confederation Of Labor) est une fédération syndicale internationale composée
de Syndicats de travailleuses/travailleurs d'origine anarcho-syndicaliste
et/ou syndicaliste révolutionnaire.

La CIT a été fondée en **mai 2018 à Parme**, avec des participants provenant de
soixante nations et régions.

La plupart des adhérents de l'international sont d'anciens membres de l'Association
internationale des travailleurs (IWA-AIT), qui a éclaté en 2016.

CNT Vignoles
==============

.. seealso::

   - :ref:`motion_8_2020_ptt95`


Syndicats participants
=======================

Les syndicats qui prennent part à la fondation de la Confédération à Parme,
le 13 mai 2018, sont principalement des syndicats appartenant auparavant
à l'AIT, comme la CNT-e, l'USI et la FAU, mais aussi des syndicats appartenant
à la Coordination rouge et noire, comme l'IP et l'ESE.

.. figure:: participants_icl_cit.png
   :align: center
