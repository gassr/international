.. index::
   ! Confédération Internationale du Travail
   ! Confederación Internacional del Trabajo
   ! International Confederation Of Labor
   ! ICL-CIT

.. _icl_cit:

=================================================================================================
**International Confederation Of Labor (ICL), Confederación Internacional del Trabajo (CIT)**
=================================================================================================

.. seealso::

   - https://www.icl-cit.org/
   - https://twitter.com/IclCit
   - https://fr.wikipedia.org/wiki/Conf%C3%A9d%C3%A9ration_internationale_du_travail
   - https://en.wikipedia.org/wiki/International_Confederation_of_Labor?oldid=874483959
   - https://es.wikipedia.org/wiki/Confederaci%C3%B3n_Internacional_del_Trabajo


.. figure:: ../images/ICL-CIT_Logo.png
   :align: center

   https://www.icl-cit.org/

.. toctree::
   :maxdepth: 3

   statutes/statutes
   congresses/congresses
   description/description
   news/news
