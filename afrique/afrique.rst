
.. index::
   pair: International; Afrique
   
   
.. _afrique:
   
===============================
Afrique
===============================

.. toctree::
   :maxdepth: 4
   
   afrique_du_sud/index
   algerie/index
   maroc/index
   mayotte/index
   tunisie/index

   
   
   
