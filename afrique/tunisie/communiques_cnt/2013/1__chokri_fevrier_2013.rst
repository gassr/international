
.. index::
   pair: Tunisie; Chokri Belaid

.. _cnt_tunisie_chokri_2013:

========================================
Manifestation justice pour Chokri Belaid 
========================================


.. contents::
   :depth: 3


Courriel
========


::

    Sujet:  [Liste-syndicats] [Fwd: [tunisie] Manifestation justice pour Chokri Belaid]
    Date :  Sat, 9 Feb 2013 00:02:49 +0100
    De :    CNT Educ RP <educ.rp@cnt-f.org>
    Pour :  liste-syndicats@bc.cnt-fr.org


La Confédération nationale du travail (CNT) condamne fermement l'assassinat 
de Chokri Belaïd, l'une des figures emblématiques de la résistance à la 
dictature de Ben Ali et de la révolution tunisienne.

Ce crime odieux, intervenu le 6 février, au matin, alors que l'avocat 
sortait de chez lui, est aussi un crime contre la révolution. 

La CNT adresse ses condoléances à sa famille et à ses proches.

Justice doit être rendue. Pour Chokri, mais aussi pour toutes les victimes 
de la répression qui s'est intensifiée ces derniers mois en Tunisie.

La CNT soutient activement la grève générale, seul moyen selon nous, 
d'arriver à  la révolution sociale.

Un coup porté contre l'un d'entre nous est un coup porté contre tous.

La CNT, appelle à se joindre aux manifestations organisées en Tunisie, 
en France, et notamment à celle du samedi 9 février à Paris, 13H (métro Barbès) 
à l'appel d'un collectif d'associations tunisiennes.

Le Secrétariat International de la CNT-F.

Tract
======

L’assassinat de Chokri Belaïd, tué par balle, mercredi 6 février au matin 
en sortant de chez lui, est assurément un crime odieux. 
La Confédération nationale du travail (CNT) adresse toutes ses condoléances 
à sa famille, ainsi qu’à ses camarades de syndicat ou de parti. 

Ce camarade avec lequel nous ne partagions pas forcément toutes les 
positions sur la conduite à mener dans la période post-14janvier, restera 
comme l’une des figures emblématiques de cette révolution. 

Il ne comptait pas comme d’autres, actuellement au gouvernement, parmi 
les « révoltés de la dernière heure ». Outre son passé de militant 
politique entamé dans les années 80, il a été en effet l’un des plus 
actifs soutiens de la révolte du bassin minier de Gafsa, en 2008, qui a 
constitué les prémices de la révolution, comme nous en avons été témoins 
lors des procès, où il assurait avec d’autres la défense des syndicalistes 
et des chômeurs incarcérés.

Ce meurtre constitue une nouvelle étape dans la répression et le climat
contre-révolutionnaire qui règne à l’heure actuelle. 

Il ne doit pas rester impuni. Justice doit être faite, comme pour toutes 
les victimes et martyrs de la révolution et leurs familles qui réclament 
le traitement des dossiers.

Depuis la chute de Ben Ali, malgré d’indéniables reculs, le système répressif 
est toujours en place. Les attaques n’ont pas cessé, de la part du régime 
ou de ses soutiens déclarés ou non (récemment en décembre 2012, plus de 
200 blessés à tirs de chevrotines à Siliana, ou encore l'attaque du cortège 
de commémoration de l’assassinat de Ferhat Ached, le fondateur de l’UGTT 
et figure de la lutte anticoloniale).

La résistance incarnée notamment par la base de la principale centrale 
syndicale du pays, l’UGTT, et une partie de la société civile, doit se 
poursuivre et s’intensifier, aux côtés des constantes mobilisations
des comités de chômeurs, concernés au premier chef par l’augmentation 
du coût de la vie, des grèves de travailleurs, malheureusement isolés 
et pas toujours soutenus par la bureaucratie syndicale.

Vendredi, jour de la grève générale à l’appel de l’UGTT, et des funérailles 
de Chokri Belaïd, le régime a une nouvelle fois montré son caractère 
anti-ouvrier, en réprimant sévèrement les manifestations dans l’ensemble 
du pays. La répression ne doit pas inciter les militants à renoncer au 
combat pour les libertés fondamentales et la révolution sociale. 

La peur doit changer de camp.

Un coup contre l’un d’entre nous, est un coup porté contre tous. 

La CNT, comme elle l’a toujours fait, se porte aux côtés des artisans 
de la révolution. 

La deuxième révolution, qui semble s’amorcer, est toujours à construire, 
dans la rue, dans les quartiers, dans les usines.

Grève générale ! Justice pour Chokri Belaïd.



