

.. _maroc_UMT_novembre_2012:

========================================================
Entretien vidéo avec un militant du FNSA (Novembre 2012)
========================================================

.. seealso::

   http://www.cnt-f.org/video/videos/44-international/430-le-syndicalisme-agricole-au-maroc



.. contents::
   :depth: 3
   
Titre
=====

Entretien vidéo avec le secrétaire général adjoint de la Fédération Nationale 
du Secteur Agricole (FNSA) affiliée à l'Union Marocaine du Travail (UMT).


Annonce
========
  
::
   
	Sujet: 	[Liste-syndicats] entretien vidéo avec le secrétaire général
	adjoint de la Fédération Nationale du Secteur Agricole (FNSA) affiliée à
	l'Union Marocaine du Travail (UMT)
	Date : 	Tue, 27 Nov 2012 12:18:35 +0100
	De : 	Secteur Vidéo CNT <secteur-video@cnt-f.org>


Bonjour,

Saïd XXX XXX, secrétaire général adjoint de la Fédération Nationale
du Secteur Agricole (FNSA) affiliée à l'Union Marocaine du Travail
(UMT), était présent à l'invitation du secrétariat international de la
CNT au 32e congrès de la CNT qui s'est tenu à Metz les 1, 2, 3 et 4
novembre 2012.

L'entretien qu'il a bien voulu accorder au secteur vidéo CNT est en
ligne ici:

http://www.cnt-f.org/video/videos/44-international/430-le-syndicalisme-agricole-au-maroc


Au sommaire de cet entretien:

1. Présentation de la FNSA UMT
2. Le mouvement du 20 février 2011 ou le printemps arabe
3. La corruption dans l'organisation
4. La répression anti-syndicale
5. Les femmes dans l'organisation
6. Le travail des enfants
7. La propagande
8. Les petits paysans
9. L'autosuffisance alimentaire


Salutations as-sr, le secteur vidéo CNT
      
