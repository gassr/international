

.. _communiques_CNT_afrique_du_sud:

=================================================================================
Communiques CNT Afrique du Sud 
=================================================================================


.. contents::
   :depth: 4

Communiques CNT Afrique du Sud 2012
===================================


::

	Sujet: 	[Liste-syndicats] communiqué Afrique du Sud
	Date : 	Wed, 22 Aug 2012 19:12:39 +0200
	De : 	"Secrétariat International CNT-F" <international@cnt-f.org>
	Pour : 	liste syndicats <liste-syndicats@bc.cnt-fr.org>
	Copie à : 	liste CA <liste.ca@bc.cnt-fr.org>, bc <liste-bc@bc.cnt-fr.org>


Bonjour,

Vous trouverez ci-dessous le communiqué du secrétariat international en
soutien aux mineurs de Marikana en Afrique du SUd.

Fraternellement
Anne (pour le SI)


Afrique du Sud : le capitalisme a encore tué
==============================================

Jeudi 17 août la police sud-africaine a tiré sur les mineurs grévistes de
Marikana et a tué 34 travailleurs en a blessé 78 et arrêté 259. 

Les mineurs,pour la plupart des foreurs exposés à des  conditions de travail
effroyables, avaient cessé le travail depuis une semaine pour réclamer des
augmentations de salaire et des améliorations de leur vie quotidienne.

Plutôt que de répondre à ces revendications légitimes, Lonmim, entreprise
propriétaire de la mine, a joué les divisions syndicales (10 morts issus
d'affrontements entre les affiliés) et la répression policière. Il n'y a
rien d'étonnant à cela, Lonmim a entre autre dans son capital Xstrata ;
société suisse qui a un lourd passif dans la destruction forcée de
villages colombiens dans la région du Cerrejon ainsi que l'expulsion par
les militaires et les paramilitaires de leurs habitants. 

Les Indiens Wayùu en ont récemment fait la triste expérience. Elle est par 
ailleurs engagée dans le projet Koniakambo d'extraction de nickel et de 
cobalt dans la zone de Koné en Nouvelle-Calédonie, projet qui soulève 
l'opposition farouche de la population locale.

Ce capitalisme cynique veut maintenant renvoyer la responsabilité des
meurtres sur les syndicalistes prétextant que ceux-ci auraient chargé
armés les policiers. C'est une ignominie.  

Les affiliés de l'AMCU, syndicat autonome, défendaient leur droit de grève et 
leur vie face aux agressions qu'ils avaient subies de la part du NUM 
(National Union of Mine workers) syndicat lié au pouvoir et instrumentalisé 
par la Lonmin.

Le 20 août la compagnie minière a menacé les grévistes de licenciements et
de sanctions s'ils ne reprenaient pas le travail. 

Va-t-elle aussi licencier les travailleurs qu'elle a fait assassiner, ceux 
qu'elle a envoyés à l'hôpital et en prison ?

On peut dire que le procès des mineurs emprisonnés n'a pu avoir lieu le 20
août suite à la mobilisation solidaire de leurs camarades et de leurs
familles. Les chefs d'accusations pesant sur les 259 mineurs sont meurtre,
tentative de meurtre et violence en réunion le jugement devrait être
reporté au 27 août. Ces chefs d'accusations démesurés sont une preuve de
la duplicité de l'État  sud-africain qui, alors qu'il décrète une semaine
de deuil national, se place clairement du côté du patronat et veut faire
condamner les travailleurs qui n'exigeaient que de meilleures conditions
de travail.

Depuis le début de l'année, 40 mineurs sont morts suite à des accidents de
travail en Afrique du Sud. La barbarie capitaliste est aussi une barbarie
quotidienne.

Le Secrétariat international de la CNT-F soutient les camarades
sud-africains dans leur lutte contre ces barbaries et appelle à soutenir
leurs revendications par la solidarité internationale, qui peut se
manifester en Europe et en France car les entreprises actionnaires sont
majoritairement européenne.

Nous exigeons la pleine satisfaction des revendications des mineurs, la
libération des travailleurs emprisonnés et que toutes les responsabilités
qu'elles soient gouvernementales ou patronales soient établies.


-- 
Secrétariat International de la CNT-F
33, rue des Vignoles
75020 Paris

www.cnt-f.org/international



