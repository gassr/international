
.. index::
   pair: Judith Butler; Judéité et critique du sionisme


.. _judith_judeite:

=====================================================
Vers la cohabitation Judéité et critique du sionisme
=====================================================

.. seealso::

   - http://www.actu-philosophia.com/spip.php?breve2617

   
Comment fonder une nouvelle éthique en Israël/Palestine ? 

Et peut-on renouer, politiquement, avec la solution d’un État unique et 
binational ? 

Dans cet ouvrage, Judith Butler s’interroge sur la possibilité d’articuler 
les expériences juives de la diaspora et du déplacement et les expériences 
palestiniennes de la dépossession pour repenser la situation dans la région. 

Elle place au centre de sa réflexion la notion de cohabitation, une condition 
de notre vie politique et non quelque chose que nous pouvons refuser. 
Nul n’est en droit de choisir avec qui cohabiter sur cette terre. 
Selon Butler, l’éthique de la judéité exige une critique du sionisme. 

La célèbre philosophe puise ainsi dans la pensée juive des instruments pour 
mettre en question la violence, le nationalisme et le colonialisme de l’État 
d’Israël. Elle engage la discussion avec des auteurs comme Hannah Arendt, 
Emmanuel Levinas, Primo Levi, Martin Buber, Walter Benjamin, mais aussi 
Edward Said ou Mahmoud Darwich. 

Elle se confronte aux problèmes du droit des dépossédés et des apatrides, du 
traumatisme de l’Holocauste, de l’oppression et de l’exil. 

Elle renouvelle, au nom du caractère irréductible de la pluralité humaine, les 
concepts classiques de droit, d’État-nation, de citoyenneté ou encore de 
souveraineté. 

Mêlant éthique, philosophie et politique, ce grand livre affirme un idéal de 
cohabitation, de justice sociale et de démocratie radicale.

Judith Butler est philosophe, professeure à l’Université de Californie à Berkeley. 

Elle est notamment l’auteure de Trouble dans le genre (La Découverte, 2005), 
La Vie psychique du pouvoir (Léo Scheer, 2002), Ce qui fait une vie (Zones, 2010). 

Traduit de l’anglais (États-Unis) par Gildas Le Dem

   
