
.. index::
   pair: Fanfare; Future   
   
   
.. _fanfare_for_the_future:
   
=============================================
Fanfare for the future
=============================================

.. seealso:: 

   - http://www.zcommunications.org/topics/fanfare-for-the-future
   - :ref:`fanfare_for_the_future_iops`    
   
.. contents::
   :depth: 3
   

Introduction
==============   

Occupy Theory offers concepts for understanding society and history as well as 
applications. 

Accessible and succinct, it facilitates undertaking visionary and strategic 
work with a participatory theory.   

The chapters of Occupy Theory, by Michael Albert and Mandisi Majavu, are
========================================================================

Preface
Introduction
Crazy Patterns
Refining Four Views
Society and History
Modes of Analysis
Participatory Theory

