
.. index::
   pair: 2012; Guadeloupe
    
.. _guadeloupe_2012:
   
===============================
Guadeloupe 2012
===============================

.. toctree::
   :maxdepth: 3
   
   
   
Tournée de Elie Domota
======================  

::

	Sujet: [Liste-syndicats] dates meeting UGT Guadeloupe
	Date : Fri, 1 Jun 2012 16:51:08 +0200 (CEST)
	De : Secrétariat International CNT-F <international@cnt-f.org>
	Pour : liste-syndicats@bc.cnt-fr.org
	Copie à : liste-international@cnt-f.org


Bonjour,

Pour information, des meeting avec Elie Domota, secrétaire générale de
l'UGT Guadeloupe à Lyon, Nantes, Paris, Lille et Limoges.

Fraternellement
Le SI


SOLIDARITE AVEC LES TRAVAILLEURS
DE GUADELOUPE

MEETINGS AVEC ELIE DOMOTA

A l'invitation du Comité international contre la répression (CICR) et de
plus de quatre cent cinquante syndicalistes de toutes appartenances, le
secrétaire général de l'Union générale des travailleurs de Guadeloupe
(UGTG), Porte parole du LKP, Elie DOMOTA, effectuera une tournée de
meetings pour l'arrêt de la répression antisyndicale en Guadeloupe,
l'arrêt de tous les procès contre des syndicalistes pour des actes
relevant de leur mandats syndicaux, la levée de toutes les sanctions et le
respect des accords.

- LYON : Lundi 11 juin, 18 h 30, Bourse du travail ;

- NANTES : Mardi 12 juin, 17 heures, Bourse du travail (salle F) ;

- PARIS: Mercredi 13 juin, 18 h 30, grande salle de la Bourse du travail
  (3, rue du Château-d'Eau) ;

- LILLE : Jeudi 14 juin, à 18 h 30,
  salle municipale d'Hellemmes ;

- LIMOGES: Vendredi 15 juin, 20 h 30, salle Auguste-Blanqui.

   
   
   
