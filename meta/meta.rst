.. index::
   ! meta-infos


.. _doc_meta_infos:

=====================
Meta infos
=====================

.. seealso::

   - https://cnt-f.gitlab.io/meta/

.. contents::
   :depth: 3


Gitlab project
================

.. seealso::

   - https://framagit.org/international/cnt


Issues
--------

.. seealso::

   - https://framagit.org/international/cnt/-/boards


Pipelines
-----------

.. seealso::

   - https://framagit.org/international/cnt/-/pipelines

root directory
===============

::

    $ ls -als

::

    total 188
    4 drwxr-xr-x 17   4096 oct.  27 18:47 .
    4 drwxr-xr-x  3   4096 oct.  27 18:34 ..
    4 drwxr-xr-x  7   4096 oct.  27 18:34 afrique
    4 drwxr-xr-x  5   4096 oct.  27 18:43 ameriques
    4 drwxr-xr-x  6   4096 oct.  27 18:43 asie
    4 -rwxr-xr-x  1   3677 oct.  27 18:44 conf.py
    4 drwxr-xr-x  3   4096 oct.  27 18:34 coordination_rouge_et_noire
    4 drwxr-xr-x 11   4096 oct.  27 18:42 europe
    4 -rw-r--r--  1     96 oct.  27 18:40 feed.xml
    4 drwxr-xr-x  8   4096 oct.  27 18:47 .git
    4 -rwxr-xr-x  1     49 oct.  27 18:34 .gitignore
    4 -rw-r--r--  1    211 oct.  27 18:45 .gitlab-ci.yml
    4 drwxr-xr-x  6   4096 oct.  27 18:34 icl_cit
    4 drwxr-xr-x  2   4096 oct.  27 18:34 images
    4 drwxr-xr-x  2   4096 oct.  19 18:09 index
    4 -rwxr-xr-x  1   1349 oct.  27 18:43 index.rst
    4 -rw-r--r--  1   1154 mars  30  2020 Makefile
    4 drwxr-xr-x  2   4096 oct.  27 18:47 meta
    4 drwxr-xr-x  3   4096 oct.  27 18:42 orga_internationales
    84 -rw-r--r--  1  85097 oct.  27 18:47 poetry.lock
    4 -rw-r--r--  1   1016 oct.  27 18:45 .pre-commit-config.yaml
    4 -rw-rw-rw-  1    334 oct.  27 18:41 pyproject.toml
    4 -rw-r--r--  1    559 oct.  27 18:34 requirements.txt
    4 drwxr-xr-x  3   4096 oct.  27 18:43 reseau_euro_mediterraneen
    4 drwxr-xr-x  2   4096 oct.  27 18:43 reseau_europeen_alternatifs
    4 drwxr-xr-x  2   4096 oct.  27 18:34 secretariat_international
    4 drwxr-xr-x  2   4096 oct.  26 17:11 _static




pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
