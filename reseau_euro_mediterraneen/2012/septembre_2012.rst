


.. _reseau_euro_med_septembre_2012:

=============================================
Réseau euro-méditerranéen septembre 2012
=============================================

.. seealso:: 

   - :ref:`reseau_euro_med_mai_2012`


::

	Sujet: 	[Liste-syndicats] demande de soutien pour la rencontre des chômeurs du réseau euro-méditerranée
	Date : 	Fri, 24 Aug 2012 16:17:15 +0200
	De : 	"Secrétariat International CNT-F" <international@cnt-f.org>
	Pour : 	liste syndicats <liste-syndicats@bc.cnt-fr.org>
	Copie à : 	liste CA <liste.ca@bc.cnt-fr.org>


Bonjour,

Les 14, 15 et 16 septembre 2012 se dérouleront à Rabat (Maroc) les 1ères
rencontres des chômeurs et précaires de Méditerranée (v. la programme en
pièce jointe). Ces rencontres sont organisées par l'Association Nationale
des Diplômés Chômeurs du Maroc (ANDCM) avec qui nous entretenons des
relations depuis plusieurs années. 

Elles sont soutenues par la Coordination Syndicale Euro-Méditerranéenne 
dont la CNT fait partie.

Sont conviées à ces rencontres les différentes structures de chômeurs et
précaires du pourtour méditerranéen avec lesquelles la CNT ou les autres
membres de la Coordination euro-méditerranéenne sont en contact.

Pour que ces rencontres puissent se dérouler, la coordination
euro-méditerranéenne a décidé de payer une partie des voyages des
représentants chômeurs/précaires mandatés par leur organisation.

En effet, les différentes organisations de chômeurs/précaires n'ont pas
toutes les moyens de financer les trajets de leurs mandatés.

Le SI de la CNT a fait donc appel aux syndicats de la confédération pour
participer à ce soutien financier.

La date des rencontres ayant été fixé courant juillet, nous n'avons guère
de temps pour recueillir de l'argent. Si vous souhaitez participer à ce
soutien mais que pour cela vous devez organiser concert, bouffe,
projection, etc., et donc que vous avez besoin d'un peu de temps, il est
possible de nous indiquer en gros la somme que vous pensez pouvoir
transmettre et nous verrons avec la trésorerie confédérale pour qu'elle
effectue une avance.

Ce serait bien si nous pouvions savoir début septembre à quelle hauteur
nous pouvons participer afin d'informer les différentes organisations du
nombre de personnes qu'elles vont pouvoir mandater pour ces rencontres.

Par ailleurs, nous ne savons pas encore si un-e camarade cntiste
participera à ces rencontres, mais dans tous les cas nous transmettrons
aux camarades de l'ANDCM un texte abordant les différents sujets proposés
dans le programme. Par conséquent, n'hésitez pas à nous transmettre des
informations sur vos luttes locales, vos expériences de
précaires/chômeurs, etc.

Fraternellement
Le GT Afrique de la CNT
-- 
Secrétariat International de la CNT-F
33, rue des Vignoles
75020 Paris
