
.. index::
   single: Réseau euro-méditerranéen
   pair: Coordination ; Euro-méditerranéene


.. _reseau_euro_mediterraneen:

===========================================
Réseau ou coordination euro-méditerranéenE
===========================================

.. contents::
   :depth: 3


Composition
============

La coordination syndicale euro mediterraneenne est composee par:


- CGT-E (Espagne)
- IAC (Catalunya)
- Solidaires (France)
- CNT-F (France)
- USI (Italie)
- CUB (Italie)
- Comité Syndical Voix Démocratique (Maroc)
- ANDCM (Maroc)
- ODT (Maroc)
- CLA (Algérie) -
- SNAPAP (Algérie)
- SESS (ancien CNES) (Algérie)


.. toctree::
   :maxdepth: 3

   2012/index
