
.. index::
   pair: IOPS; Fanfare for the future
   pair: Fanfare; for the future

.. _fanfare_for_the_future_iops:

======================
Fanfare for the future
======================

.. seealso:: 

   - http://www.zcommunications.org/topics/fanfare-for-the-future
   - :ref:`fanfare_for_the_future` 

