
.. index::
   pair: UK ; IOPS
   pair: england ; IOPS

.. _iops_uk:

=================
IOPS UK (england)
=================


.. seealso::

   - http://www.iopsociety.org/
   - http://www.iopsociety.org/england
   - :ref:`iops_europe`



History
=======

.. seealso:: http://www.iopsociety.org/blog/talking-about-iops-and-spain-with-oliver


.. _pps_uk:

The Project for a Participatory Society U.K (PPS-UK)
=====================================================

.. seealso::

   - http://www.ppsuk.org.uk/


The Project for a Participatory Society U.K (PPS-UK) is a growing movement of
people committed to developing, popularising and implementing vision and
strategy to winning a new society based on participation, solidarity, equity,
diversity and self-management.
