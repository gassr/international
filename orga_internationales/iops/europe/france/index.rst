
.. index::
   pair: France ; IOPS


.. _iops_france:

=================
IOPS France
=================


.. seealso::

   - http://www.iopsociety.org/
   - http://www.iopsociety.org/france
   - :ref:`iops_europe`



.. toctree::
   :maxdepth: 3


   rhone_alpes/index

