
.. index::
   pair: 38 ; Isère


.. _iops_isere:

=================
IOPS Isère
=================



.. seealso::

   - http://www.iopsociety.org/
   - :ref:`iops_france`



.. toctree::
   :maxdepth: 3


   grenoble/index

