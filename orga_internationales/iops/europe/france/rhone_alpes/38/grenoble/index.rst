
.. index::
   pair: Grenoble ; IOPS


.. _iops_grenoble:

=================
IOPS Grenoble
=================


.. seealso::

   - http://www.iopsociety.org/
   - http://www.iopsociety.org/france/rhone-alpes/grenoble
   - :ref:`iops_france`



.. toctree::
   :maxdepth: 3


   members/index

