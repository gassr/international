
.. index::
   ! Francis Feeley


.. _francis_feeley:

===================================
Francis McCOLLUM Feeley (15/01/1946
===================================


.. figure:: francis_feeley.gif
   :align: center
   
   Francis Feeley


.. seealso::

   - http://www.iopsociety.org/profile/francis-feeley
   - http://dimension.ucsd.edu/ceimsa-in-exile/curriculum/CV-FFeeley.html

Adresse courriel: francis.feeley@u-grenoble3.fr
================================================

:Adresse courriel: francis.feeley@u-grenoble3.fr


Activites
==========

.. toctree::
   :maxdepth: 5
   
   2013/index
   2012/index   
   2006/index
   
