
.. index::
   pair: Francis Feeley; 2012

.. _francis_feeley_2012:

==================================================
Francis Feeley 2012
==================================================

.. seealso::

   - http://www.ciip.fr/docum/docel/chamb.htm
   - http://sncs.fr/imprimer.php3?id_article=225&id_rubrique=11

CEIMSA: Centre d'Etudes des Institutions et des Mouvemnts Sociaux Américain

Newsletter : Numéro 51  17 juillet 2012
=======================================

Cher(e)s collégues et des ami(e)s du CEIMSA,

Nous sommes en train d’organiser une série de colloques internationaux sur le 
thème suivant : «comment mobiliser des communautés : les stratégies et les tactiques».

Le premier se tiendra à Grenoble, le 14 octobre 2012 ; le suivant à l’Université 
de Paris X à Nanterre, le 15 novembre 2012, et un troisième à Grenoble en mai 2013.

Le colloque d’octobre est en fait ma participation promise à un plus grand 
colloque de trois jours sur la réalisation de ‘projets utopiques’ dans la 
société réelle. L’Association Entropie de Grenoble m’a invité à organiser un 
atelier à son Festival pour repenser la société, prévu sur le campus de 
l’Université Stendhal à Grenoble du 11 au 14 octobre. 

Le dimanche 14 octobre, je présiderai une table ronde où des militants qui ont 
mobilisé différentes communautés parleront des aspects concrets de la question 
« Les moyens les fins dans la mobilisation de communautés sont-ils toujours justifiés  ?

Le second colloque que j’organise est prévu le 12 novembre 2012 à l’Université 
de Paris X à Nanterre. 

Ce colloque international est intitulé « Penser à l’échelle mondiale et agir 
au plan local : l’art et la science de la mobilisation de communautés, depuis 
l’Intifada et les Indignados jusqu’au Printemps arabe, à la Résistance grecque 
et à Occupy Wall Street à New York, et au-delà… » 

D’anciens militants qui ont mobilisé leurs communautés seront amenés à discuter 
de leur expérience. L’idée centrale de ces rencontres est d’avoir un échange 
entre des militants qui acceptent de participer et sont capables de parler 
en détail

- des techniques pratiques utilisées dans leur expériences de mobilisation de 
  communautés variées
- des objectifs et motivations avant de commencer leur travail ; 
- des obstacles et limites rencontrés au cours de la mobilisation et les 
  compromis et les alternatives qu’ils ont eu à inventer avant de trouver des 
  solutions aux problèmes qu’ils ont rencontrés.

Le troisième colloque, qui s'étendra probablement sur deux jours, est prévu en 
principe pour  les 2 et 3 mai 2013, et il se tiendra à Grenoble. 

Il sera l'occasion d'une rencontre entre intellectuels et militants locaux, et 
d'un échange sur la théorie et la pratique de mobilisation de communautés de 
types différents. 

Il abordera aussi les conflits institutionnels qui surgissent de la concurrence 
et de malentendus.

Une fois de plus, le but de ces trois rencontres publiques--à Grenoble, en 
octobre, à Nanterre, en novembre ; et de nouveau à Grenoble, en mai-- sera de 
partager des discussions personnelles de militants expérimentés avec un large 
public de quelques centaines de personnes qui écouteront des descriptions 
détaillées de leurs communautés, des problèmes qu’ils ont perçus et de la 
manière dont ces problèmes ont été abordés et parfois résolus grâce à l’aide 
d’un militant, laissant un héritage local d’approches sociales et de traditions 
politiques spécifiques qui servent parfois comme une matrice culturelle pour 
de futures actions politiques.

J’ai à mon compte plusieurs colloques internationaux très réussis depuis 2002. 
(cf le lien Internet à notre centre de recherches, CEIMSA pour lire une 
description détaillée de mes colloques locaux, régionaux et internationaux 
http://dimension.ucsd.edu/CEIMSA-IN-EXILE/colloques.html.)

Cette newslettre cherche simplement à vous demander si vous connaissez  des 
militants expérimentés qui aimeraient participer à l’un de ces trois colloques. 

Nous espérons attirer des activistes dans les communautés de nombreux pays 
différents comme la Palestine, l’Espagne, la Tunisie, l’Egypte, la Grèce, 
l’Ireland, l’Angleterre, les Etats-Unis, le Canada, la Russie, la Chine, 
le Venezuela, le Mexique, l’Argentine et bien sûr la France --- toujours sur 
le rubrique :

                « Réfléchir globalement et Agir localement. »

Ces colloques seront filmés et placés sur Internet pour l’utilisation 
pratique de militants, et nous prévoyons d’avoir des interprètes à ces 
trois colloques (cf les vidéos de notre dernier colloque à Nanterre, 
le 19 mai 2011 sur le sujet `Ethique et Politique étrangère des E.U`_

Je vous remercie à l’avance pour toute suggestion que vous pourriez faire 
concernant ce demande.

J’attends votre réponse à cette lettre.

En solidarité,
Francis Feeley


_`Ethique et Politique étrangère des E.U`:  http://www.youtube.com/results?search_query=francis+feeley&oq=francis+feeley&aq=f&aqi=&aql=&gs_sm=s&gs_upl=925l5500l0l10052l14l14l0l3l1l0l344l2408l1.3.5.2l11l0



