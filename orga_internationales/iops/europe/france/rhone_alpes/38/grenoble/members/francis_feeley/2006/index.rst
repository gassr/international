
.. index::
   pair: Francis Feeley; 2006

.. _francis_feeley_2006:

==================================================
Francis Feeley Censure de son site  CEIMSA en 2006
==================================================

.. seealso::

   - http://www.ciip.fr/docum/docel/chamb.htm
   - http://sncs.fr/imprimer.php3?id_article=225&id_rubrique=11

Toujours censurées par l'université Stendhal de Grenoble, les recherches sur la
civilisation américaine poursuivies par le professeur Francis Feeley et ses
étudiants sont accueillies sur le campus de l'université de Savoie à Chambéry.

Francis Feeley nous a habitués à organiser sur le campus grenoblois de très
intéressantes conférences scientifiques regroupant des chercheurs venant des
Etats Unis et de nombreuses universités françaises et européennes, mettant en
lumière les réalités contradictoires de l'histoire et de la société américaine
et de leurs rapports aux relations internationales. 

Depuis que les instances dirigeantes de l'université Stendhal ont décidé la 
" mise en extinction " de son centre de recherche, et refusé son intégration 
dans un des regroupements de laboratoires de leur université, les travaux de 
recherche du professeur Feeley et de ses doctorants ont été accueillis par le
laboratoire " Langues, Littératures, Sociétés " de l'université de Savoie.

C'est pourquoi le dernier colloque international qu'ils ont organisé s'est
déroulé à Chambéry, sur le thème " Histoire des mouvements pacifistes aux Etats
Unis et en France " les 5, 6 et 7 avril 2006.

32 intervenants y ont présenté des communications sur l'histoire et le rôle
actuel des mouvements pacifistes aux Etats Unis et au Canada, sur la
problématique de la paix dans le contexte de la mondialisation, sur l'histoire
et les témoignages des réfractaires à la guerre d'Algérie et à la guerre du
Viet-nam. Un tiers de ces intervenants venaient des universités de Grenoble
et de Chambéry, un autre tiers d'autres universités françaises, et le reste des
Etats Unis, d'Allemagne, de Suisse et de Suède.

Des débats animés ont suivi leurs interventions.

Jo Briant y a défendu la thèse qui fonde de nombreuses activités du CIIP dans
son intervention "Pas de paix sans justice et sans développement solidaire".

La communication de Marc Ollivier portait sur "Les refus des réfractaires à la
guerre d'Algérie, révélateurs malgré eux des déshonneurs de la gauche française".

Un reportage sur les déserteurs américains au Canada au cours des années 70 a
été présenté par André Gazut, réalisateur de la TV suisse, et le 6 avril au soir
un concert a réuni les participants avec les étudiants en grève, où on a pu
entendre au piano Tatiana Berechkova-Feeley et le chanteur Claude Vinci, lui-même
refuznik de la guerre d'Algérie.

::

	Félicitations au professeur Feeley et à ses étudiants ainsi qu'à l'université de
	Savoie pour avoir organisé ce colloque sur un thème aussi sensible et important,
	mais rarement abordé, il faut le souligner, dans un contexte universitaire.


Marc Ollivier

Pour plus d'informations sur le programme de ce colloque et sur les intervenants,
on peut se reporter au site du centre de recherche de Francis Feeley, le CEIMSA,
hébergé par l'université de San Diego en Californie depuis que l'université
Stendhal l'a éliminé de son serveur :  http://dimension.ucsd.edu/CEIMSA-IN-EXILE/

Ou sur les sites :

- http://raforum.apinc.org/article.php3?id_article=3553
- http://raforum.apinc.org/article.php3?id_article=3555
