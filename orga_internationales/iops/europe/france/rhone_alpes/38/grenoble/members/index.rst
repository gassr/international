
.. index::
   pair: IOPS; Grenoble


.. _iops_grenoble_members:

======================
IOPS Grenoble members
======================


.. seealso::

   - http://www.iopsociety.org/
   - http://www.iopsociety.org/france/rhone-alpes/grenoble
   - :ref:`iops_france`



.. toctree::
   :maxdepth: 3
   
   francis_feeley/index
   
   
