
.. index::
   pair: Rhône-Alpes ; IOPS


.. _iops_rhone_alpes:

=================
IOPS Rhône-Alpes
=================


.. seealso::

   - http://www.iopsociety.org/
   - http://www.iopsociety.org/france/rhone-alpes
   - :ref:`iops_france`



.. toctree::
   :maxdepth: 3


   38/index

