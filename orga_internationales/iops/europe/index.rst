
.. index::
   pair: Europe ; IOPS


.. _iops_europe:

==============================================================
IOPS Europe
==============================================================


.. seealso::

   - http://www.iopsociety.org/



.. toctree::
   :maxdepth: 3


   france/index
   spain/index
   uk/index

