
.. index::
   pair: Organisations ; IOPS
   pair: twitter ; IOPS
   ! Z


.. _iops:

==============================================================
International Organization for a Participatory Society (IOPS)
==============================================================

.. figure:: iops.png
   :align: center


.. seealso::

   - http://www.iopsociety.org/
   - http://en.wikipedia.org/wiki/International_Organization_for_a_Participatory_Society
   - https://plus.google.com/u/0/100462516866427384396/about
   - http://www.zcommunications.org/
   - http://www.zcommunications.org/open-letter-from-chomsky-shiva-santos-pilger-and-40-more-by-iops-supporters
   - http://www.zcommunications.org/chomsky-on-iops-by-noam-chomsky
   - http://twitter.com/#!/iopsociety/


.. contents::
   :depth: 3



About
======

The interim International Organization for a Participatory Society (IOPS) was
launched in 2012 with the aim of propelling activism for winning a new world.

IOPS is structured as a bottom-up, international organization, based on
self-managing interconnected national branches and local chapters.

Currently, IOPS is in an interim stage, and by joining IOPS you become an
interim member.

A convention, or series of conventions, will be planned within the next year,
for membership to determine the organization's definition in more detail.


Notes about the name
--------------------

.. seealso:: http://www.zcommunications.org/chomsky-on-iops-by-noam-chomsky

There is some debate in IOPS about its name. One option is International
Organization for Participatory Society.
Another option, still alive and being used! by some - with a decision to come
only at a founding convention - is International Organization for Participatory
Socialism. Which name do you favor?

I'd somewhat prefer "Society," primarily because the term "socialism," like
virtually every term of political discourse, has been so vulgarized by political
warfare.

Qui sommes nous ?
=================

.. seealso::

   - http://www.iopsociety.org/about/fr
   - :ref:`rcr`

L'Organisation internationale provisoire pour une société participaliste (IOPS)
a été créée en 2012 avec l'objectif de promouvoir l'activisme pour l'obtention
d'un monde nouveau.
L'IOPS est une organisation internationale à structure pyramidale se basant sur
des branches nationales autogérées et interconnectées, ainsi que sur des
groupes locaux.
A l'heure actuelle, l'IOPS se trouve à une étape intermédiaire de son
développement, et en adhérant à l'IOPS vous en devenez un membre provisoire.
Une convention, ou une série de conventions, est planifiée pour l'année
prochaine pour que les adhérents décident d'une définition plus détaillée de
l'organisation.


IOPS books
==========

.. toctree::
   :maxdepth: 3
   
   books/index
   


Historique et espérances
========================

.. seealso::

   - http://www.iopsociety.org/history-and-future/fr
   - http://www.iopsociety.org/blog/talking-about-iops-and-spain-with-oliver


...Puisque les initiales étaient les mêmes dans les deux cas, IOPS, il semblait
que le compromis évident était d'appeler l'organisation IOPS tout en notant
que pour certains ces initiales signifiaient Organisation internationale
intérim pour une société participaliste, et que pour d'autres il s'agissait
de Organisation internationale intérim pour un socialisme participaliste.
La décision finale entre ces deux options, ou en faveur d'une tout autre
possibilité, sera prise lors d'une convention fondatrice rassemblant les
premiers membres de l'organisation. C'est ce que nous essayons de faire, du
moins pour l'instant...

Espérances pour l'IOPS
----------------------

On ne peut pas prévoir les résultats d'une entreprise telle que celle-ci.
Parfois, le processus de création d'un nouveau projet ou d'une nouvelle
organisaiton ne mène à rien, ou bien il dure un temps puis il lâche.
D'autres fois, des structures durables sont créées qui apportent des
contributions importantes pendant des années ou même des dizaines d'années.

Quelles sont nos espérances?

L'objectif de l'IOPS est simple. Il vise à devenir une institution puissante et
importante oeuvrant pour la mise en place d'un monde nouveau.

**Le passage d'un système, d'un monde, à un autre a un nom: la révolution.**

Le but de l'IOPS, de ses branches et de ses groupes est de participer, de
manière importante espérons-le, à un processus révolutionnaire à travers le monde.

L'espoir est donc que l'IOPS, ses branches et ses groupes existeront et
contribueront à l'activisme et lutteront aussi lontemps que nécessaire en
faveur de la création d'un monde nouveau.
Elle se dissoudra en tant qu'organisation révolutionnaire quand elle se fondera
dans les structures de sociétés nouvelles à travers le monde.

Adhérer à l'IOPS, c'est dire "Oui, je veux que cela arrive. Oui, je veux faire
partie du processus menant à la création d'un monde nouveau et, compte-tenu de
mes moyens et de ma situation, oui, en plus de toutes les autres responsabilités
qui m'incombent, je veux contribuer. Comptez sur moi."

Why Join IOPS Q&A
==================

.. seealso::

   - http://www.iopsociety.org/article/why-join-iops-q-and-a


When talking to people seeking that they join IOPS, one presumably first
summarizes its mission, visionary, and structural/programmatic commitments.

Then one presumably asks people's reactions and invites them to join.
A number of responses repeatedly come up. Here we summarize a few, including
examples of possible replies. These are organized as thirty questions and
answers, in turn broken into eleven broad groups...

.. toctree::
   :maxdepth: 4

   members/index


Communication
=============

.. seealso:: http://www.zcommunications.org/zspace/groups/people-for-an-iops


Fanfare for the future
======================

.. seealso:: http://www.zcommunications.org/topics/fanfare-for-the-future


Other documents
================

.. toctree::
   :maxdepth: 4

   open_letter_chomsky_and_others


IOPS in the World
=================

.. toctree::
   :maxdepth: 4

   amerique/index
   europe/index


