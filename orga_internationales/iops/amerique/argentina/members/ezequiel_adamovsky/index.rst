
.. index::
   pair: Ezequiel ; Adamovsky

.. _ezequiel_adamovsky:

==============================================================
Ezequiel Adamovsky (1971-
==============================================================

.. figure:: Ezequiel_Adamovsky.jpg
   :align: center
   
   Ezequiel Adamovsky
   

.. seealso::

   - :ref:`iops`
   - http://en.wikipedia.org/wiki/Ezequiel_Adamovsky
   - http://www.iopsociety.org/profile/ezequiel-adamovsky/fr
   - http://www.facebook.com/ezequiel.adamovsky


Presentation
=============

Ezequiel Adamovsky (born 1971) is an Argentine historian and political activist 
who has written many articles and books about intellectual history, 
globalization, anti-capitalism and left-wing politics. 

He is a member of the interim consultative committee of the International 
Organization for a Participatory Society which he describes as offering 
**the chance to rebuild the internationalist tradition of the Left by taking 
into account the lessons we have learned from history**

