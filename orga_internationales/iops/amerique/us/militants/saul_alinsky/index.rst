
.. index::
   pair: MilitantEs; Saul Alinksy
   pair: Saul Alinksy; Etre radical
   ! Saul Alinksy
   ! conflit

    
.. _saul_alinsky:
   
===============================================
Saul Alinsky (January 30, 1909 – June 12, 1972)
===============================================

.. seealso::

   - http://en.wikipedia.org/wiki/Saul_Alinsky


.. figure:: Saul_Alinsky.jpg
   :align: center
   
   *Saul Alinsky*


.. contents::
   :depth: 3

Presentation
============

Saul David Alinsky (January 30, 1909 – June 12, 1972) was an American community 
organizer and writer. 

He is generally considered to be the founder of modern community organizing, 
and has been compared to Thomas Paine as being "one of the great American 
leaders of the nonsocialist left." 

He is often noted for his book Rules for Radicals.

In the course of nearly four decades of political organizing, Alinsky received 
much criticism, but also gained praise from many public figures. 

His organizing skills were focused on improving the living conditions of poor 
communities across North America. In the 1950s, he began turning his attention 
to improving conditions of the African-American ghettos, beginning with 
Chicago's and later traveling to other ghettos in California, Michigan, 
New York City, and a dozen other "trouble spots".

His ideas were later adapted by some U.S. college students and other young 
organizers in the late 1960s and formed part of their strategies for 
organizing on campus and beyond.

Early life and family
=====================

Alinsky was born in Chicago, Illinois in 1909 to Russian Jewish immigrant 
parents, the only surviving son of Benjamin Alinsky's marriage to his 
second wife, Sarah Tannenbaum Alinsky. 

Alinsky stated during an interview that his parents never became involved in 
the "new socialist movement." He added that they were "strict orthodox, their 
whole life revolved around work and synagogue ... 

"Whenever anyone asks me my religion, I always say—and always will say—Jewish."
At the same time, he was also an agnostic

.. _community_organizing:

community organizing
====================

Si Alinsky est quasiment inconnu en France, c’est parce qu’il fut un militant 
et un penseur résolument américain – dans ses croyances, ses références et ses 
méthodes. 

Aux États-Unis, il est généralement reconnu comme le père fondateur du 
**community organizing**, terme que l’on pourrait traduire de manière 
approximative par « animation de quartier » , mais dont le sens est à la fois 
plus politique et plus radical : il se réfère aux activités par lesquelles 
un animateur aide les habitants d’un quartier défavorisé à faire valoir leurs 
droits, que ce soit en exigeant de l’administration des HLM de mettre les 
logements sociaux aux normes sanitaires en vigueur, ou en demandant aux banques 
implantées dans le quartier d’offrir des taux d’intérêts plus raisonnables.

Né lui-même dans un ghetto de Chicago en 1909, Alinsky est issu d’une famille 
juive originaire de la Russie. Après des études à l’université de Chicago, 
il s’intéresse à la criminologie et obtient une bourse lui permettant de suivre 
de près la vie des gangs urbains : il développera ainsi une grande estime pour 
celui d’Al Capone, qu’il considère comme un vaste service public informel. 

Mais surtout, à partir de 1938, il trouve sa vocation lorsqu’il décide 
d’« organiser » le quartier Back of the Yards, le fameux ghetto dont les 
conditions de vie atroces ont été portées au grand jour par le roman d’Upton 
Sinclair, La Jungle (1905). C’est là qu’Alinksy mettra pour la première fois 
en œuvre des méthodes dont il fera plus tard un système. 

Son idée fondamentale : pour s’attaquer aux problèmes sociaux, il faut bâtir 
des « organisations populaires » (« People’s Organizations ») permettant aux 
populations de se mobiliser. Ces méthodes s’avéreront fructueuses aussi 
lorsqu’il organisa - toujours à Chicago - aux débuts des années soixante 
«The Woodlawn Organization» (TWO), du nom d’un quartier noir menacé par 
les efforts dits de « rénovation urbaine » de l’université de Chicago. 

Il fonda aussi l’Industrial Areas Foundation (IAF), une association où de 
nombreux futurs organizers (« animateurs de quartier ») apprendront 
la « méthode Alinsky » pour l’appliquer un peu partout dans le pays.


.. _methode_alinsky:

La « méthode Alinsky »
======================

.. seealso:: http://www.laviedesidees.fr/Saul-Alinsky-la-campagne.html

Bien qu’il fût avant tout un homme d’action, Alinsky tenta, dans plusieurs 
textes, d’expliquer les principes qui guident sa démarche. 

Son radicalisme puise ses racines dans l’histoire américaine – une histoire 
traversée avant tout par l’idée de la démocratie, qui a animé les penseurs 
radicaux américains depuis toujours, des révolutionnaires de Boston en 1776 
jusqu’aux fondateurs du mouvement syndical, en passant par les jeffersoniens 
et les militants œuvrant pour l’abolition de l’esclavage. 

Citons-en trois principes qui constituent, pour lui, autant de tabous à lever :

1) Le pouvoir. Alinsky est loin d’épouser une vision irénique de la démocratie. 
   Le principe primordial de l’organizer est celui du pouvoir. Le pouvoir, 
   soutient-il, est « l’essence même, la dynamo de la vie » (dans certains 
   textes, il ira jusqu’à citer Nietzsche) [2]. « Aucun individu, aucune 
   organisation ne peut négocier sans le pouvoir d’imposer la négociation ». 
   Ou encore : « Vouloir agir sur la base de la bonne foi plutôt que du pouvoir, 
   c’est de tenter quelque chose dont le monde n’a pas encore fait l’expérience—
   n’oubliez pas que pour être efficace, même la bonne foi doit être mobilisée 
   en tant qu’élément de pouvoir ». Malheureusement, poursuit-il, la culture 
   moderne tend à faire de « pouvoir » un gros mot ; dès qu’on l’évoque, 
   « c’est comme si on ouvrait les portes de l’enfer. » [3] Surmontant ce 
   moralisme gênant, l’organizer identifie le pouvoir dont une communauté 
   dispose, pour ensuite lui montrer le plaisir à l’éprouver – pour ensuite, 
   enfin, le manier à ses propres fins.

2) L’intérêt propre. Si le pouvoir est le but de l’organizer, son point d’appui 
   est l’intérêt propre (self-interest), un autre terme considéré souvent 
   comme tabou. Pour organiser une communauté, il doit faire appel à ses 
   intérêts (et les convaincre qu’il n’y a pas de honte à agir sur cette base) 
   tout en identifiant ceux des personnes qui y ont font obstacle. « Douter de 
   la force de l’intérêt particulier, qui pénètre tous les domaines de la 
   politique, insistera Alinsky, c’est refuser de voir l’homme tel qu’il est, 
   de le voir seulement comme on souhaiterait qu’il soit ». 

3) Le conflit. Mais puisque celui qui essaie de faire valoir son intérêt 
   particulier se heurte souvent aux intérêts de quelqu’un d’autre, l’organizer 
   doit accepter le conflit non seulement comme inéluctable, mais même comme 
   désirable – car rien ne mobilise autant que l’antagonisme. Sa tâche doit 
   être « de mettre du sel dans les plaies des gens de la communauté ; 
   d’attiser les hostilités latentes de beaucoup, jusqu’au point où ils les 
   expriment ouvertement ; de fournir un canal dans lequel ils puissent verser 
   leurs frustrations passées… ». [5] Loin d’être un mal nécessaire, le 
   conflit est « le noyau essentiel d’une société libre et ouverte ». 
   
   Si la démocratie était un morceau de musique, selon Alinsky, « son thème 
   majeur serait l’harmonie de la dissonance »



.. _etre_radical:

Être radical Manuel pragmatique pour radicaux réalistes Saul Alinsky
====================================================================

.. figure:: etre_radical.jpg
   :align: center
   
   Etre radical de Saul Alinsky
   
   

Traduit de l'anglais par Odile Hellier et Jean Gouriou. 

Édition revue par Hélène Hiessler et Daniel Zamora.

Ce livre s'adresse à ceux qui veulent changer le monde. Si Machiavel écrivit 
Le prince pour dire aux riches comment conserver le pouvoir, j'écris Être radical 
pour dire aux pauvres comment s'en emparer.» Saul Alinsky.

Après avoir étudié la sociologie et la criminologie à Chicago où il travailla 
sur la mafia d'Al Capone et ses techniques organisationnelles, 
Alinsky (1909-1972) s'est consacré à l'organisation politique des habitants les 
plus pauvres de Chicago à des fins émancipatrices. 

De sa pratique, il a tiré des conclusions, des recommandations passionnées et 
une méthode qu'il a systématisée dans ce livre phare, Être radical, publié pour 
la première fois en 1971. 

Rédigé dans un climat social et politique explosif aux USA (Black Panthers, 
radicalistation des campus universitaires, luttes dans les ghettos, 
Weather Underground, grèves), ce livre assurera à Alinsky bien des adeptes aux 
USA, dont un certain Barack Obama. 

Être radical donne aux  radicaux des clés pour opérer une transformation 
sociale constructive et comprendre «la différence entre un vrai radical et un 
radical de papier».

::

	«L'esprit d'Alinsky est bien vivant au sein de tous ces groupes militants 
	actifs dans d'innombrables domaines, jusqu'au récent mouvement Occupy Wall Street.» 

Noam Chomsky

Préface de Nic Görtz et Daniel Zamora.



Organisations s'inspirant de Saul Alinsky
=========================================

L'alliance citoyenne de Grenoble
---------------------------------

- :ref:`alliance_citoyenne_grenoble`

IAF ( Industrial Areas Foundation)
-----------------------------------

.. seealso:: http://www.industrialareasfoundation.org/


The leaders and organizers of the Industrial Areas Foundation build 
organizations whose primary purpose is power - the ability to act - and whose 
chief product is social change. 

They continue to practice what the Founding Fathers preached: the ongoing 
attempt to make life, liberty, and the pursuit of happiness everyday realities 
for more and more Americans.

The IAF is non-ideological and strictly non-partisan, but proudly, publicly, 
and persistently political. 

The IAF builds a political base within society's rich and complex third sector - 
the sector of voluntary institutions that includes religious congregations, 
labor locals, homeowner groups, recovery groups, parents associations, 
settlement houses, immigrant societies, schools, seminaries, orders of men and 
women religious, and others. 

And then the leaders use that base to compete at times, to confront at times, 
and to cooperate at times with leaders in the public and private sectors.






