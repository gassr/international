
.. index::
   pair: Stephen ; Rosskam Shalom

.. _stephen_rosskam_shalom:

==============================================================
Stephen Rosskamm Shalom
==============================================================

.. seealso::

   - http://en.wikipedia.org/wiki/Stephen_Rosskamm_Shalom
   - http://www.iopsociety.org/profile/steve-shalom/fr
   - http://www.iopsociety.org/united-states/new-jersey/montclair/fr


Stephen Rosskamm Shalom is a professor of political science at William Paterson 
University in New Jersey. 

He is a writer on social and political issues and is  a contributor to Znet 
and Democratic Left, the publication of Democratic Socialists of America. 

He is on the editorial boards of the Bulletin of Concerned Asian Scholars and 
the democratic socialist journal New Politics. 

He is also a member of the interim consultative committee of the :ref:`International 
Organization for a Participatory Society <iops>`.

