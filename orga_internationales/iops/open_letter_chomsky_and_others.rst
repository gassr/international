
===============================================================
Open Letter from Chomsky, Shiva, Santos, Pilger, and 40 more...
===============================================================

.. seealso::

   - http://www.zcommunications.org/chomsky-on-iops-by-noam-chomsky
   - http://www.zcommunications.org/open-letter-from-chomsky-shiva-santos-pilger-and-40-more-by-iops-supporters


We the signers of this open letter from Noam Chomsky, Vandana Shiva, Boaventura
de sousa Santos, John Pilger, and 40 other members of the interim decision body
of the new International Organization for a Participatory Society, hope that
you will circulate, email, and/or republish our letter, and, even more, that
you will engage in and publish commentary regarding the organization's purpose,
implications, prospects, etc.


An Open Letter to All Who Seek A New and Better World

We are members of what is called the the Interim Consultative Committee of the
International Organization for a Participatory Society - or IOPS for short.

IOPS is actually an interim entity, pending a future founding convention.

IOPS was convened just a few months ago and already has over 2,100 members from
85 countries and a ten language site, despite that it is barely known publicly.
IOPS is currently building local chapters, which will unite to form national
branches that in turn will compose an international organization.

We send this open letter to invite you to please visit the IOPS Site to
examine its initial features - including especially and most importantly its
Mission and Visionary and Programmatic Commitments.

The IOPS commitments emerged from a long process of discussion and debate.
We believe they correspond closely to the most prevalent, advanced, and widely
accessible political beliefs on which to build an organization for winning a
better world.

We also hope and even believe that if you read and consider the IOPS commitments,
you will likely find that they are congenial to your interests and desires and
that they provide reason for great hope that IOPS can become a very important
organization in the coming years.

If we had to summarize the IOPS commitments, we would note that they emphasize:
that IOPS focuses on cultural, kinship, political, economic, international,
and ecological aims without a priori prioritizing any of these over the rest;
that IOPS advocates and elaborates key aspects of vision for a sustainable and
peaceful world without sexism, heterosexism, racism, classism, and
authoritarianism and with equity, justice, solidarity, diversity, and, in
particular, self-management for all people and that IOPS structurally and
programmatically emphasizes planting the seeds of the future in the present,
winning immediate gains on behalf of suffering constituencies in ways contributing
to winning its long term aims as well, developing a caring and nurturing
organization and movement, and welcoming and even fostering constructive dissent
and diversity within that organization and movement and based on its commitments.

We think hundreds of thousands of people, in fact, millions of people, will, on
reading the commitments, overwhelmingly agree with them. We hope that if you
look at the commitments and feel that way, you will join and advocate that
others join as well. If you instead have problems with the IOPS commitments, we
hope you will make your concerns known so a productive discussion can ensue.

On the other hand, we also understand that agreeing with the IOPS commitments
will not alone cause those same hundreds of thousands and even millions of
people to join IOPS. There are numerous reasons why a person might support
the IOPS commitments and even hope that IOPS grows and becomes strong and
effective at the grassroots, in every neighborhood, workplace, and
social movement, and yet, at the moment, not join.

Our best effort to summarize obstacles people may feel to joining even while
they like the IOPS commitments, and to address those obstacles also appears on
the IOPS site, in a Why Join IOPS Question and Answer format.

**Essentially we argue: If not now, when? If not us, who?**

Noam Chomsky wrote
==================

Asked to provide a succinct summary paragraph for the IOPS site about his
involvement, Noam Chomsky wrote: "Hardly a day goes by when we do not hear
appeals – often laments – from people deeply concerned about the travails
of human existence and the fate of the world, desperately eager to do something
about what they rightly perceive to be intolerable and ominous, feeling
helpless because each individual effort, however dedicated, seems to merely
chip away at a mountain, placing band-aids on a cancer, never reaching to the
sources of needless suffering and the threats of much worse.
It’s an understandable reaction that all too often  leads to despair and
resignation. We all know the only answer, driven home by experience and
history, and by simple reflection on the realities of the world: join together
to construct and clarify long-term visions and goals, along with direct
engagement and activism shaped by these guidelines and contributing to a
deepening of our understanding of what we hope to achieve...
IOPS strikes the right chords, and if the opportunities it opens are pursued
with sufficient energy and participation, diligence, modesty, and desire, it
could carry us a long way towards unifying the many initiatives here and
around the world and combining them into a powerful and effective force."

Cynthia Peters wrote
====================

And as Cynthia Peters wrote: "You hear it all the time. There is always another
urgent crisis. They don't just come in a steady stream, they seem to multiply
geometrically. More draconian policies with life-threatening consequences, more
corporate control, more prisons, more bombs, more funerals. With so many
immediate fires to put out in our day-to-day organizing work, how can we make
time to attend to larger issues, such as long-term strategy, vision, and movement
building? IOPS creates the space for us to do the essential work of movement
building and envisioning and then seeking a better world. Without these elements,
we'll continue to work in isolation. By enlivening and enriching IOPS with your
presence, you will both give solidarity to and receive solidarity from so many
others -- across the world -- in the same situation -- up to their necks in
the daily fight, and at the same time turning their creativity and energy
towards revolutionary social change. That is not just good company.
It's the solid beginnings of another world being possible."

We hope you will join us as we try to make it so.

Signed,

::

    Ezequiel Adamovsky - Argentina
    M Adams - U.S.
    Michael Albert - U.S.
    Jessica Azulay - U.S.
    Elaine Bernard - U.S.
    Patrick Bond - South Africa
    Noam Chomsky - U.S.
    Jason Chrysostomou - UK
    John Cronan - U.S.
    Ben Dangl - U.S.
    Denitsa Dimitrova - UK/Bulgaria
    Mark Evans - UK
    Ann Ferguson - U.S.
    Eva Golinger - Venezuela
    Andrej Grubacic - Balkans/U.S.
    Pervez Hoodbhoy - Pakistan
    Antti Jauhiainen - Finland
    Ria Julien - U.S./Trinidad
    Dimitris Konstanstinou - Greece
    Pat Korte - U.S.
    Yohan Le Guin - Wales
    Mandisi Majavu - South Africa
    Yotam Marom - U.S.
    David Marty - Spain
    Preeti Paul - UK/India
    Cynthia Peters - U.S.
    John Pilger - UK/Aus
    Justin Podur - Canada
    Nikos Raptis - Greece
    Paulo Rodriguez - Belgium
    Charlotte Sáenz - Mexico/U.S.
    Anders Sandstrom - Sweden
    Boaventura de sousa Santos - Portugal
    Lydia Sargent - U.S.
    Stephen Shalom - U.S.
    Vandana Shiva - India
    Chris Spannos - U.S.
    Verena Stresing - France/Germany
    Elliot Tarver - U.S.
    Fernando Ramn Vegas Torrealba - Venezuela
    Taylon Tosun - Turkey
    Marie Trigona - U.S.
    Greg Wilpert - Germany/Venezuela/U.S.
    Florian Zollman - Germany




